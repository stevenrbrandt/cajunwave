#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include <math.h>
#include "here.hh"
#define zvalue (cctk_lsh[2]>>1)

// In Carpet we need to have ghost zones on either side of the plane that we are evolving
// In PUGH we don't need this, and don't want it. The min function below selects the correct
// index in each case.
#define FUNWAVE_INDEX(I,J) CCTK_GFINDEX3D(cctkGH,I,J,zvalue)


void deltas_setup(CCTK_ARGUMENTS) {
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;

	const double pie = 4.0*atan2(1.0,1.0);
	const double degree_to_rad = pie/180.0;
	const double rearth = 0.5*(earth_a+earth_b);
	const double dtheta_r = CCTK_DELTA_SPACE(1)*degree_to_rad;
	const double dphi_r = CCTK_DELTA_SPACE(0)*degree_to_rad;

	if(spherical_coordinates) {
		/*
		 *        Lat_theta(I,J)=Lat_South*pi/180.0_SP-Nghost*Dtheta_r &
	                       +(J-1)*Dtheta_r
	       Dx(I,J) = R_earth*Dphi_r*COS(Lat_theta(I,J))
	       Dy(I,J) = R_earth*Dtheta_r
	       Coriolis(I,J) = pi*SIN(Lat_theta(I,J)) / 21600.0_SP
		 *
		 */
		for(int j=0;j<cctk_lsh[1];j++) {
			for(int i=0;i<cctk_lsh[0];i++) {
				int cc = FUNWAVE_INDEX(i,j);
				lat_theta[cc] = lat_south*degree_to_rad-cctk_nghostzones[1]*dtheta_r+(cctk_lbnd[1]+j-0)*dtheta_r;
//				HEREL VAR(lat_south) VAR(lat_theta[cc]) VAR(i) VAR(j) VAR(cctk_nghostzones[1]) VAR(degree_to_rad) VAR(dtheta_r) END;
				dx[cc] = rearth*dphi_r*cos(lat_theta[cc]);
				dy[cc] = rearth*dtheta_r;
//				coriolis[cc] = pie*sin(y[cc])/21600.0;
			}
		}
	} else {
		for (int j = 0; j < cctk_lsh[1]; j++) {
			for (int i = 0; i < cctk_lsh[0]; i++) {
				int cc = FUNWAVE_INDEX(i,j);
				dx[cc] = CCTK_DELTA_SPACE(0);
				dy[cc] = CCTK_DELTA_SPACE(1);
			}
		}
	}
}
