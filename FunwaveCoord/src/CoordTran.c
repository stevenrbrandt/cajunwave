#include "math.h"

#include "cctk.h"
#include "cctk_Parameters.h"

extern CCTK_REAL FromLat(CCTK_REAL), FromLong(CCTK_REAL), MeanRadius();

CCTK_REAL FromLatImpl(CCTK_REAL x) {
	DECLARE_CCTK_PARAMETERS;
	// average of y (long), in radiants
	CCTK_REAL yavg = M_PI/360*(ymax+ymin);
	// approximate radius at center of grid
	CCTK_REAL mean_radius = sqrt(pow(earth_a*cos(yavg),2) +
			pow(earth_b*sin(yavg),2));
	return mean_radius*(x - xmin);
}

CCTK_REAL FromLongImpl(CCTK_REAL y) {
	DECLARE_CCTK_PARAMETERS;
	// average of y (long), in radiants
	CCTK_REAL yavg = M_PI/360*(ymax+ymin);
	// approximate radius at center of grid
	CCTK_REAL mean_radius = sqrt(pow(earth_a*cos(yavg),2) +
			pow(earth_b*sin(yavg),2));
	return mean_radius*(y - ymin);
}
