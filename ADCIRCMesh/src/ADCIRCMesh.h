#ifndef _ADCIRCMESH_H_
#define _ADCIRCMESH_H_

#include "cctk.h"

#ifdef __cplusplus

extern "C"
{
#endif

#define MALLOC_SAFE_CALL( call )                                           \
  {                                                                        \
    void *err = call;                                                      \
    if(err == NULL)                                                        \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "Malloc error: %s", "failed to allocate memory !");            \
      exit(-1);                                                            \
    }                                                                      \
  }

#define FOPEN_SAFE_CALL( call )                                             \
  {                                                                        \
    FILE *err = call;                                                      \
    if(err == NULL)                                                        \
    {                                                                      \
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,   \
            "fopen error: %s", "failed to open file !");                   \
      exit(-1);                                                            \
    }                                                                      \
  }

  /* structure for 2d vertices */
  typedef struct _vert2d
  {
    CCTK_INT vertid;
    /* x = x[0], y = x[1] */
    CCTK_REAL x[2];
    short marked;
  } vert2d;

  /* structure for 2d cells (only consider triangular elements here) */
  typedef struct _cell2d
  {
    CCTK_INT cellid;
    CCTK_INT vertid[3];
    short marked;
  } cell2d;

  /* structure for ADCIRC data */

  typedef struct _field1d
  {
    CCTK_INT id;

    /* scalar associated with this vertex */

    CCTK_REAL f;
  } field1d;

  typedef struct _field2d
  {
    CCTK_INT id;
    CCTK_REAL f[2];
  } field2d;

  typedef struct _field3d
  {
    CCTK_INT id;
    CCTK_REAL f[3];
  } field3d;

  typedef struct _field4d
  {
    CCTK_INT id;
    CCTK_REAL f[4];
  } field4d;

  void POIReader(const char * poi_file);
  void POIOutput(const int dim, FILE *fp, void *data);
  void ADCIRCMeshReader(const char *mesh_file);
  void ADCIRCMeshOutput(const char *mesh_file);
  void ADCIRCCoastalReader(FILE *fp, void *data, const int field_id);

  inline int PointInElement(const CCTK_REAL * p, const CCTK_INT id);

  inline int PointInTriangle(const CCTK_REAL * p, const CCTK_REAL * x1,
      const CCTK_REAL * x2, const CCTK_REAL * x3);

  inline CCTK_REAL VecDotProduct(const CCTK_INT n, const CCTK_REAL * v1,
      const CCTK_REAL * v2);

  inline CCTK_REAL Distance(const CCTK_INT n, const CCTK_REAL * x1,
      const CCTK_REAL * x2);

  /* max buffer size for reading files */
#define LINELENGTH 100

  /*convert global variables into struct, ccui*/
#ifndef _SHARED_PARA
#define _SHARED_PARA

  typedef struct _mesh_parameter_struct {
	  int poi_count;
	  int vert_count;
	  int cell_count;
	  int num_time_steps_scattered;
	  int num_time_steps_structured;

	  vert2d *adcirc_vert;
	  cell2d *adcirc_cell;

	  field1d *adcirc_depth;
	  field2d *poi;
} mesh_parameter_struct;
 extern mesh_parameter_struct mesh_para_A;

#endif

//  extern int poi_count;
//  extern int vert_count;
//  extern int cell_count;
//  extern int num_time_steps;	//only used by: ADCIRCScatteredInterp.c, ADCIRCStructuredInterp.c
//
//  extern vert2d *adcirc_vert;
//  extern cell2d *adcirc_cell;
//
//  extern field1d *adcirc_depth;
//  extern field2d *poi;

//#define DEBUGADCIRCMESH
#ifdef __cplusplus
}
#endif

#endif                          //_ADCIRCMESH_H_
