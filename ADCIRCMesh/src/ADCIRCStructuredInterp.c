#include <stdio.h>
#include <stdlib.h>
#include "ADCIRCMesh.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#define min(X, Y) ((X) < (Y) ? (X) : (Y))

//int num_time_steps;		//need change, local interest
//int num_time_steps_structured;	//local interest


/* local inline functions */
void OutputOneStep( CCTK_ARGUMENTS, FILE *fp, const int fieldid);

void ADCIRCMesh_CalcWeight( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i, j, l;
  field2d target_pt;
  /* find the element */

  for (j = 0; j < cctk_lsh[1]; j++)
  {
    for (l = 0; l < cctk_lsh[0]; l++)
    {
      int cell_found = 0;
      int k = 0;
      int i3D = CCTK_GFINDEX3D(cctkGH, l, j, k);
      target_pt.f[0] = x[i3D];
      target_pt.f[1] = y[i3D];

      for (i = 1; i <= mesh_para_A.cell_count; i++)
      {
        /* find targe_pt in the element i and interpolate */
        if (PointInElement(target_pt.f, i))
        {
          int v1, v2, v3;
          CCTK_REAL w1, w2, w3, w_denom;

          v1 = mesh_para_A.adcirc_cell[i].vertid[0];
          v2 = mesh_para_A.adcirc_cell[i].vertid[1];
          v3 = mesh_para_A.adcirc_cell[i].vertid[2];

          w1 = pow(Distance(2, target_pt.f, mesh_para_A.adcirc_vert[v1].x),
        		  	  	  	  	  	  	  	  -invdist_power);
          w2 = pow(Distance(2, target_pt.f, mesh_para_A.adcirc_vert[v2].x),
        		  	  	  	  	  	  	  	  -invdist_power);
          w3 = pow(Distance(2, target_pt.f, mesh_para_A.adcirc_vert[v3].x),
        		  	  	  	  	  	  	  	  -invdist_power);

          w_denom = w1 + w2 + w3;

          weight1[i3D] = w1 / w_denom;
          weight2[i3D] = w2 / w_denom;
          weight3[i3D] = w3 / w_denom;

          vert1[i3D] = v1;
          vert2[i3D] = v2;
          vert3[i3D] = v3;
          cell_found = 1;
#ifdef DEBUGADCIRCMESH
          printf("Found in Cell =  %10d %10d %10d %10d \n", i,
        	mesh_para_A.adcirc_cell[i].vertid[0],
        	mesh_para_A.adcirc_cell[i].vertid[1],
        	mesh_para_A.adcirc_cell[i].vertid[2]);

          printf("          POI =  %10d %10.8g, %10.8g \n", i3D, target_pt.f[0],
              target_pt.f[1]);

          printf("           V1 =  %10d %10.8g, %10.8g \n",
        	mesh_para_A.adcirc_cell[i].vertid[0],
        	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].x[0],
        	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].x[1]);

          printf("           V2 =  %10d %10.8g, %10.8g \n",
        	mesh_para_A.adcirc_cell[i].vertid[1],
        	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].x[0],
        	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].x[1]);

          printf("           V3 =  %10d %10.8g, %10.8g \n",
        	mesh_para_A.adcirc_cell[i].vertid[2],
        	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].x[0],
        	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].x[1]);

          printf("           W1 =  %10.8g \n", weight1[i3D]);
          printf("           W2 =  %10.8g \n", weight2[i3D]);
          printf("           W3 =  %10.8g \n", weight3[i3D]);
          printf("--------------------------------------------------\n");
#endif /* end debugging output */

          break;
        }
      }
      if (cell_found == 0)
      {
#ifdef DEBUGADCIRCMESH
        printf("          POI =  %10d %10.8g, %10.8g \n", i3D, target_pt.f[0],
            target_pt.f[1]);

        printf("the POI is not found in any cell in the mesh !\n");
        printf("--------------------------------------------------\n");
#endif /* end debugging output */
      }
    }
  }
}

void ADCIRCMesh_Interp( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i, j, k, l, i3D;
  int v1, v2, v3;
  FILE *fp;

  int structured_wind_vert_count =0;
  int structured_current_vert_count =0;
  int structured_elevation_vert_count=0;

  field1d *structured_adcirc_elevation = NULL;
  field2d *structured_adcirc_currentfield = NULL ;
  field2d *structured_adcirc_windfield =NULL;

  if (poi_output_bathymetry)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "StructuredInterp: interpolating bathymetry to %s",
        poi_bathymetry_file);

    FOPEN_SAFE_CALL(fp = fopen(poi_bathymetry_file, "w"));

    fprintf(fp, "# %s\n", poi_bathymetry_file);
    fprintf(fp, "# Time Steps : 1\n");

    for (j = 0; j < cctk_lsh[1]; j++)
    {
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        int i3D = CCTK_GFINDEX3D(cctkGH, i, j, 0);
        if ((vert1[i3D] == -1) || (vert2[i3D] == -1) || (vert3[i3D] == -1))
        {
          depth[i3D] = unknown_default;
        }
        else
        {
          v1 = vert1[i3D];
          v2 = vert2[i3D];
          v3 = vert3[i3D];

          depth[i3D] = weight1[i3D] * mesh_para_A.adcirc_depth[v1].f
              + weight2[i3D] * mesh_para_A.adcirc_depth[v2].f
              + weight3[i3D] * mesh_para_A.adcirc_depth[v3].f;
        }
#ifdef DEBUGADCIRCMESH
        printf("Bathymetry at POI : %10d %10.8g \n", i3D, depth[i3D]);

        printf("V1: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		mesh_para_A.adcirc_depth[v1].f,
        		weight1[i3D], v1,
        		mesh_para_A.adcirc_vert[v1].x[0],
        		mesh_para_A.adcirc_vert[v1].x[1]);

        printf("V2: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		mesh_para_A.adcirc_depth[v2].f,
        		weight2[i3D], v2,
        		mesh_para_A.adcirc_vert[v2].x[0],
        		mesh_para_A.adcirc_vert[v2].x[1]);

        printf("V3: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		mesh_para_A.adcirc_depth[v3].f,
        		weight3[i3D], v3,
        		mesh_para_A.adcirc_vert[v3].x[0],
        		mesh_para_A.adcirc_vert[v3].x[1]);

        printf("----------------------------------------------------------\n");
#endif /* end debugging output */
      }
    }

    OutputOneStep(CCTK_PASS_CTOC, fp, 14);
    fclose(fp);
  }

  if (poi_output_elevation)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "StructuredInterp: interpolating elevation to %s", poi_elevation_file);

    FILE *fp_elevation;

    char line[LINELENGTH];
    /* read file */
    FOPEN_SAFE_CALL(fp_elevation = fopen(adcirc_elevation_file, "r"));

    /* skip the first line */
    fgets(line, LINELENGTH, fp_elevation);

    fscanf(fp_elevation, "%d %d %*G %*d %*d %*s %*d",
    		&mesh_para_A.num_time_steps_structured,
    		&structured_elevation_vert_count);

    MALLOC_SAFE_CALL(
    		structured_adcirc_elevation = (field1d *) malloc(
            (structured_elevation_vert_count + 1) * sizeof(field1d)));

    /* interpolation */
    FOPEN_SAFE_CALL(fp = fopen(poi_elevation_file, "w"));

    field1d * poi_elevation;
    MALLOC_SAFE_CALL(
        poi_elevation =
        		(field1d *) malloc((mesh_para_A.poi_count + 1) * sizeof(field1d)));

    //check that init_output_time_step and num_output_time_steps is a sensible choice.
    if (mesh_para_A.num_time_steps_structured < start_output_time_step
        || mesh_para_A.num_time_steps_structured < num_output_time_steps)
    {
      CCTK_WARN(0,
          "start_output_time_step or num_output_time_steps is out of range");
    }
    if (num_output_time_steps != -1)
    {
    	mesh_para_A.num_time_steps_structured =
    			min(mesh_para_A.num_time_steps_structured,
    					num_output_time_steps);
    }

    fprintf(fp, "# %s\n", poi_elevation_file);
    fprintf(fp, "# Time Steps : %d\n", mesh_para_A.num_time_steps_structured);

    /* iterate through time steps */
    for (int t=start_output_time_step; t<mesh_para_A.num_time_steps_structured; t++)
    {
#ifdef DEBUGADCIRCMESH
      printf("= Time step : %d \n", t);
#endif /* end debugging output */

      ADCIRCCoastalReader(fp_elevation, structured_adcirc_elevation, 63);

      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          i3D = CCTK_GFINDEX3D(cctkGH, i, j, k);
          if ((vert1[i3D] == -1) || (vert2[i3D] == -1) || (vert3[i3D] == -1))
          {
            eta[i3D] = unknown_default;
          }
          else
          {
            v1 = vert1[i3D];
            v2 = vert2[i3D];
            v3 = vert3[i3D];

            eta[i3D] = weight1[i3D] * structured_adcirc_elevation[v1].f
                + weight2[i3D] * structured_adcirc_elevation[v2].f
                + weight3[i3D] * structured_adcirc_elevation[v3].f;
          }
#ifdef DEBUGADCIRCMESH
          printf("Elevation at POI : %10d %10.8g \n", i3D, eta[i3D]);

          printf("V1: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_elevation[v1].f, weight1[i3D], v1,
              mesh_para_A.adcirc_vert[v1].x[0],
              mesh_para_A.adcirc_vert[v1].x[1]);

          printf("V2: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_elevation[v2].f, weight2[i3D], v2,
              mesh_para_A.adcirc_vert[v2].x[0],
              mesh_para_A.adcirc_vert[v2].x[1]);

          printf("V3: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_elevation[v3].f, weight3[i3D], v3,
              mesh_para_A.adcirc_vert[v3].x[0],
              mesh_para_A.adcirc_vert[v3].x[1]);

          printf(
              "----------------------------------------------------------\n");
#endif /* end debugging output */
        }
      }
      /* print time step */
      OutputOneStep(CCTK_PASS_CTOC, fp, 63);
      fprintf(fp, "\n");
    }
    free(structured_adcirc_elevation);
    fclose(fp);
    fclose(fp_elevation);
  }

  if (poi_output_current_velocity)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "StructuredInterp: interpolating current velocity to %s",
        poi_current_velocity_file);
    FILE *fp_current;

    char line[LINELENGTH];
    /* read file */
    FOPEN_SAFE_CALL(fp_current = fopen(adcirc_current_file, "r"));

    /* skip the first line */
    fgets(line, LINELENGTH, fp_current);

    fscanf(fp_current, "%d %d %*G %*d %*d %*s %*d",
    		&mesh_para_A.num_time_steps_structured,
    		&structured_current_vert_count);

    MALLOC_SAFE_CALL(
    		structured_adcirc_currentfield = (field2d *)
        		malloc((structured_current_vert_count + 1) * sizeof(field2d)));

    /* interpolation */
    FOPEN_SAFE_CALL(fp = fopen(poi_current_velocity_file, "w"));

    field2d * poi_current_velocity;
    MALLOC_SAFE_CALL(
        poi_current_velocity = (field2d *)
        		malloc((mesh_para_A.poi_count + 1) * sizeof(field2d)));

    //check that init_output_time_step and num_output_time_steps is a sensible choice.
    if (mesh_para_A.num_time_steps_structured < start_output_time_step
        || mesh_para_A.num_time_steps_structured < num_output_time_steps)
    {
      CCTK_WARN(0,
          "start_output_time_step or num_output_time_steps is out of range");
    }
    if (num_output_time_steps != -1)
    {
    	mesh_para_A.num_time_steps_structured =
    			min(mesh_para_A.num_time_steps_structured,
    					num_output_time_steps);
    }

    fprintf(fp, "# %s\n", poi_current_velocity_file);
    fprintf(fp, "# Time Steps : %d\n", mesh_para_A.num_time_steps_structured);

    /* iterate through time steps */
    for (int t=start_output_time_step; t<mesh_para_A.num_time_steps_structured; t++)
    {
#ifdef DEBUGADCIRCMESH
      printf("= Time step : %d \n", t);
#endif /* end debugging output */

      ADCIRCCoastalReader(fp_current, structured_adcirc_currentfield, 64);

      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          i3D = CCTK_GFINDEX3D(cctkGH, i, j, k);
          if ((vert1[i3D] == -1) || (vert2[i3D] == -1) || (vert3[i3D] == -1))
          {
            uc1[i3D] = unknown_default;
            uc2[i3D] = unknown_default;
          }
          else
          {
            v1 = vert1[i3D];
            v2 = vert2[i3D];
            v3 = vert3[i3D];

            uc1[i3D] = weight1[i3D] * structured_adcirc_currentfield[v1].f[0]
                + weight2[i3D] * structured_adcirc_currentfield[v2].f[0]
                + weight3[i3D] * structured_adcirc_currentfield[v3].f[0];

            uc2[i3D] = weight1[i3D] * structured_adcirc_currentfield[v1].f[1]
                + weight2[i3D] * structured_adcirc_currentfield[v2].f[1]
                + weight3[i3D] * structured_adcirc_currentfield[v3].f[1];
          }
#ifdef DEBUGADCIRCMESH
          printf("Current Velocity at POI : %10d (%10.8g %10.8g)\n", i3D,
              uc1[i3D], uc2[i3D]);

          printf("V1: %10.8g, %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_currentfield[v1].f[0],
        		  structured_adcirc_currentfield[v1].f[1],
        		  weight1[i3D], v1,
        		  mesh_para_A.adcirc_vert[v1].x[0],
        		  mesh_para_A.adcirc_vert[v1].x[1]);

          printf("V2: %10.8g, %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_currentfield[v2].f[0],
        		  structured_adcirc_currentfield[v2].f[1],
        		  weight2[i3D], v2,
        		  mesh_para_A.adcirc_vert[v2].x[0],
        		  mesh_para_A.adcirc_vert[v2].x[1]);

          printf("V3: %10.8g, %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_currentfield[v3].f[0],
        		  structured_adcirc_currentfield[v3].f[1],
        		  weight3[i3D], v3,
        		  mesh_para_A.adcirc_vert[v3].x[0],
        		  mesh_para_A.adcirc_vert[v3].x[1]);

          printf(
              "----------------------------------------------------------\n");
#endif /* end debugging output */
        }
      }
      /* print time step */
      OutputOneStep(CCTK_PASS_CTOC, fp, 64);
      fprintf(fp, "\n");
    }
    free(structured_adcirc_currentfield);
    free(poi_current_velocity);
    fclose(fp);
    fclose(fp_current);
  }

  if (poi_output_wind_velocity)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "StructuredInterp: interpolating wind velocity to %s",
        poi_wind_velocity_file);

    FILE *fp_wind;

    char line[LINELENGTH];
    /* read file */
    FOPEN_SAFE_CALL(fp_wind = fopen(adcirc_wind_file, "r"));

    /* skip the first line */
    fgets(line, LINELENGTH, fp_wind);

    fscanf(fp_wind, "%d %d %*G %*d %*d %*s %*d",
    		&mesh_para_A.num_time_steps_structured,
    		&structured_wind_vert_count);

    MALLOC_SAFE_CALL(
    		structured_adcirc_windfield = (field2d *) malloc(
            (structured_wind_vert_count + 1) * sizeof(field2d)));

    /* interpolation */
    FOPEN_SAFE_CALL(fp = fopen(poi_wind_velocity_file, "w"));

    field2d * poi_wind_velocity;
    MALLOC_SAFE_CALL(
        poi_wind_velocity = (field2d *) malloc(
            (mesh_para_A.poi_count + 1) * sizeof(field2d)));

    //check that init_output_time_step and num_output_time_steps is a sensible choice.
    if (mesh_para_A.num_time_steps_structured < start_output_time_step
        || mesh_para_A.num_time_steps_structured < num_output_time_steps)
    {
      CCTK_WARN(0,
          "start_output_time_step or num_output_time_steps is out of range");
    }
    if (num_output_time_steps != -1)
    {
    	mesh_para_A.num_time_steps_structured =
    			min(mesh_para_A.num_time_steps_structured,
    					num_output_time_steps);
    }

    fprintf(fp, "# %s\n", poi_wind_velocity_file);
    fprintf(fp, "# Time Steps : %d\n", mesh_para_A.num_time_steps_structured);

    /* iterate through time steps */
    for (int t=start_output_time_step; t<mesh_para_A.num_time_steps_structured; t++)
    {
#ifdef DEBUGADCIRCMESH
      printf("= Time step : %d \n", t);
#endif /* end debugging output */

      ADCIRCCoastalReader(fp_wind, structured_adcirc_windfield, 74);

      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          i3D = CCTK_GFINDEX3D(cctkGH, i, j, k);
          if ((vert1[i3D] == -1) || (vert2[i3D] == -1) || (vert3[i3D] == -1))
          {
            uw1[i3D] = unknown_default;
            uw2[i3D] = unknown_default;
          }
          else
          {
            v1 = vert1[i3D];
            v2 = vert2[i3D];
            v3 = vert3[i3D];

            uw1[i3D] = weight1[i3D] * structured_adcirc_windfield[v1].f[0]
                + weight2[i3D] * structured_adcirc_windfield[v2].f[0]
                + weight3[i3D] * structured_adcirc_windfield[v3].f[0];

            uw2[i3D] = weight1[i3D] * structured_adcirc_windfield[v1].f[1]
                + weight2[i3D] * structured_adcirc_windfield[v2].f[1]
                + weight3[i3D] * structured_adcirc_windfield[v3].f[1];
          }
#ifdef DEBUGADCIRCMESH
          printf("Wind Velocity at POI : %10d (%10.8g %10.8g)\n", i3D,
              uc1[i3D], uc2[i3D]);

          printf("V1: %10.8g, %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_windfield[v1].f[0],
        		  structured_adcirc_windfield[v1].f[1],
        		  weight1[i3D], v1,
        		  mesh_para_A.adcirc_vert[v1].x[0],
        		  mesh_para_A.adcirc_vert[v1].x[1]);

          printf("V2: %10.8g, %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_windfield[v2].f[0],
        		  structured_adcirc_windfield[v2].f[1],
        		  weight2[i3D], v2,
        		  mesh_para_A.adcirc_vert[v2].x[0],
        		  mesh_para_A.adcirc_vert[v2].x[1]);

          printf("V3: %10.8g, %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		  structured_adcirc_windfield[v3].f[0],
        		  structured_adcirc_windfield[v3].f[1],
        		  weight3[i3D], v3,
        		  mesh_para_A.adcirc_vert[v3].x[0],
        		  mesh_para_A.adcirc_vert[v3].x[1]);

          printf(
              "----------------------------------------------------------\n");
#endif /* end debugging output */
        }
      }
      /* print time step */
      OutputOneStep(CCTK_PASS_CTOC, fp, 74);
      fprintf(fp, "\n");
    }
    free(structured_adcirc_windfield);
    free(poi_wind_velocity);
    fclose(fp);
    fclose(fp_wind);
  }
}

void OutputOneStep( CCTK_ARGUMENTS, FILE *fp, const int fieldid)
{ /* output */
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i, j, k, i3D;
  switch (fieldid)
  {
    case 14:
    {

      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          int i3D = CCTK_GFINDEX3D(cctkGH, i, j, 0);
          fprintf(fp, "%13.7g %13.7g %13.7g\n", x[i3D], y[i3D], depth[i3D]);
        }
        fprintf(fp, "\n");
      }
      break;
    }
    case 63:
    {
      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          int i3D = CCTK_GFINDEX3D(cctkGH, i, j, 0);
          fprintf(fp, "%13.7g %13.7g %13.7g\n", x[i3D], y[i3D], eta[i3D]);
        }
        fprintf(fp, "\n");
      }
      break;
    }
    case 64:
    {
      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          int i3D = CCTK_GFINDEX3D(cctkGH, i, j, 0);
          fprintf(fp, "%13.7g %13.7g %13.7g %13.7g\n", x[i3D], y[i3D], uc1[i3D],
              uc2[i3D]);
        }
        fprintf(fp, "\n");
      }
      break;
    }
    case 74:
    {
      for (j = 0; j < cctk_lsh[1]; j++)
      {
        for (i = 0; i < cctk_lsh[0]; i++)
        {
          int i3D = CCTK_GFINDEX3D(cctkGH, i, j, 0);
          fprintf(fp, "%13.7g %13.7g %13.7g %13.7g\n", x[i3D], y[i3D], uw1[i3D],
              uw2[i3D]);
        }
        fprintf(fp, "\n");
      }
      break;
    }
    default:
      CCTK_WARN(0, "field type not supported !");
  }
}
