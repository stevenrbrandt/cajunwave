#include <stdio.h>
#include <stdlib.h>
#include "ADCIRCMesh.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

//#define DEBUGADCIRCMESH
/*
 * Interface to the readers of different type of mesh formats.
 * Potential formats will be HDF5 and F5
 * */

/* global variable, ccui */
mesh_parameter_struct mesh_para_A =
{
	.poi_count	= 0,
	.vert_count	= 0,
	.cell_count	= 0,
	.num_time_steps_scattered	= 0,
	.num_time_steps_structured	= 0,
	.adcirc_vert	= NULL,
	.adcirc_cell	= NULL,
	.adcirc_depth	= NULL,
	.poi =	NULL
};

//int vert_count;	//global, used only in this file
//int cell_count;	//global, used by other: ADCIRCScatteredInterp.c, ADCIRCStructuredInterp.c
//
//vert2d *adcirc_vert;	//global,used by other: ADCIRCScatteredInterp.c, ADCIRCStructuredInterp.c
//cell2d *adcirc_cell;	//global,used by other: ADCIRCScatteredInterp.c, ADCIRCStructuredInterp.c
//
//field1d *adcirc_depth;	//global, used by other: ADCIRCScatteredInterp.c, ADCIRCStructuredInterp.c
//field2d *poi;	//global, used by other: ADCIRCScatteredInterp.c

//field2d *adcirc_windfield;	//local variable
//field2d *adcirc_currentfield;	//local variable
//int wind_vert_count;	//local variable
//int current_vert_count;	//local variable

void ADCIRCMesh_ADCIRCMeshReader( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  ADCIRCMeshReader(input_mesh_file);
}

void ADCIRCMesh_ADCIRCMeshMark( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL centroidx, centroidy;

  for (int i = 1; i <= mesh_para_A.cell_count; i++)
  {
    centroidx = (mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].x[0]
        + mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].x[0]
        + mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].x[0]) / 3.0;

    centroidy = (mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].x[1]
        + mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].x[1]
        + mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].x[1]) / 3.0;

    if ((centroidx >= xmin) && (centroidx <= xmax) && (centroidy >= ymin)
        && (centroidy <= ymax))
    {
    	mesh_para_A.adcirc_cell[i].marked = 1;
    	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].marked = 1;
    	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].marked = 1;
    	mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].marked = 1;
    }
  }
}

void ADCIRCMesh_ADCIRCMeshExtract( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i;
  int marked_vert_count = 0;
  int marked_cell_count = 0;
  vert2d *marked_adcirc_vert;
  cell2d *marked_adcirc_cell;
  field1d *marked_adcirc_depth;

  /* first count marked vertices and elements */
  for (i = 1; i <= mesh_para_A.vert_count; i++)
  {
    if (mesh_para_A.adcirc_vert[i].marked == 1)
      marked_vert_count++;
  }

  for (i = 1; i <= mesh_para_A.cell_count; i++)
  {
    if (mesh_para_A.adcirc_cell[i].marked == 1)
      marked_cell_count++;
  }

  MALLOC_SAFE_CALL(
      marked_adcirc_vert = (vert2d *) malloc(
          (marked_vert_count + 1) * sizeof(vert2d)));
  MALLOC_SAFE_CALL(
      marked_adcirc_cell = (cell2d *) malloc(
          (marked_cell_count + 1) * sizeof(cell2d)));
  MALLOC_SAFE_CALL(
      marked_adcirc_depth = (field1d *) malloc(
          (marked_vert_count + 1) * sizeof(field1d)));

  marked_adcirc_vert[0].vertid = marked_vert_count;
  marked_adcirc_cell[0].cellid = marked_cell_count;
  marked_adcirc_depth[0].id = marked_vert_count;

  if (marked_vert_count == 0 || marked_cell_count == 0)
  {
    CCTK_INFO("There are no vertices or cells to output !");
    return;
  }
  else
  {
    CCTK_VInfo(CCTK_THORNSTRING, "original  :%10d vertices; %10d cells",
    		mesh_para_A.vert_count, mesh_para_A.cell_count);
    CCTK_VInfo(CCTK_THORNSTRING, "extracted :%10d vertices; %10d cells",
        marked_vert_count, marked_cell_count);
  }

  /* keep the adcirc index */
  /* use for outputting mesh file only */
  if (keep_adcirc_index)
  {
    int j = 1;

    for (i = 1; i <= mesh_para_A.vert_count; i++)
    {
      if (mesh_para_A.adcirc_vert[i].marked == 1)
      {
        marked_adcirc_vert[j] = mesh_para_A.adcirc_vert[i];
        marked_adcirc_depth[j] = mesh_para_A.adcirc_depth[i];
        j++;
      }
    }

    j = 1;
    for (i = 1; i <= mesh_para_A.cell_count; i++)
    {
      if (mesh_para_A.adcirc_cell[i].marked == 1)
      {
        marked_adcirc_cell[j] = mesh_para_A.adcirc_cell[i];
        j++;
      }
    }
  }

  /* reordering the indices from 1 */
  else
  {
    int j = 1;

    for (i = 1; i <= mesh_para_A.vert_count; i++)
    {
      if (mesh_para_A.adcirc_vert[i].marked == 1)
      {
        /* adcirc_vert will be replaced later here we
         * use vertid as a temporary storage */
    	  mesh_para_A.adcirc_vert[i].vertid = j;
    	  marked_adcirc_vert[j] = mesh_para_A.adcirc_vert[i];
    	  marked_adcirc_depth[j] = mesh_para_A.adcirc_depth[i];
    	  marked_adcirc_depth[j].id = j;
    	  j++;
      }
    }

    j = 1;
    for (i = 1; i <= mesh_para_A.cell_count; i++)
    {
      if (mesh_para_A.adcirc_cell[i].marked == 1)
      {
        marked_adcirc_cell[j] = mesh_para_A.adcirc_cell[i];
        marked_adcirc_cell[j].cellid = j;
        marked_adcirc_cell[j].vertid[0] =
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].vertid;
        marked_adcirc_cell[j].vertid[1] =
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].vertid;
        marked_adcirc_cell[j].vertid[2] =
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].vertid;
        j++;
      }
    }
  }

  // Now free original global set and replace with reduced set
    free(mesh_para_A.adcirc_vert);
    free(mesh_para_A.adcirc_cell);
    free(mesh_para_A.adcirc_depth);
    mesh_para_A.adcirc_vert = marked_adcirc_vert;
    mesh_para_A.adcirc_cell = marked_adcirc_cell;
    mesh_para_A.adcirc_depth = marked_adcirc_depth;
    mesh_para_A.vert_count = marked_vert_count;
    mesh_para_A.cell_count = marked_cell_count;
}

void ADCIRCMesh_ADCIRCMeshOutput( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  if (CCTK_EQUALS(output_mesh_format, "adcirc"))
  {
    ADCIRCMeshOutput(output_mesh_file);
  }

  if (output_to_gnuplot)
  {
    FILE *fpin, *fpout;
    int i;
    int output_vert_count = 0;
    int output_cell_count = 0;

    if ((fpin = fopen(gnuplot_input_mesh_file, "w")) == NULL)
    {
      CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s",
          gnuplot_input_mesh_file);
    }
    if ((fpout = fopen(gnuplot_output_mesh_file, "w")) == NULL)
    {
      CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s",
          gnuplot_output_mesh_file);
    }

    for (i = 1; i <= mesh_para_A.vert_count; i++)
    {
      fprintf(fpin, "%lf %lf %lf\n", mesh_para_A.adcirc_vert[i].x[0],
    		  mesh_para_A.adcirc_vert[i].x[1], mesh_para_A.adcirc_depth[i].f);

      if (mesh_para_A.adcirc_vert[i].marked == 1)
      {
        fprintf(fpout, "%lf %lf %lf\n", mesh_para_A.adcirc_vert[i].x[0],
        		mesh_para_A.adcirc_vert[i].x[1], mesh_para_A.adcirc_depth[i].f);
      }
    }
    fclose(fpin);
    fclose(fpout);

    CCTK_VInfo(CCTK_THORNSTRING, "output %s and %s in gnuplot format",
        gnuplot_input_mesh_file, gnuplot_output_mesh_file);
  }

}

int ADCIRCMesh_ADCIRCMeshReaderMemFree(void)
{
  free(mesh_para_A.adcirc_vert);
  free(mesh_para_A.adcirc_cell);
  free(mesh_para_A.adcirc_depth);
  free(mesh_para_A.poi);
  return 0;
}

inline void ADCIRCMeshReader(const char * mesh_file)
{
  FILE *fp;
  char line[LINELENGTH];
  int i;

  if ((fp = fopen(mesh_file, "r")) == NULL)
  {
    CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s",
        mesh_file);
  }

  /* read in the mesh name */
  fgets(line, LINELENGTH, fp);

  CCTK_VInfo(CCTK_THORNSTRING, "start reading ADCIRC mesh file %s with %s",
      mesh_file, line);

  /* read in the total number of vertices and cells */
  fscanf(fp, "%d %d", &mesh_para_A.cell_count, &mesh_para_A.vert_count);

  CCTK_VInfo(CCTK_THORNSTRING, "found %d vertices and %d cells",
		  mesh_para_A.vert_count, mesh_para_A.cell_count);

  /* Allocate space for vertex and cell arrays. */

  /* notice that if we want to use the array index to refer to the vertices we
   * need to allocate an extra room for the 0th. We do the same for the water
   * depth*/
  mesh_para_A.adcirc_vert = (vert2d *) malloc((mesh_para_A.vert_count + 1) *
		  	  	  	  	  	  	  	  	  	  sizeof(vert2d));
  mesh_para_A.adcirc_cell = (cell2d *) malloc((mesh_para_A.cell_count + 1) *
		  	  	  	  	  	  	  	  	  	  sizeof(cell2d));
  mesh_para_A.adcirc_depth = (field1d *) malloc((mesh_para_A.vert_count + 1) *
		  	  	  	  	  	  	  	  	  	  sizeof(field1d));

  mesh_para_A.adcirc_vert[0].vertid = mesh_para_A.vert_count;
  mesh_para_A.adcirc_cell[0].cellid = mesh_para_A.cell_count;
  mesh_para_A.adcirc_depth[0].id = mesh_para_A.vert_count;

  if (mesh_para_A.adcirc_vert == NULL || mesh_para_A.adcirc_cell == NULL ||
		  mesh_para_A.adcirc_depth == NULL)
  {
    CCTK_WARN(0, "failed to allocate memory for the mesh.");
  }

  for (i = 1; i <= mesh_para_A.vert_count; i++)
  {
    fscanf(fp, "%*d %lf %lf %lf", &(mesh_para_A.adcirc_vert[i].x[0]),
        &(mesh_para_A.adcirc_vert[i].x[1]), &(mesh_para_A.adcirc_depth[i].f));
    mesh_para_A.adcirc_vert[i].marked = 0;
    mesh_para_A.adcirc_vert[i].vertid = i;
    mesh_para_A.adcirc_depth[i].id = i;
  }
  /* Read in which verts are associated with each cell. */
  for (i = 1; i <= mesh_para_A.cell_count; i++)
  {
    fscanf(fp, "%*d %*d %d %d %d", &(mesh_para_A.adcirc_cell[i].vertid[0]),
        &(mesh_para_A.adcirc_cell[i].vertid[1]),
        &(mesh_para_A.adcirc_cell[i].vertid[2]));
    mesh_para_A.adcirc_cell[i].marked = 0;
    mesh_para_A.adcirc_cell[i].cellid = i;
  }

  fclose(fp);

  CCTK_VInfo(CCTK_THORNSTRING, "finish reading ADCIRC mesh file %s", mesh_file);
}

void ADCIRCMeshOutput(const char * mesh_file)
{
  FILE *fp;
  int i;
  int output_vert_count = 0;
  int output_cell_count = 0;

  if (mesh_para_A.vert_count == 0 || mesh_para_A.cell_count == 0)
  {
    CCTK_INFO("There are no vertices or cells to output !");
    return;
  }

  if ((fp = fopen(mesh_file, "w")) == NULL)
  {
    CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s",
        mesh_file);
  }

  fprintf(fp, "%s\n", mesh_file);
  fprintf(fp, "%d %d\n", mesh_para_A.cell_count, mesh_para_A.vert_count);
  for (i = 1; i <= mesh_para_A.vert_count; i++)
  {
    fprintf(fp, "%d %lf %lf %lf\n", mesh_para_A.adcirc_vert[i].vertid,
    		mesh_para_A.adcirc_vert[i].x[0], mesh_para_A.adcirc_vert[i].x[1],
    		mesh_para_A.adcirc_depth[i].f);
  }
  for (i = 1; i <= mesh_para_A.cell_count; i++)
  {
    fprintf(fp, "%d 3 %d %d %d\n", mesh_para_A.adcirc_cell[i].cellid,
    		mesh_para_A.adcirc_cell[i].vertid[0],
    		mesh_para_A.adcirc_cell[i].vertid[1],
    		mesh_para_A.adcirc_cell[i].vertid[2]);
  }
  fclose(fp);

  CCTK_VInfo(CCTK_THORNSTRING, "finish writing the selected region in %s",
      mesh_file);
}
