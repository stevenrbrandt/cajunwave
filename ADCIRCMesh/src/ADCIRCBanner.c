#include <cctk.h>

int ADCIRCMesh_Banner (void)
{
  const char *const banner =
"\n=================================================\n"
"      o-o               o \n"
"     |     o            | \n"
"      o-o    o-O-o o  o | o-o  o-o o-o  oo o-o \n"
"         | | | | | |  | | | | |    |-' | | |  | \n"
"     o--o  | o o o o--o o o-o  o-o o-o o-o-o  o \n"
"\n"
"  Simulocean Coastal Modeling Toolkit on Cactus  \n\n"
"         (c) Copyright The Authors \n"
"         GNU Licensed. No Warranty \n"
"=================================================\n\n";
  CCTK_RegisterBanner(banner);
  return 0;
}
