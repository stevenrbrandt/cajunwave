#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void ADCIRCMesh_Init (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        weight1[i3D] = unknown_default;
        weight2[i3D] = unknown_default;
        weight3[i3D] = unknown_default;
        vert1[i3D] = -1;
        vert2[i3D] = -1;
        vert3[i3D] = -1;
      }
}
