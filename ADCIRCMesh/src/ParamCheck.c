#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void ADCIRCMesh_ParamCheck( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  if (xmin < -180 || xmax > 180 || ymin < -180 || ymax > 180)
  {
    CCTK_WARN(0, "The domain must be in [-180:180][-180:180]");
  }

  if (output_selected_mesh
      && (poi_output_bathymetry || poi_output_elevation
          || poi_output_current_velocity || poi_output_wind_velocity))
  {
    CCTK_WARN(
        0,
        "Outputting a selected mesh has to be done separately with interpolations at the points of interests. This shall be fixed soon.");
  }
}
