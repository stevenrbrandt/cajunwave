#include <stdio.h>
#include <stdlib.h>
#include "ADCIRCMesh.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#define min(X, Y) ((X) < (Y) ? (X) : (Y))

//int num_time_steps;		//need change, local interest
//int num_time_steps_scattered;		//local interest
//int poi_count;	//global, used by other: ADCIRCScatteredInterp.c, ADCIRCStructuredInterp.c


field3d *poi_weight;

/* local inline functions */

void ADCIRCMesh_POICalcWeight( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i, j;
  MALLOC_SAFE_CALL(
      poi_weight = (field3d *) malloc((mesh_para_A.poi_count + 1) * sizeof(field3d)));

  /* find the element */
  for (j = 1; j <= mesh_para_A.poi_count; j++)
  {
    int cell_found = 0;
    for (i = 1; i <= mesh_para_A.cell_count; i++)
    {
      /* find mesh_para_A.poi[j] in the element i and interpolate */
      if (PointInElement(mesh_para_A.poi[j].f, i))
      {
        int v1, v2, v3;
        CCTK_REAL w1, w2, w3, w_denom;
        v1 = mesh_para_A.adcirc_cell[i].vertid[0];
        v2 = mesh_para_A.adcirc_cell[i].vertid[1];
        v3 = mesh_para_A.adcirc_cell[i].vertid[2];

        w1 = pow(Distance(2, mesh_para_A.poi[j].f, mesh_para_A.adcirc_vert[v1].x),
        			-invdist_power);

        w2 = pow(Distance(2, mesh_para_A.poi[j].f, mesh_para_A.adcirc_vert[v2].x),
        			-invdist_power);

        w3 = pow(Distance(2, mesh_para_A.poi[j].f, mesh_para_A.adcirc_vert[v3].x),
        			-invdist_power);

        w_denom = w1 + w2 + w3;
        poi_weight[j].f[0] = w1 / w_denom;
        poi_weight[j].f[1] = w2 / w_denom;
        poi_weight[j].f[2] = w3 / w_denom;
        /* store the cell index */
        poi_weight[j].id = i;
        cell_found = 1;

#ifdef DEBUGADCIRCMESH
        printf("Found in Cell =  %10d %10d %10d %10d \n", i,
        		mesh_para_A.adcirc_cell[i].vertid[0],
        		mesh_para_A.adcirc_cell[i].vertid[1],
        		mesh_para_A.adcirc_cell[i].vertid[2]);

        printf("          POI =  %10d %10.8g, %10.8g \n", j,
        		mesh_para_A.poi[j].f[0],
        		mesh_para_A.poi[j].f[1]);

        printf("           V1 =  %10d %10.8g, %10.8g \n",
        		mesh_para_A.adcirc_cell[i].vertid[0],
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].x[0],
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[0]].x[1]);

        printf("           V2 =  %10d %10.8g, %10.8g \n",
        		mesh_para_A.adcirc_cell[i].vertid[1],
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].x[0],
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[1]].x[1]);

        printf("           V3 =  %10d %10.8g, %10.8g \n",
        		mesh_para_A.adcirc_cell[i].vertid[2],
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].x[0],
        		mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[i].vertid[2]].x[1]);

        printf("           W1 =  %10.8g \n", poi_weight[j].f[0]);
        printf("           W2 =  %10.8g \n", poi_weight[j].f[1]);
        printf("           W3 =  %10.8g \n", poi_weight[j].f[2]);
        printf("--------------------------------------------------\n");
#endif /* end debugging output */

        break;
      }
    }
    if (cell_found == 0)
    {
      poi_weight[j].f[0] = 0.0;
      poi_weight[j].f[1] = 0.0;
      poi_weight[j].f[2] = 0.0;
      poi_weight[j].id = -1;
#ifdef DEBUGADCIRCMESH
      printf("POI           =  %10.8g, %10.8g \n", mesh_para_A.poi[j].f[0],
    		  mesh_para_A.poi[j].f[1]);

      printf("the POI is not found in any cell in the mesh !\n");
      printf("--------------------------------------------------\n");
#endif /* end debugging output */

    }
  }
}

void ADCIRCMesh_POIInterp( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i;
  int id, v1, v2, v3;
  FILE *fp;

  int scattered_wind_vert_count = 0;
  int scattered_current_vert_count = 0;
  int scattered_elevation_vert_count = 0;

  field1d *scattered_adcirc_elevation = NULL;
  field2d *scattered_adcirc_currentfield = NULL;
  field2d *scattered_adcirc_windfield = NULL;

  if (poi_output_bathymetry)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "ScatteredInterp: interpolating bathymetry to %s", poi_bathymetry_file);

    FOPEN_SAFE_CALL(fp = fopen(poi_bathymetry_file, "w"));

    fprintf(fp, "# %s\n", poi_bathymetry_file);
    fprintf(fp, "# Time Steps : 1\n");

    field1d * poi_bathymetry;

    MALLOC_SAFE_CALL(
        poi_bathymetry = (field1d *) malloc((mesh_para_A.poi_count + 1) *
        									sizeof(field1d)));

    for (i = 1; i <= mesh_para_A.poi_count; i++)
    {
      id = poi_weight[i].id;
      if (id == -1)
      {
        poi_bathymetry[i].id = id;
        poi_bathymetry[i].f = unknown_default;
      }
      else
      {
        v1 = mesh_para_A.adcirc_cell[id].vertid[0];
        v2 = mesh_para_A.adcirc_cell[id].vertid[1];
        v3 = mesh_para_A.adcirc_cell[id].vertid[2];

        poi_bathymetry[i].id = poi_weight[i].id;

        poi_bathymetry[i].f = poi_weight[i].f[0] * mesh_para_A.adcirc_depth[v1].f
            + poi_weight[i].f[1] * mesh_para_A.adcirc_depth[v2].f
            + poi_weight[i].f[2] * mesh_para_A.adcirc_depth[v3].f;
      }
#ifdef DEBUGADCIRCMESH
      printf("Bathymetry at POI : %10.8g \n", poi_bathymetry[i].f);

      printf("V1: %10.8g %10.8g %10d %10.8g, %10.8g \n",
    		  mesh_para_A.adcirc_depth[v1].f,
    		  poi_weight[i].f[0], v1,
    		  mesh_para_A.adcirc_vert[v1].x[0],
    		  mesh_para_A.adcirc_vert[v1].x[1]);

      printf("V2: %10.8g %10.8g %10d %10.8g, %10.8g \n",
    		  mesh_para_A.adcirc_depth[v2].f,
    		  poi_weight[i].f[1], v2,
    		  mesh_para_A.adcirc_vert[v2].x[0],
    		  mesh_para_A.adcirc_vert[v2].x[1]);

      printf("V3: %10.8g %10.8g %10d %10.8g, %10.8g \n",
    		  mesh_para_A.adcirc_depth[v3].f,
    		  poi_weight[i].f[2], v3,
    		  mesh_para_A.adcirc_vert[v3].x[0],
    		  mesh_para_A.adcirc_vert[v3].x[1]);

      printf("----------------------------------------------------------\n");
#endif /* end debugging output */
    }

    /* print time step */
    fprintf(fp, "%d", 1);
    POIOutput(1, fp, poi_bathymetry);
    free(poi_bathymetry);
    fclose(fp);
  }

  if (poi_output_elevation)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "ScatteredInterp: interpolating elevation to %s", poi_elevation_file);
    FILE *fp_elevation;

    char line[LINELENGTH];
    /* read file */
    FOPEN_SAFE_CALL(fp_elevation = fopen(adcirc_elevation_file, "r"));

    /* skip the first line */
    fgets(line, LINELENGTH, fp_elevation);

    fscanf(fp_elevation, "%d %d %*G %*d %*d %*s %*d",
    		&mesh_para_A.num_time_steps_scattered,
    		&scattered_elevation_vert_count);

    MALLOC_SAFE_CALL(
    		scattered_adcirc_elevation = (field1d *) malloc(
            (scattered_elevation_vert_count + 1) * sizeof(field1d)));

    /* interpolation */
    FOPEN_SAFE_CALL(fp = fopen(poi_elevation_file, "w"));
    field1d * poi_elevation;

    MALLOC_SAFE_CALL(
        poi_elevation = (field1d *) malloc((mesh_para_A.poi_count + 1) *
        									sizeof(field1d)));

    //check that init_output_time_step and num_output_time_steps is a sensible choice.
    if (mesh_para_A.num_time_steps_scattered < start_output_time_step
        || mesh_para_A.num_time_steps_scattered < num_output_time_steps)
    {
      CCTK_WARN(0,
          "start_output_time_step or num_output_time_steps is out of range");
    }
    if (num_output_time_steps != -1)
    {
    	mesh_para_A.num_time_steps_scattered =
    			min(mesh_para_A.num_time_steps_scattered, num_output_time_steps);
    }

    fprintf(fp, "# %s\n", poi_elevation_file);
    fprintf(fp, "# Time Steps : %d\n", mesh_para_A.num_time_steps_scattered);

    /* iterate through time steps */
    for(int t=start_output_time_step; t<mesh_para_A.num_time_steps_scattered; t++)
    {
      ADCIRCCoastalReader(fp_elevation, scattered_adcirc_elevation, 63);

      for (i = 1; i <= mesh_para_A.poi_count; i++)
      {
        id = poi_weight[i].id;
        if (id == -1)
        {
          poi_elevation[i].id = id;
          poi_elevation[i].f = unknown_default;
        }
        else
        {
          v1 = mesh_para_A.adcirc_cell[id].vertid[0];
          v2 = mesh_para_A.adcirc_cell[id].vertid[1];
          v3 = mesh_para_A.adcirc_cell[id].vertid[2];

          poi_elevation[i].id = poi_weight[i].id;

          poi_elevation[i].f = poi_weight[i].f[0] * scattered_adcirc_elevation[v1].f
              + poi_weight[i].f[1] * scattered_adcirc_elevation[v2].f
              + poi_weight[i].f[2] * scattered_adcirc_elevation[v3].f;
        }
#ifdef DEBUGADCIRCMESH
        printf("Time Step: %d\n", t);
        printf("Elevation at POI : %10.8g \n", poi_elevation[i].f);

        printf("V1: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_elevation[v1].f, poi_weight[i].f[0], v1,
        		mesh_para_A.adcirc_vert[v1].x[0],
        		mesh_para_A.adcirc_vert[v1].x[1]);

        printf("V2: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_elevation[v2].f, poi_weight[i].f[1], v2,
        		mesh_para_A.adcirc_vert[v2].x[0],
        		mesh_para_A.adcirc_vert[v2].x[1]);

        printf("V3: %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_elevation[v3].f, poi_weight[i].f[2], v3,
        		mesh_para_A.adcirc_vert[v3].x[0],
        		mesh_para_A.adcirc_vert[v3].x[1]);

        printf("----------------------------------------------------------\n");
#endif /* end debugging output */
      }

      /* print time step */
      fprintf(fp, "%d", t);
      POIOutput(1, fp, poi_elevation);
    }
    free(poi_elevation);
    fclose(fp_elevation);
  }

  if (poi_output_current_velocity)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "ScatteredInterp: interpolating current velocity to %s",
        poi_current_velocity_file);

    FILE *fp_current;

    char line[LINELENGTH];
    /* read file */
    FOPEN_SAFE_CALL(fp_current = fopen(adcirc_current_file, "r"));

    /* skip the first line */
    fgets(line, LINELENGTH, fp_current);

    fscanf(fp_current, "%d %d %*G %*d %*d %*s %*d",
    		&mesh_para_A.num_time_steps_scattered,
    		&scattered_current_vert_count);
    MALLOC_SAFE_CALL(
    		scattered_adcirc_currentfield = (field2d *) malloc(
            (scattered_current_vert_count + 1) * sizeof(field2d)));

    /* interpolation */
    FOPEN_SAFE_CALL(fp = fopen(poi_current_velocity_file, "w"));
    field2d * poi_current_velocity;
    MALLOC_SAFE_CALL(
        poi_current_velocity = (field2d *) malloc(
            (mesh_para_A.poi_count + 1) * sizeof(field2d)));

    //check that init_output_time_step and num_output_time_steps is a sensible choice.
    if (mesh_para_A.num_time_steps_scattered < start_output_time_step
        || mesh_para_A.num_time_steps_scattered < num_output_time_steps)
    {
      CCTK_WARN(0,
          "start_output_time_step or num_output_time_steps is out of range");
    }
    if (num_output_time_steps != -1)
    {
    	mesh_para_A.num_time_steps_scattered =
    			min(mesh_para_A.num_time_steps_scattered, num_output_time_steps);
    }

    fprintf(fp, "# %s\n", poi_current_velocity_file);
    fprintf(fp, "# Time Steps : %d\n", mesh_para_A.num_time_steps_scattered);

    /* iterate through time steps */
    for (int t=start_output_time_step; t<mesh_para_A.num_time_steps_scattered; t++)
    {
      ADCIRCCoastalReader(fp_current, scattered_adcirc_currentfield, 64);

      for (i = 1; i <= mesh_para_A.poi_count; i++)
      {
        id = poi_weight[i].id;
        if (id == -1)
        {
          poi_current_velocity[i].id = id;
          poi_current_velocity[i].f[0] = unknown_default;
          poi_current_velocity[i].f[1] = unknown_default;
        }
        else
        {
          v1 = mesh_para_A.adcirc_cell[id].vertid[0];
          v2 = mesh_para_A.adcirc_cell[id].vertid[1];
          v3 = mesh_para_A.adcirc_cell[id].vertid[2];

          poi_current_velocity[i].id = poi_weight[i].id;

          poi_current_velocity[i].f[0] = poi_weight[i].f[0]
              * scattered_adcirc_currentfield[v1].f[0]
              + poi_weight[i].f[1] * scattered_adcirc_currentfield[v2].f[0]
              + poi_weight[i].f[2] * scattered_adcirc_currentfield[v3].f[0];

          poi_current_velocity[i].f[1] = poi_weight[i].f[0]
              * scattered_adcirc_currentfield[v1].f[1]
              + poi_weight[i].f[1] * scattered_adcirc_currentfield[v2].f[1]
              + poi_weight[i].f[2] * scattered_adcirc_currentfield[v3].f[1];
        }
#ifdef DEBUGADCIRCMESH
        printf("Time Step: %d\n", t);
        printf("Current velocity at POI : %10.8g,%10.8g \n",
            poi_current_velocity[i].f[0], poi_current_velocity[i].f[1]);

        printf("V1: %10.8g %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_currentfield[v1].f[0],
        		scattered_adcirc_currentfield[v1].f[1],
        		poi_weight[i].f[0], v1,
        		mesh_para_A.adcirc_vert[v1].x[0],
        		mesh_para_A.adcirc_vert[v1].x[1]);

        printf("V2: %10.8g %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_currentfield[v2].f[0],
        		scattered_adcirc_currentfield[v2].f[1],
        		poi_weight[i].f[1], v2,
        		mesh_para_A.adcirc_vert[v2].x[0],
        		mesh_para_A.adcirc_vert[v2].x[1]);

        printf("V3: %10.8g %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_currentfield[v3].f[0],
        		scattered_adcirc_currentfield[v3].f[1],
        		poi_weight[i].f[2], v3,
        		mesh_para_A.adcirc_vert[v3].x[0],
        		mesh_para_A.adcirc_vert[v3].x[1]);

        printf("----------------------------------------------------------\n");
#endif /* end debugging output */
      }
      /* print time step */
      fprintf(fp, "%d", t);
      POIOutput(2, fp, poi_current_velocity);
    }
    free(poi_current_velocity);
    fclose(fp_current);
  }

  if (poi_output_wind_velocity)
  {
    CCTK_VInfo(CCTK_THORNSTRING,
        "ScatteredInterp: interpolating wind velocity to %s",
        poi_current_velocity_file);
    FILE *fp_wind;

    char line[LINELENGTH];
    /* read file */
    FOPEN_SAFE_CALL(fp_wind = fopen(adcirc_wind_file, "r"));

    /* skip the first line */
    fgets(line, LINELENGTH, fp_wind);

    fscanf(fp_wind, "%d %d %*G %*d %*d %*s %*d", &mesh_para_A.num_time_steps_scattered,
        &scattered_wind_vert_count);

    MALLOC_SAFE_CALL(
    		scattered_adcirc_windfield = (field2d *) malloc(
            (scattered_wind_vert_count + 1) * sizeof(field2d)));

    /* interpolation */
    FOPEN_SAFE_CALL(fp = fopen(poi_wind_velocity_file, "w"));

    field2d * poi_wind_velocity;
    MALLOC_SAFE_CALL(
        poi_wind_velocity = (field2d *) malloc(
            (mesh_para_A.poi_count + 1) * sizeof(field2d)));

    //check that init_output_time_step and num_output_time_steps is a sensible choice.
    if (mesh_para_A.num_time_steps_scattered < start_output_time_step
        || mesh_para_A.num_time_steps_scattered < num_output_time_steps)
    {
      CCTK_WARN(0,
          "start_output_time_step or num_output_time_steps is out of range");
    }
    if (num_output_time_steps != -1)
    {
    	mesh_para_A.num_time_steps_scattered =
    			min(mesh_para_A.num_time_steps_scattered, num_output_time_steps);
    }

    fprintf(fp, "# %s\n", poi_wind_velocity_file);
    fprintf(fp, "# Time Steps : %d\n", mesh_para_A.num_time_steps_scattered);

    /* iterate through time steps */
    for (int t=start_output_time_step; t<mesh_para_A.num_time_steps_scattered; t++)
    {
      ADCIRCCoastalReader(fp_wind, scattered_adcirc_windfield, 64);

      for (i = 1; i <= mesh_para_A.poi_count; i++)
      {
        id = poi_weight[i].id;
        if (id == -1)
        {
          poi_wind_velocity[i].id = id;
          poi_wind_velocity[i].f[0] = unknown_default;
          poi_wind_velocity[i].f[1] = unknown_default;
        }
        else
        {
          v1 = mesh_para_A.adcirc_cell[id].vertid[0];
          v2 = mesh_para_A.adcirc_cell[id].vertid[1];
          v3 = mesh_para_A.adcirc_cell[id].vertid[2];

          poi_wind_velocity[i].id = poi_weight[i].id;

          poi_wind_velocity[i].f[0] = poi_weight[i].f[0]
              * scattered_adcirc_windfield[v1].f[0]
              + poi_weight[i].f[1] * scattered_adcirc_windfield[v2].f[0]
              + poi_weight[i].f[2] * scattered_adcirc_windfield[v3].f[0];

          poi_wind_velocity[i].f[1] = poi_weight[i].f[0]
              * scattered_adcirc_windfield[v1].f[1]
              + poi_weight[i].f[1] * scattered_adcirc_windfield[v2].f[1]
              + poi_weight[i].f[2] * scattered_adcirc_windfield[v3].f[1];
        }
#ifdef DEBUGADCIRCMESH
        printf("Time Step: %d\n", t);
        printf("Wind velocity at POI : %10.8g,%10.8g \n",
            poi_wind_velocity[i].f[0], poi_wind_velocity[i].f[1]);

        printf("V1: %10.8g %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_windfield[v1].f[0],
        		scattered_adcirc_windfield[v1].f[1],
        		poi_weight[i].f[0], v1,
        		mesh_para_A.adcirc_vert[v1].x[0],
        		mesh_para_A.adcirc_vert[v1].x[1]);

        printf("V2: %10.8g %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_windfield[v2].f[0],
        		scattered_adcirc_windfield[v2].f[1],
        		poi_weight[i].f[1], v2,
        		mesh_para_A.adcirc_vert[v2].x[0],
        		mesh_para_A.adcirc_vert[v2].x[1]);

        printf("V3: %10.8g %10.8g %10.8g %10d %10.8g, %10.8g \n",
        		scattered_adcirc_windfield[v3].f[0],
        		scattered_adcirc_windfield[v3].f[1],
        		poi_weight[i].f[2], v3,
        		mesh_para_A.adcirc_vert[v3].x[0],
        		mesh_para_A.adcirc_vert[v3].x[1]);

        printf("----------------------------------------------------------\n");
#endif /* end debugging output */
      }
      /* print time step */
      fprintf(fp, "%d", t);
      POIOutput(2, fp, poi_wind_velocity);
    }
    free(poi_wind_velocity);
    fclose(fp_wind);
  }
  free(poi_weight);
}

void ADCIRCMesh_POIReader( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  POIReader(poi_input_file);
}

void POIOutput(const int dim, FILE *fp, void *data)
{ /* output */

  switch (dim)
  {
    case 1:
    {
      field1d *data1d = (field1d *) data;
      for (int i = 1; i <= mesh_para_A.poi_count; i++)
      {
        fprintf(fp, "%13.5g ", data1d[i].f);
      }
      fprintf(fp, "\n");
      break;
    }
    case 2:
    {
      field2d *data2d = (field2d *) data;
      for (int i = 1; i <= mesh_para_A.poi_count; i++)
      {
        fprintf(fp, "%13.5g %13.5g ", data2d[i].f[0], data2d[i].f[1]);
      }
      fprintf(fp, "\n");
      break;
    }
    case 3:
    {
      field3d *data3d = (field3d *) data;
      for (int i = 1; i <= mesh_para_A.poi_count; i++)
      {
        fprintf(fp, "%13.5g %13.5g ", data3d[i].f[0], data3d[i].f[1],
            data3d[i].f[2]);
      }
      fprintf(fp, "\n");
      break;
    }
    default:
      CCTK_WARN(0, "field type not supported !");
  }
// This is another way of outputing. We may provide an option later
//  switch (dim)
//  {
//    case 1:
//    {
//      field1d *data1d = (field1d *) data;
//      for (int i = 1; i <= mesh_para_A.poi_count; i++)
//      {
//        fprintf(fp, "%13.5g %13.5g %13.5g\n", mesh_para_A.poi[i].f[0], mesh_para_A.poi[i].f[1],
//            data1d[i].f);
//      }
//      break;
//    }
//    case 2:
//    {
//      field2d *data2d = (field2d *) data;
//      for (int i = 1; i <= mesh_para_A.poi_count; i++)
//      {
//        fprintf(fp, "%13.5g %13.5g %13.5g %13.5g\n", i, mesh_para_A.poi[i].f[0],
//            mesh_para_A.poi[i].f[1], data2d[i].f[0], data2d[i].f[1]);
//      }
//      break;
//    }
//    case 3:
//    {
//      field3d *data3d = (field3d *) data;
//      for (int i = 1; i <= mesh_para_A.poi_count; i++)
//      {
//        fprintf(fp, "%13.5g %13.5g %13.5g %13.5g %13.5g\n", mesh_para_A.poi[i].f[0],
//            mesh_para_A.poi[i].f[1], data3d[i].f[0], data3d[i].f[1], data3d[i].f[2]);
//      }
//      break;
//    }
//    default:
//      CCTK_WARN(0, "field type not supported !");
//  }
}

/* 1 in, 0 not in */
inline int PointInElement(const CCTK_REAL * p, const CCTK_INT id)
{
  return PointInTriangle(p,
		  mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[id].vertid[0]].x,
		  mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[id].vertid[1]].x,
		  mesh_para_A.adcirc_vert[mesh_para_A.adcirc_cell[id].vertid[2]].x);
}

/*
 * For a triangle formed by three vertice x1,x2,x3. The cooridnate of any
 * point P can be expressed in term of x1,x2,x3 and two coefficients u, v.
 * P = x1 + u * (x2 - x1) + v * (x3 - x1) test if a point is in a given
 * trangle form by three vertice x1, x2, x3 1 in, 0 not in.
 */

inline int PointInTriangle(const CCTK_REAL * p, const CCTK_REAL * x1,
    const CCTK_REAL * x2, const CCTK_REAL * x3)
{
  CCTK_REAL sub0[2], sub1[2], sub2[2];
  CCTK_REAL u, v, d00, d01, d02, d11, d12, invdenom;
  int i;

  for (i = 0; i < 2; i++)
  {
    sub0[i] = x3[i] - x1[i];
    sub1[i] = x2[i] - x1[i];
    sub2[i] = p[i] - x1[i];
  }

  /* Compute dot products */
  d00 = VecDotProduct(2, sub0, sub0);
  d01 = VecDotProduct(2, sub0, sub1);
  d02 = VecDotProduct(2, sub0, sub2);
  d11 = VecDotProduct(2, sub1, sub1);
  d12 = VecDotProduct(2, sub1, sub2);

  /* Compute barycentric coordinates */
  invdenom = 1.0 / (d00 * d11 - d01 * d01);
  u = (d11 * d02 - d01 * d12) * invdenom;
  v = (d00 * d12 - d01 * d02) * invdenom;

  /* 0 not in, 1 in */
  return (u >= 0) && (v >= 0) && (u + v <= 1.0);
}

/* return dot product of vector x and y with size n */

inline CCTK_REAL VecDotProduct(const CCTK_INT n, const CCTK_REAL * v1,
    const CCTK_REAL * v2)
{
  CCTK_INT i;
  CCTK_REAL sum = 0.0;
  for (i = 0; i < n; i++)
  {
    sum += v1[i] * v2[i];
  }
  return sum;
}

/* return the distance between points x1 and x2 of n dimension */

inline CCTK_REAL Distance(const CCTK_INT n, const CCTK_REAL * x1,
    const CCTK_REAL * x2)
{
  CCTK_INT i;
  CCTK_REAL distance = 0.0;
  for (i = 0; i < n; i++)
  {
    distance += (x1[i] - x2[i]) * (x1[i] - x2[i]);
  }
  return sqrt(distance);
}

void POIReader(const char * poi_file)
{
  FILE * fp;
  char line[LINELENGTH];
  int i;

  if ((fp = fopen(poi_file, "r")) == NULL)
  {
    CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s",
        poi_file);
  }

  /* read in the poi name */
  fgets(line, LINELENGTH, fp);

  /* read in the total number of POI */
  fscanf(fp, "%d", &mesh_para_A.poi_count);

  CCTK_VInfo(CCTK_THORNSTRING, "found %d points of interest from file %s",
		  mesh_para_A.poi_count, poi_file);

  /* Allocate space for vertex and cell arrays. */

  /* notice that if we want to use the array index to refer to the vertices we
   * need to allocate an extra room for the 0th. We do the same for the water depth*/
  MALLOC_SAFE_CALL(mesh_para_A.poi =
		  (field2d *) malloc((mesh_para_A.poi_count + 1) * sizeof(field2d)));

  if (mesh_para_A.poi == NULL)
  {
    CCTK_WARN(0, "failed to allocate memory for the point of interest.");
  }

  mesh_para_A.poi[0].id = mesh_para_A.poi_count;

  for (i = 1; i <= mesh_para_A.poi_count; i++)
  {
    fscanf(fp, "%d %lf %lf", &(mesh_para_A.poi[i].id),
    		&(mesh_para_A.poi[i].f[0]), &(mesh_para_A.poi[i].f[1]));
  }

  fclose(fp);

#ifdef DEBUGADCIRCMESH
  for (i = 1; i <= mesh_para_A.poi_count; i++)
  {
    printf("%d : (%lf, %lf)\n",
    		mesh_para_A.poi[i].id,
    		mesh_para_A.poi[i].f[0],
    		mesh_para_A.poi[i].f[1]);
  }
#endif
}

/* This only deals with the striped file where the top 2 lines of the original
 * adcirc file were stripped */
void ADCIRCCoastalReader(FILE *fp, void *data, const int field_id)
{
  int i, idx, num_lines;

  switch (field_id)
  {
    case 63:
    {
      fscanf(fp, "%*G %*d %d %*G", &num_lines);
      field1d *data1d = (field1d *) data;

      CCTK_REAL f0;
      for (i = 0; i < num_lines; i++)
      {
        fscanf(fp, "%d %lf", &idx, &f0);
        data1d[idx].id = idx;
        data1d[idx].f = f0;
      }
//      scattered_elevation_vert_count = num_lines;

      break;
    }

    case 64:
    {
      fscanf(fp, "%*G %*d %d %*G", &num_lines);
      field2d *data2d = (field2d *) data;

      CCTK_REAL f0, f1;

      for (i = 0; i < num_lines; i++)
      {
        fscanf(fp, "%d %lf %lf", &idx, &f0, &f1);
        data2d[idx].id = idx;
        data2d[idx].f[0] = f0;
        data2d[idx].f[1] = f1;
      }
//      scattered_current_vert_count = num_lines;

      break;
    }
    case 74:
    {
      fscanf(fp, "%*G %*d %d %*G", &num_lines);
      field2d *data2d = (field2d *) data;
      CCTK_REAL f0, f1;

      for (i = 0; i < num_lines; i++)
      {
        fscanf(fp, "%d %lf %lf", &idx, &f0, &f1);
        data2d[idx].id = idx;
        data2d[idx].f[0] = f0;
        data2d[idx].f[1] = f1;
      }
//      scattered_wind_vert_count = num_lines;

      break;
    }
    default:
    {
      CCTK_WARN(0, "You should never get here !");
    }
  }
  return;
}
