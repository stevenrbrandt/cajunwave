#ifndef _ADCIRCINTERP_H_
#define _ADCIRCINTERP_H_

#include "cctk.h"

#ifdef __cplusplus

extern "C"
{
#endif


/* structure for 2d vertices */
  typedef struct _vert2d
  {
    /* x = x[0], y = x[1] */
    CCTK_REAL x[2];
  } vert2d;

/* structure for 2d cells (only consider triangular elements here) */
  typedef struct _cell2d
  {
    CCTK_INT vertid[3];
  } cell2d;

  /* structure of the weight for inverse distance weighted interpolation */
  typedef struct _weight3
  {
    CCTK_REAL weight[3];
  } weight3;

  /* structure for ADCIRC data */

  typedef struct _field2d
  {
    CCTK_INT vertid;

/* 2d vector associated with this vertex */

    CCTK_REAL f[2];
  } field2d;


  /* structure for user's point of interest on a given time step */
  typedef struct _poi
  {
    /* time */
    CCTK_REAL t;
    /* number of points */
    CCTK_INT c;
    /* points */
    vert2d *p;
    cell2d *v;
    weight3 *w;
    /* interpolated value */
    CCTK_REAL *uc1, *uc2, *uw1, *uw2, *d;
  } poi2d;

/* prototype for routines dealing with ADCIRC files */
  void ADCIRCCoastalFileSplitter (const char *datafile, const char *prefix);
  void ADCIRCMeshReaderASCII (const char *mesh_file);
  void ADCIRCCoastalReaderASCII (const char *dir, const char *prefix,
                                 const int step, const int field);

/* used by qsort and bsearch for searching vertices */
  extern int CompareVertid (const void *c1, const void *c2);

/* max buffer size for reading files */
#define LINELENGTH 100

  extern int vert_count;
  extern int cell_count;
  extern vert2d *adcirc_vert;
  extern cell2d *adcirc_cell;

  extern CCTK_REAL *adcirc_depth;
  extern field2d *adcirc_windfield;
  extern field2d *adcirc_currentfield;
  extern int wind_vert_count;
  extern int current_vert_count;

  extern int poi_timecount;
  extern poi2d *poi;
  /* distribution of points */
  extern int *lc;


#ifdef __cplusplus
}
#endif

#endif                          //_ADCIRCINTERP_H_
