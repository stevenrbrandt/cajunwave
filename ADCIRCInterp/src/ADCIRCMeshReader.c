#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ADCIRCInterp.h"

/*
 * Interface to the readers of different type of mesh formats.
 * Potential formats will be HDF5 and F5
 * */

int vert_count;
int cell_count;
vert2d *adcirc_vert;
cell2d *adcirc_cell;
CCTK_REAL *adcirc_depth;

inline int CompareCentroidY (const void *c1, const void *c2);

void ADCIRCInterp_ADCIRCMeshReaderASCII (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i;

  CCTK_REAL centroidx, centroidy;

  /* read cells into adcirc_cell and read vertices into adcirc_vert */
  ADCIRCMeshReaderASCII (adcirc_mesh_file);

  /*sort cells by y coordinate of the centoid */
  /* we sort along y coordinate. notice that we sort the all cells here */

  CCTK_INFO ("start sorting cells ...");
  qsort (adcirc_cell, cell_count, sizeof (cell2d), CompareCentroidY);

/*
  printf ("Vert1   Index =  %13d \n", adcirc_cell[280190].vertid[0]);
  printf ("        Vert1 =  (%13.5lf, %13.5lf) \n",
          adcirc_vert[adcirc_cell[280190].vertid[0]].x[0],
          adcirc_vert[adcirc_cell[280190].vertid[0]].x[1]);
  printf ("Vert2   Index =  %13d \n", adcirc_cell[280190].vertid[1]);
  printf ("        Vert2 =  (%13.5lf, %13.5lf) \n",
          adcirc_vert[adcirc_cell[280190].vertid[1]].x[0],
          adcirc_vert[adcirc_cell[280190].vertid[1]].x[1]);
  printf ("Vert3   Index =  %13d \n", adcirc_cell[280190].vertid[2]);
  printf ("        Vert3 =  (%13.5lf, %13.5lf) \n",
          adcirc_vert[adcirc_cell[280190].vertid[2]].x[0],
          adcirc_vert[adcirc_cell[280190].vertid[2]].x[1]);
*/


  CCTK_INFO ("finish sorting cells.");

  return;
}

/*
 * Default ASCII reader.
 * */

void ADCIRCMeshReaderASCII (const char *mesh_file)
{
  FILE *fp;
  char line[LINELENGTH];
  int i;

  if ((fp = fopen (mesh_file, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                "can't open: %s\n", mesh_file);
  }

  /* skip the first line */
  fgets (line, LINELENGTH, fp);

  CCTK_VInfo (CCTK_THORNSTRING, "start reading ADCIRC mesh file %s with %s",
              mesh_file, line);


  /* read in the total number of vertices and cells */
  fscanf (fp, "%d %d", &cell_count, &vert_count);

  CCTK_VInfo (CCTK_THORNSTRING, "found %d vertices and %d cells", vert_count,
              cell_count);

  /* Allocate space for vertex and cell arrays. */

  /* notice that if we want to use the array index to refer to the vertices we
   * need to allocate an extra room for the 0th. We do the same for the water depth*/
  if (((adcirc_vert =
        (vert2d *) malloc ((vert_count + 1) * sizeof (vert2d))) == NULL)
      || ((adcirc_cell = (cell2d *) malloc (cell_count * sizeof (cell2d))) == NULL)
      || ((adcirc_depth =
        (CCTK_REAL *) malloc ((vert_count + 1) * sizeof (CCTK_REAL))) == NULL))
  {
    CCTK_WARN (0, "failed to allocate memory for the mesh.");
  }

  for (i = 1; i <= vert_count; i++)
  {
    fscanf (fp, "%*d %lf %lf %lf", &(adcirc_vert[i].x[0]),
            &(adcirc_vert[i].x[1]), &(adcirc_depth[i]));
  }
  /* Read in which verts are associated with each cell. */
  for (i = 0; i < cell_count; i++)
  {
    fscanf (fp, "%*d %*d %d %d %d",
            &(adcirc_cell[i].vertid[0]), &(adcirc_cell[i].vertid[1]),
            &(adcirc_cell[i].vertid[2]));
  }

  fclose (fp);

  CCTK_VInfo (CCTK_THORNSTRING, "finish reading ADCIRC mesh file %s\n", mesh_file);

  return;
}

/*
 * Free up all the memory allocated for the vertices and cells.
 * */


int ADCIRCInterp_ADCIRCMeshReaderMemFree (void)
{
  free (adcirc_vert);
  free (adcirc_cell);
  return 0;
}



/* compare the Y coordinate of the centroid of the cells */
inline int CompareCentroidY (const void *c1, const void *c2)
{
  CCTK_REAL centroidy1, centroidy2;
  cell2d *ic1 = (cell2d *) c1;
  cell2d *ic2 = (cell2d *) c2;

  centroidy1 = (adcirc_vert[ic1->vertid[0]].x[1]
                + adcirc_vert[ic1->vertid[1]].x[1]
                + adcirc_vert[ic1->vertid[2]].x[1]) / 3.0;
  centroidy2 = (adcirc_vert[ic2->vertid[0]].x[1]
                + adcirc_vert[ic2->vertid[1]].x[1]
                + adcirc_vert[ic2->vertid[2]].x[1]) / 3.0;

  if (centroidy1 > centroidy2)
  {
    return 1;
  }
  else if (centroidy1 < centroidy2)
  {
    return -1;
  }
  else
  {
    return 0;
  }
}
