#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ADCIRCInterp.h"

#ifndef PI
#define PI 3.14159265358979323846       /* pi */
#endif

field2d *adcirc_windfield;
field2d *adcirc_currentfield;
int wind_vert_count;
int current_vert_count;
poi2d *poi;

inline int CompareVertid (const void *c1, const void *c2);

/* local inline functions */
inline int FileLineCounter (const char *filename);

void ADCIRCInterp_ADCIRCCoastalReaderASCII (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i;
  CCTK_REAL tempy;
  CCTK_REAL conv_x, conv_y;

/*
 * adcirc_wind_dir = "/scratch/gustav/wind"
 * wind_file_prefix= wind
 * will point to files like /scratch/gustav/wind/wind.0 etc.
 * */

  ADCIRCCoastalReaderASCII (adcirc_current_dir, current_file_prefix,
		  (CCTK_INT)poi[cctk_iteration - 1].t, 64);
  ADCIRCCoastalReaderASCII (adcirc_wind_dir, wind_file_prefix,
		  (CCTK_INT)poi[cctk_iteration - 1].t, 74);

  /*
   * converting the units:
   * the units of ADCIRC data are,
   * for mesh,  Longitude (in deg), latitude(in deg), depth (in m);
   * for winds, windx(in m/s) and windy (in m/s);
   * for elevations, el (in m);
   * for currents, u(in m/s) and v(in m/s).
   *
   * From http://en.wikipedia.org/wiki/Geographic_coordinate_system
   * Expressing latitude and longitude as linear units
   *
   * Along y or longitude (in degree) in the code, for each degree we have
   * 111132.954 - 559.822*cos ( PI * y / 90.0)  + 1.175 * cos ( PI * y/45.0) meters
   *
   * Along x or latitude
   *
   * (6378137.0 * PI)/(180.0 * sqrt(1.0 + 0.9933056213348961 * power(tan((Pi * y)/180.),2.0))) meters
   * */
  if (do_convert_units)
  {
    for (i = 0; i < wind_vert_count; i++)
    {
      tempy = adcirc_vert[adcirc_windfield[i].vertid].x[1];
      conv_x =
        (6378137.0 * PI) / (180.0 *
                            sqrt (1.0 +
                                  0.9933056213348961 *
                                  pow (tan ((PI * tempy) / 180.), 2.0)));
      conv_y =
        111132.954 - 559.822 * cos (PI * tempy / 90.0) +
        1.175 * cos (PI * tempy / 45.0);
      adcirc_windfield[i].f[0] = 3600.0 * adcirc_windfield[i].f[0] / conv_x;
      adcirc_windfield[i].f[1] = 3600.0 * adcirc_windfield[i].f[1] / conv_y;
    }

    for (i = 0; i < current_vert_count; i++)
    {
      tempy = adcirc_vert[adcirc_currentfield[i].vertid].x[1];
      conv_x =
        (6378137.0 * PI) / (180.0 *
                            sqrt (1.0 +
                                  0.9933056213348961 *
                                  pow (tan ((PI * tempy) / 180.), 2.0)));
      conv_y =
        111132.954 - 559.822 * cos (PI * tempy / 90.0) +
        1.175 * cos (PI * tempy / 45.0);
      adcirc_currentfield[i].f[0] = 3600.0 * adcirc_currentfield[i].f[0] / conv_x;
      adcirc_currentfield[i].f[1] = 3600.0 * adcirc_currentfield[i].f[1] / conv_y;

    }

  }

  return;
}

void ADCIRCCoastalReaderASCII (const char *dir, const char *prefix,
                               const int step, const int field)
{
  char filename[LINELENGTH];
  FILE *fp;
  char line[LINELENGTH];
  int i;
  int linecount;

/*  printf("address of adcirc_vert %d\n", adcirc_vert);*/
  sprintf (filename, "%s/%s.%d", dir, prefix, step);

/* get total of lines (number of vertices) */
  linecount = FileLineCounter (filename);

  if ((fp = fopen (filename, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s", filename);
  }

  CCTK_VInfo (CCTK_THORNSTRING,
              "start reading ADCIRC data file %s with %d lines.", filename,
              linecount);

  switch (field)
  {

  case 64:
    if ((adcirc_currentfield =
         (field2d *) malloc (linecount * sizeof (field2d))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the ADCIRC current data.");
    }
    for (i = 0; i < linecount; i++)
    {
      fscanf (fp, "%d %lf %lf", &(adcirc_currentfield[i].vertid),
              &(adcirc_currentfield[i].f[0]), &(adcirc_currentfield[i].f[1]));
    }
    current_vert_count = linecount;

    /* we sort with the vertex id */
    qsort (adcirc_currentfield, current_vert_count, sizeof (field2d), CompareVertid);

    break;

  case 74:
    if ((adcirc_windfield =
         (field2d *) malloc (linecount * sizeof (field2d))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the ADCIRC wind data.");
    }
    for (i = 0; i < linecount; i++)
    {
      fscanf (fp, "%d %lf %lf", &(adcirc_windfield[i].vertid),
              &(adcirc_windfield[i].f[0]), &(adcirc_windfield[i].f[1]));
    }
    wind_vert_count = linecount;

    /* we sort with the vertex id */
    qsort (adcirc_windfield, wind_vert_count, sizeof (field2d), CompareVertid);

    break;

  default:
    CCTK_WARN (0, "You should never get here !");;
  }

  return;
}


inline int FileLineCounter (const char *filename)
{
  FILE *fp;
  char line[LINELENGTH];
  int linecount = 0;

  if ((fp = fopen (filename, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s", filename);
  }

  while (fgets (line, LINELENGTH, fp) != NULL)
  {
    linecount++;
  }
  fclose (fp);
  return linecount;
}

/*
 * Free up all the memory allocated for the wind and current field.
 * */

void ADCIRCInterp_ADCIRCCoastalReaderMemFree (CCTK_ARGUMENTS)
{
  free (adcirc_windfield);
  free (adcirc_currentfield);
  return;
}


/* compare the vertex id of associated with the field data */
inline int CompareVertid (const void *c1, const void *c2)
{
  field2d *ic1 = (field2d *) c1;
  field2d *ic2 = (field2d *) c2;

  return (ic1->vertid - ic2->vertid);
}
