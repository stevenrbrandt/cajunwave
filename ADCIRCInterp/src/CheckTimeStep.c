#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk_Arguments.h"

#include "ADCIRCInterp.h"

poi2d *poi;
int poi_timecount;

void ADCIRCInterp_CheckTimeStep (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  int i;
  /* terminate the iteration when the iteration is bigger than the */

  if(poi_timecount ==  cctk_iteration)
  {
    CCTK_TerminateNext (cctkGH);
    CCTK_VInfo (CCTK_THORNSTRING,
                "Check time %d, %d, %lf", cctk_iteration, poi_timecount,
                (CCTK_INT)poi[i].t);
  }
  return;
}
