#include <stdio.h>
#include <stdlib.h>
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ADCIRCInterp.h"

poi2d *poi;
int do_interp;

//#define GAVINDEBUG

/* local inline functions */
inline int PointInTriangle (const CCTK_REAL * p, const CCTK_REAL * x1,
                            const CCTK_REAL * x2, const CCTK_REAL * x3);

inline CCTK_REAL VecDotProduct (const CCTK_INT n, const CCTK_REAL * v1,
                                const CCTK_REAL * v2);

inline CCTK_REAL Distance (const CCTK_INT n, const CCTK_REAL * x1,
                           const CCTK_REAL * x2);

void ADCIRCInterp_InvDist (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* Cactus reserved indices */
  CCTK_INT i, j, k;

  /* used to iterate through points and cells */
  CCTK_INT l;

  /* internal current and wind data of three vertices */
  CCTK_REAL iuc1[3], iuc2[3], iuw1[3], iuw2[3];

  /* define the key for bsearch */
  field2d bkey[3], *result;

  /* the main loop */
  i = cctk_iteration - 1;
    for (j = 0; j < poi[i].c; j++)
    {

      /* initailize the searching key with three surrounding vertices */
      bkey[0].vertid = poi[i].v[j].vertid[0];
      bkey[1].vertid = poi[i].v[j].vertid[1];
      bkey[2].vertid = poi[i].v[j].vertid[2];


      /* the wind and current data have been sorted with vertid,
       * so we can use bsearch to speed up searching */

      /* first search through wind data */

      /* vertex 1 */
      if ((result = (field2d *) bsearch
           (&(bkey[0]), adcirc_windfield, wind_vert_count, sizeof (field2d),
            CompareVertid)) == NULL)
      {
        iuw1[0] = 0.0;
        iuw2[0] = 0.0;
      }
      else
      {
        iuw1[0] = result->f[0];
        iuw2[0] = result->f[1];
      }

      /* vertex 2 */
      if ((result = (field2d *) bsearch
           (&(bkey[1]), adcirc_windfield, wind_vert_count, sizeof (field2d),
            CompareVertid)) == NULL)
      {
        iuw1[1] = 0.0;
        iuw2[1] = 0.0;
      }
      else
      {
        iuw1[1] = result->f[0];
        iuw2[1] = result->f[1];
      }

      /* vertex 3 */
      if ((result = (field2d *) bsearch
           (&(bkey[2]), adcirc_windfield, wind_vert_count, sizeof (field2d),
            CompareVertid)) == NULL)
      {
        iuw1[2] = 0.0;
        iuw2[2] = 0.0;
      }
      else
      {
        iuw1[2] = result->f[0];
        iuw2[2] = result->f[1];
      }

      /* then search through current data */

      /* vertex 1 */
      if ((result = (field2d *) bsearch
           (&(bkey[0]), adcirc_currentfield, current_vert_count,
            sizeof (field2d), CompareVertid)) == NULL)
      {
        iuc1[0] = 0.0;
        iuc2[0] = 0.0;
      }
      else
      {
#ifdef  GAVINDEBUG
        printf ("Found ! \n");
        printf ("currentfield  =  (%13lf, %13lf) \n", result->f[0], result->f[1]);
#endif /* end debugging output */

        iuc1[0] = result->f[0];
        iuc2[0] = result->f[1];
      }

      /* vertex 2 */
      if ((result = (field2d *) bsearch
           (&(bkey[1]), adcirc_currentfield, current_vert_count,
            sizeof (field2d), CompareVertid)) == NULL)
      {
        iuc1[1] = 0.0;
        iuc2[1] = 0.0;
      }
      else
      {
        iuc1[1] = result->f[0];
        iuc2[1] = result->f[1];
      }
      /* vertex 3 */
      if ((result = (field2d *) bsearch
           (&(bkey[2]), adcirc_currentfield, current_vert_count,
            sizeof (field2d), CompareVertid)) == NULL)
      {
        iuc1[2] = 0.0;
        iuc2[2] = 0.0;
      }
      else
      {
        iuc1[2] = result->f[0];
        iuc2[2] = result->f[1];
      }

      /* 
       * now I need to get the value of the data points in 
       * adcirc_windfield and adcirc_currentfield 
       * */
      poi[i].uc1[j] =
        poi[i].w[j].weight[0] * iuc1[0] + poi[i].w[j].weight[1] * iuc1[1] +
        poi[i].w[j].weight[2] * iuc1[2];
      poi[i].uc2[j] =
        poi[i].w[j].weight[0] * iuc2[0] + poi[i].w[j].weight[1] * iuc2[1] +
        poi[i].w[j].weight[2] * iuc2[2];
      poi[i].uw1[j] =
        poi[i].w[j].weight[0] * iuw1[0] + poi[i].w[j].weight[1] * iuw1[1] +
        poi[i].w[j].weight[2] * iuw1[2];
      poi[i].uw2[j] =
        poi[i].w[j].weight[0] * iuw2[0] + poi[i].w[j].weight[1] * iuw2[1] +
        poi[i].w[j].weight[2] * iuw2[2];

      /* water depth is much easier since it comes with the mesh file */
      poi[i].d[j] = poi[i].w[j].weight[0] * adcirc_depth[poi[i].v[j].vertid[0]]
                    + poi[i].w[j].weight[1] * adcirc_depth[poi[i].v[j].vertid[1]]
                    + poi[i].w[j].weight[2] * adcirc_depth[poi[i].v[j].vertid[2]];
    }                           /* end of the main loop */

  return;
}

/* Weight functions solely depend on the mesh and targetting points 
 * when both the mesh and targetting points are fixed, we can store
 * the weight functions on each targetting point and use them during
 * the time evolution without any more calculations. */

void ADCIRCInterp_CalcWeight (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /*
   * Cactus reserved indices 
   */
  CCTK_INT i, j;

  /*
   * used to iterate through points and cells 
   */
  CCTK_INT l, m;

  /*
   * used for searching the interested cells 
   */

  CCTK_INT cellidxmin, cellidxmax;
  CCTK_REAL centroidymin, centroidymax;
  CCTK_INT cell_found = 0;

  /* internal vertex for checking if the target point is in a triangle formed by the 3 closest vertices */
  vert2d target_pt;

  /* counting lost points */
  CCTK_INT num_lost_pt = 0;

  /*
   * interpolated value and part of the weight function 
   */
  CCTK_REAL w_denom;

  CCTK_INFO
    ("start interpolating scattered data with inverse distance weighted method.");

  /* for each structured grid point find the cell that contains it */

  /* the main loop */
  for (i = 0; i < poi_timecount; i++)
    for (j = 0; j < poi[i].c; j++)
    {
      /* reset all the status variables */
      cellidxmin = 0;
      cellidxmax = cell_count - 1;
      cell_found = 0;

      /*
       * this is working because we sorted the cells with coordinate y. 
       * This will greatly limit the number of cells and vertices we are 
       * going to search.
       * */

      for (l = 0; l < cell_count; l++)
      {
        centroidymin = (adcirc_vert[adcirc_cell[l].vertid[0]].x[1]
                        + adcirc_vert[adcirc_cell[l].vertid[1]].x[1]
                        + adcirc_vert[adcirc_cell[l].vertid[2]].x[1]) / 3.0;

        if (poi[i].p[j].x[1] >= (centroidymin + search_range))
        {
          cellidxmin = l;
        }

        centroidymax =
          (adcirc_vert[adcirc_cell[cell_count - l].vertid[0]].x[1] +
           adcirc_vert[adcirc_cell[cell_count - l].vertid[1]].x[1] +
           adcirc_vert[adcirc_cell[cell_count - l].vertid[2]].x[1]) / 3.0;

        if (poi[i].p[j].x[1] <= (centroidymax - search_range))
        {
          cellidxmax = cell_count - l;
        }
      }

#ifdef  GAVINDEBUG
      printf ("ADCIRCInterp_InvDist \n");
      printf ("(i, j)        =  (%13d, %13d) \n", i, j);
      printf ("(X, Y)        =  (%13.5lf, %13.5lf) \n", poi[i].p[j].x[0],
              poi[i].p[j].x[1]);
      printf ("Search Y     in  (%13.5lf, %13.5lf) \n",
              poi[i].p[j].x[1] - search_range, poi[i].p[j].x[1] + search_range);
      printf ("Search Cell  in  (%13d, %13d)\n", cellidxmin, cellidxmax);
      printf ("Centroid Y   in  (%13.5lf, %13.5lf)\n",
              (adcirc_vert[adcirc_cell[cellidxmin].vertid[0]].x[1] +
               adcirc_vert[adcirc_cell[cellidxmin].vertid[1]].x[1] +
               adcirc_vert[adcirc_cell[cellidxmin].vertid[2]].x[1]) / 3.0,
              (adcirc_vert[adcirc_cell[cellidxmax].vertid[0]].x[1] +
               adcirc_vert[adcirc_cell[cellidxmax].vertid[1]].x[1] +
               adcirc_vert[adcirc_cell[cellidxmax].vertid[2]].x[1]) / 3.0);
#endif /* end debugging output */


      target_pt.x[0] = poi[i].p[j].x[0];
      target_pt.x[1] = poi[i].p[j].x[1];
      w_denom = 0.0;


      for (m = cellidxmin; m < cellidxmax; m++)
      {
        /* things to do if we found it */
        if (PointInTriangle
            (target_pt.x,
             adcirc_vert[adcirc_cell[m].vertid[0]].x,
             adcirc_vert[adcirc_cell[m].vertid[1]].x,
             adcirc_vert[adcirc_cell[m].vertid[2]].x) != 0)
        {
          /* record the vertices of the cell */
          poi[i].v[j].vertid[0] = adcirc_cell[m].vertid[0];
          poi[i].v[j].vertid[1] = adcirc_cell[m].vertid[1];
          poi[i].v[j].vertid[2] = adcirc_cell[m].vertid[2];

          /* calculate the weight functions */
          poi[i].w[j].weight[0] =
            pow (Distance
                 (2, target_pt.x, adcirc_vert[poi[i].v[j].vertid[0]].x),
                 -invdist_power);
          poi[i].w[j].weight[1] =
            pow (Distance (2, target_pt.x, adcirc_vert[poi[i].v[j].vertid[1]].x),
                 -invdist_power);
          poi[i].w[j].weight[2] =
            pow (Distance (2, target_pt.x, adcirc_vert[poi[i].v[j].vertid[2]].x),
                 -invdist_power);

          w_denom =
            poi[i].w[j].weight[0] + poi[i].w[j].weight[1] + poi[i].w[j].weight[2];

          poi[i].w[j].weight[0] /= w_denom;
          poi[i].w[j].weight[1] /= w_denom;
          poi[i].w[j].weight[2] /= w_denom;

#ifdef  GAVINDEBUG
          printf ("Found in Cell =  %13d \n", m);
          printf ("Vert1   Index =  %13d \n", adcirc_cell[m].vertid[0]);
          printf ("        Vert1 =  (%13.5lf, %13.5lf) \n",
                  adcirc_vert[adcirc_cell[m].vertid[0]].x[0],
                  adcirc_vert[adcirc_cell[m].vertid[0]].x[1]);
          printf ("Vert2   Index =  %13d \n", adcirc_cell[m].vertid[1]);
          printf ("        Vert2 =  (%13.5lf, %13.5lf) \n",
                  adcirc_vert[adcirc_cell[m].vertid[1]].x[0],
                  adcirc_vert[adcirc_cell[m].vertid[1]].x[1]);
          printf ("Vert3   Index =  %13d \n", adcirc_cell[m].vertid[2]);
          printf ("        Vert3 =  (%13.5lf, %13.5lf) \n",
                  adcirc_vert[adcirc_cell[m].vertid[2]].x[0],
                  adcirc_vert[adcirc_cell[m].vertid[2]].x[1]);
          printf ("--------------------------------------------------\n");
#endif /* end debugging output */

          cell_found = 1;
          break;
        }
      }                         /* end iteration through cells */

      /* if we haven't found any cells for the grid point after
       * all the work, we will set the weight function to zero. */
      if (cell_found == 0)
      {
        poi[i].w[j].weight[0] = 0.0;
        poi[i].w[j].weight[1] = 0.0;
        poi[i].w[j].weight[2] = 0.0;

        num_lost_pt++;

#ifdef  GAVINDEBUG
        printf ("Failed to find a cell to contain the grid point !!!\n");
        printf ("Total Failed  =  %13d\n", num_lost_pt);
        printf ("--------------------------------------------------\n");
#endif /* end debugging output */

      }
    }                           /* iteration over all Cactus grid point. */

  CCTK_VInfo (CCTK_THORNSTRING,
              "Done interpolating scattered data. Totally %d points are found outside of the mesh.\n",
              num_lost_pt);

  return;
}

/*
 * For a triangle formed by three vertice x1,x2,x3. The cooridnate of any
 * point P can be expressed in term of x1,x2,x3 and two coefficients u, v.
 * P = x1 + u * (x2 - x1) + v * (x3 - x1) test if a point is in a given
 * trangle form by three vertice x1, x2, x3 1 in, 0 not in. 
 */

inline int PointInTriangle (const CCTK_REAL * p, const CCTK_REAL * x1,
                            const CCTK_REAL * x2, const CCTK_REAL * x3)
{
  CCTK_REAL sub0[2], sub1[2], sub2[2];
  CCTK_REAL u, v, d00, d01, d02, d11, d12, invdenom;
  int i;

  for (i = 0; i < 2; i++)
  {
    sub0[i] = x3[i] - x1[i];
    sub1[i] = x2[i] - x1[i];
    sub2[i] = p[i] - x1[i];
  }

  /* Compute dot products */
  d00 = VecDotProduct (2, sub0, sub0);
  d01 = VecDotProduct (2, sub0, sub1);
  d02 = VecDotProduct (2, sub0, sub2);
  d11 = VecDotProduct (2, sub1, sub1);
  d12 = VecDotProduct (2, sub1, sub2);

  /* Compute barycentric coordinates */
  invdenom = 1.0 / (d00 * d11 - d01 * d01);
  u = (d11 * d02 - d01 * d12) * invdenom;
  v = (d00 * d12 - d01 * d02) * invdenom;

  /* 0 not in, 1 in */
  return (u >= 0) && (v >= 0) && (u + v <= 1.0);
}

/* return dot product of vector x and y with size n */

inline CCTK_REAL VecDotProduct (const CCTK_INT n, const CCTK_REAL * v1,
                                const CCTK_REAL * v2)
{
  CCTK_INT i;
  CCTK_REAL sum = 0.0;

  for (i = 0; i < n; i++)
  {
    sum += v1[i] * v2[i];
  }
  return sum;
}

/* return the distance between points x1 and x2 of n dimension */

inline CCTK_REAL Distance (const CCTK_INT n, const CCTK_REAL * x1,
                           const CCTK_REAL * x2)
{
  CCTK_INT i;
  CCTK_REAL distance = 0.0;

  for (i = 0; i < n; i++)
  {
    distance += (x1[i] - x2[i]) * (x1[i] - x2[i]);
  }
  return sqrt (distance);
}
