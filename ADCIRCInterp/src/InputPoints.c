#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "ADCIRCInterp.h"

//#define GAVINDEBUG

poi2d *poi;
int poi_timecount;


/* get the number of time steps and number of POI from a given file */
inline int ADCIRCInterp_POICounter (const char *filename, int *pmaxcount);


void ADCIRCInterp_InputPoints (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  FILE *fp = NULL;
  char line[LINELENGTH];
  CCTK_REAL x, y;
  int i, j;
  int pcount = 0;
  int tcount = 0;
  int retitem = 0;

  poi_timecount = ADCIRCInterp_POICounter (poi_input_file, &pcount);
  printf ("poi_timecount = %d, poi_pointcount = %d\n", poi_timecount, pcount);

  /*
   * Allocate memory for the POI
   * */

  if ((poi = (poi2d *) malloc (poi_timecount * sizeof (poi2d))) == NULL)
  {
    CCTK_WARN (0, "failed to allocate memory for the points of interest array");
  }

  for (i = 0; i < poi_timecount; i++)
  {

    for (j = 0; j < pcount; j++)
    {
      if ((poi[i].p = (vert2d *) malloc (pcount * sizeof (vert2d))) == NULL)
      {
        CCTK_WARN (0, "failed to allocate memory for the points");
      }
    if ((poi[i].v = (cell2d *) malloc (pcount * sizeof (cell2d))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    if ((poi[i].w = (weight3 *) malloc (pcount * sizeof (weight3))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    if ((poi[i].uc1 = (CCTK_REAL *) malloc (pcount * sizeof (CCTK_REAL))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    if ((poi[i].uc2 = (CCTK_REAL *) malloc (pcount * sizeof (CCTK_REAL))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    if ((poi[i].uw1 = (CCTK_REAL *) malloc (pcount * sizeof (CCTK_REAL))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    if ((poi[i].uw2 = (CCTK_REAL *) malloc (pcount * sizeof (CCTK_REAL))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    if ((poi[i].d = (CCTK_REAL *) malloc (pcount * sizeof (CCTK_REAL))) == NULL)
    {
      CCTK_WARN (0, "failed to allocate memory for the points");
    }
    }

  }

  /*
   * Now start reading data into the allocated memory
   * */


  CCTK_VInfo (CCTK_THORNSTRING, "start reading the point of interest file %s",
              poi_input_file);

  if ((fp = fopen (poi_input_file, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                "can't open: %s", poi_input_file);
  }

  while (fgets (line, LINELENGTH, fp) != NULL)
  {
    retitem = sscanf (line, "%lf %lf", &x, &y);
    if (retitem == 1)
    {

      poi[tcount].t = x;
      if (tcount >= 1)
      {
        poi[tcount - 1].c = pcount;

      }
      pcount = 0;
      tcount++;
    }
    else if (retitem == 2)
    {
      poi[tcount - 1].p[pcount].x[0] = x;
      poi[tcount - 1].p[pcount].x[1] = y;
      pcount++;
    }

    else
      continue;
  }
  poi[tcount - 1].c = pcount;
  fclose (fp);

#ifdef  GAVINDEBUG
  for (i = 0; i < poi_timecount; i++)
  {
    printf ("Time          =  (%13.5lf) \n", poi[i].t);
    printf ("Num of Points =  (%d) \n", poi[i].c);
    for (j = 0; j < poi[i].c; j++)
    {
      printf ("(X, Y)        =  (%13.5lf, %13.5lf) \n", poi[i].p[j].x[0],
              poi[i].p[j].x[1]);
    }
  }
#endif /* end debugging output */

  return;
}


/* get the number of time steps and number of POI from a given file */

inline int ADCIRCInterp_POICounter (const char *filename, int *pmaxcount)
{
  FILE *fp;
  int retitem;
  char line[LINELENGTH];

  /* count time steps */
  /* count max number of points */
  int pcount = 0;
  int tcount = 0;
  CCTK_REAL x, y;

  *pmaxcount = 0;
  if ((fp = fopen (filename, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s", filename);
  }

  while (fgets (line, LINELENGTH, fp) != NULL)
  {
    retitem = sscanf (line, "%lf %lf", &x, &y);

    /* there is only one number in a line for the time step */
    if (retitem == 1)
    {
      tcount++;
      *pmaxcount = ((*pmaxcount) > pcount) ? (*pmaxcount) : pcount;
      pcount = 0;
    }
    else if (retitem == 2)
      pcount++;
    else
      continue;
  }
  *pmaxcount = ((*pmaxcount) > pcount) ? (*pmaxcount) : pcount;
  fclose (fp);
  return tcount;
}


/* free memory allocated for ADCIRCInterp */
int ADCIRCInterp_MemFree (void)
{
  int i;

  for (i = 0; i < poi_timecount; i++)
  {
    free (poi[i].p);
    free (poi[i].v);
    free (poi[i].w);
    free (poi[i].uc1);
    free (poi[i].uc2);
    free (poi[i].uw1);
    free (poi[i].uw2);
  }
  free (poi);

  return 0;
}
