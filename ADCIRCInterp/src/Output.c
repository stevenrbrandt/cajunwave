#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "ADCIRCInterp.h"

poi2d *poi;
int poi_timecount;
int do_interp;

void ADCIRCInterp_Output (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  FILE *fp = NULL;
  int myproc;
  int j;

  myproc = CCTK_MyProc (cctkGH);

  if (myproc == 0)
  {

    /* start outputting */
    if ((fp = fopen (poi_output_file, "a")) == NULL)
    {
      CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                  "can't open: %s", poi_output_file);
    }
      fprintf (fp, "%13lf\t%d\n", poi[cctk_iteration-1].t, poi[cctk_iteration-1].c);
      for (j = 0; j < poi[cctk_iteration-1].c; j++)
      {
          printf ("%d%13lf\t%13lf\t"
                           "%d\t%d\t%d\t"
                           "%13lf\t%13lf\t%13lf\t"
                           "%13lf\t%13lf\t"
                           "%13lf\t%13lf\n",
                           cctk_iteration-1,
                           poi[cctk_iteration-1].p[j].x[0],
                           poi[cctk_iteration-1].p[j].x[1],
                           poi[cctk_iteration-1].v[j].vertid[0],
                           poi[cctk_iteration-1].v[j].vertid[1],
                           poi[cctk_iteration-1].v[j].vertid[2],
                           poi[cctk_iteration-1].w[j].weight[0],
                           poi[cctk_iteration-1].w[j].weight[1],
                           poi[cctk_iteration-1].w[j].weight[2],
                           poi[cctk_iteration-1].uc1[j],
                           poi[cctk_iteration-1].uc2[j],
                           poi[cctk_iteration-1].uw1[j],
                           poi[cctk_iteration-1].uw2[j]);

    	  fprintf (fp, "%13lf\t%13lf\t"
                 "%d\t%d\t%d\t"
                 "%13lf\t%13lf\t%13lf\t"
                 "%13lf\t%13lf\t"
                 "%13lf\t%13lf\t"
    			 "%13lf\n",
                 poi[cctk_iteration-1].p[j].x[0],
                 poi[cctk_iteration-1].p[j].x[1],
                 poi[cctk_iteration-1].v[j].vertid[0],
                 poi[cctk_iteration-1].v[j].vertid[1],
                 poi[cctk_iteration-1].v[j].vertid[2],
                 poi[cctk_iteration-1].w[j].weight[0],
                 poi[cctk_iteration-1].w[j].weight[1],
                 poi[cctk_iteration-1].w[j].weight[2],
                 poi[cctk_iteration-1].uc1[j],
                 poi[cctk_iteration-1].uc2[j],
                 poi[cctk_iteration-1].uw1[j],
                 poi[cctk_iteration-1].uw2[j],
                 poi[cctk_iteration-1].d[j]);
      }
    fclose (fp);
  }
  return;
}
