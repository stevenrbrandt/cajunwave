#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "cctk_declare.h"

void funwave_paramcheck(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_paramcheck");
  cctk_auto_thorn = "FUNWAVE";
  DECLAREi_CCTK_SCALAR(state_wavemaker_type);
  DECLARE_CCTK_PARAMETERS;
  if (gamma1 != 0.0 && gamma1 != 1.0)
  {
    CCTK_WARN(0, "paramcheck fail ! gamma1 must be either 0.0 or 1.0");
  }

  if (gamma2 != 0.0 && gamma2 != 1.0)
  {
    CCTK_WARN(0, "paramcheck fail ! gamma2 must be either 0.0 or 1.0");
  }

  if (gamma3 != 0.0 && gamma3 != 1.0)
  {
    CCTK_WARN(0, "paramcheck fail ! gamma3 must be either 0.0 or 1.0");
  }

  if (gamma3 == 0.0 && (gamma1 != 0.0 || gamma2 != 0.0))
  {
    CCTK_WARN(0, "paramcheck fail ! gamma1 and gamma2 shall all be zero with gamma3 being zero");
  }

  *state_wavemaker_type = -1;
  if      (CCTK_EQUALS(wavemaker_type, "ini_rec"))
    *state_wavemaker_type = INI_REC;
  else if (CCTK_EQUALS(wavemaker_type, "lef_sol"))
    *state_wavemaker_type = LEF_SOL;
  else if (CCTK_EQUALS(wavemaker_type, "ini_oth"))
    *state_wavemaker_type = INI_OTH;
  else if (CCTK_EQUALS(wavemaker_type, "wk_reg"))
    *state_wavemaker_type = WK_REG;
  else if (CCTK_EQUALS(wavemaker_type, "wk_irr"))
    *state_wavemaker_type = WK_IRR;
  else if (CCTK_EQUALS(wavemaker_type, "wk_time_series"))
    *state_wavemaker_type = WK_TIME_SERIES;
  else if (CCTK_EQUALS(wavemaker_type, "ini_gau"))
    *state_wavemaker_type = INI_GAU;
  else if (CCTK_EQUALS(wavemaker_type, "ini_sol"))
    *state_wavemaker_type = INI_SOL;
  else if (CCTK_EQUALS(wavemaker_type, "ini_uvz"))
    *state_wavemaker_type = INI_UVZ;
  else
    CCTK_WARN(0, "Unknown wavemaker_type");
}
