#include "cctk_declare.h"

std::string cctk_func_stack;
const char *cctk_auto_thorn = CCTK_EMPTY;
int func_count=0;

void getxy(const cGH *GH,int n,int& x,int& y) {
  const int nx = GH->cctk_lsh[0], ny = GH->cctk_lsh[1], nz = (GH->cctk_lsh[2]>>1);
  const int nmax = CCTK_GFINDEX3D(GH,nx-1,ny-1,GH->cctk_lsh[2]-1)+1;
  if(n < 0 || n >= nmax) {
    std::cout << "n=" << n << " nmax=" << nmax << std::endl;
    assert(false);
  }
  const int zero = CCTK_GFINDEX3D(GH,0,0,nz);
  const int delx = CCTK_GFINDEX3D(GH,1,0,nz) - zero;
  const int dely = CCTK_GFINDEX3D(GH,0,1,nz) - zero;
  // n = delx*i+dely*j;
  if(delx > dely) {
    x = (n-zero)/delx;
    y = (n-zero-x*delx)/dely;
  } else {
    y = (n-zero)/dely;
    x = (n-zero-y*dely)/delx;
  }
  assert(CCTK_GFINDEX3D(GH,x,y,nz)==n);
}
int gety(const cGH *GH,int n) {
  int x,y;
  getxy(GH,n,x,y);
  return y;
}
int getx(const cGH *GH,int n) {
  int x,y;
  getxy(GH,n,x,y);
  return x;
}

void line_calc_(const cGH *GH,int cv) {
	int k = GH->cctk_lsh[2]>>1;
	for(int i=0;i< GH->cctk_lsh[0];i++) {
		for(int j=0;j < GH->cctk_lsh[1];j++) {
			int cc = CCTK_GFINDEX3D(GH,i,j,k);
			if(cc == cv) {
				std::cout << "LINE: cc=" << cc << " i=" << i << " j=" << j << std::endl;
				std::cout << "LINE: cc=" << cc << " cctk_lsh[0]-i=" << (GH->cctk_lsh[0]-i) << " cctk_lsh[1]-j=" << (GH->cctk_lsh[1]-j) << std::endl;
				return;
			}
		}
	}
	std::cout << "NO SUCH LINE: " << cv << std::endl;
}

template<typename T>
void cksum(const cGH *GH,const char *var,T *data,const char *) {
}

unsigned long shift(unsigned long lg,int bits) {
  return (lg << bits) | (lg >> (8*sizeof(lg)-bits));
}

template<>
void cksum(const cGH *GH,const char *var,double *data,const char *rdwr) {
  unsigned long sum = 0;
  assert(sizeof(long)==sizeof(double));
  unsigned long *ldata = (unsigned long *)data;
  const int nx = GH->cctk_lsh[0], ny = GH->cctk_lsh[1], nz = (GH->cctk_lsh[2]>>1);
  for(int j=3;j < ny-3;j++) {
    for(int i=3;i < nx-3;i++) {
      int cc = CCTK_GFINDEX3D(GH,i,j,nz);
      sum ^= ldata[cc];//shift(ldata[cc],cc % 64);
    }
  }
  std::cout << "CKSUM:" << var << "_" << rdwr << "=" << std::hex << sum << std::dec << std::endl;
}

template<>
void cksum(const cGH *GH,const char *var,int *data,const char *rdwr) {
}

typedef std::numeric_limits<double> Fp;

void poison(const cGH *GH,CCTK_REAL *var,const char *varname,const char *file,int line) {
	//std::cout << "POISONING: " << varname << " " << file << ":" << line << std::endl;
  if(var == NULL)
    return;
	for(int k=0;k < GH->cctk_lsh[2];k++) {
		for(int j=0;j< GH->cctk_lsh[1];j++) {
			for(int i=0;i< GH->cctk_lsh[0];i++) {
				int cc = CCTK_GFINDEX3D(GH,i,j,k);
				var[cc] = Fp::signaling_NaN();
			}
		}
	}
}

void poison_boundary(const cGH *GH,CCTK_REAL *var,const char *varname,const char *file,int line) {
//	std::cout << "Poisoning: " << varname << " " << file << ":" << line << std::endl;
  if(var == NULL)
    return;
  const int ilo = GH->cctk_nghostzones[0];
  const int ihi = GH->cctk_lsh[0]-GH->cctk_nghostzones[0];
  const int jlo = GH->cctk_nghostzones[1];
  const int jhi = GH->cctk_lsh[1]-GH->cctk_nghostzones[1];
  //const int klo = GH->cctk_nghostzones[2];
  //const int khi = GH->cctk_lsh[2]-GH->cctk_nghostzones[2];

	for(int k=0;k < GH->cctk_lsh[2];k++) {
		for(int j=0;j< GH->cctk_lsh[1];j++) {
			for(int i=0;i< GH->cctk_lsh[0];i++) {
				int cc = CCTK_GFINDEX3D(GH,i,j,k);
        if(ilo <= i && i < ihi
        && jlo <= j && j < jhi) {
          ;//interior
        } else {
          var[cc] = Fp::signaling_NaN();
        }
			}
		}
	}
}
