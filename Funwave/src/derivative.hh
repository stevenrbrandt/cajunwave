#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_declare.h"

void derivative_x (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_y (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_xx (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_xy (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_yy (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);

void derivative_x_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_y_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_xx_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_xy_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
void derivative_yy_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout);
