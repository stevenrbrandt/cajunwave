#include "derivative.hh"
#include "funwave_util.hh"
#include "cctk_declare.h"

const int nbounds = 3;

#define START_LOOP(LOX,HIX,LOY,HIY) \
for(int j=LOY;j<HIY;j++) {          \
  int cc_ = FUNWAVE_INDEX(0,j);     \
  for(int i=LOX;i<HIX;i++) {        \
    int cc = cc_+i;

void derivative_x (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_x");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    int cc_ = FUNWAVE_INDEX( 0, j);
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
      int cc = cc_+i;
      int im1 = cc_+i-1;//FUNWAVE_INDEX( i - 1, j);
      int ip1 = cc_+i+1;//FUNWAVE_INDEX( i + 1, j);

      uout[cc] = 0.5 * mask9[cc] * (uin[ip1] - uin[im1]) / dx[cc];
    }
  }
}

void derivative_xx_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_xx_high");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    int cc_ = FUNWAVE_INDEX(0, j);
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
		  int cc = cc_+i;
		  int im1 = cc_+i-1;//FUNWAVE_INDEX( i - 1, j);
		  int ip1 = cc_+i+1;//FUNWAVE_INDEX( i + 1, j);
		  int im2 = cc_+i-2;//FUNWAVE_INDEX( i - 2, j);
		  int ip2 = cc_+i+2;//FUNWAVE_INDEX( i + 2, j);
		  ASSERT_(dx[cc] != 0);

		  uout[cc] = mask9[cc] * (-uin[im2] + 16.0*uin[im1] - 30.0*uin[cc] + 16*uin[ip1] - uin[ip2]) /
				  dx[cc] / dx[cc] / 12.0;
	  }
  }
}

void derivative_x_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("derivative_xx_high");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READS(mask9);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    int cc_ = FUNWAVE_INDEX(0, j);
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
			int cc =  cc_+i;
			int im2 = cc_+i-2;//FUNWAVE_INDEX( i-2, j);
			int im1 = cc_+i-1;//FUNWAVE_INDEX( i-1, j);
			int ip1 = cc_+i+1;//FUNWAVE_INDEX( i+1, j);
			int ip2 = cc_+i+2;//FUNWAVE_INDEX( i+2, j);
			uout[cc] = mask9[cc]*(uin[ip2]+2.0*uin[ip1]-2.0*uin[im1]-uin[im2])/8.0/dx[cc];
		}
	}
}

void derivative_y_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("derivative_y_high");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READS(mask9);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dy);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
			int cc =  FUNWAVE_INDEX( i, j);
			int jm2 = FUNWAVE_INDEX( i, j-2);
			int jm1 = FUNWAVE_INDEX( i, j-1);
			int jp1 = FUNWAVE_INDEX( i, j+1);
			int jp2 = FUNWAVE_INDEX( i, j+2);
			uout[cc] = mask9[cc]*(uin[jp2]+2.0*uin[jp1]-2.0*uin[jm1]-uin[jm2])/8.0/dy[cc];
		}
	}
}

void derivative_xy_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG uout)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("derivative_xy_high");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READS(mask9);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);
	DECLARE_CCTK_READS(dy);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
		      int cc = FUNWAVE_INDEX( i, j);

		      int im2jm2 = FUNWAVE_INDEX( i-2, j-2);
		      int im2jm1 = FUNWAVE_INDEX( i-2, j-1);
		      int im2jp1 = FUNWAVE_INDEX( i-2, j+1);
		      int im2jp2 = FUNWAVE_INDEX( i-2, j+2);

		      int im1jm2 = FUNWAVE_INDEX( i-1, j-2);
		      int im1jm1 = FUNWAVE_INDEX( i-1, j-1);
		      int im1jp1 = FUNWAVE_INDEX( i-1, j+1);
		      int im1jp2 = FUNWAVE_INDEX( i-1, j+2);

		      int ip2jm2 = FUNWAVE_INDEX( i+2, j-2);
		      int ip2jm1 = FUNWAVE_INDEX( i+2, j-1);
		      int ip2jp1 = FUNWAVE_INDEX( i+2, j+1);
		      int ip2jp2 = FUNWAVE_INDEX( i+2, j+2);

		      int ip1jm2 = FUNWAVE_INDEX( i+1, j-2);
		      int ip1jm1 = FUNWAVE_INDEX( i+1, j-1);
		      int ip1jp1 = FUNWAVE_INDEX( i+1, j+1);
		      int ip1jp2 = FUNWAVE_INDEX( i+1, j+2);

		      CCTK_REAL fy = 1.0/12.0/dy[cc];
		      CCTK_REAL fx = 1.0/12.0/dx[cc];

		      CCTK_REAL tmp1 = uin[im2jm2]-8.0*uin[im2jm1]+8.0*uin[im2jp1]-uin[im2jp2];
		      CCTK_REAL tmp2 = uin[im1jm2]-8.0*uin[im1jm1]+8.0*uin[im1jp1]-uin[im1jp2];
		      CCTK_REAL tmp3 = uin[ip1jm2]-8.0*uin[ip1jm1]+8.0*uin[ip1jp1]-uin[ip1jp2];
		      CCTK_REAL tmp4 = uin[ip2jm2]-8.0*uin[ip2jm1]+8.0*uin[ip2jp1]-uin[ip2jp2];

		      uout[cc] = mask9[cc]*fx*fy*(tmp1-8.0*tmp2+8.0*tmp3-tmp4);
		}
	}
}

void derivative_yy_high (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_yy_high");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dy);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j-1);
      int jp1 = FUNWAVE_INDEX( i, j+1);
      int jm2 = FUNWAVE_INDEX( i, j-2);
      int jp2 = FUNWAVE_INDEX( i, j+2);

      uout[cc] = mask9[cc] * (-uin[jm2] + 16.0*uin[jm1] - 30.0*uin[cc] + 16*uin[jp1] - uin[jp2]) /
    		  dy[cc] / dy[cc] / 12.0;
    }
  }
}

void derivative_xx (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_xx");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int im1 = FUNWAVE_INDEX( i - 1, j);
      int ip1 = FUNWAVE_INDEX( i + 1, j);

      uout[cc] = mask9[cc] * (uin[ip1] - 2.0 * uin[cc] + uin[im1]) / dx[cc] / dx[cc];
    }
  }
}

void derivative_y (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_y");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dy);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j - 1);
      int jp1 = FUNWAVE_INDEX( i, j + 1);

      uout[cc] = 0.5 * mask9[cc] * (uin[jp1] - uin[jm1]) / dy[cc];
    }
  }
}

void derivative_yy (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_yy");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dy);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j - 1);
      int jp1 = FUNWAVE_INDEX( i, j + 1);

      uout[cc] = mask9[cc] * (uin[jp1] - 2.0 * uin[cc] + uin[jm1]) / dy[cc] / dy[cc];
    }
  }
}

void derivative_xy (CCTK_ARGUMENTS, CCTK_READ_ARG uin, CCTK_WRITE_ARG  uout)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("derivative_xy");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask9);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  DECLARE_CCTK_READS(dy);
  //#pragma omp parallel for
  for (int j = nbounds; j < cctk_lsh[1]-nbounds; j++)
  {
    for (int i = nbounds; i < cctk_lsh[0]-nbounds; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1ip1 = FUNWAVE_INDEX( i + 1, j - 1);
      int jp1ip1 = FUNWAVE_INDEX( i + 1, j + 1);
      int jm1im1 = FUNWAVE_INDEX( i - 1, j - 1);
      int jp1im1 = FUNWAVE_INDEX( i - 1, j + 1);
      CCTK_REAL divp1 = 0.5 * (uin[jp1ip1] - uin[jm1ip1]) / dy[cc];
      CCTK_REAL divm1 = 0.5 * (uin[jp1im1] - uin[jm1im1]) / dy[cc];

      uout[cc] = 0.5 * mask9[cc] * (divp1 - divm1) / dx[cc];
    }
  }
}

