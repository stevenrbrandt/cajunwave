#ifndef FUNWAVE_UTIL_H_
#define FUNWAVE_UTIL_H

#include <algorithm>


#include "cctk_ScheduleFunctions.h"

void copy_to_east_border(CCTK_ARGUMENTS,CCTK_REAL *var);
void copy_to_north_border(CCTK_ARGUMENTS,CCTK_REAL *var);
void copy_to_borders(CCTK_ARGUMENTS,CCTK_REAL *var);

void funwave_exchange_dispersion(CCTK_ARGUMENTS);

int const nbound = 3;


template<typename T> T max(const T a, const T b, const T c)
{
  return std::max(a,std::max(b,c));
}

template<typename T> T min(const T a, const T b, const T c)
{
  return std::min(a,std::min(b,c));
}

template<typename T> T sq(const T t)
{
	return t*t;
}

#define DECLARE_FUNWAVE_BOUNDS \
const int ilo = cctk_bbox[0] ? nbound : cctk_nghostzones[0]; \
const int ihi = cctk_bbox[1] ? cctk_lsh[0]-nbound : cctk_lsh[0]-cctk_nghostzones[0]; \
const int jlo = cctk_bbox[2] ? nbound : cctk_nghostzones[1]; \
const int jhi = cctk_bbox[3] ? cctk_lsh[1]-nbound : cctk_lsh[1]-cctk_nghostzones[1]; \

const CCTK_REAL rho_aw = 0.0012041;

using std::max;
using std::min;

#define zvalue (cctk_lsh[2]>>1)

// In Carpet we need to have ghost zones on either side of the plane that we are evolving
// In PUGH we don't need this, and don't want it.
#define FUNWAVE_INDEX(I,J) CCTK_GFINDEX3D(cctkGH,I,J,zvalue)

enum enum_wavemaker_type {INI_REC, LEF_SOL, INI_OTH, WK_REG, WK_IRR, WK_TIME_SERIES, INI_GAU, INI_SOL, INI_UVZ};

#endif
