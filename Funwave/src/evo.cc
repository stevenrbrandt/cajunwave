#include <cstdlib>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "here.hh"
#include "Tridiagonal_Prototypes.h"
#include "cctk_declare.h"

extern "C" int GetRefinementLevel(const cGH *);

void funwave_placeholder(CCTK_ARGUMENTS) {}
void funwave_phi_coll(CCTK_ARGUMENTS, CCTK_REAL *phi, int vtype);

CCTK_REAL fc,dep,depl,depr,zero=0.0,tmp1,tmp2,tmp3,tmp4,dep_tmp;
CCTK_REAL reta,retal,retar;

/* from the subroutine GET_Eta_U_V_HU_HV with dispersion turned off */
void funwave_poststep_1(CCTK_ARGUMENTS) {
	_DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;

	cctk_func fdecl("funwave_poststep_1");

	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_WRITEREADS(h);
	DECLARE_CCTK_WRITES(u0);
	DECLARE_CCTK_READS(eta);
	DECLARE_CCTK_READS(depth);
	DECLARE_CCTK_READWRITES(u);
	DECLARE_CCTK_READS(v);
	DECLARE_CCTK_READS(mask9);
	DECLARE_CCTK_READS(mask);
	DECLARE_CCTK_READS(vxy);
	DECLARE_CCTK_READS(dvxy);
	DECLARE_CCTK_READS(etax);
	DECLARE_CCTK_READS(vy);
	DECLARE_CCTK_READS(dvy);
	DECLARE_CCTK_SCALAR(b1);
	DECLARE_CCTK_SCALAR(b2);
	DECLARE_CCTK_READS(ubar);
	DECLARE_CCTK_WRITES(v0);
	cctk_auto_thorn = "TRIDIAGONAL";
	DECLARE_CCTK_WRITES(axx);
	DECLARE_CCTK_WRITES(bxx);
	DECLARE_CCTK_WRITES(cxx);
	DECLARE_CCTK_WRITES(dxx);
	DECLAREi_CCTK_SCALAR(trid_ilo);
	DECLAREi_CCTK_SCALAR(trid_ihi);
	DECLAREi_CCTK_SCALAR(trid_jlo);
	DECLAREi_CCTK_SCALAR(trid_jhi);
	DECLAREi_CCTK_SCALAR(trid_klo);
	DECLAREi_CCTK_SCALAR(trid_khi);

  int ilo = nbound;
  int jlo = nbound;
  int ihi = cctk_lsh[0]-nbound;
  int jhi = cctk_lsh[1]-nbound;
	for (int j=0;j<cctk_lsh[1];j++) {
		for (int i=0;i<cctk_lsh[0];i++) {
			int cc = FUNWAVE_INDEX( i, j);
			h[cc] = gamma3 * eta[cc] + depth[cc];
			u0[cc] = u[cc];
			v0[cc] = v[cc];
		}
	}
	if (dispersion_on) {
        if(!spherical_coordinates) {
		  for(int j=jlo;j<jhi;j++) {
			for(int i=ilo;i<ihi;i++) {
				int cc = FUNWAVE_INDEX(i,j);
				int im1 = FUNWAVE_INDEX(i-1,j);
				int ip1 = FUNWAVE_INDEX(i+1,j);
				dep=max(depth[cc],mindepthfrc);
				depl=max(depth[im1],mindepthfrc);
				depr=max(depth[ip1],mindepthfrc);

				if (disp_time_left) {
					reta=eta[cc];
					retal=eta[im1];
					retar=eta[ip1];

					tmp1=gamma1*mask9[cc]*((*b1)/2.0/dx[cc]/dx[cc]*dep*dep + (*b2)/dx[cc]/dx[cc]*depl*dep)
			            		 -gamma2*mask9[cc]*((reta+retal)*depl/2.0/dx[cc]/dx[cc]+(retal+reta)*(retal+reta)/8.0/dx[cc]/dx[cc]);
					tmp2=1.0+gamma1*mask9[cc]*(-(*b1)/dx[cc]/dx[cc]*dep*dep-2.0*(*b2)/dx[cc]/dx[cc]*dep*dep)
			            		 +gamma2*mask9[cc]*((retar+retal+2.0*reta)/2.0/dx[cc]/dx[cc]
			            				 +(retar*retar+2.0*reta*reta
			            						 +2.0*reta*retar+2.0*retal*reta+retal*retal )/8.0/dx[cc]/dx[cc]);
					tmp3=gamma1*mask9[cc]*((*b1)/2.0/dx[cc]/dx[cc]*dep*dep + (*b2)/dx[cc]/dx[cc]*dep*depr)
			            		 -gamma2*mask9[cc]*((reta+retar)*depr/2.0/dx[cc]/dx[cc]+(retar+reta)*(retar+reta)/8.0/dx[cc]/dx[cc]);
					tmp4=ubar[cc]*mask[cc]/max((CCTK_REAL)h[cc],mindepthfrc)
			            		+ gamma1*mask9[cc]*( -(*b1)/2.0*dep*dep*vxy[cc]-(*b2)*dep*dvxy[cc])
			            		+ gamma2*mask9[cc]*(reta*reta/2.0*vxy[cc]+reta*dvxy[cc]
			            		                                                    + reta*etax[cc]*vy[cc] + etax[cc]*dvy[cc] );
				} else {
					tmp1=gamma1*mask9[cc]*((*b1)/2.0/dx[cc]/dx[cc]*dep*dep + (*b2)/dx[cc]/dx[cc]*depl*dep);
					tmp2=1.0+gamma1*mask9[cc]*(-(*b1)/dx[cc]/dx[cc]*dep*dep-2.0*(*b2)/dx[cc]/dx[cc]*dep*dep);
					tmp3=gamma1*mask9[cc]*((*b1)/2.0/dx[cc]/dx[cc]*dep*dep + (*b2)/dx[cc]/dx[cc]*dep*depr);
					tmp4=ubar[cc]*mask[cc]/max((CCTK_REAL)h[cc],mindepthfrc)
			            		+ gamma1*mask9[cc]*( -(*b1)/2.0*dep*dep*vxy[cc]-(*b2)*dep*dvxy[cc]);
				}

				if (tmp2  !=  0.0 || mask[cc] > 0.5) {
					axx[cc]=tmp1/tmp2;
					bxx[cc]=1.0;
					cxx[cc]=tmp3/tmp2;
					dxx[cc]=tmp4/tmp2;
				} else {
					axx[cc]=zero;
					bxx[cc]=1.0;
					cxx[cc]=zero;
					dxx[cc]=zero;
				}
			}
		  }
		} else { // spherical
      //#pragma omp parallel for
		  for(int j=jlo;j<jhi;j++) {
       //#pragma omp simd
			 for(int i=ilo;i<ihi;i++) {
				int cc = FUNWAVE_INDEX(i,j);
				int im1 = FUNWAVE_INDEX(i-1,j);
				int ip1 = FUNWAVE_INDEX(i+1,j);
				dep=max(depth[cc],mindepthfrc);
				depl=max(depth[im1],mindepthfrc);
				depr=max(depth[ip1],mindepthfrc);
			    tmp1=gamma1*mask9[cc]*((*b1)/2.0/dx[cc]/dx[cc]*dep*dep + (*b2)/dx[cc]/dx[cc]*depl*dep);
		        tmp2=1.0+gamma1*mask9[cc]*(-(*b1)/dx[cc]/dx[cc]*dep*dep-2.0*(*b2)/dx[cc]/dx[cc]*dep*dep);
		        tmp3=gamma1*mask9[cc]*((*b1)/2.0/dx[cc]/dx[cc]*dep*dep + (*b2)/dx[cc]/dx[cc]*dep*depr);
		        tmp4=ubar[cc]*mask[cc]/max((CCTK_REAL)h[cc],mindepthfrc)
		             + gamma1*mask9[cc]*( -(*b1)/2.0*dep*dep*vxy[cc]-(*b2)*dep*dvxy[cc]);

				if (tmp2  !=  0.0 || mask[cc] > 0.5) {
					axx[cc]=tmp1/tmp2;
					bxx[cc]=1.0;
					cxx[cc]=tmp3/tmp2;
					dxx[cc]=tmp4/tmp2;
				} else {
					axx[cc]=zero;
					bxx[cc]=1.0;
					cxx[cc]=zero;
					dxx[cc]=zero;
				}
			 }
		  }
		}
	}
	*trid_ilo = nbound;
	*trid_ihi = cctk_lsh[0]-nbound-1;
	*trid_jlo = nbound;
	*trid_jhi = cctk_lsh[1]-nbound-1;
	*trid_klo = zvalue;
	*trid_khi = zvalue;
  if(GetRefinementLevel(cctkGH)>0) {
    *trid_ilo=2;
    *trid_ihi=cctk_lsh[0]-3;
    *trid_jlo=2;
    *trid_jhi=cctk_lsh[1]-3;
  }
  #ifdef TRACE_ON
  std::cout << "cctk_exec: tridiagonal_setup_x" << std::endl;
  std::cout << "cctk_exec: tridiagonal_x_parallel_impl" << std::endl;
  #endif
  int zz = FUNWAVE_INDEX(2,2);
  if(GetRefinementLevel(cctkGH)>0) {
    u[zz]=999;
  }
}

void funwave_poststep_1_5(CCTK_ARGUMENTS) {
	_DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	cctk_auto_thorn = "FUNWAVE";
	cctk_func fdecl("funwave_poststep_1_5");
	DECLARE_CCTK_READS(u);
  int zz = FUNWAVE_INDEX(2,2);
  if(GetRefinementLevel(cctkGH)>0) {
    assert(u[zz] != 999);
    if(dispersion_on) {
      Tridiagonal_CopyIn(CCTK_PASS_CTOC,(CCTK_REAL *)u);
    }
  }
}

		//Tridiagonal_x(CCTK_PASS_CTOC,u,nbound,cctk_lsh[0]-nbound-2,nbound,cctk_lsh[1]-1-nbound,zvalue,zvalue);
void funwave_poststep_2(CCTK_ARGUMENTS) {
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("funwave_poststep_2");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READWRITES(u);
	DECLARE_CCTK_READWRITES(v);
	DECLARE_CCTK_READS(depth);
	DECLARE_CCTK_READS(eta);
	DECLARE_CCTK_READS(mask9);
	DECLARE_CCTK_READS(mask);
	DECLARE_CCTK_READS(h);
	DECLARE_CCTK_READS(uxy);
	DECLARE_CCTK_READS(duxy);
	DECLARE_CCTK_READS(etay);
	DECLARE_CCTK_READS(ux);
	DECLARE_CCTK_READS(dux);
	DECLARE_CCTK_SCALAR(b1);
	DECLARE_CCTK_SCALAR(b2);
	DECLARE_CCTK_READS(vbar);
	DECLARE_CCTK_READS(ubar);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dy);
	cctk_auto_thorn = "TRIDIAGONAL";
	DECLARE_CCTK_WRITES(axx);
	DECLARE_CCTK_WRITES(bxx);
	DECLARE_CCTK_WRITES(cxx);
	DECLARE_CCTK_WRITES(dxx);
	DECLARE_CCTK_PARAMETERS;

  int ilo = nbound;
  int jlo = nbound;
  int ihi = cctk_lsh[0]-nbound;
  int jhi = cctk_lsh[1]-nbound;

	if(dispersion_on) {
		Tridiagonal_CopyOut(CCTK_PASS_CTOC,u);
		funwave_phi_coll(cctkGH,u,2);
	}
	if(dispersion_on) {
		// y direction;
        if(!spherical_coordinates) {

		for (int j = jlo; j < jhi; j++) {
			for (int i = ilo; i < ihi; i++) {
				int cc = FUNWAVE_INDEX(i,j);
				int jm1 = FUNWAVE_INDEX(i,j-1);
				int jp1 = FUNWAVE_INDEX(i,j+1);
				dep=max((CCTK_REAL)depth[cc],mindepthfrc);
				depl=max((CCTK_REAL)depth[jm1],mindepthfrc);
				depr=max((CCTK_REAL)depth[jp1],mindepthfrc);

				if (disp_time_left) {
					reta=eta[cc];
					retal=eta[jm1];
					retar=eta[jp1];
					tmp1=gamma1*mask9[cc]*((*b1)/2.0/dy[cc]/dy[cc]*dep*dep + (*b2)/dy[cc]/dy[cc]*depl*dep)
			            		 -gamma2*mask9[cc]*((reta+retal)*depl/2.0/dy[cc]/dy[cc]+(retal+reta)*(retal+reta)/8.0/dy[cc]/dy[cc]);
					tmp2=1.0+gamma1*mask9[cc]*(-(*b1)/dy[cc]/dy[cc]*dep*dep-2.0*(*b2)/dy[cc]/dy[cc]*dep*dep)
			            		 +gamma2*mask9[cc]*((retar+retal+2.0*reta)/2.0/dy[cc]/dy[cc]
			            				 +(retar*retar+2.0*reta*reta
			            						 +2.0*reta*retar+2.0*retal*reta+retal*retal)/8.0/dy[cc]/dy[cc]);
					tmp3=gamma1*mask9[cc]*((*b1)/2.0/dy[cc]/dy[cc]*dep*dep + (*b2)/dy[cc]/dy[cc]*dep*depr)
			            		 -gamma2*mask9[cc]*((reta+retar)*depr/2.0/dy[cc]/dy[cc]+(retar+reta)*(retar+reta)/8.0/dy[cc]/dy[cc]);
					tmp4=vbar[cc]*mask[cc]/max((CCTK_REAL)h[cc],mindepthfrc)
			            		 + gamma1*mask9[cc]*(-(*b1)/2.0*dep*dep*uxy[cc]-(*b2)*dep*duxy[cc])
			            		 + gamma2*mask9[cc]*(reta*reta/2.0*uxy[cc]+reta*duxy[cc]
			            		                                                     + reta*etay[cc]*ux[cc] + etay[cc]*dux[cc] );
				} else {
					tmp1=gamma1*mask9[cc]*((*b1)/2.0/dy[cc]/dy[cc]*dep*dep + (*b2)/dy[cc]/dy[cc]*depl*dep);
					tmp2=1.0+gamma1*mask9[cc]*(-(*b1)/dy[cc]/dy[cc]*dep*dep-2.0*(*b2)/dy[cc]/dy[cc]*dep*dep);
					tmp3=gamma1*mask9[cc]*((*b1)/2.0/dy[cc]/dy[cc]*dep*dep + (*b2)/dy[cc]/dy[cc]*dep*depr);
					tmp4=vbar[cc]*mask[cc]/max(h[cc],mindepthfrc)
			            		 + gamma1*mask9[cc]*(-(*b1)/2.0*dep*dep*uxy[cc]-(*b2)*dep*duxy[cc]);
				}

				if (tmp2  !=  0.0 || mask[cc] > 0.5) {
					axx[cc]=tmp1/tmp2;
					bxx[cc]=1.0;
					cxx[cc]=tmp3/tmp2;
					dxx[cc]=tmp4/tmp2;
				} else {
					axx[cc]=zero;
					bxx[cc]=1.0;
					cxx[cc]=zero;
					dxx[cc]=zero;
				}

			}
		}
		} else { // spherical
			for (int j = jlo; j < jhi; j++) {
        //#pragma omp simd
				for (int i = ilo; i < ihi; i++) {
					int cc = FUNWAVE_INDEX(i,j);
					tmp1=gamma1*mask9[cc]*((*b1)/2.0/dy[cc]/dy[cc]*dep*dep + (*b2)/dy[cc]/dy[cc]*depl*dep);
				    tmp2=1.0+gamma1*mask9[cc]*(-(*b1)/dy[cc]/dy[cc]*dep*dep-2.0*(*b2)/dy[cc]/dy[cc]*dep*dep);
				    tmp3=gamma1*mask9[cc]*((*b1)/2.0/dy[cc]/dy[cc]*dep*dep + (*b2)/dy[cc]/dy[cc]*dep*depr);
				    tmp4=vbar[cc]*mask[cc]/max(h[cc],mindepthfrc)
				        + gamma1*mask9[cc]*(-(*b1)/2.0*dep*dep*uxy[cc]-(*b2)*dep*duxy[cc]);


					if (tmp2  !=  0.0 || mask[cc] > 0.5) {
						axx[cc]=tmp1/tmp2;
						bxx[cc]=1.0;
						cxx[cc]=tmp3/tmp2;
						dxx[cc]=tmp4/tmp2;
					} else {
						axx[cc]=zero;
						bxx[cc]=1.0;
						cxx[cc]=zero;
						dxx[cc]=zero;
					}
				}
			}
		}
		//Tridiagonal_y(CCTK_PASS_CTOC,v,nbound,cctk_lsh[0]-nbound-2,nbound,cctk_lsh[1]-nbound-2,zvalue,zvalue);
	} else {
	  for (int j = 0; j < cctk_lsh[1]; j++) {
		  for (int i = 0; i < cctk_lsh[0]; i++) {
				int cc = FUNWAVE_INDEX( i, j);
				fc = max(h[cc], mindepthfrc);

				u[cc] = ubar[cc] / fc;
				v[cc] = vbar[cc] / fc;
			}
		}
	}
  #ifdef TRACE_ON
  std::cout << "cctk_exec: tridiagonal_setup_y" << std::endl;
  std::cout << "cctk_exec: tridiagonal_y_parallel_impl" << std::endl;
  #endif

	if(dispersion_on) {
		Tridiagonal_CopyIn(CCTK_PASS_CTOC,(CCTK_REAL *)v);
  }
}

void funwave_poststep_3(CCTK_ARGUMENTS) {
	_DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	cctk_func fdecl("funwave_poststep_3");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READWRITES(v);
	DECLARE_CCTK_READWRITES(u);
	DECLARE_CCTK_WRITES(hv);
	DECLARE_CCTK_WRITES(hu);
	DECLARE_CCTK_READWRITES(ubar);
	DECLARE_CCTK_READWRITES(vbar);
	DECLARE_CCTK_READS(h);
	DECLARE_CCTK_READS(mask);

	if(dispersion_on) {
		Tridiagonal_CopyOut(CCTK_PASS_CTOC,v);
		funwave_phi_coll(cctkGH,v,3);
  }

	for (int j=0;j<cctk_lsh[1];j++) {
    //#pragma omp simd
		for (int i=0;i<cctk_lsh[0];i++) {
			int cc = FUNWAVE_INDEX( i, j);
			fc = max(h[cc], mindepthfrc);
			if (mask[cc] < 0.5) {
				ubar[cc] = 0.0;
				vbar[cc] = 0.0;
				u[cc] = 0.0;
				v[cc] = 0.0;
				hu[cc] = 0.0;
				hv[cc] = 0.0;
			} else {
        MARK(ubar[cc]);
        MARK(vbar[cc]);
				hu[cc] = fc * u[cc];
				hv[cc] = fc * v[cc];
				CCTK_REAL utotal = sqrt((CCTK_REAL)(u[cc] * u[cc] + v[cc] * v[cc]));
				CCTK_REAL fr = sqrt(grav * fc);

				if (utotal / fr > froudecap) {
					CCTK_REAL utheta = atan2((CCTK_REAL)v[cc], (CCTK_REAL)u[cc]);

					u[cc] = froudecap * fr * cos(utheta);
					v[cc] = froudecap * fr * sin(utheta);
					hu[cc] = fc * u[cc];
					hv[cc] = fc * v[cc];
				} else {
          MARK(u[cc]);
          MARK(v[cc]);
        }
			}
		}
	}
}

extern "C" CCTK_REAL FromLat(CCTK_REAL);
extern "C" CCTK_REAL FromLong(CCTK_REAL);

void funwave_estimate_huv(CCTK_ARGUMENTS) // sync
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("funwave_estimate_huv");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_WRITEREADS(eta_rhs);
	DECLAREi_CCTK_SCALAR(state_wavemaker_type);
	DECLARE_CCTK_READS(p);
	DECLARE_CCTK_READS(q);

	DECLARE_CCTK_SCALAR(fun_time);
	DECLARE_CCTK_SCALAR(width_wk);
	DECLARE_CCTK_SCALAR(beta_gen);
	DECLARE_CCTK_SCALAR(d_gen);
	DECLARE_CCTK_SCALAR(rlamda);
	DECLARE_CCTK_READS(hv);
	DECLARE_CCTK_READS(fx);
	DECLARE_CCTK_READS(fy);
	DECLARE_CCTK_READS(sourcex);
	DECLARE_CCTK_READS(sourcey);
	DECLARE_CCTK_READS(gx);
	DECLARE_CCTK_READS(gy);
	DECLARE_CCTK_WRITES(ubar_rhs);
	DECLARE_CCTK_WRITES(vbar_rhs);
	DECLARE_CCTK_VREADS(cm);
	DECLARE_CCTK_VREADS(sm);
	DECLARE_CCTK_READS(beta_gens);
	DECLARE_CCTK_READS(d_gens);
	DECLARE_CCTK_READS(omgn_ir);
  DECLARE_CCTK_READS(wave_comp1);
  DECLARE_CCTK_READS(wave_comp3);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);
	DECLARE_CCTK_READS(dy);
	DECLARE_CCTK_READS(lat_theta);
	cctk_auto_thorn = "GRID";
	DECLARE_CCTK_READS(x);
	DECLARE_CCTK_READS(y);

	DECLARE_CCTK_PARAMETERS;
	DECLARE_FUNWAVE_BOUNDS;
	CCTK_REAL x0 = FromLat(drain_center_x);
	CCTK_REAL y0 = FromLong(drain_center_y);
	const CCTK_REAL n_bottom = -1.0, n_left = -1.0, n_right = 1.0, n_top = 1.0;

	for (int i = ilo; i < ihi; i++) {
    //This pragma causes segfaults
    //#pragma omp simd
		for (int j = jlo; j < jhi; j++) {
			int cc = FUNWAVE_INDEX( i, j);
			int ip1 = FUNWAVE_INDEX( i + 1, j);
			int jp1 = FUNWAVE_INDEX( i, j + 1);
			CCTK_REAL f_left = p[cc];
			CCTK_REAL f_right = p[ip1];
			CCTK_REAL f_bottom = q[cc];
			CCTK_REAL f_top = q[jp1];
			CCTK_REAL tm = *fun_time;
      CCTK_REAL yc_wk = int((cctk_gsh[1]-cctk_nghostzones[1]*2)/2)*dy[cc];
			int rhs=0;
      if (*state_wavemaker_type == WK_IRR) {
        CCTK_REAL wk_source = 0;
        bool trigger = false;
        if (fabs(x[cc] - xc_wk) < *width_wk
            && fabs(y[cc] - yc_wk) < ywidth_wk / 2.0) {
          trigger = true;
          for(int kf=0;kf < num_frequencies;kf++) {
            int kc = CCTK_VECTGFINDEX3D(cctkGH,i,j,zvalue,kf);
            CCTK_REAL cmkc = cm[kc];
            CCTK_REAL smkc = sm[kc];
            CCTK_REAL omgn_irkf = omgn_ir[kf];
            wk_source += tanh(M_PI/(time_ramp/freqpeak)*tm)
              *(cmkc*cos(omgn_irkf*tm)
              +smkc*sin(omgn_irkf*tm));

          }
        }
				eta_rhs[cc] =
          -1.0 / dx[cc] * (f_right * n_right +   f_left * n_left)
					-1.0 / dy[cc] * (f_top   * n_top   + f_bottom * n_bottom)
          + wk_source;
      } else if (*state_wavemaker_type == WK_REG) {
        if (fabs(x[cc] - xc_wk) < *width_wk
            && fabs(y[cc] - yc_wk) < ywidth_wk / 2.0) {
          eta_rhs[cc] = -1.0 / dx[cc]
            * (f_right * n_right + f_left * n_left)
            - 1.0 / dy[cc] * (f_top * n_top + f_bottom * n_bottom)
            // wavemaker;
            + tanh(M_PI / (time_ramp * tperiod) * (tm)) * (*d_gen)
              * exp( -(*beta_gen) * sq(x[cc] - xc_wk))
              * sin( (*rlamda) * (y[cc] - 0.0) - 2.0 * M_PI / tperiod * tm);
          rhs = 3;
        } else {
          eta_rhs[cc] = -1.0 / dx[cc]
            * (f_right * n_right + f_left * n_left)
            - 1.0 / dy[cc]
            * (f_top * n_top + f_bottom * n_bottom);
          rhs = 4;
        }
      } else if (*state_wavemaker_type == WK_TIME_SERIES) {
        // Untested. Initialization for omgn_ir, cm, and sm commented
        // out in Dr. Shi's code.
        CCTK_REAL wk_source = 0;
        CCTK_REAL xmk = x[cc]-0*dx[cc];
        CCTK_REAL ymk = y[cc]-0*dy[cc];
        if (fabs(xmk - xc_wk) < *width_wk
            && fabs(ymk - yc_wk) < ywidth_wk / 2.0) {
          for(int kf=0;kf < num_wave_components;kf++) {
            wk_source += tanh(M_PI/(time_ramp*peak_period)*tm)*d_gens[kf]
              *exp(-beta_gens[kf]*(xmk - xc_wk)*(xmk - xc_wk))
              *cos(2.0*M_PI/wave_comp1[kf]*tm-wave_comp3[kf]);
          }
        }
				eta_rhs[cc] =
          -1.0 / dx[cc] * (f_right * n_right +   f_left * n_left)
					-1.0 / dy[cc] * (f_top   * n_top   + f_bottom * n_bottom)
          + wk_source;
        // END WK_TIME_SERIES block
      } else {
				eta_rhs[cc] =
          -1.0 / dx[cc] * (f_right * n_right +   f_left * n_left)
					-1.0 / dy[cc] * (f_top   * n_top   + f_bottom * n_bottom);
				rhs =7;
			}
			if(spherical_coordinates) {
				const double rearth = 0.5*(earth_a+earth_b);
				eta_rhs[cc] += 1.0/rearth*tan(lat_theta[cc])*hv[cc];
			}
			// Drain
			double xc = (x[cc]-x0), yc = (y[cc]-y0);
			if(xc*xc + yc*yc < drain_radius*drain_radius) {
				eta_rhs[cc] += drain_rate;
			}

			// solve ubar;
			f_left = fx[cc];
			f_right = fx[ip1];
			f_bottom = fy[cc];
			f_top = fy[jp1];
			ubar_rhs[cc] = -1.0 / dx[cc] * (f_right * n_right + f_left * n_left)
					- 1.0 / dy[cc] * (f_top * n_top + f_bottom * n_bottom)
					+ sourcex[cc];
			// solve vbar;
			f_left = gx[cc];
			f_right = gx[ip1];
			f_bottom = gy[cc];
			f_top = gy[jp1];
			vbar_rhs[cc] = -1.0 / dx[cc] * (f_right * n_right + f_left * n_left)
					- 1.0 / dy[cc] * (f_top * n_top + f_bottom * n_bottom)
					+ sourcey[cc];
		}
	}
}
