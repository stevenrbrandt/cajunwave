#include <cmath>
#include <cstdio>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_declare.h"
#include "funwave_util.hh"
#include <assert.h>
#include "here.hh"
#include <ios>

void funwave_assert_dt(CCTK_ARGUMENTS) {
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("funwave_assert_dt");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_SCALAR(fun_dt);
	cctkGH->cctk_delta_time = *fun_dt;
}

void funwave_estimate_dt (CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_estimate_dt");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_SCALAR(fun_dt);
  DECLARE_CCTK_SCALAR(fun_time);
  DECLARE_CCTK_READS(u);
  DECLARE_CCTK_READS(v);
  DECLARE_CCTK_READS(h);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  DECLARE_CCTK_READS(dy);
  DECLARE_CCTK_PARAMETERS;
  DECLARE_FUNWAVE_BOUNDS;
  CCTK_REAL tmp1, tmp2, tmp3, myvar;

  CCTK_INT operation_handle;

  /*
  if(dt_size > 0) {
	  //HERE VAR(cctkGH->cctk_delta_time) VAR(dt_size) END;
	  //assert(cctkGH->cctk_delta_time == dt_size);
	  cctkGH->cctk_delta_time = dt_size*2;
	  *fun_time = cctk_time + dt_size;
	  *fun_dt = dt_size;
	  return;
  }
  */

  tmp3 = large;
  for (int j = jlo; j < jhi; j++)
  {
	  for (int i = ilo; i < ihi; i++)
	  {
      int cc = FUNWAVE_INDEX(i, j);

      tmp1 = fabs (u[cc]) + sqrt (grav * max (h[cc], mindepthfrc));
      if (tmp1 < small)
      {
        tmp2 = dx[cc] / small;
      }
      else
      {
        tmp2 = dx[cc] / tmp1;
      }
      if (tmp2 < tmp3)
        tmp3 = tmp2;
      // y direction;
      tmp1 = fabs (v[cc]) + sqrt (grav * max (h[cc], mindepthfrc));
      if (tmp1 < small)
      {
        tmp2 = dy[cc] / small;
      }
      else
      {
        tmp2 = dy[cc] / tmp1;
      }
      if (tmp2 < tmp3)
        tmp3 = tmp2;
    }
  }

  operation_handle = CCTK_ReductionArrayHandle ("minimum");

  /* -1 here will let Cactus distribute the data to all processors */
  CCTK_ReduceLocScalar (cctkGH, -1, operation_handle, &tmp3, &myvar,
                        CCTK_VARIABLE_REAL);

  tmp3 = myvar;
  if(show_dt)
    std::cout << "Estimate dt got " << (dtfac*tmp3) << std::endl;

  if(dt_size > 0) {
    //double tfac = (dtfac * tmp3)/cctk_delta_time;
    //HERE VAR(tfac) END;
	  assert(cctkGH->cctk_delta_time == dt_size);
	  *fun_time = cctk_time + dt_size;
	  *fun_dt = dt_size;
  } else {
    cctkGH->cctk_delta_time = dtfac * tmp3;
    *fun_time = cctk_time + dtfac * tmp3-1;
    *fun_dt = dtfac * tmp3;
  }
}
