#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "here.hh"
#include "funwave_util.hh"
#include "cctk_declare.h"

// reads phi inside domain, writes phi within boundary
// TODO: replace nbound with cctk_nghostzones
void funwave_phi_coll(CCTK_ARGUMENTS, CCTK_REAL * phi, int vtype)
{
  _DECLARE_CCTK_ARGUMENTS;
  const int sgni = vtype == 2 ? -1 : 1;
  const int sgnj = vtype == 3 ? -1 : 1;

  for (int j = nbound; j < cctk_lsh[1]-nbound; j++)
  {
	  if(cctk_bbox[0]>0)
		  for (int i = 0; i < nbound; i++)
		  {
			  int c1 = FUNWAVE_INDEX( i, j);
			  int c2 = FUNWAVE_INDEX( 2*nbound - i - 1, j);

			  phi[c1] = sgni * phi[c2];
		  }
	  if(cctk_bbox[1]>0)
		  for (int i = 0; i < nbound; i++)
		  {
			  int c1 = FUNWAVE_INDEX( cctk_lsh[0] - nbound + i    , j);
			  int c2 = FUNWAVE_INDEX( cctk_lsh[0] - nbound - i - 1, j);

			  phi[c1] = sgni * phi[c2];
		  }
  }
  // This iteration is intentionally different from the one
  // above. The first iteration fills in the X boundary zones,
  // this one fills in the Y. This second iteration fills in
  // the corners.
  for (int i = 0; i < cctk_lsh[0]; i++)
  {
	  if(cctk_bbox[2] > 0)
		  for (int j = 0; j < nbound; j++)
		  {
			  int c1 = FUNWAVE_INDEX( i, j);
			  int c2 = FUNWAVE_INDEX( i, 2*nbound - j - 1);

			  phi[c1] = sgnj * phi[c2];
		  }
	  if(cctk_bbox[3] > 0)
		  for (int j = 0; j < nbound; j++)
		  {
			  int c1 = FUNWAVE_INDEX( i, cctk_lsh[1] - nbound + j    );
			  int c2 = FUNWAVE_INDEX( i, cctk_lsh[1] - nbound - j - 1);

			  phi[c1] = sgnj * phi[c2];
		  }
  }
}

void funwave_exchange_dispersion(CCTK_ARGUMENTS)
{
	  _DECLARE_CCTK_ARGUMENTS;
	  cctk_func fdecl("funwave_exchange_dispersion");
	  cctk_auto_thorn = "FUNWAVE";
	  DECLARE_CCTK_BWRITES(uxx);
	  DECLARE_CCTK_BWRITES(duxx);
	  DECLARE_CCTK_BWRITES(vyy);
	  DECLARE_CCTK_BWRITES(dvyy);
	  DECLARE_CCTK_BWRITES(uxy);
	  DECLARE_CCTK_BWRITES(duxy);
	  DECLARE_CCTK_BWRITES(vxy);
	  DECLARE_CCTK_BWRITES(dvxy);
	  DECLARE_CCTK_BWRITES(ux);
	  DECLARE_CCTK_BWRITES(dux);
	  DECLARE_CCTK_BWRITES(vy);
	  DECLARE_CCTK_BWRITES(dvy);
	  DECLARE_CCTK_BWRITES(uy);
	  DECLARE_CCTK_BWRITES(duy);
	  DECLARE_CCTK_BWRITES(vx);
	  DECLARE_CCTK_BWRITES(dvx);
	  DECLARE_CCTK_BWRITES(etax);
	  DECLARE_CCTK_BWRITES(etay);
	  DECLARE_CCTK_BWRITES(etat);
	  DECLARE_CCTK_BWRITES(etatx);
	  DECLARE_CCTK_BWRITES(etaty);
	  DECLARE_CCTK_BWRITES(ut);
	  DECLARE_CCTK_BWRITES(vt);
	  DECLARE_CCTK_BWRITES(utx);
	  DECLARE_CCTK_BWRITES(vty);
	  DECLARE_CCTK_BWRITES(utxx);
	  DECLARE_CCTK_BWRITES(vtyy);
	  DECLARE_CCTK_BWRITES(utxy);
	  DECLARE_CCTK_BWRITES(vtxy);
	  DECLARE_CCTK_BWRITES(dvtxy);
	  DECLARE_CCTK_BWRITES(dutxx);
	  DECLARE_CCTK_BWRITES(dvtyy);
	  DECLARE_CCTK_BWRITES(dutxy);

	  DECLARE_CCTK_PARAMETERS;
	  int vtype;
	  CCTK_REAL zero = 0.0;

	  if(spherical_coordinates) {
		  vtype=2;
		  funwave_phi_coll(CCTK_PASS_CTOC,uxx,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,duxx,vtype);
		  vtype=3;
		  funwave_phi_coll(CCTK_PASS_CTOC,vyy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dvyy,vtype);

		  vtype=1;
		  funwave_phi_coll(CCTK_PASS_CTOC,uxy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,duxy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,vxy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dvxy,vtype);


		  vtype=1;  // symetric in both direction;
		  funwave_phi_coll(CCTK_PASS_CTOC,ux,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dux,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,vy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dvy,vtype);
		  vtype=3;  //like v;
		  funwave_phi_coll(CCTK_PASS_CTOC,uy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,duy,vtype);
		  vtype=2;  //like u;
		  funwave_phi_coll(CCTK_PASS_CTOC,vx,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dvx,vtype);
		  vtype=2;  // like u;
		  funwave_phi_coll(CCTK_PASS_CTOC,etax,vtype);
		  vtype=3;  // like v;
		  funwave_phi_coll(CCTK_PASS_CTOC,etay,vtype);
	  } else {
		  vtype=2;
		  funwave_phi_coll(CCTK_PASS_CTOC,uxx,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,duxx,vtype);
		  vtype=3;
		  funwave_phi_coll(CCTK_PASS_CTOC,vyy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dvyy,vtype);

		  vtype=1;
		  funwave_phi_coll(CCTK_PASS_CTOC,uxy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,duxy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,vxy,vtype);
		  funwave_phi_coll(CCTK_PASS_CTOC,dvxy,vtype);

		  if (gamma2>zero) {

			  if (disp_time_left) {
				  vtype=1; // symetric in both direction;
				  funwave_phi_coll(CCTK_PASS_CTOC,etat,vtype);
				  vtype=2; // like u;
				  funwave_phi_coll(CCTK_PASS_CTOC,etatx,vtype);
				  vtype=3; // like v;
				  funwave_phi_coll(CCTK_PASS_CTOC,etaty,vtype);
			  } else {
				  vtype=2;
				  funwave_phi_coll(CCTK_PASS_CTOC,ut,vtype);
				  vtype=3;
				  funwave_phi_coll(CCTK_PASS_CTOC,vt,vtype);

				  vtype=1;
				  funwave_phi_coll(CCTK_PASS_CTOC,utx,vtype);
				  vtype=1;
				  funwave_phi_coll(CCTK_PASS_CTOC,vty,vtype);

				  vtype=2;
				  funwave_phi_coll(CCTK_PASS_CTOC,utxx,vtype);
				  vtype=3;
				  funwave_phi_coll(CCTK_PASS_CTOC,vtyy,vtype);

				  vtype=1;
				  funwave_phi_coll(CCTK_PASS_CTOC,utxy,vtype);
				  funwave_phi_coll(CCTK_PASS_CTOC,vtxy,vtype);

				  vtype=2;
				  funwave_phi_coll(CCTK_PASS_CTOC,dutxx,vtype);
				  vtype=3;
				  funwave_phi_coll(CCTK_PASS_CTOC,dvtyy,vtype);

				  vtype=1;
				  funwave_phi_coll(CCTK_PASS_CTOC,dutxy,vtype);
				  funwave_phi_coll(CCTK_PASS_CTOC,dvtxy,vtype);

			  }

			  vtype=1;  // symetric in both direction;
			  funwave_phi_coll(CCTK_PASS_CTOC,ux,vtype);
			  funwave_phi_coll(CCTK_PASS_CTOC,dux,vtype);
			  funwave_phi_coll(CCTK_PASS_CTOC,vy,vtype);
			  funwave_phi_coll(CCTK_PASS_CTOC,dvy,vtype);
			  vtype=3;  //like v;
			  funwave_phi_coll(CCTK_PASS_CTOC,uy,vtype);
			  funwave_phi_coll(CCTK_PASS_CTOC,duy,vtype);
			  vtype=2;  //like u;
			  funwave_phi_coll(CCTK_PASS_CTOC,vx,vtype);
			  funwave_phi_coll(CCTK_PASS_CTOC,dvx,vtype);
			  vtype=2;  // like u;
			  funwave_phi_coll(CCTK_PASS_CTOC,etax,vtype);
			  vtype=3;  // like v;
			  funwave_phi_coll(CCTK_PASS_CTOC,etay,vtype);
		  }
	  }
}

void funwave_exchange_1(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_exchange_1");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(eta);
  DECLARE_CCTK_READWRITES(mask);
  DECLARE_CCTK_READWRITES(u);
  DECLARE_CCTK_READWRITES(ubar);
  DECLARE_CCTK_READWRITES(hu);
  DECLARE_CCTK_READWRITES(v);
  DECLARE_CCTK_READWRITES(vbar);
  DECLARE_CCTK_READWRITES(hv);
  int vtype;
  vtype = 1;
  funwave_phi_coll(CCTK_PASS_CTOC, eta, vtype);
  funwave_phi_coll(CCTK_PASS_CTOC, mask, vtype);

  vtype = 2;
  funwave_phi_coll(CCTK_PASS_CTOC, u, vtype);
  funwave_phi_coll(CCTK_PASS_CTOC, ubar, vtype);
  funwave_phi_coll(CCTK_PASS_CTOC, hu, vtype);

  vtype = 3;
  funwave_phi_coll(CCTK_PASS_CTOC, v, vtype);
  funwave_phi_coll(CCTK_PASS_CTOC, vbar, vtype);
  funwave_phi_coll(CCTK_PASS_CTOC, hv, vtype);
}

void funwave_exchange_2(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_exchange_2");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(mask);
  DECLARE_CCTK_READWRITES(u);
  DECLARE_CCTK_READWRITES(hu);
  DECLARE_CCTK_READWRITES(v);
  DECLARE_CCTK_READWRITES(hv);
  DECLARE_CCTK_PARAMETERS;
  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 0; i < cctk_lsh[0]; i++) 
    {
      int cc = FUNWAVE_INDEX( i, j);

      u[cc] = mask[cc] * u[cc];
      v[cc] = mask[cc] * v[cc];
      hu[cc] = mask[cc] * hu[cc];
      hv[cc] = mask[cc] * hv[cc];
    }
  }
}


void funwave_boundary(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_func fdecl("funwave_boundary");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(etaryr);
  DECLARE_CCTK_READS(etaryl);
  DECLARE_CCTK_READS(depthy);
  DECLARE_CCTK_READS(etarxr);
  DECLARE_CCTK_READS(etarxl);
  DECLARE_CCTK_READS(depthx);
  DECLARE_CCTK_READS(mask);
  DECLARE_CCTK_BWRITES(p);
  DECLARE_CCTK_BWRITES(q);
  DECLARE_CCTK_BWRITES(fy);
  DECLARE_CCTK_BWRITES(gy);
  DECLARE_CCTK_BWRITES(fx);
  DECLARE_CCTK_BWRITES(gx);

  if (CCTK_EQUALS(boundary, "funwave"))
  {
    /* -y direction */
    if (cctk_bbox[2] == 1)
    {
      for (int i = nbound; i < cctk_lsh[0]-nbound; i++)
      {
        int c0 = FUNWAVE_INDEX( i, nbound);

        q[c0] = 0.0;
        fy[c0] = 0.0;
        CCTK_REAL xi = etaryr[c0];
        CCTK_REAL deps = depthy[c0];

        gy[c0] = 0.5 * grav * (xi * xi * gamma3 + 2.0 * xi * deps);
      }
    }
    /* +y direction */
    if (cctk_bbox[3] == 1)
    {
      for (int i = nbound; i < cctk_lsh[0]-nbound; i++)
      {
        int c0 = FUNWAVE_INDEX( i, cctk_lsh[1] - nbound);

        q[c0] = 0.0;
        fy[c0] = 0.0;
        CCTK_REAL xi = etaryl[c0];
        CCTK_REAL deps = depthy[c0];

        gy[c0] = 0.5 * grav * (xi * xi * gamma3 + 2.0 * xi * deps);
      }
    }

    /* -x direction */
    if (cctk_bbox[0] == 1)
    {
      for (int j = nbound; j < cctk_lsh[1]-nbound; j++)
      {
        int c0 = FUNWAVE_INDEX( nbound, j);

        p[c0] = 0.0;
        CCTK_REAL xi = etarxr[c0];
        CCTK_REAL deps = depthx[c0];

        fx[c0] = 0.5 * grav * (xi * xi * gamma3 + 2.0 * xi * deps);
        gx[c0] = 0.0;
      }
    }

    /* +x direction */
    if (cctk_bbox[1] == 1)
    {
      for (int j = nbound; j < cctk_lsh[1]-nbound; j++)
      {
        int c0 = FUNWAVE_INDEX( cctk_lsh[0] - nbound, j);

        p[c0] = 0;
        CCTK_REAL xi = etarxl[c0];
        CCTK_REAL deps = depthx[c0];

        fx[c0] = 0.5 * grav * (xi * xi * gamma3 + 2.0 * xi * deps);
        gx[c0] = 0.0;
      }
    }
  }
  else
  {
    CCTK_WARN(0, "specified boundary condition is not supported !");
  }
  // TODO: Weird bounds
  for(int j=nbound-1;j<cctk_lsh[1]-nbound+1;j++) {
	  for(int i=nbound-1;i<cctk_lsh[0]-nbound+1;i++) {
		  int cc = FUNWAVE_INDEX(i,j);
		  int ip1 = FUNWAVE_INDEX(i+1,j);
		  int im1 = FUNWAVE_INDEX(i-1,j);
		  int jp1 = FUNWAVE_INDEX(i,j+1);
		  int jm1 = FUNWAVE_INDEX(i,j-1);
		  int ibeg = nbound;
		  int iend = cctk_lsh[0]-nbound-1;
		  int jbeg = nbound;
		  int jend = cctk_lsh[1]-nbound-1;
		  CCTK_REAL xi, deps, zero = 0;
		  if (mask[cc] < 0.5) {
			  p[cc]=zero;
			  // jeff reported a bug here for parallel version;
			  if (i>ibeg) {
				  //         fx[cc]=0.5*grav*hxl[cc]*hxl[cc]*mask[im1];
				  //new splitting method;
				  xi=etarxl[cc];
				  deps=depthx[cc];
				  fx[cc]=0.5*grav*(xi*xi*gamma3+2.0*xi*deps)*mask[im1];
			  } else {
				  fx[cc]=zero;
			  }
			  gx[cc]=zero;

			  p[ip1]=zero;
			  // jeff also here;
			  if (i<iend) {
				  //         fx[ip1]=0.5*grav*hxr[ip1]*hxr[ip1]*mask[ip1];
				  // new splitting method;
				  xi=etarxr[ip1];
				  deps=depthx[ip1];
				  fx[ip1]=0.5*grav*(xi*xi*gamma3+2.0*xi*deps)*mask[ip1];
			  } else {
				  fx[ip1]=zero;
			  }
			  gx[ip1]=zero;

			  q[cc]=zero;
			  fy[cc]=zero;
			  // jeff also here;
			  if (j>jbeg) {
				  //         gy[cc]=0.5*grav*hyl[cc]*hyl[cc]*mask[jm1];
				  // new splitting method;
				  xi=etaryl[cc];
				  deps=depthy[cc];
				  gy[cc]=0.5*grav*(xi*xi*gamma3+2.0*xi*deps)*mask[jm1];
			  } else {
				  gy[cc]=zero;
			  }

			  q[jp1]=zero;
			  fy[jp1]=zero;
			  // jeff also here;
			  if (j<jend) {
				  //         gy[jp1]=0.5*grav*hyr[jp1]*hyr[jp1]*mask[jp1];
				  // new splitting method;
				  xi=etaryr[jp1];
				  deps=depthy[jp1];
				  gy[jp1]=0.5*grav*(xi*xi*gamma3+2.0*xi*deps)*mask[jp1];
			  } else {
				  gy[jp1]=zero;
			  }
		  }
	  }
  }
}
