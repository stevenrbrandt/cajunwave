#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "here.hh"
#include "cctk_declare.h"


void funwave_wind_effect(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("funwave_wind_effect");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_SCALAR(fun_time);
	DECLAREi_CCTK_SCALAR(wind_counter);
	DECLARE_CCTK_WRITEREADS(windu2d);
	DECLARE_CCTK_WRITEREADS(windv2d);
	DECLARE_CCTK_WRITES(mask_wind);
	DECLARE_CCTK_READS(etax);
	DECLARE_CCTK_READS(etay);
	DECLARE_CCTK_READS(h);
	DECLARE_CCTK_READS(etat);
	DECLARE_CCTK_READS(eta);
	DECLARE_CCTK_READS(height_max);
	DECLARE_CCTK_PARAMETERS;
	DECLARE_FUNWAVE_BOUNDS;
	CCTK_REAL tmp1,tmp2,tmp3,celerity,wave_angle;

	if(*fun_time > timewind[*wind_counter] && *wind_counter < num_time_wind_data) {
		(*wind_counter)++;
	}
	if(*wind_counter>=1) { // wind start
		if(*fun_time>timewind[*wind_counter]) {
			tmp2 = 0;
		} else {
			tmp2 = (timewind[*wind_counter]-*fun_time)/
					(timewind[*wind_counter]-timewind[-1+*wind_counter]);
		}
		for (int j = 0; j < cctk_lsh[1]; j++)
		{
			for (int i = 0; i < cctk_lsh[0]; i++)
			{
				int cc = FUNWAVE_INDEX( i, j);
				windu2d[cc] = wu[*wind_counter]*(1.0-tmp2)+wu[*wind_counter-1]*tmp2;
				windv2d[cc] = wv[*wind_counter]*(1.0-tmp2)+wv[*wind_counter-1]*tmp2;
			}
		}
	}

	for (int j = 0; j < cctk_lsh[1]; j++)
	{
		for (int i = 0; i < cctk_lsh[0]; i++)
		{
			int cc = FUNWAVE_INDEX( i, j);
			tmp3 = sqrt(grav*max(mindepthfrc,h[cc]));
			tmp1 = max(sqrt(etax[cc]*etax[cc]+etay[cc]*etay[cc]),small);
			celerity = min(fabs(etat[cc])/tmp1,sqrt(grav*fabs(h[cc])));
			wave_angle = atan2(etay[cc],etax[cc]);
			windu2d[cc]=windu2d[cc]-celerity*cos(wave_angle);
			windv2d[cc]=windv2d[cc]-celerity*sin(wave_angle);
		}
	}
	  if(use_wind_mask) {
		  for (int j = jlo; j < jhi; j++)
		  {
			  for (int i = ilo; i < ihi; i++)
			  {
				  int cc = FUNWAVE_INDEX( i, j);
				  // height_max should be updated by the "stations" subroutine
				  if(eta[cc]<small+height_max[cc]*(1-0.01*wind_crest_percent)) {
					  mask_wind[cc] = 0.0;
				  } else {
					  mask_wind[cc] = 1.0;
				  }
			  }
		  }
	  }
}
