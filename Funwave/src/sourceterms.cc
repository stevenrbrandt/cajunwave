#include <cstdio>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "here.hh"
#include "cctk_declare.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <iostream>
#include <fstream>
#include <math.h>

const int alpha = 50;
const int beta = 10;

double weight_calc(double Xi, double Yi, double Xs, double Ys, double DX, double DY);

double distance_x(double XI, double XS, double DX);

double distance_y(double YI, double YS, double DY);

#define sq(X) (X)*(X)
#define DIV1X(RES,VAR) double RES = (VAR[ip1] - VAR[im1])/(2*delx);
#define DIV2X(RES,VAR) double RES = (VAR[ip1] + VAR[im1] - 2*VAR[cc])/(delx*delx);
#define DIV1Y(RES,VAR) double RES = (VAR[jp1] - VAR[jm1])/(2*dely);
#define DIV2Y(RES,VAR) double RES = (VAR[jp1] + VAR[jm1] - 2*VAR[cc])/(dely*dely);
#define DIVXY(RES,VAR) double RES = (VAR[pp] + VAR[mm] - VAR[pm] - VAR[mp])/(4*delx*dely);

inline CCTK_REAL veg_integrandx(
  CCTK_REAL z,int cc,
  CCTK_REAL alpha,CCTK_REAL beta,
  const CCTK_REAL *h,const CCTK_REAL *eta,
  const CCTK_REAL *u0,const CCTK_REAL *v0,
  CCTK_REAL u0x,CCTK_REAL u0y,
  CCTK_REAL v0x,CCTK_REAL v0y,
  CCTK_REAL u0xx,CCTK_REAL v0yy,
  CCTK_REAL v0xy,CCTK_REAL u0xy,
  CCTK_REAL hx,CCTK_REAL hy,
  CCTK_REAL hxx,CCTK_REAL hxy,CCTK_REAL hyy
) {
  return -(sqrt(sq(z - beta*eta[cc] - alpha*h[cc])*(4*hy*(u0x*u0xy + u0xx*v0x \
+ v0x*v0xy + u0x*v0yy + 2*v0y*(u0xy + v0yy))*(z + beta*eta[cc] + \
2*h[cc] + alpha*h[cc]) + 4*hx*(2*hy*(v0x*(2*u0x + v0y) + u0y*(u0x + \
2*v0y)) + ((u0xx + v0xy)*(2*u0x + v0y) + u0y*(u0xy + v0yy))*(z + \
beta*eta[cc] + 2*h[cc] + alpha*h[cc])) + 4*sq(hy)*(4*u0x*v0y + \
sq(u0x) + sq(v0x) + 4*sq(v0y)) + 4*sq(hx)*(sq(u0y) + sq(2*u0x + v0y)) \
+ (2*u0xy*v0yy + sq(u0xy) + sq(u0xx + v0xy) + sq(v0yy))*sq(z + \
beta*eta[cc] + 2*h[cc] + alpha*h[cc])) + 4*(sq(hxy)*sq(z - \
beta*eta[cc] - alpha*h[cc]) + sq(1 + hxx*(-z + beta*eta[cc] + \
alpha*h[cc])))*sq(u0[cc]) + 4*(1 - 2*hyy*(z - beta*eta[cc] - \
alpha*h[cc]) + sq(hxy)*sq(z - beta*eta[cc] - alpha*h[cc]) + \
sq(hyy)*sq(z - beta*eta[cc] - alpha*h[cc]))*sq(v0[cc]) + 4*(z - \
beta*eta[cc] - alpha*h[cc])*(2*hx*(hxy*(2*u0x + v0y)*(z - \
beta*eta[cc] - alpha*h[cc]) + u0y*(-1 + hyy*(z - beta*eta[cc] - \
alpha*h[cc]))) + 2*hy*(hxy*v0x*(z - beta*eta[cc] - alpha*h[cc]) + \
u0x*(-1 + hyy*(z - beta*eta[cc] - alpha*h[cc])) + 2*v0y*(-1 + hyy*(z \
- beta*eta[cc] - alpha*h[cc]))) + (z + beta*eta[cc] + 2*h[cc] + \
alpha*h[cc])*(hxy*(u0xx + v0xy)*(z - beta*eta[cc] - alpha*h[cc]) + \
u0xy*(-1 + hyy*(z - beta*eta[cc] - alpha*h[cc])) + v0yy*(-1 + hyy*(z \
- beta*eta[cc] - alpha*h[cc]))))*v0[cc] + 4*(z - beta*eta[cc] - \
alpha*h[cc])*u0[cc]*(-2*hy*v0x + 2*hxy*hy*u0x*z - u0xx*z + \
2*hxx*hy*v0x*z - v0xy*z + 4*hxy*hy*v0y*z - 2*u0xx*h[cc] - \
2*v0xy*h[cc] + 2*hxx*u0xx*z*h[cc] + 2*hxy*u0xy*z*h[cc] + \
2*hxx*v0xy*z*h[cc] + 2*hxy*v0yy*z*h[cc] - 2*hxy*hy*u0x*(beta*eta[cc] \
+ alpha*h[cc]) - u0xx*(beta*eta[cc] + alpha*h[cc]) - \
2*hxx*hy*v0x*(beta*eta[cc] + alpha*h[cc]) - v0xy*(beta*eta[cc] + \
alpha*h[cc]) - 4*hxy*hy*v0y*(beta*eta[cc] + alpha*h[cc]) - \
2*hxx*u0xx*h[cc]*(beta*eta[cc] + alpha*h[cc]) - \
2*hxy*u0xy*h[cc]*(beta*eta[cc] + alpha*h[cc]) - \
2*hxx*v0xy*h[cc]*(beta*eta[cc] + alpha*h[cc]) - \
2*hxy*v0yy*h[cc]*(beta*eta[cc] + alpha*h[cc]) + 2*hx*(hxy*u0y*(z - \
beta*eta[cc] - alpha*h[cc]) + (2*u0x + v0y)*(-1 + hxx*(z - \
beta*eta[cc] - alpha*h[cc]))) + hxx*u0xx*sq(z) + hxy*u0xy*sq(z) + \
hxx*v0xy*sq(z) + hxy*v0yy*sq(z) - hxx*u0xx*sq(beta*eta[cc] + \
alpha*h[cc]) - hxy*u0xy*sq(beta*eta[cc] + alpha*h[cc]) - \
hxx*v0xy*sq(beta*eta[cc] + alpha*h[cc]) - hxy*v0yy*sq(beta*eta[cc] + \
alpha*h[cc]) + 2*hxy*(-2 + hxx*(z - beta*eta[cc] - alpha*h[cc]) + \
hyy*(z - beta*eta[cc] - alpha*h[cc]))*v0[cc]))*(2*(-1 + hxx*(z - \
beta*eta[cc] - alpha*h[cc]))*u0[cc] + (z - beta*eta[cc] - \
alpha*h[cc])*(2*hy*v0x + 2*hx*(2*u0x + v0y) + u0xx*z + v0xy*z + \
2*u0xx*h[cc] + 2*v0xy*h[cc] + u0xx*(beta*eta[cc] + alpha*h[cc]) + \
v0xy*(beta*eta[cc] + alpha*h[cc]) + 2*hxy*v0[cc])))/4.;
}
inline CCTK_REAL veg_integrandy(
  CCTK_REAL z,int cc,
  CCTK_REAL alpha,CCTK_REAL beta,
  const CCTK_REAL *h,const CCTK_REAL *eta,
  const CCTK_REAL *u0,const CCTK_REAL *v0,
  CCTK_REAL u0x,CCTK_REAL u0y,
  CCTK_REAL v0x,CCTK_REAL v0y,
  CCTK_REAL u0xx,CCTK_REAL v0yy,
  CCTK_REAL v0xy,CCTK_REAL u0xy,
  CCTK_REAL hx,CCTK_REAL hy,
  CCTK_REAL hxx,CCTK_REAL hxy,CCTK_REAL hyy
) {
  return -(sqrt(sq(z - beta*eta[cc] - alpha*h[cc])*(4*hy*(u0x*u0xy + u0xx*v0x \
+ v0x*v0xy + u0x*v0yy + 2*v0y*(u0xy + v0yy))*(z + beta*eta[cc] + \
2*h[cc] + alpha*h[cc]) + 4*hx*(2*hy*(v0x*(2*u0x + v0y) + u0y*(u0x + \
2*v0y)) + ((u0xx + v0xy)*(2*u0x + v0y) + u0y*(u0xy + v0yy))*(z + \
beta*eta[cc] + 2*h[cc] + alpha*h[cc])) + 4*sq(hy)*(4*u0x*v0y + \
sq(u0x) + sq(v0x) + 4*sq(v0y)) + 4*sq(hx)*(sq(u0y) + sq(2*u0x + v0y)) \
+ (2*u0xy*v0yy + sq(u0xy) + sq(u0xx + v0xy) + sq(v0yy))*sq(z + \
beta*eta[cc] + 2*h[cc] + alpha*h[cc])) + 4*(sq(hxy)*sq(z - \
beta*eta[cc] - alpha*h[cc]) + sq(1 + hxx*(-z + beta*eta[cc] + \
alpha*h[cc])))*sq(u0[cc]) + 4*(1 - 2*hyy*(z - beta*eta[cc] - \
alpha*h[cc]) + sq(hxy)*sq(z - beta*eta[cc] - alpha*h[cc]) + \
sq(hyy)*sq(z - beta*eta[cc] - alpha*h[cc]))*sq(v0[cc]) + 4*(z - \
beta*eta[cc] - alpha*h[cc])*(2*hx*(hxy*(2*u0x + v0y)*(z - \
beta*eta[cc] - alpha*h[cc]) + u0y*(-1 + hyy*(z - beta*eta[cc] - \
alpha*h[cc]))) + 2*hy*(hxy*v0x*(z - beta*eta[cc] - alpha*h[cc]) + \
u0x*(-1 + hyy*(z - beta*eta[cc] - alpha*h[cc])) + 2*v0y*(-1 + hyy*(z \
- beta*eta[cc] - alpha*h[cc]))) + (z + beta*eta[cc] + 2*h[cc] + \
alpha*h[cc])*(hxy*(u0xx + v0xy)*(z - beta*eta[cc] - alpha*h[cc]) + \
u0xy*(-1 + hyy*(z - beta*eta[cc] - alpha*h[cc])) + v0yy*(-1 + hyy*(z \
- beta*eta[cc] - alpha*h[cc]))))*v0[cc] + 4*(z - beta*eta[cc] - \
alpha*h[cc])*u0[cc]*(-2*hy*v0x + 2*hxy*hy*u0x*z - u0xx*z + \
2*hxx*hy*v0x*z - v0xy*z + 4*hxy*hy*v0y*z - 2*u0xx*h[cc] - \
2*v0xy*h[cc] + 2*hxx*u0xx*z*h[cc] + 2*hxy*u0xy*z*h[cc] + \
2*hxx*v0xy*z*h[cc] + 2*hxy*v0yy*z*h[cc] - 2*hxy*hy*u0x*(beta*eta[cc] \
+ alpha*h[cc]) - u0xx*(beta*eta[cc] + alpha*h[cc]) - \
2*hxx*hy*v0x*(beta*eta[cc] + alpha*h[cc]) - v0xy*(beta*eta[cc] + \
alpha*h[cc]) - 4*hxy*hy*v0y*(beta*eta[cc] + alpha*h[cc]) - \
2*hxx*u0xx*h[cc]*(beta*eta[cc] + alpha*h[cc]) - \
2*hxy*u0xy*h[cc]*(beta*eta[cc] + alpha*h[cc]) - \
2*hxx*v0xy*h[cc]*(beta*eta[cc] + alpha*h[cc]) - \
2*hxy*v0yy*h[cc]*(beta*eta[cc] + alpha*h[cc]) + 2*hx*(hxy*u0y*(z - \
beta*eta[cc] - alpha*h[cc]) + (2*u0x + v0y)*(-1 + hxx*(z - \
beta*eta[cc] - alpha*h[cc]))) + hxx*u0xx*sq(z) + hxy*u0xy*sq(z) + \
hxx*v0xy*sq(z) + hxy*v0yy*sq(z) - hxx*u0xx*sq(beta*eta[cc] + \
alpha*h[cc]) - hxy*u0xy*sq(beta*eta[cc] + alpha*h[cc]) - \
hxx*v0xy*sq(beta*eta[cc] + alpha*h[cc]) - hxy*v0yy*sq(beta*eta[cc] + \
alpha*h[cc]) + 2*hxy*(-2 + hxx*(z - beta*eta[cc] - alpha*h[cc]) + \
hyy*(z - beta*eta[cc] - alpha*h[cc]))*v0[cc]))*((z - beta*eta[cc] - \
alpha*h[cc])*(2*hy*u0x + 2*hx*u0y + 4*hy*v0y + u0xy*z + v0yy*z + \
2*u0xy*h[cc] + 2*v0yy*h[cc] + u0xy*(beta*eta[cc] + alpha*h[cc]) + \
v0yy*(beta*eta[cc] + alpha*h[cc]) + 2*hxy*u0[cc]) + 2*(-1 + hyy*(z - \
beta*eta[cc] - alpha*h[cc]))*v0[cc]))/4.;
}

const CCTK_REAL delta_z = .05;

inline CCTK_REAL veg_integralx(
  CCTK_REAL zlo,CCTK_REAL zhi,int cc,
  CCTK_REAL alpha,CCTK_REAL beta,
  const CCTK_REAL *h,const CCTK_REAL *eta,
  const CCTK_REAL *u0,const CCTK_REAL *v0,
  CCTK_REAL u0x,CCTK_REAL u0y,
  CCTK_REAL v0x,CCTK_REAL v0y,
  CCTK_REAL u0xx,CCTK_REAL v0yy,
  CCTK_REAL v0xy,CCTK_REAL u0xy,
  CCTK_REAL hx,CCTK_REAL hy,
  CCTK_REAL hxx,CCTK_REAL hxy,CCTK_REAL hyy
) {
  if(zlo >= zhi) return 0;
  int n = int((zhi-zlo)/delta_z)+1;
  CCTK_REAL dz = (zhi-zlo)/n;
  CCTK_REAL sum=0;
  for(int i=0;i<n;i++) {
    CCTK_REAL z = zlo+dz*(i+.5);
    sum += veg_integrandx(z,cc,
          alpha,beta,
          h,eta,
          u0,v0,
          u0x,u0y,
          v0x,v0y,
          u0xx,v0yy,
          v0xy,u0xy,
          hx,hy,
          hxx,hxy,hyy);
  }
  return sum/n;
}

inline CCTK_REAL veg_integraly(
  CCTK_REAL zlo,CCTK_REAL zhi,int cc,
  CCTK_REAL alpha,CCTK_REAL beta,
  const CCTK_REAL *h,const CCTK_REAL *eta,
  const CCTK_REAL *u0,const CCTK_REAL *v0,
  CCTK_REAL u0x,CCTK_REAL u0y,
  CCTK_REAL v0x,CCTK_REAL v0y,
  CCTK_REAL u0xx,CCTK_REAL v0yy,
  CCTK_REAL v0xy,CCTK_REAL u0xy,
  CCTK_REAL hx,CCTK_REAL hy,
  CCTK_REAL hxx,CCTK_REAL hxy,CCTK_REAL hyy
) {
  if(zlo >= zhi) return 0;
  int n = int((zhi-zlo)/delta_z)+1;
  CCTK_REAL dz = (zhi-zlo)/n;
  CCTK_REAL sum=0;
  for(int i=0;i<n;i++) {
    CCTK_REAL z = zlo+dz*(i+.5);
    sum += veg_integrandy(z,cc,
          alpha,beta,
          h,eta,
          u0,v0,
          u0x,u0y,
          v0x,v0y,
          u0xx,v0yy,
          v0xy,u0xy,
          hx,hy,
          hxx,hxy,hyy);
  }
  return sum/n;
}

double igv(double a,double b,double c,double z0,double z2);

// New way
void veg_integral_v2(
  CCTK_REAL z0,CCTK_REAL z2,int cc,
  CCTK_REAL alpha,CCTK_REAL beta,
  const CCTK_REAL *h,const CCTK_REAL *eta,
  const CCTK_REAL *u0,const CCTK_REAL *v0,
  CCTK_REAL u0x,CCTK_REAL u0y,
  CCTK_REAL v0x,CCTK_REAL v0y,
  CCTK_REAL u0xx,CCTK_REAL v0yy,
  CCTK_REAL v0xy,CCTK_REAL u0xy,
  CCTK_REAL hx,CCTK_REAL hy,
  CCTK_REAL hxx,CCTK_REAL hxy,CCTK_REAL hyy,
  CCTK_REAL& fricx,CCTK_REAL& fricy
) {
CCTK_REAL rr,th;
rr = sqrt(sq(u0[cc])+sq(v0[cc]));
th = atan2(v0[cc],u0[cc]);
#include "Veg.hh"
}

//initialize the IB levee, determine the four surrounding grid points, set Fox, Foy, Udesired, Vdesired to 0
bool init_levee (CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_AWRITES(Hisx);
  DECLARE_CCTK_AWRITES(Hisy);
  DECLARE_CCTK_AWRITES(fox);
  DECLARE_CCTK_AWRITES(foy);
  DECLARE_CCTK_AWRITES(Uib);
  DECLARE_CCTK_AWRITES(Vib);
  DECLARE_CCTK_AWRITES(Udesired);
  DECLARE_CCTK_AWRITES(Vdesired);
  DECLARE_CCTK_AWRITES(IBFx);
  DECLARE_CCTK_AWRITES(IBFy);
  DECLAREi_CCTK_WRITES(libl);
  DECLAREi_CCTK_WRITES(ljbl);
  cctk_auto_thorn = "GRID";
  DECLARE_CCTK_READS(x);
  DECLARE_CCTK_READS(y);

  std::cout << "init_levee" << std::endl;

  for (int k = 0; k < levee_pts; k++)
  {
    int zero = FUNWAVE_INDEX(0,0);
    int one = FUNWAVE_INDEX(1,1);
    libl[k] = (xlevee[k] - x[zero])/(x[one]-x[zero]);
    ljbl[k] = (ylevee[k] - y[zero])/(y[one]-y[zero]);
  }

  for (int k = 0; k < levee_pts; k++) {
    Udesired[k] = 0;
    Vdesired[k] = 0;
    Hisx[k] = 0;
    Hisy[k] = 0;
    fox[k] = 0;
    foy[k] = 0;
    Uib[k] = 0;
    Vib[k] = 0;
    IBFx[k] = 0;
    IBFy[k] = 0;
  }
  return true;
}


void funwave_source_terms (CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_source_terms");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(sourcex);
  DECLARE_CCTK_WRITEREADS(sourcey);
  //DECLARE_CCTK_WRITEREADS(source_eta);
  //DECLARE_CCTK_WRITEREADS(depth);
  DECLARE_CCTK_READS(depthx);
  DECLARE_CCTK_READS(depthy);
  DECLARE_CCTK_READS(vegetation_height);
  DECLARE_CCTK_READS(depth);
  DECLARE_CCTK_READS(mask);
  DECLARE_CCTK_READS(mask9);
  DECLARE_CCTK_READS(h);
  DECLARE_CCTK_READS(u4);
  DECLARE_CCTK_READS(u3);
  DECLARE_CCTK_READS(u2);
  DECLARE_CCTK_READS(u);
  DECLARE_CCTK_READS(v4);
  DECLARE_CCTK_READS(v3);
  DECLARE_CCTK_READS(v2);
  DECLARE_CCTK_READS(v);
  DECLARE_CCTK_READS(p);
  DECLARE_CCTK_READS(q);
  DECLARE_CCTK_READS(eta);
  DECLARE_CCTK_READS(u1p);
  DECLARE_CCTK_READS(u1pp);
  DECLARE_CCTK_READS(v1p);
  DECLARE_CCTK_READS(v1pp);
  DECLARE_CCTK_READS(mask_wind);
  DECLARE_CCTK_READS(windu2d);
  DECLARE_CCTK_READS(windv2d);
  DECLARE_CCTK_READS(slopex);
  DECLARE_CCTK_READS(slopey);
  DECLARE_CCTK_READS(coriolis);
  DECLARE_CCTK_READWRITES(FORCEX);
  DECLARE_CCTK_READWRITES(FORCEY);
  DECLAREi_CCTK_READWRITES(finalcount);
  DECLARE_CCTK_AWRITES(Hisx);
  DECLARE_CCTK_AWRITES(Hisy);
  DECLARE_CCTK_AWRITES(fox);
  DECLARE_CCTK_AWRITES(foy);
  DECLARE_CCTK_AWRITES(Uib);
  DECLARE_CCTK_AWRITES(Vib);
  DECLARE_CCTK_AWRITES(Udesired);
  DECLARE_CCTK_AWRITES(Vdesired);
  DECLARE_CCTK_AWRITES(IBFx);
  DECLARE_CCTK_AWRITES(IBFy);
  DECLAREi_CCTK_READS(libl);
  DECLAREi_CCTK_READS(ljbl);
  DECLARE_CCTK_READWRITES(vfx);
  DECLARE_CCTK_READWRITES(vfy);
  cctk_auto_thorn = "GRID";
  DECLARE_CCTK_READS(x);
  DECLARE_CCTK_READS(y);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  DECLARE_CCTK_READS(dy);
  DECLARE_CCTK_PARAMETERS;
  DECLARE_FUNWAVE_BOUNDS;

  if(levee_pts > 0) {
    static bool init_lev = true;
    if(init_lev) {
      init_lev = false;
      init_levee(CCTK_PASS_CTOC);
    }

    //initialize the velocities on the IB points to zero
    for (int k = 0; k < levee_pts; k++) {
      Uib[k] = 0;
      Vib[k] = 0;
    }

    //determine what the velocity is on each IB point
    for (int k = 0; k < levee_pts; k++) {
      for (int i = libl[k]; i <= libl[k]+1; i++) {
        for (int j = ljbl[k]; j <= ljbl[k]+1; j++) {
          int cc = FUNWAVE_INDEX(i,j);
          CCTK_REAL dxv = dx[cc];
          CCTK_REAL dyv = dy[cc];
          CCTK_REAL xv = x[cc];
          CCTK_REAL yv = y[cc];
          CCTK_REAL weight = weight_calc(xv, yv, xlevee[k], ylevee[k], dxv,dyv);
          Uib[k] = u[cc]*weight + Uib[k];
          Vib[k] = v[cc]*weight + Vib[k];
        }
      }  
    }

    //initialize the count to 0
    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX(0,j);
      for(int i = ilo; i < ihi; i++) {
        int cc = cc_+i;
        finalcount[cc] = 0;
      }
    }

    //determine how many IB points affect each grid point
    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX(0,j);
      for(int i = ilo; i < ihi; i++) {
        int cc = cc_+i;
        CCTK_REAL dxv = dx[cc];
        CCTK_REAL dyv = dy[cc];
        CCTK_REAL xv = x[cc];
        CCTK_REAL yv = y[cc];

        for (int k = 0; k < levee_pts; k++) {
          CCTK_REAL weight1 = weight_calc(xv, yv, xlevee[k], ylevee[k], dxv,dyv);

          // TODO: Remove hard-coded constant
          if (weight1 > .001) {
            finalcount[cc] += 1;
          }
        }
      }
    }

    // Determine the amount of force each IB point has
    for (int k = 0; k < levee_pts; k++) {
      fox[k] = Udesired[k] - Uib[k];
      foy[k] = Vdesired[k] - Vib[k];
    }

    // TODO: This is wrong!
    for (int k = 0; k < levee_pts; k++) {
      Hisx[k] += 0;//fox[k]*cctkGH->cctk_delta_time;
      Hisy[k] += 0;//foy[k]*cctkGH->cctk_delta_time;
    }

    for (int k = 0; k < levee_pts; k++) {
      IBFx[k] = alpha*Hisx[k] + beta*fox[k];
      IBFy[k] = alpha*Hisy[k] + beta*foy[k];
    }

    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX(0,j);
      for (int i = ilo; i < ihi; i++) {
        int cc = cc_+i;

        FORCEX[cc] = 0;
        FORCEY[cc] = 0;
      }
    }

    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX(0,j);
      for (int i = ilo; i < ihi; i++) {
        int cc = cc_+i;
        for (int k = 0; k < levee_pts; k++) {
          CCTK_REAL dxv = dx[cc];
          CCTK_REAL dyv = dy[cc];
          CCTK_REAL xv = x[cc];
          CCTK_REAL yv = y[cc];

          CCTK_REAL weight2 = weight_calc(xv, yv, xlevee[k], ylevee[k], dxv,dyv);
          //CCTK_REAL distancex = distance_x(xv, xlevee[k], dxv);
          //CCTK_REAL distancey = distance_y(yv, ylevee[k], dyv);

          //	std::cout << distancex << " " << distancey;
          if (fabs(xlevee[k] - x[cc]) < dx[cc] && fabs(ylevee[k] - y[cc]) < dy[cc]) {
            FORCEX[cc] = (weight2*IBFx[k]) + FORCEX[cc];
            FORCEY[cc] = (weight2*IBFy[k]) + FORCEY[cc];
          }
        }
      }
    }
  }

  if(!spherical_coordinates) {
    CCTK_REAL xmax_calc = 0, xmin_calc = 9999;
    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX( 0, j);
      for (int i = ilo; i < ihi; i++) {
        int cc = cc_+i;
        if(x[cc] > xmax_calc)
          xmax_calc = x[cc];
        if(x[cc] < xmin_calc)
          xmin_calc = x[cc];
      }
    }
    //#pragma omp parallel for
    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX(0,j);
      int jm1_ = FUNWAVE_INDEX(0,j-1);
      int jp1_ = FUNWAVE_INDEX(0,j+1);
      for (int i = ilo; i < ihi; i++) {
        int cc = cc_+i;
        int ip1 = cc+1;
        int im1 = cc-1;
        int jp1 = jp1_+i;
        int jm1 = jm1_+i;
        int pp  = jp1_+i+1;//FUNWAVE_INDEX( i + 1, j + 1);
        int mm  = jm1_+i-1;//FUNWAVE_INDEX( i - 1, j - 1);
        int mp  = jp1_+i-1;//FUNWAVE_INDEX( i - 1, j + 1);
        int pm  = jm1_+i+1;//FUNWAVE_INDEX( i + 1, j - 1);

        sourcex[cc] = grav * (eta[cc]) / dx[cc] * (depthx[ip1] - depthx[cc]) * mask[cc]
          // friction;
          - cd * u[cc] * sqrt (u[cc] * u[cc] + v[cc] * v[cc])
          // dispersion;
          // (h+eta)(u*\nabla v4 + v4 * \nabla u - v1pp-v2-v3);
          + gamma1 * mask9[cc] * max(h[cc], mindepthfrc) *
            (u[cc] * 0.5 * (u4[ip1] - u4[im1]) / dx[cc] +
             v[cc] * 0.5 * (u4[jp1] - u4[jm1]) / dy[cc] +
             u4[cc] * 0.5 * (u[ip1] - u[im1]) / dx[cc] +
             v4[cc] * 0.5 * (u[jp1] - u[jm1]) / dy[cc] -
                gamma2 * mask9[cc] * (u1pp[cc] +
                  u2[cc] +
                  u3[cc]))
                // ht(-v4+v1p) = div(m)*(u4-u1p);
                + gamma1 * mask9[cc] * ((p[ip1] - p[cc]) / dx[cc] +
                    (q[jp1] - q[cc]) / dy[cc]) * (u4[cc] - u1p[cc]);

        sourcey[cc] = grav * (eta[cc]) / dy[cc] * (depthy[jp1] - depthy[cc]) * mask[cc]
          // friction;
          - cd * v[cc] * sqrt (u[cc] * u[cc] + v[cc] * v[cc])
          // dispersion;
          // (h+eta)(u*\nabla v4 + v4 * \nabla u -v1pp-v2-v3);
          + gamma1 * mask9[cc] * max (h[cc],
              mindepthfrc) * (u[cc] * 0.5 * (v4[ip1] -
                  v4[im1]) /
                dx[cc] + v[cc] * 0.5 * (v4[jp1] -
                  v4[jm1])
                / dy[cc] +
                u4[cc] * 0.5 * (v[ip1] -
                  v[im1]) /
                dx[cc] + v4[cc] * 0.5 * (v[jp1] -
                  v[jm1])
                / dy[cc] -
                gamma2 * mask9[cc] * (v1pp[cc] +
                  v2[cc] +
                  v3[cc]))
                // ht(-v4+v1p) = div(q)*(v4-v1p);
                + gamma1 * mask9[cc] * ((p[ip1] - p[cc]) / dx[cc] +
                    (q[jp1] - q[cc]) / dy[cc]) * (v4[cc] - v1p[cc]);

        // TODO: Should this be before or after wind force?
        for (int k = 0; k < levee_pts; k++)
        {
          if (fabs(xlevee[k] - x[cc]) < dx[cc] && fabs(ylevee[k] - y[cc]) < dy[cc] && finalcount[cc] >= 1)
          {
            sourcex[cc] += FORCEX[cc]/finalcount[cc];
            sourcey[cc] += FORCEY[cc]/finalcount[cc];
            // source_eta[cc] = 0;
          }
        }
        if(wind_force) 
        {
          CCTK_REAL fac = mask_wind[cc]*rho_aw*cdw*sqrt(windu2d[cc]*windu2d[cc]+windv2d[cc]*windv2d[cc]);
          sourcex[cc] += windu2d[cc]*fac;
          sourcey[cc] += windv2d[cc]*fac;
        }

        //CCTK_REAL cutoff = xmax_calc/2.0;
        //CCTK_REAL fac = 1.0e-7;
        //CCTK_REAL xlo = (2*xmin_calc+  xmax_calc)/3.0;
        //CCTK_REAL xhi = (  xmin_calc+2*xmax_calc)/3.0;

        /*
        CCTK_REAL hfac = (x[cc]-cutoff)/(xmax_calc-cutoff);
        if(x[cc] < cutoff) {
          hfac = 0;
        }
        if(xlo <= x[cc] && x[cc] <= xhi) {
          hfac = .6;
        } else {
          hfac = 0;
        }
        CCTK_REAL zhi = hfac*h[cc];
        */

        // Vegetation Form Drag Coefficient
        //CCTK_REAL C_D = .15; // .1 - 1.5
        // Frontal Area
        //CCTK_REAL b_v = .01*vegetation_depth[cc]; //h[cc]*hfac;
        // Stem number density
        //CCTK_REAL N = 500; // 200 - 800
        //CCTK_REAL fac = 0.5*C_D*b_v*N;
        CCTK_REAL hv = min(vegetation_height[cc],max(h[cc],mindepthfrc));
        CCTK_REAL zlo = -depth[cc];
        CCTK_REAL zhi = zlo + hv;//min(eta[cc],vegetation_height[cc]-depth[cc]);
        CCTK_REAL fac = 0.5*veg_cd*(veg_bv*hv)*veg_N;

        CCTK_REAL delx = 0.5*(x[ip1]-x[im1]);
        CCTK_REAL dely = 0.5*(y[jp1]-y[jm1]);

        DIV1X(hx,h)
        DIV2X(hxx,h)
        DIV1Y(hy,h)
        DIV2Y(hyy,h)
        DIVXY(hxy,h)

        DIV1X(u0x,u)
        DIV1Y(u0y,u)
        DIV2X(u0xx,u)
        DIVXY(u0xy,u)

        DIV1Y(v0y,v)
        DIV1X(v0x,v)
        DIV2Y(v0yy,v)
        DIVXY(v0xy,v)

        CCTK_REAL alpha_v = -.53;
        CCTK_REAL beta_v =  .47;
        // z_alpha = alpha_v h + beta eta

        if(first_order_veg) {

          //CCTK_REAL zhi = depth[cc] - max(0.0,depth[cc]-vegetation_height[cc]);
          vfx[cc] = fac*u[cc]*sqrt(u[cc]*u[cc]+v[cc]*v[cc]);
          vfy[cc] = fac*v[cc]*sqrt(u[cc]*u[cc]+v[cc]*v[cc]);

        } else {

          /*
          vfx[cc] = fac*veg_integralx(zlo,zhi,cc,
              alpha_v,beta_v,
              h,eta,
              u,v,
              u0x,u0y,
              v0x,v0y,
              u0xx,v0yy,
              v0xy,u0xy,
              hx,hy,
              hxx,hxy,hyy);

          vfy[cc] = fac*veg_integraly(zlo,zhi,cc,
              alpha_v,beta_v,
              h,eta,
              u,v,
              u0x,u0y,
              v0x,v0y,
              u0xx,v0yy,
              v0xy,u0xy,
              hx,hy,
              hxx,hxy,hyy);
              */
          veg_integral_v2(zlo,zhi,cc,
              alpha_v,beta_v,
              h,eta,
              u,v,
              u0x,u0y,
              v0x,v0y,
              u0xx,v0yy,
              v0xy,u0xy,
              hx,hy,
              hxx,hxy,hyy,
              vfx[cc],vfy[cc]);
          vfx[cc] *= fac;
          vfy[cc] *= fac;
        }

        /*
        if(vfx[cc] > 1.0e-12) {
          HERE VAR(vfx[cc]) END;
        }
        if(vegetation_depth[cc] != 0) {
          HERE VAR(vegetation_depth[cc]) END;
        }
        */

        sourcex[cc] = sourcex[cc] - vfx[cc];
        sourcey[cc] = sourcey[cc] - vfy[cc];
      }
    }
  } else {
    for (int j = jlo; j < jhi; j++) {
      int cc_ = FUNWAVE_INDEX( 0, j);
      int jp1_ = FUNWAVE_INDEX( 0, j+1);
      for (int i = ilo; i < ihi; i++) {
        int cc = cc_+i;
        int ip1 = cc+1;//FUNWAVE_INDEX( i + 1, j);
        int jp1 = jp1_+i;//FUNWAVE_INDEX( i, j + 1);
        sourcex[cc]=grav*(eta[cc])*slopex[cc]*mask[cc]
          // friction
          -cd*u[cc]*sqrt(u[cc]*u[cc]+v[cc]*v[cc])
          // dispersion
          // ht(+v1p) = div(m)*(-u1p)
          +(gamma1)*mask9[cc]*((p[ip1]-p[cc])/dx[cc]+(q[jp1]-q[cc])/dy[cc])
          *(-u1p[cc])
          // coriolis
          +coriolis[cc]*0.5*(q[cc]+q[jp1]);

        sourcey[cc]=grav*(eta[cc])*slopey[cc]*mask[cc]
          // friction
          -cd*v[cc]*sqrt(u[cc]*u[cc]+v[cc]*v[cc])
          // dispersion
          // ht(+v1p) = div(q)*(-v1p)
          +(gamma1)*mask9[cc]*((p[ip1]-p[cc])/dx[cc]+(q[jp1]-q[cc])/dy[cc])
          *(-v1p[cc])
          // coriolis
          -coriolis[cc]*0.5*(p[cc]+p[ip1]);

      }
    }
  }
}
CCTK_REAL weight_calc(CCTK_REAL Xi, CCTK_REAL Yi, CCTK_REAL Xs, CCTK_REAL Ys, CCTK_REAL DX, CCTK_REAL DY)
{
  CCTK_REAL answer;

  if (fabs(Xs-Xi) <= DX && fabs(Ys-Yi) <= DY)
  {
    answer = (DX-fabs(Xs-Xi))*(DY-fabs(Ys-Yi))/(DX*DY);
  }
  else 
  {
    answer = 0;
  }
  return answer;
}

CCTK_REAL distance_x(CCTK_REAL XI, CCTK_REAL XS, CCTK_REAL DX)
{
  CCTK_REAL answer;

  if (fabs(XS-XI) <= DX)
  {
    answer = fabs(XS-XI);
  }
  else
  {
    answer = 0;
  }
  return answer;
}

CCTK_REAL distance_y(CCTK_REAL YI, CCTK_REAL YS, CCTK_REAL DY)
{
  CCTK_REAL answer;

  if (fabs(YS-YI) <= DY)
  {
    answer = fabs(YS-YI);
  }
  else
  {
    answer = 0;
  }
  return answer;
}
