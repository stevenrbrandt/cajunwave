//const CCTK_REAL grav = 9.801;
//const CCTK_REAL mindepthfrc = 0.5;
//const CCTK_REAL cd = 0.005;
//const CCTK_REAL cfl = 0.15;
//const CCTK_REAL small = 0.000001;
//const CCTK_REAL R_earth = 6371000.0;
//#define CARTESIAN 1
//#define MIXING 1

const CCTK_REAL beta_ref=-0.531;
const CCTK_REAL beta_1 = 1.0+beta_ref;
const CCTK_REAL beta_2 = .04;
