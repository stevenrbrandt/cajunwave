#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_constants.hh"
#include "funwave_util.hh"
#include "derivative.hh"
#include "here.hh"
#include "cctk_declare.h"

void update_etat(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("update_etat");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_WRITES(etat);
	DECLARE_CCTK_READS(p);
	DECLARE_CCTK_READS(q);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);
	DECLARE_CCTK_READS(dy);
	for(int j=0;j<cctk_lsh[1]-1;j++) {
		for(int i=0;i<cctk_lsh[0]-1;i++) {
			int cc = FUNWAVE_INDEX(i,j);
			int ip1 = FUNWAVE_INDEX(i+1,j);
			int jp1 = FUNWAVE_INDEX(i,j+1);
			etat[cc] = -(p[ip1]-p[cc])/dx[cc]-
					(q[jp1]-q[cc])/dy[cc];
		}
	}
}

/**
 * Corresponds to cal_dispersion
 */
void funwave_dispersion(CCTK_ARGUMENTS) {
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("funwave_dispersion");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READS(u);
	DECLARE_CCTK_READS(v);
	DECLARE_CCTK_READS(eta);
	DECLARE_CCTK_READS(depth);
	DECLARE_CCTK_READS(u0);
	DECLARE_CCTK_READS(v0);
	DECLARE_CCTK_READS(etat);
	DECLARE_CCTK_SCALAR(fun_dt);
	DECLARE_CCTK_WRITEREADS(uxx);
	DECLARE_CCTK_WRITEREADS(uxy);
	DECLARE_CCTK_WRITEREADS(vxy);
	DECLARE_CCTK_WRITEREADS(vyy);
	DECLARE_CCTK_WRITEREADS(ux);
	DECLARE_CCTK_WRITEREADS(vx);
	DECLARE_CCTK_WRITEREADS(uy);
	DECLARE_CCTK_WRITEREADS(vy);
	DECLARE_CCTK_WRITEREADS(etax);
	DECLARE_CCTK_WRITEREADS(etay);
	DECLARE_CCTK_WRITEREADS(du);
	DECLARE_CCTK_WRITEREADS(dv);
	DECLARE_CCTK_WRITEREADS(ut);
	DECLARE_CCTK_WRITEREADS(vt);
	DECLARE_CCTK_WRITEREADS(dut);
	DECLARE_CCTK_WRITEREADS(dvt);
	DECLARE_CCTK_WRITEREADS(duxx);
	DECLARE_CCTK_WRITEREADS(duxy);
	DECLARE_CCTK_WRITEREADS(dvxy);
	DECLARE_CCTK_WRITEREADS(dvyy);
	DECLARE_CCTK_WRITEREADS(dux);
	DECLARE_CCTK_WRITEREADS(dvx);
	DECLARE_CCTK_WRITEREADS(duy);
	DECLARE_CCTK_WRITEREADS(dvy);
	DECLARE_CCTK_WRITEREADS(etatx);
	DECLARE_CCTK_WRITEREADS(etaty);
	DECLARE_CCTK_WRITEREADS(utx);
	DECLARE_CCTK_WRITEREADS(vty);
	DECLARE_CCTK_WRITEREADS(utxx);
	DECLARE_CCTK_WRITEREADS(vtyy);
	DECLARE_CCTK_WRITEREADS(utxy);
	DECLARE_CCTK_WRITEREADS(vtxy);
	DECLARE_CCTK_WRITEREADS(dutx);
	DECLARE_CCTK_WRITEREADS(dvty);
	DECLARE_CCTK_WRITEREADS(dutxx);
	DECLARE_CCTK_WRITEREADS(dutxy);
	DECLARE_CCTK_WRITEREADS(dvtxy);
	DECLARE_CCTK_WRITEREADS(dvtyy);
	DECLARE_CCTK_PARAMETERS;
	DECLARE_FUNWAVE_BOUNDS;

	derivative_xx_high(CCTK_PASS_CTOC,u,uxx);
	derivative_xy_high(CCTK_PASS_CTOC,u,uxy);
	derivative_xy_high(CCTK_PASS_CTOC,v,vxy);
	derivative_yy_high(CCTK_PASS_CTOC,v,vyy);
	if(gamma2>0.0 && !spherical_coordinates) {
		derivative_x(CCTK_PASS_CTOC,u,ux);
		derivative_x(CCTK_PASS_CTOC,v,vx);
		derivative_y(CCTK_PASS_CTOC,u,uy);
		derivative_y(CCTK_PASS_CTOC,v,vy);
		derivative_x(CCTK_PASS_CTOC,eta,etax);
		derivative_y(CCTK_PASS_CTOC,eta,etay);
	} else if(show_breaking) {
		derivative_x(CCTK_PASS_CTOC,eta,etax);
		derivative_y(CCTK_PASS_CTOC,eta,etay);
	}

	for(int j=0;j<cctk_lsh[1];j++) { 
		for(int i=0;i<cctk_lsh[0];i++) {
			int cc = FUNWAVE_INDEX(i,j);
			du[cc] = max(depth[cc],mindepthfrc)*u[cc];
			dv[cc] = max(depth[cc],mindepthfrc)*v[cc];
		}
	}

	if(gamma2>0.0 && !spherical_coordinates) {
		if(disp_time_left) {
			update_etat(CCTK_PASS_CTOC);
		} else {
			if(show_breaking) {
				update_etat(CCTK_PASS_CTOC);
			}
			for(int j=0;j<cctk_lsh[1];j++) {
				for(int i=0;i<cctk_lsh[0];i++) {
					int cc = FUNWAVE_INDEX(i,j);
					CCTK_REAL delt;
					if(use_correct_dispersion) {
						delt = cctk_delta_time;
					} else {
						delt = *fun_dt;
					}
					ut[cc] = (u[cc]-u0[cc])/ delt /3.0;
					vt[cc] = (v[cc]-v0[cc])/ delt /3.0;
					dut[cc] = max(depth[cc],mindepthfrc)*ut[cc];
					dvt[cc] = max(depth[cc],mindepthfrc)*vt[cc];
				}
			}
		}
	} else if(show_breaking) {
		update_etat(CCTK_PASS_CTOC);
	}
	derivative_xx_high(CCTK_PASS_CTOC,du,duxx);
	derivative_xy_high(CCTK_PASS_CTOC,du,duxy);
	derivative_xy_high(CCTK_PASS_CTOC,dv,dvxy);
	derivative_yy_high(CCTK_PASS_CTOC,dv,dvyy);
	if(gamma2 > 0 && !spherical_coordinates) {
		derivative_x(CCTK_PASS_CTOC,du,dux);
		derivative_x(CCTK_PASS_CTOC,dv,dvx);
		derivative_y(CCTK_PASS_CTOC,du,duy);
		derivative_y(CCTK_PASS_CTOC,dv,dvy);
		if(disp_time_left) {
			derivative_x_high(CCTK_PASS_CTOC,etat,etatx);
			derivative_y_high(CCTK_PASS_CTOC,etat,etaty);
		} else {
			derivative_x(CCTK_PASS_CTOC,(CCTK_READ_ARG)ut,utx);
			derivative_y(CCTK_PASS_CTOC,(CCTK_READ_ARG)vt,vty);

			derivative_xx(CCTK_PASS_CTOC,(CCTK_READ_ARG)ut,utxx);
			derivative_yy(CCTK_PASS_CTOC,(CCTK_READ_ARG)vt,vtyy);
			derivative_xy(CCTK_PASS_CTOC,(CCTK_READ_ARG)ut,utxy);
			derivative_xy(CCTK_PASS_CTOC,(CCTK_READ_ARG)vt,vtxy);

			derivative_x(CCTK_PASS_CTOC,dut,dutx);
			derivative_y(CCTK_PASS_CTOC,dvt,dvty);

			derivative_xx(CCTK_PASS_CTOC,dut,dutxx);
			derivative_yy(CCTK_PASS_CTOC,dvt,dvtyy);
			derivative_xy(CCTK_PASS_CTOC,dut,dutxy);
			derivative_xy(CCTK_PASS_CTOC,dvt,dvtxy);
		}
	}

	// TODO: Why is this a single zone?
	if(cctk_bbox[0]>0) {
		for (int j = 0; j < cctk_lsh[1]; j++) {
			int cc = FUNWAVE_INDEX(ilo,j);
			uxy[cc] = 0.0;
			duxy[cc] = 0.0;
			vxy[cc] = 0.0;
			dvxy[cc] = 0.0;
			if(!disp_time_left) {
				utxy[cc]=0.0;
				dutxy[cc]=0.0;
				vtxy[cc]=0.0;
				dvtxy[cc]=0.0;
			}
		}
	}

	if(cctk_bbox[1]>0) {
		for (int j = 0; j < cctk_lsh[1]; j++) {
			int cc = FUNWAVE_INDEX(ihi-1,j);
			uxy[cc] = 0.0;
			duxy[cc] = 0.0;
			vxy[cc] = 0.0;
			dvxy[cc] = 0.0;
			if(!disp_time_left) {
				utxy[cc]=0.0;
				dutxy[cc]=0.0;
				vtxy[cc]=0.0;
				dvtxy[cc]=0.0;
			}
		}
	}

	if(cctk_bbox[2]>0) {
		for (int i = 0; i < cctk_lsh[0]; i++) {
			int cc = FUNWAVE_INDEX(i,jlo);
			uxy[cc] = 0.0;
			duxy[cc] = 0.0;
			vxy[cc] = 0.0;
			dvxy[cc] = 0.0;
			if(!disp_time_left) {
				utxy[cc]=0.0;
				dutxy[cc]=0.0;
				vtxy[cc]=0.0;
				dvtxy[cc]=0.0;
			}
		}
	}

	if(cctk_bbox[3]>0) {
		for (int i = 0; i < cctk_lsh[0]; i++) {
			int cc = FUNWAVE_INDEX(i,jhi-1);
			uxy[cc] = 0.0;
			duxy[cc] = 0.0;
			vxy[cc] = 0.0;
			dvxy[cc] = 0.0;
			if(!disp_time_left) {
				utxy[cc]=0.0;
				dutxy[cc]=0.0;
				vtxy[cc]=0.0;
				dvtxy[cc]=0.0;
			}
		}
	}

	funwave_exchange_dispersion(CCTK_PASS_CTOC);
}

void funwave_dispersion_2(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("funwave_dispersion_2");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_READS(uxx);
	DECLARE_CCTK_READS(vxy);
	DECLARE_CCTK_READS(uxy);
	DECLARE_CCTK_READS(vyy);
	DECLARE_CCTK_READS(duxx);
	DECLARE_CCTK_READS(dvxy);
	DECLARE_CCTK_READS(duxy);
	DECLARE_CCTK_READS(dvyy);
	DECLARE_CCTK_WRITEREADS(u4);
	DECLARE_CCTK_WRITEREADS(v4);
	DECLARE_CCTK_WRITEREADS(u1p);
	DECLARE_CCTK_WRITEREADS(v1p);
	DECLARE_CCTK_READS(depth);
	DECLARE_CCTK_READS(eta);
	DECLARE_CCTK_READS(mask9);
	DECLARE_CCTK_READS(etax);
	DECLARE_CCTK_READS(ux);
	DECLARE_CCTK_READS(vy);
	DECLARE_CCTK_READS(dux);
	DECLARE_CCTK_READS(dvy);
	DECLARE_CCTK_READS(etay);
	DECLARE_CCTK_WRITES(u1pp);
	DECLARE_CCTK_WRITES(v1pp);
	DECLARE_CCTK_READS(etat);
	DECLARE_CCTK_READS(utx);
	DECLARE_CCTK_READS(vty);
	DECLARE_CCTK_READS(etatx);
	DECLARE_CCTK_READS(etaty);
	DECLARE_CCTK_READS(utxx);
	DECLARE_CCTK_READS(dutx);
	DECLARE_CCTK_READS(dvty);
	DECLARE_CCTK_READS(vtxy);
	DECLARE_CCTK_READS(dutxx);
	DECLARE_CCTK_READS(dutxy);
	DECLARE_CCTK_READS(dvtxy);
	DECLARE_CCTK_READS(dvtyy);
	DECLARE_CCTK_READS(utxy);
	DECLARE_CCTK_READS(vtyy);
	DECLARE_CCTK_WRITES(u2);
	DECLARE_CCTK_READS(u);
	DECLARE_CCTK_READS(v);
	DECLARE_CCTK_READS(vx);
	DECLARE_CCTK_WRITES(v2);
	DECLARE_CCTK_READS(uy);
	DECLARE_CCTK_SCALAR(b2);
	DECLARE_CCTK_WRITES(u3);
	DECLARE_CCTK_WRITES(v3);

	DECLARE_CCTK_PARAMETERS;

	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);
	DECLARE_CCTK_READS(dy);
	DECLARE_FUNWAVE_BOUNDS;
	if(!spherical_coordinates) {
        // TODO: I believe that we should be
        // iterating from 1 to lsh[0]-1, and
        // 1 to lsh[1]-1, in accord with what's
        // updated later. I believe this corresponds
        // to a bug in the original Funwave code.
        //#pragma omp parallel for
		for(int j=0;j<cctk_lsh[1];j++) {
			for(int i=0;i<cctk_lsh[0];i++) {
				int cc = FUNWAVE_INDEX(i,j);
				u4[cc] = (1./3.-beta_1+0.5*beta_1*beta_1)*
						depth[cc]*depth[cc]*
						(uxx[cc]+vxy[cc])+
						(beta_1-0.5)*depth[cc]*(duxx[cc]+dvxy[cc]);
				v4[cc] = (1./3.-beta_1+0.5*beta_1*beta_1)*
						depth[cc]*depth[cc]*
						(uxy[cc]+vyy[cc])+
						(beta_1-0.5)*depth[cc]*(duxy[cc]+dvyy[cc]);
				u1p[cc] = 0.5*(1.0-beta_1)*depth[cc]*depth[cc]*(uxx[cc]+vxy[cc])+
						(beta_1-1.0)*depth[cc]*(duxx[cc]+dvxy[cc]);
				v1p[cc] = 0.5*(1.0-beta_1)*depth[cc]*depth[cc]*(uxy[cc]+vyy[cc])+
						(beta_1-1.0)*depth[cc]*(duxy[cc]+dvyy[cc]);
			}
		}
	} else { // spherical coordinates
		for(int j=0;j<cctk_lsh[1];j++) {
			for(int i=0;i<cctk_lsh[0];i++) {
				int cc = FUNWAVE_INDEX(i,j);
				u1p[cc]=0.5*(1.0-beta_1)*depth[cc]*depth[cc]*(uxx[cc]+vxy[cc])
	            		   +(beta_1-1.0)*depth[cc]*(duxx[cc]+dvxy[cc]);
				v1p[cc]=0.5*(1.0-beta_1)*depth[cc]*depth[cc]*(uxy[cc]+vyy[cc])
	            		   +(beta_1-1.0)*depth[cc]*(duxy[cc]+dvyy[cc]);

			}
		}
	}
	if (gamma2>0 && !spherical_coordinates) {
    //#pragma omp parallel for
		for(int j=jlo;j<jhi;j++) {
			for(int i=ilo;i<ihi;i++) {

				int cc = FUNWAVE_INDEX(i,j);
				int jm1 = FUNWAVE_INDEX(i,j-1);
				int jp1 = FUNWAVE_INDEX(i,j+1);
				int ip1 = FUNWAVE_INDEX(i+1,j);
				int im1 = FUNWAVE_INDEX(i-1,j);
				CCTK_REAL uxxvxy=uxx[cc]+vxy[cc];
				CCTK_REAL uxyvyy=uxy[cc]+vyy[cc];
				CCTK_REAL uxxvxy_x=(uxx[ip1]+vxy[ip1]-uxx[im1]-vxy[im1])/2.0/dx[cc];
				CCTK_REAL uxxvxy_y=(uxx[jp1]+vxy[jp1]-uxx[jm1]-vxy[jm1])/2.0/dy[cc];
				CCTK_REAL uxyvyy_x=(uxy[ip1]+vyy[ip1]-uxy[im1]-vyy[im1])/2.0/dx[cc];
				CCTK_REAL uxyvyy_y=(uxy[jp1]+vyy[jp1]-uxy[jm1]-vyy[jm1])/2.0/dy[cc];

				CCTK_REAL huxxhvxy=duxx[cc]+dvxy[cc];
				CCTK_REAL huxyhvyy=duxy[cc]+dvyy[cc];
				CCTK_REAL huxxhvxy_x=(duxx[ip1]+dvxy[ip1]-duxx[im1]-dvxy[im1])/2.0/dx[cc];
				CCTK_REAL huxxhvxy_y=(duxx[jp1]+dvxy[jp1]-duxx[jm1]-dvxy[jm1])/2.0/dy[cc];
				CCTK_REAL huxyhvyy_x=(duxy[ip1]+dvyy[ip1]-duxy[im1]-dvyy[im1])/2.0/dx[cc];
				CCTK_REAL huxyhvyy_y=(duxy[jp1]+dvyy[jp1]-duxy[jm1]-dvyy[jm1])/2.0/dy[cc];

				CCTK_REAL rh=depth[cc];
				CCTK_REAL rhx=(depth[ip1]-depth[im1])/2.0/dx[cc];
				CCTK_REAL rhy=(depth[jp1]-depth[jm1])/2.0/dy[cc];
				CCTK_REAL reta=eta[cc];
				
				// kennedy et al. 2001;
				CCTK_REAL ken1=(1.0/6.0-beta_1+beta_1*beta_1)*rh*reta*beta_2
						+(1.0/2.0*beta_1*beta_1-1.0/6.0)*reta*reta*beta_2*beta_2;
				CCTK_REAL ken2=(beta_1-1.0/2.0)*reta*beta_2;
				u4[cc]=u4[cc]+gamma2*mask9[cc]*(ken1*uxxvxy
						+ken2*huxxhvxy);
				v4[cc]=v4[cc]+gamma2*mask9[cc]*(ken1*uxyvyy
						+ken2*huxyhvyy);

				if (disp_time_left) {
					u1p[cc]=u1p[cc]-gamma2*mask9[cc]*(
							reta*beta_2*etax[cc]*beta_2*(ux[cc]+vy[cc])
							+ reta*reta*beta_2*beta_2/2.0
							*uxxvxy
							+etax[cc]*beta_2*(dux[cc]+dvy[cc])
							+reta*beta_2*huxxhvxy
					);
					v1p[cc]=v1p[cc]-gamma2*mask9[cc]*(
							reta*beta_2*etay[cc]*beta_2*(ux[cc]+vy[cc])
							+ reta*reta*beta_2*beta_2/2.0
							*uxyvyy
							+etay[cc]*beta_2*(dux[cc]+dvy[cc])
							+reta*beta_2*huxyhvyy
					);

					u1pp[cc]=(etax[cc]*etat[cc]*beta_2*beta_2+reta*beta_2*etatx[cc]*beta_2)
	                    		*(ux[cc]+vy[cc])
	                    		+reta*beta_2*etat[cc]*beta_2*uxxvxy
	                    		+etax[cc]*beta_2*(dux[cc]+dvy[cc])
	                    		+reta*beta_2*huxxhvxy;

					v1pp[cc]=(etay[cc]*etat[cc]*beta_2*beta_2+reta*beta_2*etaty[cc]*beta_2)
	                    		*(ux[cc]+vy[cc])
	                    		+reta*beta_2*etat[cc]*beta_2*uxyvyy
	                    		+etay[cc]*beta_2*(dux[cc]+dvy[cc])
	                    		+reta*beta_2*huxyhvyy;

				} else {
					u1pp[cc]=-reta*beta_2*etax[cc]*beta_2*(utx[cc]+vty[cc]) - 0.5*reta*reta*beta_2*beta_2*(utxx[cc]+vtxy[cc])
	                		  -etax[cc]*beta_2*(dutx[cc]+dvty[cc]) -reta*beta_2*(dutxx[cc]+dvtxy[cc]);

					v1pp[cc]=-reta*beta_2*etay[cc]*beta_2*(utx[cc]+vty[cc]) - 0.5*reta*reta*beta_2*beta_2*(utxy[cc]+vtyy[cc])
	                		  -etay[cc]*beta_2*(dutx[cc]+dvty[cc]) -reta*beta_2*(dutxy[cc]+dvtyy[cc]);
				}

				//  kennedy et al 2001;
				ken1=(beta_1-1.0)*(rhx+etax[cc])*beta_2;
				ken2=(beta_1-1.0)*(rh+reta)*beta_2;
				CCTK_REAL ken3=( (1.0-beta_1)*(1.0-beta_1)*rh*rhx*beta_2*beta_2-beta_1*(1.0-beta_1)*(rhx*reta*beta_2+rh*etax[cc]*beta_2)
						+(beta_1*beta_1-1.0)*reta*etax[cc]*beta_2*beta_2 );
				CCTK_REAL ken4=( 0.5*(1.0-beta_1)*(1.0-beta_1)*rh*rh*beta_2*beta_2-beta_1*(1.0-beta_1)*rh*reta*beta_2
						+0.5*(beta_1*beta_1-1.0)*reta*reta*beta_2*beta_2 );
				CCTK_REAL ken5=( (1.0-beta_1)*(1.0-beta_1)*rh*rhy*beta_2*beta_2-beta_1*(1.0-beta_1)*(rhy*reta*beta_2+rh*etay[cc]*beta_2)
						+(beta_1*beta_1-1.0)*reta*etay[cc]*beta_2*beta_2 );

				u2[cc]=ken1*(u[cc]*huxxhvxy+v[cc]*huxyhvyy)
	                		+ken2*(ux[cc]*huxxhvxy+u[cc]*huxxhvxy_x
	                				+vx[cc]*huxyhvyy+v[cc]*huxyhvyy_x)
	                				+ken3
	                				*(u[cc]*uxxvxy+v[cc]*uxyvyy)
	                				+ken4
	                				*(ux[cc]*uxxvxy+u[cc]*uxxvxy_x+vx[cc]*uxyvyy+v[cc]*uxyvyy_x)
	                				+beta_2*beta_2*(dux[cc]+dvy[cc]+reta*beta_2*(ux[cc]+vy[cc]))
	                				*(huxxhvxy+etax[cc]*beta_2*(ux[cc]+vy[cc])+reta*beta_2*uxxvxy);

				v2[cc]=ken1*(u[cc]*huxxhvxy+v[cc]*huxyhvyy)
	                		+ken2*(uy[cc]*huxxhvxy+u[cc]*huxxhvxy_y
	                				+vy[cc]*huxyhvyy+v[cc]*huxyhvyy_y)
	                				+ken5
	                				*(u[cc]*uxxvxy+v[cc]*uxyvyy)
	                				+ken4
	                				*(uy[cc]*uxxvxy+u[cc]*uxxvxy_y+vy[cc]*uxyvyy+v[cc]*uxyvyy_y)
	                				+beta_2*beta_2*(dux[cc]+dvy[cc]+reta*beta_2*(ux[cc]+vy[cc]))
	                				*(huxyhvyy+etay[cc]*beta_2*(ux[cc]+vy[cc])+reta*beta_2*uxyvyy);

				CCTK_REAL omega_0=vx[cc]-uy[cc];
				CCTK_REAL omega_1=(*b2)*rhx*beta_2*(huxyhvyy+(*b2)*rh*beta_2*uxyvyy)
	                		- (*b2)*rhy*beta_2*(huxxhvxy+(*b2)*rh*beta_2*uxxvxy);
				ken1=((beta_1-1.0/2.0)*(reta+rh)*beta_2);
				ken2=(1.0/3.0-beta_1+0.5*beta_1*beta_1)*rh*rh*beta_2*beta_2
						+ (1.0/6.0-beta_1+beta_1*beta_1)*rh*reta*beta_2
						+(1.0/2.0*beta_1*beta_1-1.0/6.0)*reta*reta*beta_2*beta_2;

				u3[cc]=-v[cc]*omega_1 - omega_0
						*(ken1*huxyhvyy
								+ken2*uxyvyy);
				v3[cc]=-v[cc]*omega_1 - omega_0
						*(ken1*huxxhvxy
								+ken2*uxxvxy);
			}
		}
	}
}
