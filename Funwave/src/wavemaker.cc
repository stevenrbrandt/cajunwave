#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "here.hh"
#include "funwave_util.hh"
#include "derivative.hh"
#include "cctk_declare.h"
#include "funwave_constants.hh"
#include <math.h>
#include <limits>
#include <fstream>

void readmatrix(CCTK_ARGUMENTS,const char *file,CCTK_REAL *inp);

void funwave_wavemaker_uvz(CCTK_ARGUMENTS);

double randgen() {
  static int init = 1;
  if(init) {
    srand(0);
    init = 0;
  }
  return (1.0*rand())/RAND_MAX;
}

/*
 "ini_rec" :: "initial rectangular hump, need xc,yc and wid"
 "lef_sol" :: "initial solitary wave, WKN B solution, need amp, dep, and xwavemaker"
 "ini_oth" :: "other initial distribution specified by users"
 "wk_reg" :: "Wei and Kirby 1999 internal wave maker, need xc_wk, tperiod, amp_wk, dep_wk, theta_wk, and time_ramp (factor of period)"
 "wk_irr" :: "Wei and Kirby 1999 TMA spectrum wavemaker, need xc_wk, dep_wk, time_ramp, delta_wk, freqpeak, freqmin, freqmax, hmo, gammatma, theta-peak"
 "wk_time_series" :: "fft a time series to get each wave component and then use Wei and Kirby’s ( 1999) wavemaker. Need input wavecompfile (including 3 columns:per,amp,pha) and numwavecomp, peakperiod, dep_wk, xc_wk, ywidth_wk"
 "ini_gau" :: "initial Gausian hump, need amp, xc, yc, and wid"
 */

void funwave_wavemaker_wkreg(CCTK_ARGUMENTS);
void funwave_wavemaker_inigau(CCTK_ARGUMENTS);
void funwave_wavemaker_inisol(CCTK_ARGUMENTS);
void funwave_wavemaker_irregular_wave(CCTK_ARGUMENTS);
void funwave_wavemaker_time_series(CCTK_ARGUMENTS);
void funwave_phi_coll(CCTK_ARGUMENTS, CCTK_REAL * phi, int vtype);

void funwave_init_wavemaker(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_func fdecl("funwave_init_wavemaker");
  if (CCTK_EQUALS(wavemaker_type, "wk_reg"))
  {
    funwave_wavemaker_wkreg( CCTK_PASS_CTOC);
  }
  else if (CCTK_EQUALS(wavemaker_type, "ini_gau"))
  {
    funwave_wavemaker_inigau( CCTK_PASS_CTOC);
  }
  else if (CCTK_EQUALS(wavemaker_type, "ini_sol"))
  {
	  funwave_wavemaker_inisol( CCTK_PASS_CTOC);
  }
  else if (CCTK_EQUALS(wavemaker_type, "wk_irr"))
  {
	  funwave_wavemaker_irregular_wave( CCTK_PASS_CTOC);
  }
  else if (CCTK_EQUALS(wavemaker_type, "wk_time_series"))
  {
	  funwave_wavemaker_time_series( CCTK_PASS_CTOC);
  }
  else if (CCTK_EQUALS(wavemaker_type, "ini_uvz"))
  {
    funwave_wavemaker_uvz( CCTK_PASS_CTOC);
  }
  else
  {
    CCTK_WARN(0, "specified wave maker is not supported !");
  }

}

void sub_sltry(double a0, double h1, double alp, double &r1, double &bue, double &ae1, double &ae2, double &au, double &C_ph) {
	double alp2 = alp + 1.0/3.0;
	double eps = a0/h1;
	const double g = 9.81;

	double p = -(alp2 + 2.0*alp*(1.0 + eps))/(2.0*alp);
	double q = eps*alp2/alp;
	double r = alp2/(2.0*alp);

	int ite = 0;
	double x = 1.2;
	double fx = r + x*(q + x*(p + x));
	double fpx = q + x*(2.0*p + 3.0*x);
	while(fabs(fx) > 1e-5) {
		fx = r + x*(q + x*(p + x));
		fpx = q + x*(2.0*p + 3.0*x);
		x = x - fx/fpx;
		if(ite > 10) {
			CCTK_WARN(0, "no solitary solution !");
			break;
		}
		++ite;
	}

	double rx = sqrt(x);
	double cph = sqrt(g*h1);
	r1 = rx*cph;
	C_ph = rx*cph;

	au = (x - 1.0)/(eps*rx)*cph*eps;
	bue = sqrt((x - 1.0)/(4.0*(alp2-alp*x)))/h1;
	ae1 = (x - 1.0)/(eps*3.0*(alp2 - alp*x))*a0;
	ae2 = -(x - 1.0)/(2.0*eps)*(x - 1.0)*(2.0*alp*x + alp2)/(x*(alp2 - alp*x))*a0;
}

void funwave_wavemaker_irregular_wave(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_SCALAR(width_wk);
	DECLARE_CCTK_VWRITES(cm);
	DECLARE_CCTK_VWRITES(sm);
	DECLARE_CCTK_WRITES(omgn_ir);
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dy);
	cctk_auto_thorn = "GRID";
	DECLARE_CCTK_READS(x);
	DECLARE_CCTK_READS(y);
	DECLARE_CCTK_PARAMETERS;
	CCTK_REAL phi,sigma_spec,fm,h_gen,etma,ef10000[10000];
	CCTK_REAL ef,fre,fmin,fmax,omiga_spec,ef100,ef_add;
	CCTK_INT kb,n_spec,ktotal,mcenter,i,cc,jend,jbeg;
	const CCTK_INT nfreq = num_frequencies, ntheta = 21;
	CCTK_REAL freq[nfreq],sigma_theta_calc,theta_1,ag[ntheta];
	CCTK_REAL theta_p,theta_m,theta_10,theta_11,theta_21,alpha_spec;
	CCTK_REAL hmo_each[ntheta],ap,theta,theta_input,alpha,alpha1;
	CCTK_REAL tb,tc,wkn,c_phase,wave_length;
	CCTK_REAL rlamda[nfreq][ntheta],d_gen[nfreq][ntheta],rl_gen;
	CCTK_REAL gamma_spec,ri,delta,beta_gen[nfreq],omgn_tmp,width;
	CCTK_REAL phi1[nfreq][ntheta],tmp1,tmp3,tmp2;
	const CCTK_REAL zero = 0.0;
	ef=0.0;
	// ---  get freq(100) and hmo_each;
  fm = freqpeak;
  fmin = freqmin;
  fmax = freqmax;
  h_gen = dep_wk;
  gamma_spec = gammatma;
  delta = delta_wk;
  theta_input = thetapeak;

	for(int kf=1;kf<=10000;kf++) {

		fre=fmin+(fmax-fmin)/10000.0*(kf-1.0);
		omiga_spec=2.0*M_PI*fre*sqrt(h_gen/grav);
		phi=1.0-0.5*pow(2.0-omiga_spec,2);
		if(omiga_spec <= 1.0) phi=0.5*pow(omiga_spec,2);
		if(omiga_spec >= 2.0) phi=1.0;

		sigma_spec=0.07;
		if(fre > fm)sigma_spec=0.09;

		etma=pow(grav,2)*pow(fre,-5)*pow(2.0*M_PI,-4)*phi
				*exp(-5.0/4.0*pow(fre/fm,-4))
		*pow(gamma_spec,exp(-pow(fre/fm-1.0,2)/(2.0*pow(sigma_spec,2))));

		ef=ef+etma*(fmax-fmin)/10000.0;
		ef10000[kf-1]=etma;

	}

	//---   get 100 frequecies;

	ef100=ef/101.0;
	//        print*, ef100, ef;
	kb=0;
	for(int kff=1;kff<=nfreq;kff++) {

		ef_add=0.0;
		for(int k=kb+1;k<=10000;k++) {

			ef_add=ef_add+ef10000[k-1]*(fmax-fmin)/10000.0;
			if (ef_add >= ef100 || k == 10000)  {

				freq[kff-1]=fmin+(fmax-fmin)/10000.0*(k-(k-kb)/2);

				kb=k;
				break;

			}

		}

	}

	// --- directional wave;
	sigma_theta_calc=sigma_theta*M_PI/180.0;
	n_spec=20.0/sigma_theta_calc;

	ktotal=ntheta;

	theta_1=-M_PI/3.0;
	ag[0]=(theta_1+M_PI)/(2.0*M_PI);
	for(int k_n=1;k_n<=n_spec;k_n++) {
		ag[0]=ag[0]+1.0/M_PI/k_n
				*exp(-pow(k_n*sigma_theta_calc,2)/2.0)
		*(sin(k_n*theta_1));
	}

	for(int ktheta=2;ktheta <= (ntheta-1)/2; ktheta++) {
		theta_p=theta_1+2.0/3.0*M_PI/(ktotal-1.0)*(ktheta-1.0);
		theta_m=theta_1+2.0/3.0*M_PI/(ktotal-1.0)*(ktheta-2.0);
		ag[ktheta-1]=(theta_p-theta_m)/(2.0*M_PI);
		for(int k_n=1;k_n <= n_spec;k_n++) {
			ag[ktheta-1]=ag[ktheta-1]+1.0/M_PI/k_n
					*exp(-pow(k_n*sigma_theta_calc,2)/2.0)
			*(sin(k_n*theta_p)-sin(k_n*theta_m));
		}
	}

	theta_10=-2.0/3.0*M_PI/(ktotal-1.0);
	theta_11=2.0/3.0*M_PI/(ktotal-1.0);
	mcenter=(ntheta-1)/2+1;
	ag[mcenter-1]=(theta_11-theta_10)/(2.0*M_PI);
	for(int k_n=1;k_n <= n_spec;k_n++) {
		ag[mcenter-1]=ag[mcenter-1]+1.0/M_PI/k_n
				*exp(-pow(k_n*sigma_theta_calc,2)/2.0)
		*(sin(k_n*theta_11)-sin(k_n*theta_10));
	}

	for(int ktheta=mcenter+1;ktheta <= ntheta-1;ktheta++) {
		theta_p=theta_1+2.0/3.0*M_PI/(ktotal-1.0)*(ktheta-0.0);
		theta_m=theta_1+2.0/3.0*M_PI/(ktotal-1)*(ktheta-1.0);
		ag[ktheta-1]=(theta_p-theta_m)/(2.0*M_PI);
		for(int k_n=1;k_n <= n_spec;k_n++) {
			ag[ktheta-1]=ag[ktheta-1]+1.0/M_PI/k_n
					*exp(-pow(k_n*sigma_theta_calc,2)/2.0)
			*(sin(k_n*theta_p)-sin(k_n*theta_m));
		}
	}

	theta_21=M_PI/3.0;
	ag[ntheta-1]=(M_PI-theta_21)/(2.0*M_PI);
	for(int k_n=1;k_n <= n_spec;k_n++) {
		ag[ntheta-1]=ag[ntheta-1]+1.0/M_PI/k_n
				*exp(-pow(k_n*sigma_theta_calc,2)/2.0)
		*(sin(-k_n*theta_21));
	}


	alpha_spec=pow(hmo,2)/16.0/ef;

	int ktheta;
	for(ktheta=1;ktheta <= ntheta;ktheta++) {
		hmo_each[ktheta-1]=4.0*sqrt((alpha_spec*ef100*ag[ktheta-1]));
	}
	// ---  wave generation parameter;
	for(int kf=1;kf <= nfreq;kf++) {

		for(ktheta=1;ktheta <= ntheta;ktheta++) {

			// here should convert hmo to hrms;
			//  hrms=1.0/sqrt(2)*hmo;
			// 05/12/2011 joe has reported that the conversion should be removed;
			//        ap=hmo_each(ktheta)/sqrt(2.0)/2.0;
			ap=hmo_each[ktheta-1]/2.0;
			theta=-M_PI/3.0+theta_input*M_PI/180.0+2.0/3.0*M_PI/ktotal*ktheta;
			alpha=-0.39;
			alpha1=alpha+1.0/3.0;
      CCTK_REAL omgn_tmp = 2.0*M_PI*freq[kf-1];
			omgn_ir[kf-1] = omgn_tmp;

			tb=omgn_tmp*omgn_tmp*h_gen/grav;
			tc=1.0+tb*alpha;
			wkn=sqrt((tc-sqrt(tc*tc-4.0*alpha1*tb))/(2.0*alpha1))/h_gen;

			// here i use fixed c_phase and wave_length to determine beta_gen;
			// as suggested by wei and kirby 1999;

			//        c_phase=1.0/wkn*freq(kf)*2.0*M_PI;
			//        wave_length=c_phase/freq(kf);

			c_phase=1.0/wkn*fm*2.0*M_PI;
			wave_length=c_phase/fm;

			// for periodic boundary conditions;
			// ________________;

			if (false && periodic) {
				tmp1=wkn;
				if (theta > zero) {
					tmp3=zero;
					i=0;
					while (tmp3<theta) {
						i=i+1;
						tmp2=i*2.0*M_PI/CCTK_DELTA_SPACE(1)/(jend-jbeg);     // rlamda;
						if (tmp2 >= tmp1) {
							tmp3=M_PI/2.0;
						} else {
							tmp3=asin(tmp2/tmp1);      // theta, based on rlamda=wkn*sin(theta);
						}
						if (i>1000) {
							CCTK_Warn(1,__LINE__,__FILE__,CCTK_THORNSTRING,"could not find a wave angle for periodic boundary condition, stop");
						}
					}
				}
				if(tmp2 < tmp1) tmp3=asin((i-1)*2.0*M_PI/dy[cc]/(jend-jbeg)/tmp1);
				else if(theta < zero) {
					tmp3=zero;
					i=0;
					while (tmp3>theta) {
						i=i+1;
						tmp2=i*2.0*M_PI/CCTK_DELTA_SPACE(1)/(jend-jbeg);     // rlamda;
						if (tmp2 >= tmp1) {
							tmp3=-M_PI/2.0;
						} else {
							tmp3=-asin(tmp2/tmp1);      // theta, based on rlamda=wkn*sin(theta);
						}
						if (i>1000) {
							CCTK_Warn(1,__LINE__,__FILE__,CCTK_THORNSTRING,"could not find a wave angle for periodic boundary condition, stop");
						}
					}
					if(tmp2 < tmp1) tmp3=-asin((i-1)*2.0*M_PI/CCTK_DELTA_SPACE(1)/(jend-jbeg)/tmp1);
				}
				theta = tmp3;
			}
			// ________________;

			rlamda[kf-1][ktheta-1]=wkn*sin(theta);
			beta_gen[kf-1]=80.0/pow(delta,2)/pow(wave_length,2);

			rl_gen=wkn*cos(theta);
			ri=sqrt(M_PI/beta_gen[kf-1])*exp(-pow(rl_gen,2)/4.0/beta_gen[kf-1]);

			d_gen[kf-1][ktheta-1]=2.0*ap*cos(theta)
				*(pow(omgn_tmp,2)-alpha1*grav*pow(wkn,4)*pow(h_gen,3))
				/(omgn_tmp*wkn*ri*(1.0-alpha*pow(wkn*h_gen,2)));

		}
	}

	// calculate wavemaker width;
	omgn_tmp=2.0*M_PI*fm;
	tb=omgn_tmp*omgn_tmp*h_gen/grav;
	tc=1.0+tb*alpha;
	wkn=sqrt((tc-sqrt(tc*tc-4.0*alpha1*tb))/(2.0*alpha1))/h_gen;
	//        c_phase=1.0/wkn*fm*2.0*M_PI;
	//        wave_length=c_phase/fm;
	width=delta*wave_length/2.0;
	// ---   create phi1;

  for(int ktheta=1;ktheta <= ntheta;ktheta++) {
    for(int kf=1;kf <= nfreq;kf++) {
      phi1[kf-1][ktheta-1]=randgen()*2.0*M_PI;
    }
  }
  *width_wk = width;

  // Set SM and CM
  for(int j=0;j<cctk_lsh[1];++j) {
    for(int i=0;i<cctk_lsh[0];++i) {
      int cc = FUNWAVE_INDEX(i,j);
      CCTK_REAL yv = y[cc];
      CCTK_REAL xv = x[cc];
      for(int kf=1;kf<=nfreq;++kf) {
        int kc = CCTK_VECTGFINDEX3D(cctkGH,i,j,zvalue,kf-1);
        sm[kc] = 0.0;
        cm[kc] = 0.0;
        for(int ktheta=1;ktheta<=ntheta;++ktheta) {
          CCTK_REAL bg = beta_gen[kf-1];
          CCTK_REAL fac = d_gen[kf-1][ktheta-1]*exp(-bg*pow(xv-xc_wk,2));
          CCTK_REAL rl = rlamda[kf-1][ktheta-1];
          CCTK_REAL p1 = phi1[kf-1][ktheta-1];
          cm[kc] += fac*cos(rl*(yv-zero)+p1);
          sm[kc] += fac*sin(rl*(yv-zero)+p1);
        }
      }
    }
  }
}

/* initial Gaussian hump, need amp, xc, yc, and wid */
void funwave_wavemaker_inigau(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(eta);
  DECLARE_CCTK_WRITES(u);
  DECLARE_CCTK_WRITES(h);
  DECLARE_CCTK_READS(depth);
  cctk_auto_thorn = "GRID";
  DECLARE_CCTK_READS(x);
  DECLARE_CCTK_READS(y);
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL radius;
  cctk_func fdecl("funwave_wavemaker_inigau");

  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      eta [cc] = 0.0;
      u[cc] = 0.0;
      radius = sqrt((x[cc] - xc)*(x[cc] - xc) + (y[cc] - yc)*(y[cc] - yc));
      eta[cc] = amp * exp(-pow(radius, 2) / 2.0 / pow(wid, 2));
      h[cc] = depth[cc] + eta[cc];
    }
  }
 
}

/* initial Solitary wave */
void funwave_wavemaker_inisol(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_auto_thorn = "FUNWAVE";
  cctk_func fdecl("funwave_wavemaker_inisol");
	DECLARE_CCTK_WRITES(uxx);
	DECLARE_CCTK_WRITES(uxy);
	DECLARE_CCTK_WRITES(duxx);
	DECLARE_CCTK_WRITES(duxy);
	DECLARE_CCTK_WRITES(eta);
	DECLARE_CCTK_READWRITES(h);
	DECLARE_CCTK_READWRITES(hu);
	DECLARE_CCTK_READWRITES(hv);
	DECLARE_CCTK_WRITEREADS(du);
	DECLARE_CCTK_WRITEREADS(u);
	DECLARE_CCTK_WRITES(v);
	DECLARE_CCTK_WRITES(ubar);
	DECLARE_CCTK_READS(u1p);
	DECLARE_CCTK_WRITES(vbar);
	DECLARE_CCTK_READS(v1p);
	DECLARE_CCTK_READS(mask);
	DECLARE_CCTK_WRITES(mask9);
	DECLARE_CCTK_READS(depth);
	cctk_auto_thorn = "GRID";
	DECLARE_CCTK_READS(x);
	DECLARE_CCTK_PARAMETERS;
	cctk_auto_thorn = "FUNWAVECOORD";
	DECLARE_CCTK_READS(dx);

	CCTK_REAL Cph, B, A1, A2, A, C_ph;
	CCTK_REAL AMP_Soli = amp;
	CCTK_REAL Dep_Soli = wdep;
	CCTK_REAL alpha = -0.39;

	sub_sltry(AMP_Soli, Dep_Soli, alpha, Cph, B, A1, A2, A, C_ph);

	CCTK_REAL x0 = xmin - cctk_nghostzones[0]*dx[0];

	for (int j = 0; j < cctk_lsh[1]; j++)
	{
		for (int i = 0; i < cctk_lsh[0]; i++)
		{
			int cc = FUNWAVE_INDEX( i, j);
			CCTK_REAL sc = 1.0/cosh(B*(x[cc]-xwavemaker-x0));
			CCTK_REAL sc2 = sc*sc;
			CCTK_REAL sc4 = sc2*sc2;
			CCTK_REAL etav = A1*sc2+A2*sc4;
			eta[cc] = etav;

			CCTK_REAL hval = std::max(etav+gamma3*depth[cc],mindepthfrc);
			h[cc] = hval;

			CCTK_REAL uv = A*sc2;
			u[cc] = uv;
			du[cc] = uv*std::max(depth[cc],mindepthfrc);

			CCTK_REAL huval = mask[cc]*uv*hval;
			hu[cc] = huval;
			v[cc] = 0.0;
			hv[cc] = 0.0;
			mask9[cc] = 1;

			ubar[cc] = huval+gamma1*u1p[cc]*hval;
			vbar[cc] = huval+gamma1*v1p[cc]*hval;
		}
	}
	derivative_xx_high(CCTK_PASS_CTOC,(CCTK_READ_ARG)u,(CCTK_WRITE_ARG)uxx);
	derivative_xy_high(CCTK_PASS_CTOC,(CCTK_READ_ARG)u,(CCTK_WRITE_ARG)uxy);
	derivative_xx_high(CCTK_PASS_CTOC,(CCTK_READ_ARG)du,(CCTK_WRITE_ARG)duxx);
	derivative_xy_high(CCTK_PASS_CTOC,(CCTK_READ_ARG)du,(CCTK_WRITE_ARG)duxy);
	funwave_phi_coll(CCTK_PASS_CTOC,uxx,2);
}

void funwave_wavemaker2(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_auto_thorn = "FUNWAVE";
  cctk_func fdecl("funwave_wavemaker2");
	DECLARE_CCTK_WRITES(ubar);
	DECLARE_CCTK_WRITES(vbar);
	DECLARE_CCTK_READS(hu);
	DECLARE_CCTK_READS(hv);
	DECLARE_CCTK_READS(h);
	DECLARE_CCTK_READS(u1p);
	DECLARE_CCTK_READS(v1p);
	DECLARE_CCTK_PARAMETERS;
	for (int j = 0; j < cctk_lsh[1]; j++)
	{
		for (int i = 0; i < cctk_lsh[0]; i++)
		{
			int cc = FUNWAVE_INDEX( i, j);
			ubar[cc] = hu[cc]+gamma1*u1p[cc]*h[cc];
			vbar[cc] = hv[cc]+gamma1*v1p[cc]*h[cc];
		}
	}
}

/* Wei and Kirby 1999 internal wave maker, need xc_wk, tperiod,
 * amp_wk, dep_wk, theta_wk, and time_ramp (factor of period) */
void funwave_wavemaker_wkreg(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_SCALAR(rlamda);
  DECLARE_CCTK_SCALAR(width_wk);
  DECLARE_CCTK_SCALAR(beta_gen);
  DECLARE_CCTK_SCALAR(d_gen);
  CCTK_REAL theta = theta_wk * M_PI / 180.0;
  CCTK_REAL alpha = -0.39;
  CCTK_REAL alpha1 = alpha + 1.0 / 3.0;
  CCTK_REAL omgn = 2.0 * M_PI / tperiod;
  CCTK_REAL tb = omgn * omgn * dep_wk / grav;
  CCTK_REAL tc = 1.0 + tb * alpha;
  CCTK_REAL wkn, c_phase, wave_length, rl_gen, ri;

  if (dep_wk == 0.0 || tperiod == 0.0)
  {
    CCTK_WARN(0, "re-set depth, tperiod for wavemaker, stop //");
  }
  else
  {
    wkn = sqrt((tc - sqrt(tc * tc - 4.0 * alpha1 * tb)) / (2.0 * alpha1))
        / dep_wk;
    c_phase = 1.0 / wkn / tperiod * 2.0 * M_PI;
  }
  wave_length = c_phase * tperiod;

  *rlamda = wkn * sin(theta);
  *width_wk = delta_wk * wave_length / 2.0;
  *beta_gen = 80.0 / pow(delta_wk, 2) / pow(wave_length, 2);
  rl_gen = wkn * cos(theta);
  ri = sqrt(M_PI / *beta_gen) * exp(-pow(rl_gen, 2) / 4. / *beta_gen);

  *d_gen = 2.0 * amp_wk * cos(theta) * (pow(omgn, 2) - alpha1 * grav * pow(wkn,
      4) * pow(dep_wk, 3)) / (omgn * wkn * ri * (1.0 - alpha * pow(
      wkn * dep_wk, 2)));
}

void funwave_wavemaker_uvz(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_func fdecl("funwave_wavemaker_uvz");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(eta);
  DECLARE_CCTK_READWRITES(u);
  DECLARE_CCTK_READWRITES(v);
  //void readmatrix(CCTK_ARGUMENTS,const char *file,CCTK_REAL *inp);
  readmatrix(CCTK_PASS_CTOC,u_file,u);
  readmatrix(CCTK_PASS_CTOC,v_file,v);
  readmatrix(CCTK_PASS_CTOC,z_file,eta);
}

void funwave_wavemaker_time_series(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_func fdecl("funwave_wavemaker_time_series");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(wave_comp1);
  DECLARE_CCTK_WRITEREADS(wave_comp2);
  DECLARE_CCTK_WRITEREADS(wave_comp3);
  DECLARE_CCTK_READWRITES(beta_gens);
  DECLARE_CCTK_READWRITES(d_gens);
  DECLARE_CCTK_READS(depth);
  DECLARE_CCTK_WRITES(h);
  DECLARE_CCTK_SCALAR(width_wk);
//!        theta=Theta_WK*pi/180._SP
  CCTK_REAL theta = 0.0; //        theta = ZERO  ! assume zero because no or few cases include directions
  CCTK_REAL alpha = -.39; //        alpha=-0.39_SP
  CCTK_REAL alpha1 = alpha+1./3.;//        alpha1=alpha+1.0_SP/3.0_SP
  CCTK_REAL h_gen = dep_wk;
  CCTK_REAL delta = delta_wk;
  std::ifstream f(wave_component_file);
  assert(f.good());
  for(int i=0;i<num_wave_components;i++) {//       DO I=1,NumWaveComp
    CCTK_REAL tmp;
    f >> tmp; wave_comp1[i] = tmp;
    f >> tmp; wave_comp2[i] = tmp;
    f >> tmp; wave_comp3[i] = tmp;
  }
  f.close();

  // Things needed by evo.cc
  // time_ramp, peak_period, wave_comp
  // As near as I can tell, this only needs/uses
  // a few of the things it computes

  for(int i=0;i<num_wave_components;i++) {//       DO I=1,NumWaveComp
    CCTK_REAL omgn    = 2.0*M_PI/wave_comp1[i];//        omgn=2.0_SP*pi/Wave_COMP(I,1)
    CCTK_REAL tperiod = wave_comp1[i];//        Tperiod = Wave_COMP(I,1)
    CCTK_REAL amp_wk = wave_comp2[i];//        AMP_WK = WAVE_COMP(I,2)

    CCTK_REAL tb=omgn*omgn*h_gen/grav; //        tb=omgn*omgn*h_gen/grav
    CCTK_REAL tc=1.0+tb*alpha; //        tc=1.0_SP+tb*alpha
    CCTK_REAL c_phase, wkn;
    if(h_gen == 0 || tperiod == 0) {//        IF(h_gen==ZERO.OR.Tperiod==ZERO)THEN
      abort();//         WRITE(*,*)'re-set depth, Tperiod for wavemaker, STOP!'
      //         STOP
    } else {//        ELSE
      wkn=sqrt((tc-sqrt(tc*tc-4*alpha1*tb))/(2.0*alpha1))/h_gen;//   wkn=SQRT((tc-SQRT(tc*tc-4.0_SP*alpha1*tb))  &
        //                /(2.0_SP*alpha1))/h_gen
      c_phase=2.0*M_PI/wkn/tperiod; //          C_phase=1.0_SP/wkn/Tperiod*2.0_SP*pi
    } //        ENDIF
    CCTK_REAL wave_length = c_phase*tperiod;//        wave_length=C_phase*Tperiod

    // Unused
    //        rlamda=wkn*sin(theta)
    //!        width=delta*wave_length/2.0_SP
    beta_gens[i]=80.0/delta/delta/wave_length/wave_length;//        beta_gen(I)=80.0_SP/delta**2/wave_length**2
    CCTK_REAL rl_gen=wkn*cos(theta);//        rl_gen=wkn*cos(theta)
    CCTK_REAL ri=sqrt(M_PI/beta_gens[i])*exp(-rl_gen*rl_gen/4.0/beta_gens[i]);// rI=SQRT(pi/beta_gen(I))*exp(-rl_gen**2/4.0_SP/beta_gen(I))

    d_gens[i]=2.0*amp_wk   //        D_gen(I)=2.0_SP*AMP_WK  &
      *cos(theta)*(omgn*omgn-alpha1*grav*pow(wkn,4)*pow(h_gen,3)) //*cos(theta)*(omgn**2-alpha1*grav*wkn**4*h_gen**3) &
      /(omgn*wkn*ri*(1.0-alpha*pow(wkn*h_gen,2)));//            /(omgn*wkn*rI*(1.0_SP-alpha*(wkn*h_gen)**2))

  }//       ENDDO

  //! calculate width
  CCTK_REAL omgn_tmp=2.0*M_PI/peak_period; //        omgn_tmp=2.0_SP*pi/PeakPeriod
  CCTK_REAL tb=omgn_tmp*omgn_tmp*h_gen/grav;//        tb=omgn_tmp*omgn_tmp*h_gen/grav
  CCTK_REAL tc=1.0+tb*alpha;//        tc=1.0_SP+tb*alpha
  CCTK_REAL wkn=sqrt((tc-sqrt(tc*tc-4.0*alpha1*tb))/(2.0*alpha1))/h_gen;// wkn=SQRT((tc-SQRT(tc*tc-4.0_SP*alpha1*tb))/(2.0_SP*alpha1))/h_gen
  assert(wkn != 0);
  assert(!std::isnan(tc));
  assert(!std::isnan(tb));
  assert(!std::isnan(h_gen));
  assert(!std::isnan(alpha1));
  assert(!std::isnan(wkn));
  CCTK_REAL c_phase=2.0*M_PI/wkn/peak_period;//        C_phase=1.0_SP/wkn/PeakPeriod*2.0_SP*pi
  CCTK_REAL wave_length=c_phase*peak_period;//        wave_length=C_phase*PeakPeriod
  *width_wk=delta*wave_length/2.0;//        width=delta*wave_length/2.0_SP
  assert(!std::isnan(wave_length));
  assert(!std::isnan(*width_wk));
  for(int j=0;j<cctk_lsh[1];j++) {
    for(int i=0;i<cctk_lsh[0];i++) {
      int cc = FUNWAVE_INDEX(i,j);
      h[cc] = depth[cc];
    }
  }
}
