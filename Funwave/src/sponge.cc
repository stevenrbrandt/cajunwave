#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "cctk_declare.h"
#include "here.hh"

void funwave_init_sponge(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_init_sponge");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(sponge);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  DECLARE_CCTK_READS(dy);
  DECLARE_CCTK_PARAMETERS;
  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);

      sponge[cc] = 1.0;
    }
  }
  if (!sponge_on)
    return;
  const int ndel = 3;

  if (sponge_west_width > 0 && cctk_bbox[0])
  {
	  int cc = FUNWAVE_INDEX(0,0);
    int iwidth = int(sponge_west_width / dx[cc]) + ndel;

    for (int i = 0; i < iwidth && i < cctk_lsh[0]; i++)
    {
      for (int j = 0; j < cctk_lsh[1]; j++)
      {
        cc = FUNWAVE_INDEX( i, j);
        CCTK_REAL lim = max(static_cast<CCTK_REAL>(sponge[cc]), 1.0);
        CCTK_REAL ri =
          pow(sponge_decay_rate, 50 * i / (iwidth-1));

        sponge[cc] = max(pow(sponge_damping_magnitude, ri), lim);
      }
    }
  }
  if (sponge_east_width > 0 && cctk_bbox[1])
  {
	int cc = FUNWAVE_INDEX(0,0);
    int iwidth = int(sponge_east_width / dx[cc]) + ndel;

    for (int i = max(0, cctk_lsh[0] - iwidth); i < cctk_lsh[0]; i++)
    {
      for (int j = 0; j < cctk_lsh[1]; j++)
      {
        cc = FUNWAVE_INDEX( i, j);
        CCTK_REAL lim = max((CCTK_REAL)sponge[cc], 1.0);
        CCTK_REAL ri =
            pow(sponge_decay_rate, 50 * (cctk_lsh[0] - i-1) / (iwidth-1));

        sponge[cc] = max(pow(sponge_damping_magnitude, ri), lim);
      }
    }
  }
  if (sponge_south_width > 0.0 && cctk_bbox[2])
  {
	int cc = FUNWAVE_INDEX(0,0);
    int iwidth = int(sponge_south_width / dy[cc]) + ndel;

    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      for (int j = 0; j < iwidth && j < cctk_lsh[1]; j++)
      {
        cc = FUNWAVE_INDEX( i, j);
        CCTK_REAL lim = max((CCTK_REAL)sponge[cc], 1.0);
        CCTK_REAL ri = pow(sponge_decay_rate, 50 * j / (iwidth-1));

        sponge[cc] = max(pow(sponge_damping_magnitude, ri), lim);
      }
    }
  }
  if (sponge_north_width > 0.0 && cctk_bbox[3])
  {
	int cc = FUNWAVE_INDEX(0,0);
    int iwidth = int(sponge_north_width / dy[cc]) + ndel;

    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      for (int j = max(0, cctk_lsh[1] - iwidth); j < cctk_lsh[1]; j++)
      {
        cc = FUNWAVE_INDEX( i, j);
        CCTK_REAL lim = max((CCTK_REAL)sponge[cc], 1.0);
        CCTK_REAL ri =
            pow(sponge_decay_rate, 50 * (cctk_lsh[1] - j-1) / (iwidth-1));

        sponge[cc] = max(pow(sponge_damping_magnitude, ri), lim);
      }
    }
  }
}

void funwave_sponge (CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_sponge");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(eta);
  DECLARE_CCTK_READWRITES(u);
  DECLARE_CCTK_READWRITES(v);
  DECLARE_CCTK_READS(sponge);
  DECLARE_CCTK_READS(mask);
  DECLARE_CCTK_PARAMETERS;
  if (!sponge_on)
    return;

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX(i, j);

      if (mask[cc] > 0.5)
      {
        eta[cc] /= sponge[cc];
      }
      u[cc] /= sponge[cc];
      v[cc] /= sponge[cc];
    }
  }
}
