#include <cmath>
#include <cstdio>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include <assert.h>
#include "cctk_declare.h"
//#include "here.hh"

void funwave_update_height_max (CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_update_height_max");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(eta);
  DECLARE_CCTK_READWRITES(height_max);
  DECLARE_CCTK_READS(mask);
  DECLARE_CCTK_PARAMETERS;

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
	  for (int j = 0; j < cctk_lsh[1]; j++)
	  {
		  int cc = FUNWAVE_INDEX(i, j);
		  if(eta[cc] > height_max[cc] && mask[cc]>0.5)
        height_max[cc] = eta[cc];
      else
        MARK(height_max[cc]);
	  }
  }
}
