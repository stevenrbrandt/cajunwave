#ifndef AUTOSCHED_H
#define AUTOSCHED_H

#include <assert.h>
#include "cctk_Groups.h"
#include <cstring>
#include <iostream>
#include "math.h"
#include <limits>
#include "cctk_core.h"

const char * const CCTK_EMPTY = "$empty";
extern std::string cctk_func_stack;
extern const char *cctk_auto_thorn;
extern int func_count;


// Use our own nan testing function and
// a template specialization for double
// so that we can use the same templatized
// class for WriArray<T> with T=double
// and T=int
template<typename T>
inline bool is_it_nan(const T x) { return false; }

template<>
inline bool is_it_nan(const double x) {
	using namespace std;
	return isnan(x);
	return isinf(x);
}

//#define TRACE_ON 1
//#define CCTK_DEBUG 1

#ifdef CCTK_DEBUG
#define ASSERT_(X) assert(X)
#else
#define ASSERT_(X) 
#endif

class cctk_func {
//	const char *func;
	std::string func;
public:
	cctk_func(const char *func_) : func(cctk_func_stack) {
		cctk_func_stack += "::";
		cctk_func_stack += func_;
#ifdef TRACE_ON
    std::cout << "cctk_exec: " << func_ << std::endl;
#endif
//		std::cout << "EXEC:: " << cctk_func_stack << std::endl;
	}
	~cctk_func() {
		cctk_func_stack = func;
	}
};

extern "C" void *CCTKi_VarDataPtr(const cGH *GH, int timelevel,
                       const char *implementation, const char *varname);

//extern "C" CCTK_REAL *CCTK_RVarDataPtrMask(const cGH *GH,int timelevel,const char *,const char *,const int mask);

#ifdef CCTK_DEBUG

void line_calc_(const cGH *GH,int cv);

template<typename T>
inline void nanmap(const cGH *GH,const T *var,const char *varname,int n) {}

template<>
inline void nanmap(const cGH *GH,const CCTK_REAL *var,const char *varname,int n) {
	using namespace std;
	int k = GH->cctk_lsh[2]>>1;
	std::cout << "NANMAP: " << varname << std::endl;
	for(int j=0;j < GH->cctk_lsh[1];j++) {
		for(int i=0;i < GH->cctk_lsh[0];i++) {
			int cc = CCTK_GFINDEX3D(GH,i,j,k);
			if(is_it_nan(var[cc])) {
				std::cout << (cc == n ? "X" : "*");
			} else {
				std::cout << (cc == n ? "O" :".");
			}
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

//#define CCTK_NDEBUG 1
typedef std::numeric_limits<double> Fp;
void poison(const cGH *GH,CCTK_REAL *var,const char *varname,const char *file,int line);

template<typename T>
class WriElem {
    T *data;
    int n;
public:
    WriElem(T *data_,int n_) : data(data_), n(n_) {}
    ~WriElem() {
    }
    void operator=(const T t) {
    	using namespace std;
    	//ASSERT_(isnan(data[n]));
    	data[n] = t;
      ASSERT_(!is_it_nan(data[n]));
    }
};

template<typename T>
class RdWriArray;

template<typename T>
void cksum(const cGH *GH,const char *,T *,const char *);

template<typename T>
class WriArray {
	const cGH *GH;
    T *data;
    const char *thorn;
    std::string func;
    const char *var;
    T& operator*() { return data[0]; }
public:
    WriArray(const cGH *GH_,T *data_,const char *var_) : GH(GH_), data(data_), thorn(cctk_auto_thorn), func(cctk_func_stack), var(var_) {
    	if(data == NULL) {
    		std::cout << cctk_auto_thorn << "::" << var << std::endl;
    		//ASSERT_(false);
    	}
    }
    WriArray(RdWriArray<T>& rwt);
    ~WriArray() {
      if(data != 0)
        cksum(GH,var,data,"w");
    }

    WriElem<T> operator[](int n) {
        WriElem<T> w(data,n);
        return w;
    }

    operator T*() const {
    	return data;
    }
    bool isdef(int n) {
    	using namespace std;
    	return !is_it_nan(data[n]);
    }
};

template<typename T>
class RdArray {
	const cGH *GH;
    T *data;
    const char *thorn;
    std::string func;
    const char *var;
    T& operator*() { return data[0]; }
public:
    RdArray(const cGH *GH_,T *data_,const char *var_) : GH(GH_), data(data_), thorn(cctk_auto_thorn), func(cctk_func_stack), var(var_) {
      if(data != 0)
        cksum(GH,var,data,"r");
    }
    RdArray(RdWriArray<T>& rw);

    const T operator[](int n) {
    	using namespace std;
      ASSERT_(data != NULL);
    	bool b = is_it_nan(data[n]);
    	if(b) {
    		nanmap(GH,data,var,n);
    		line_calc_(GH,n);
    		std::cout << "(1)Nan detected in " << thorn << "::" << var << "[" << n << "]" << std::endl;
    		ASSERT_(false);
    	}
    	return data[n];
    }
    operator const T*() const {
    	return data;
    }
    bool isdef(int n) {
    	using namespace std;
    	return !is_it_nan(data[n]);
    }
};

template<typename T>
class RdWriArray;

template<typename T>
class RdWriElem {
	const cGH *GH;
	const char *thorn;
	const char *var;
  T *data;
  int n;
  RdWriArray<T> *arr;
public:
    RdWriElem(const cGH *GH_,T *data_,int n_,const char *t,const char *v,RdWriArray<T> *arr_) : GH(GH_), thorn(t), var(v), data(data_), n(n_), arr(arr_) {}
    ~RdWriElem() {
    }
    void operator=(T t) {
      data[n] = t;
      arr->mark(n);
    }
    void operator=(const RdWriElem<T>& rdwr) {
    	data[n] = rdwr.data[rdwr.n];
    	bool b = is_it_nan(data[n]);
    	if(b) {
    		std::cout << "(5)Nan detected in " << thorn << "::" << rdwr.var << "[" << rdwr.n << "]" << std::endl;
    		ASSERT_(false);
    	}
      arr->mark(n);
    }
    void operator+=(T t) {
    	bool b = is_it_nan(data[n]);
    	if(b) {
    		std::cout << "(2)Nan detected in " << thorn << "::" << var << "[" << n << "]" << std::endl;
    		ASSERT_(false);
    	}
      data[n] += t;
      arr->mark(n);
    }
    void operator/=(T t) {
    	bool b = is_it_nan(data[n]);
    	if(b) {
    		std::cout << "(3)Nan detected in " << thorn << "::" << var << "[" << n << "]" << std::endl;
    		ASSERT_(false);
    	}
      data[n] /= t;
      arr->mark(n);
    }
    operator const T() {
    	bool b = is_it_nan(data[n]);
    	if(b) {
    		nanmap(GH,data,var,n);
    		line_calc_(GH,n);
    		std::cout << "(4)Nan detected in " << thorn << "::" << var << "[" << n << "]" << std::endl;
    		ASSERT_(false);
    	}
    	return data[n];
    }
};

int getx(const cGH *,int);
int gety(const cGH *,int);

template<typename T>
class RdWriArray {
    std::string func;
    bool writeFirst;
    T& operator*() { return data[0]; }
    int lox,hix,loy,hiy;
public:
    const cGH *GH;
    T *data;
    const char *var;
    const char *thorn;
    RdWriArray(const cGH *GH_,T *data_,const char *var_,bool writeFirst_)
		: GH(GH_), data(data_), thorn(cctk_auto_thorn), func(cctk_func_stack), var(var_), writeFirst(writeFirst_) {
      lox = GH->cctk_lsh[0];
      loy = GH->cctk_lsh[1];
      hix = 0;
      hiy = 0;
    }
    ~RdWriArray() {
      if(data != 0 && lox <= hix) {
        assert(GH != 0);
        /*
        std::cout << "POISONING " << thorn << ":" << var << std::endl;
        assert(lox <= 3);
        assert(loy <= 3);
        if(hix+1 < GH->cctk_lsh[0]-3) {
          std::cout << "lox=" << lox << " hix=" << hix << " >= " << (GH->cctk_lsh[0]-3) << std::endl;
        }
        assert(hix+1 >= GH->cctk_lsh[0]-3);
        assert(hiy+1 >= GH->cctk_lsh[1]-3);
        for(int j=0;j<GH->cctk_lsh[1];j++) {
          for(int i=0;i<GH->cctk_lsh[0];i++) {
            if(i >= lox && i <= hix && j >= loy && j <= hiy) {
              ;//interior
            } else {
              int cc = CCTK_GFINDEX3D(GH,i,j,(GH->cctk_lsh[2]>>1));
              assert(i == getx(GH,cc));
              assert(j == gety(GH,cc));
              CCTK_REAL n = nan("17");
              data[cc] = n;
            }
          }
        }
        */
      } else {
        //std::cout << "Not poisoning " << thorn << ":" << var << " data=" << data << " lox=" << lox << " hix=" << hix << std::endl;
      }
      if(data != 0)
        cksum(GH,var,data,"w");
    }

    RdWriElem<T> operator[](int n) {
      ASSERT_(data != NULL);
      int x = getx(GH,n);
      int y = gety(GH,n);
      assert(x >= 0);
      assert(y >= 0);
      assert(x < GH->cctk_lsh[0]);
      assert(y < GH->cctk_lsh[1]);
    	RdWriElem<T> elem(GH,data,n,thorn,var,this);
      return elem;
    }
    void mark(int n) {
      int x = getx(GH,n);
      int y = gety(GH,n);
      if(x < lox) lox = x;
      if(x > hix) hix = x;
      if(y < loy) loy = y;
      if(y > hiy) hiy = y;
    }
    operator T*() const {
    	return data;
    }
    operator const T*() const {
    	return data;
    }
    bool isdef(int n) {
    	using namespace std;
    	return !is_it_nan(data[n]);
    }
};
template<typename T>
inline std::ostream& operator<<(std::ostream& o,const RdWriArray<T>& rw) {
	return o << (T*)rw;
}

template<typename T>
inline WriArray<T>::WriArray(RdWriArray<T>& rw) : GH(rw.GH), data(rw.data), var(rw.var), thorn(rw.thorn) {
	poison(GH,data,var,__FILE__,__LINE__);
}
template<typename T>
inline RdArray<T>::RdArray(RdWriArray<T>& rw) : GH(rw.GH), data(rw.data), var(rw.var), thorn(rw.thorn) {}

// This version allows writes but prevents reads, only for C++
#define DECLARE_CCTK_WRITEST1(X) \
  WriArray<CCTK_REAL> X ## _p(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X),#X "_p");\
  poison(cctkGH,X ## _p,#X "_p",__FILE__,__LINE__);

#define DECLAREi_CCTK_WRITES(X) \
  WriArray<CCTK_INT> X(cctkGH,(CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X);

#define DECLARE_CCTK_WRITES(X) \
  WriArray<CCTK_REAL> X(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X);\
  poison(cctkGH,X,#X,__FILE__,__LINE__);
#define DECLARE_CCTK_WRITES1(X) \
  WriArray<CCTK_REAL> X ## _p(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X),#X);\
  poison(cctkGH,X ## _p,#X "_p",__FILE__,__LINE__);

#define DECLARE_CCTK_WRITES_ALL(X) \
  WriArray<CCTK_REAL> X(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X);\
  poison(cctkGH,X,#X,__FILE__,__LINE__);

#define DECLAREi_CCTK_READS(X) \
  RdArray<CCTK_INT> X(cctkGH,(CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X);

#define DECLARE_CCTK_WRITEREADS(X) \
  RdWriArray<CCTK_REAL> X(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X,true);\
  poison(cctkGH,X,#X,__FILE__,__LINE__);
#define DECLARE_CCTK_WRITEREADS1(X) \
  RdWriArray<CCTK_REAL> X ## _p(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X),#X,true);\
  poison(cctkGH,X ## _p,#X "_p",__FILE__,__LINE__);
#define DECLARE_CCTK_WRITEREADS_ALL(X) \
  RdWriArray<CCTK_REAL> X(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X,true);\
  poison(cctkGH,X,#X,__FILE__,__LINE__);

#define DECLARE_CCTK_READWRITES(X) \
  RdWriArray<CCTK_REAL> X(cctkGH,(CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X,false);

#define DECLAREi_CCTK_READWRITES(X) \
  RdWriArray<CCTK_INT> X(cctkGH,(CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X,false);
#define DECLARE_CCTK_SCALAR(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLAREi_CCTK_SCALAR(X) \
  CCTK_INT *X = (CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define CCTK_READ_ARG RdArray<CCTK_REAL>
#define CCTK_WRITE_ARG WriArray<CCTK_REAL>

#define DECLARE_CCTK_READS(X) \
  RdArray<CCTK_REAL> X(cctkGH,(CCTK_REAL*)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X);
#define DECLARE_CCTK_READS1(X) \
  RdArray<CCTK_REAL> X ## _p(cctkGH,(CCTK_REAL*)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X),#X);
#define DECLARE_CCTK_BWRITES(X) \
  WriArray<CCTK_REAL> X(cctkGH,(CCTK_REAL*)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X),#X);
// Add checking
#define DECLARE_CCTK_VREADS(X) \
  const CCTK_REAL *X = (const CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X "[0]");
#define DECLARE_CCTK_VWRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X "[0]");
#define DECLARE_CCTK_AWRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_AWRITES1(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X );

#define MARK(X) X=X

#else

#define MARK(X)

#define DECLARE_CCTK_WRITEST1(X) \
  CCTK_REAL *X ## _p = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X);

#define DECLAREi_CCTK_WRITES(X) \
  CCTK_INT * X = (CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define DECLARE_CCTK_WRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_WRITES1(X) \
  CCTK_REAL *X ## _p= (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X);
#define DECLARE_CCTK_VWRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X "[0]");
#define DECLARE_CCTK_BWRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_AWRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define DECLARE_CCTK_WRITES_ALL(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define DECLAREi_CCTK_READS(X) \
  const CCTK_INT *X = (const CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_READS(X) \
  const CCTK_REAL *X = (const CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_READS1(X) \
  const CCTK_REAL *X ## _p = (const CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 1, cctk_auto_thorn, #X);
#define DECLARE_CCTK_VREADS(X) \
  const CCTK_REAL *X = (const CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X "[0]");

#define DECLARE_CCTK_WRITEREADS(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_WRITEREADS_ALL(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define DECLARE_CCTK_READWRITES(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_READWRITES_ALL(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define DECLAREi_CCTK_READWRITES(X) \
  CCTK_INT *X = (CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_SCALAR(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_REAL_SCALAR(X) \
  CCTK_REAL *X = (CCTK_REAL *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLAREi_CCTK_SCALAR(X) \
  CCTK_INT *X = (CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);
#define DECLARE_CCTK_INT_SCALAR(X) \
  CCTK_INT *X = (CCTK_INT *)CCTKi_VarDataPtr(cctkGH, 0, cctk_auto_thorn, #X);

#define CCTK_READ_ARG const CCTK_REAL *
#define CCTK_WRITE_ARG CCTK_REAL *

#endif
#endif
