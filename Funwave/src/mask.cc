#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "here.hh"
#include "cctk_declare.h"

//void set_maskp(CCTK_ARGUMENTS)
//{
//	_DECLARE_CCTK_ARGUMENTS;
//	cctk_func fdecl("set_maskp");
//	cctk_auto_thorn = "FUNWAVE";
//	DECLARE_CCTK_READWRITES(maskp);
//	DECLARE_CCTK_READWRITES(mask9p);
//	DECLARE_CCTK_READS(mask);
//	DECLARE_CCTK_READS(mask9);
//	return;
//	for(int j=1;j<cctk_lsh[1]-1;j++) {
//		for(int i=1;i<cctk_lsh[0]-1;i++) {
//			int ind[9];
//			ind[0] = FUNWAVE_INDEX(i,j);
//			ind[1] = FUNWAVE_INDEX(i+1,j);
//			ind[2] = FUNWAVE_INDEX(i-1,j);
//			ind[3] = FUNWAVE_INDEX(i,j+1);
//			ind[4] = FUNWAVE_INDEX(i,j-1);
//			ind[5] = FUNWAVE_INDEX(i+1,j+1);
//			ind[6] = FUNWAVE_INDEX(i+1,j-1);
//			ind[7] = FUNWAVE_INDEX(i-1,j+1);
//			ind[8] = FUNWAVE_INDEX(i-1,j-1);
//			int cc = ind[0];
//			maskp[cc] = 0;
//			mask9p[cc] = 0;
//			for(int n=0;n < 9;n++) {
//				if(mask[ind[n]] > 0.5) {
//					maskp[cc] += (1<<n);
//				}
//			}
//			mask9p[cc]=0;
//			for(int n=0;n < 9;n++) {
//				if(mask9[ind[n]] > 0.5) {
//					mask9p[cc] += (1<<n);
//				}
//			}
//		}
//	}
//}
void update_mask9(CCTK_ARGUMENTS)
{
	_DECLARE_CCTK_ARGUMENTS;
	cctk_func fdecl("update_mask9");
	cctk_auto_thorn = "FUNWAVE";
	DECLARE_CCTK_WRITES(mask9);
	DECLARE_CCTK_READS(mask);
	DECLARE_CCTK_READS(depth);
	DECLARE_CCTK_READS(eta);
	DECLARE_CCTK_PARAMETERS;

	/* update mask9 */
	for (int j = 2; j < cctk_lsh[1]-2; j++)
	{
		for (int i = 2; i < cctk_lsh[0]-2; i++)
		{
			int cc = FUNWAVE_INDEX( i, j);

			if(fabs(eta[cc])/max(depth[cc],mindepthfrc)>swe_eta_dep) {
				mask9[cc] = 0;
			} else {
				int im1 = FUNWAVE_INDEX( i - 1, j);
				int im1jm1 = FUNWAVE_INDEX( i - 1, j - 1);
				int im1jp1 = FUNWAVE_INDEX( i - 1, j + 1);
				int ip1 = FUNWAVE_INDEX( i + 1, j);
				int ip1jm1 = FUNWAVE_INDEX( i + 1, j - 1);
				int ip1jp1 = FUNWAVE_INDEX( i + 1, j + 1);
				int jp1 = FUNWAVE_INDEX( i, j + 1);
				int jm1 = FUNWAVE_INDEX( i, j - 1);
				mask9[cc] = mask[cc] * mask[im1] * mask[ip1] * mask[ip1jp1] * mask[jp1]
				            * mask[im1jp1] * mask[ip1jm1] * mask[jm1] * mask[im1jm1];
			}
		}
	}
}

void funwave_init_mask(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_init_mask");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITES(mask);
  DECLARE_CCTK_READS(depth);
  DECLARE_CCTK_READWRITES(eta);
  DECLARE_CCTK_PARAMETERS;

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      if (static_mask > 0 && static_mask < depth[cc])
      {
        mask[cc] = 1;
      }
      else if (enable_masks && eta[cc] < -depth[cc])
      {
        mask[cc] = 0;
        eta[cc] = mindepth - depth[cc];
      }
      else
      {
        mask[cc] = 1;
        MARK(eta[cc]);
      }

    }
  }
}

//extern "C" void funwave_update_mask(CCTK_ARGUMENTS);

// TODO: Figure out what's wrong with this function
// Should it be cctk_bbox[] or !cctk_bbox
void funwave_update_mask(CCTK_ARGUMENTS)
{
  const CCTK_REAL *mask_ptr;
  _DECLARE_CCTK_ARGUMENTS;
  {
  cctk_func fdecl("funwave_update_mask");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(mask);
  DECLARE_CCTK_READS(depth);
  DECLARE_CCTK_READWRITES(eta);
  DECLARE_CCTK_READS(mask_struc);
  DECLARE_CCTK_PARAMETERS;
  if(!enable_masks)
	  return;
  mask_ptr = (const CCTK_REAL *)mask;

  for (int j = 1; j < cctk_lsh[1]-1; j++)
  {
	  for (int i = 1; i < cctk_lsh[0]-1; i++)
	  {
		  int cc = FUNWAVE_INDEX( i, j);
		  int ip1 = FUNWAVE_INDEX( i + 1, j);
		  int im1 = FUNWAVE_INDEX( i - 1, j);
		  int jp1 = FUNWAVE_INDEX( i, j + 1);
		  int jm1 = FUNWAVE_INDEX( i, j - 1);
      MARK(mask[cc]);
      MARK(eta[cc]);

		  if (mask_struc[cc] > 0.5)
		  {
			  if (mask[cc] < 0.5)
			  {
				  // left;
				  if (i >= 1)
				  {
					  if (mask[im1] > 0.5 && eta[im1] > eta[cc])
					  {
						  mask[cc] = 1;
					  }
				  }
				  // right;
				  if (i < cctk_lsh[0])
				  {
					  if (mask[ip1] > 0.5 && eta[ip1] > eta[cc])
					  {
						  mask[cc] = 1;
					  }
				  }
				  // bottom;
				  if (j >= 1)
				  {
					  if (mask[jm1] > 0.5 && eta[jm1] > eta[cc])
					  {
						  mask[cc] = 1;
					  }
				  }
				  // top;
				  if (j < cctk_lsh[1])
				  {
					  if (mask[jp1] > 0.5 && eta[jp1] > eta[cc])
					  {
						  mask[cc] = 1;
					  }
				  }
				  // drying;
			  }
			  else if (eta[cc] < -depth[cc])
			  {
				  mask[cc] = 0;
				  eta[cc] = mindepth - depth[cc];
			  }

		  }
	  }
  }
  }
  update_mask9(CCTK_PASS_CTOC);
  //nanmap(cctkGH,mask_ptr,"mask",0);
  //assert(false);
}
