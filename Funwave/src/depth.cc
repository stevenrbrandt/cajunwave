#include <fstream>
#include <sstream>
#include <iostream>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "here.hh"
#include "cctk_declare.h"
#include <vector>
#include <math.h>
#include <hdf5.h>
#include <mpi.h>

extern "C" int GetRefinementLevel(const cGH*);

/*
 * prototypes of local functions
 * */
void readmatrix(CCTK_ARGUMENTS,const char *file,CCTK_REAL *inp);
bool getline(std::istream & in, std::string & line);
void funwave_init_interface_ele(CCTK_ARGUMENTS);

void writeFile(CCTK_ARGUMENTS, const char *fname, const CCTK_REAL *phi)
{
  _DECLARE_CCTK_ARGUMENTS
  std::ofstream of(fname);
  of << std::scientific;
  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      of << phi[cc] << " ";
    }
    of << std::endl;
  }
  of.close();
}

void funwave_init_depth_flat(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_depth_flat");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITES_ALL(depthx);
  DECLARE_CCTK_WRITES_ALL(depthy);
  DECLARE_CCTK_WRITES_ALL(depth);
  DECLARE_CCTK_WRITES_ALL(h);
  DECLARE_CCTK_READS(eta);
  for (int j = 0; j < cctk_lsh[1]; j++)
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);

      depthx[cc] = depth_flat;
      depthy[cc] = depth_flat;
      depth[cc] = depth_flat;
      h[cc] = depth_flat + eta[cc];
    }
}

void funwave_init_depth_slope(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_depth_slope");
  cctk_auto_thorn = "FUNWAVE";
  //DECLARE_CCTK_WRITES_ALL(depthx);
  //DECLARE_CCTK_WRITES_ALL(depthy);
  DECLARE_CCTK_WRITEREADS(depth);

  //int last = cctk_lsh[0];
  //if (cctk_bbox[1] == 1) last = last - cctk_nghostzones[0];
  for (int j = 0; j < cctk_lsh[1]; j++) {
    for (int i = 0; i < cctk_lsh[0]; i++) {
      int cc = FUNWAVE_INDEX(i,j);
      int crd = (int) (depth_xslp / cctk_delta_space[0]) + cctk_nghostzones[0];
      int xcrd = cctk_lbnd[0] + i;
      /*if (cctk_bbox[1] == 1 && i >= cctk_lsh[0] - cctk_nghostzones[0]) {
        int pr = FUNWAVE_INDEX(i - 1, j);
        depth[cc] = depth_flat;
        printf ("BOUNDARIES");
      } else*/ if (xcrd > crd) {
        //if (spherical_coordinates); //NOT IMPLEMENTED
        //else 
          depth[cc] = depth_flat - depth_slope * (xcrd - crd - 1) * cctk_delta_space[0];
      } else {
        depth[cc] = depth_flat;
      }
    }
  }
  
  if (cctk_ubnd[0] >= cctk_gsh[0] - 2 * cctk_nghostzones[0] - 1) {
    for (int j = 0; j < cctk_lsh[1]; j++) {
      int c1 = FUNWAVE_INDEX(cctk_lsh[0] - cctk_nghostzones[0] - 1, j);
      CCTK_REAL slopeval = depth[c1];
      //slopeval = 1.4;
      //printf ("XGHOSTZONES: %d", cctk_nghostzones[0]);
      for (int i = cctk_lsh[0] - cctk_nghostzones[0]; i < cctk_lsh[0]; i++) {
        printf ("I: %d, J: %d;", i, j);
        int c2 = FUNWAVE_INDEX(i, j);
        depth[c2] = slopeval;
      }
    }
  }
  /*CCTK_LOOP3_BND(testloop, cctkGH, i,j,k, ni,nj,nk) {
    
  } CCTK_ENDLOOP3_BND(testloop);*/
 
  funwave_init_interface_ele(CCTK_PASS_CTOC);
}

void funwave_init_interface_ele(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_interface_ele");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITES_ALL(depthx);
  DECLARE_CCTK_WRITES_ALL(depthy);
  DECLARE_CCTK_WRITEREADS(depth);

  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 1; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int im1 = FUNWAVE_INDEX( i - 1, j);

      depthx[cc] = 0.5 * (depth[cc] + depth[im1]);
    }
    int cc = FUNWAVE_INDEX( 0, j);
    int ip1 = FUNWAVE_INDEX( 1, j);
    depthx[cc] = 0.5 * (3.0 * depth[cc] - depth[ip1]);
  }

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 1; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j - 1);

      depthy[cc] = 0.5 * (depth[cc] + depth[jm1]);
    }
    int cc = FUNWAVE_INDEX( i, 0);
    int jp1 = FUNWAVE_INDEX( i, 1);
    depthy[cc] = 0.5 * (3.0 * depth[cc] - depth[jp1]);
  }
}

void funwave_init_depth_data(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_depth_data");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(depth);
  readmatrix(CCTK_PASS_CTOC,depth_file, depth);
}

void funwave_init_vegetation_height_data(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_vegetation_height_data");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(vegetation_height);
  if(veg_cd == 0)
    return;
  if(strcmp(vegetation_height_file,"NONE")==0)
    return;
  //int n = 0;
  readmatrix(CCTK_PASS_CTOC,vegetation_height_file, vegetation_height);
}

void funwave_init_depth_data_ele(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_depth_data_ele");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READWRITES(depth);
  DECLARE_CCTK_WRITES(depthx);
  DECLARE_CCTK_WRITES(depthy);
  DECLARE_CCTK_WRITES(h);
  DECLARE_CCTK_READS(eta);

  if(cctk_bbox[2]>0)
  for (int j = 0; j < nbound; j++)
  {
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int cj = FUNWAVE_INDEX( i, nbound);
      depth[cc] = depth[cj];
    }
  }
  if(cctk_bbox[3]>0)
  for (int j = cctk_lsh[1]-nbound; j < cctk_lsh[1]; j++)
  {
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int cj = FUNWAVE_INDEX( i, cctk_lsh[1]-nbound-1);
      depth[cc] = depth[cj];
    }
  }
  if(cctk_bbox[0]>0)
  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 0; i < nbound; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int ci = FUNWAVE_INDEX(nbound,j);
      depth[cc] = depth[ci];
    }
  }
  if(cctk_bbox[1]>0)
  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = cctk_lsh[0]-nbound; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int ci = FUNWAVE_INDEX(cctk_lsh[0]-nbound-1,j);
      depth[cc] = depth[ci];
    }
  }

  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 1; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int im1 = FUNWAVE_INDEX( i - 1, j);

      depthx[cc] = 0.5 * (depth[cc] + depth[im1]);
    }
    int cc = FUNWAVE_INDEX( 0, j);
    int ip1 = FUNWAVE_INDEX( 1, j);
    depthx[cc] = 0.5 * (3.0 * depth[cc] - depth[ip1]);
  }

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 1; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j - 1);

      depthy[cc] = 0.5 * (depth[cc] + depth[jm1]);
    }
    int cc = FUNWAVE_INDEX( i, 0);
    int jp1 = FUNWAVE_INDEX( i, 1);
    depthy[cc] = 0.5 * (3.0 * depth[cc] - depth[jp1]);
  }
  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      h[cc] = depth[cc]+eta[cc];
    }
  }
}

void funwave_init_depth_data_nod(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_init_depth_data_nod");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(depth);
  DECLARE_CCTK_WRITES(depthx);
  DECLARE_CCTK_WRITES(depthy);
  for (int j = 0; j < cctk_lsh[1] - 1; j++)
  {
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jp1 = FUNWAVE_INDEX( 1, j + 1);

      depthx[cc] = 0.5 * (depth[cc] + depth[jp1]);
    }
  }
  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 0; i < cctk_lsh[0] - 1; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int ip1 = FUNWAVE_INDEX( i + 1, j);

      depthy[cc] = 0.5 * (depth[cc] + depth[ip1]);
    }
  }
}

void funwave_post_init_depth_2(CCTK_ARGUMENTS);

void funwave_post_init_depth(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  cctk_func fdecl("funwave_post_init_depth");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(depth);
  DECLARE_CCTK_READS(depthx);
  DECLARE_CCTK_READS(depthy);


  for (int j = 0; j < cctk_lsh[1] - 1; j++)
  {
    for (int i = 0; i < cctk_lsh[0] - 1; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int ip1 = FUNWAVE_INDEX( i + 1, j);
      int jp1 = FUNWAVE_INDEX( i, j + 1);
      depth[cc] = 0.25 * (depthx[cc] + depthx[ip1] + depthy[cc] + depthy[jp1]);
    }
  }
  // Need to put something in the outer zones
  for(int j=0;j<cctk_lsh[1]-1;j++) {
    int i = cctk_lsh[0]-1;
    int cc = FUNWAVE_INDEX( i, j);
    int im1 = FUNWAVE_INDEX( i-1, j);
    depth[cc] = depth[im1];
  }
  for(int i=0;i<cctk_lsh[0];i++) {
    int j = cctk_lsh[1]-1;
    int cc = FUNWAVE_INDEX( i, j);
    int jm1 = FUNWAVE_INDEX( i, j-1);
    depth[cc] = depth[jm1];
  }
}

void funwave_gen_depth ( CCTK_ARGUMENTS )
{
	_DECLARE_CCTK_ARGUMENTS
	cctk_func fdecl("generate_depth");
	cctk_auto_thorn = "GRID";
	DECLARE_CCTK_READS(x);
	DECLARE_CCTK_READS(y);
	DECLARE_CCTK_PARAMETERS
  std::cout << "RUNNING DEPTH FILE '" << depth_file << "'" << std::endl;
	if(cctk_bbox[0] && cctk_bbox[2] && generate_test_depth_data) {
		std::cout << "GENERATING DEPTH FILE '" << depth_file << "'" << std::endl;

    std::ofstream fx, fy;
    fx.open("x.txt");
    fy.open("y.txt");
    if(!fx.good()) {
			CCTK_Warn(0,__LINE__,__FILE__,CCTK_THORNSTRING,"Cannot open depth x file for writing");
    }
    if(!fy.good()) {
			CCTK_Warn(0,__LINE__,__FILE__,CCTK_THORNSTRING,"Cannot open depth y file for writing");
    }

		std::ofstream f;
		f.open(depth_file);
		if(!f.good()) {
			CCTK_Warn(0,__LINE__,__FILE__,CCTK_THORNSTRING,"Cannot open depth file for writing");
		}
		int c0 = FUNWAVE_INDEX(0,0);
		int c1 = FUNWAVE_INDEX(1,1);
		CCTK_REAL dx = x[c1]-x[c0];
		CCTK_REAL dy = y[c1]-y[c0];
		CCTK_REAL x0 = x[c0];
		CCTK_REAL y0 = y[c0];
		for(int j=0;j<cctk_gsh[1];j++) {
			for(int i=0;i<cctk_gsh[0];i++) {
				CCTK_REAL xv = x0+i*dx;
				CCTK_REAL yv = y0+j*dy;
				CCTK_REAL cm2 = sq(xv-test_depth_island_x)+sq(yv-test_depth_island_y);
				CCTK_REAL xm = xv-test_depth_shore_x;
				CCTK_REAL height = -0.5+exp(-0.1*cm2)+0.5*(1+tanh(0.3*xm));
				if(i > 0) f << ' ';
				f << (-height);
				if(i > 0) fx << ' ';
				fx << xv;
				if(i > 0) fy << ' ';
				fy << yv;
			}
			f << std::endl;
			fx << std::endl;
			fy << std::endl;
		}
		f.close();
    fx.close();
    fy.close();
	} else {
		std::cout << "WAITING FOR GENERATION OF DEPTH FILE" << std::endl;
	}
}

void funwave_post_init_depth_2(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS
  cctk_func fdecl("funwave_post_init_depth_2");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITES(ubar);
  DECLARE_CCTK_WRITES(vbar);
  DECLARE_CCTK_READS(hu);
  DECLARE_CCTK_READS(hv);
  DECLARE_CCTK_READS(h);
  DECLARE_CCTK_READS(u1p);
  DECLARE_CCTK_READS(v1p);
  DECLARE_CCTK_WRITES(mask);
  DECLARE_CCTK_READWRITES(eta);
  DECLARE_CCTK_READS(depth);
  DECLARE_CCTK_PARAMETERS
  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);

      ubar[cc] = hu[cc] + gamma1 * u1p[cc] * h[cc];
      vbar[cc] = hv[cc] + gamma1 * v1p[cc] * h[cc];
      if(enable_masks && eta[cc] < -depth[cc]) {
        mask[cc] = 0.0;
        eta[cc] = mindepth - depth[cc];
      } else {
        mask[cc] = 1.0;
      }
    }
  }
}

// read a single line of input
bool getline(std::istream & in, std::string & line)
{
  bool ret = false;

  line.resize(0);
  while (true)
  {
    char ch = in.get();

    if (ch == EOF)
      break;
    ret = true;
    if (ch == '\n')
      break;
    line += ch;
  }
  return ret;
}

/*
 * This function reads the submatrix from the matrix contained in
 * the text file. The origin of the matrix is given by int*origin,
 * and the shape by int*lsh. Format is 0 when the input depth is
 * cell centered and 1 when the input depth is on grid point.
 */
void readmatrix(CCTK_ARGUMENTS,const char *file,CCTK_REAL *inp)
{
	_DECLARE_CCTK_ARGUMENTS
	cctk_func fdecl("readmatrix");
	cctk_auto_thorn = "GRID";
	DECLARE_CCTK_READS(x);
	DECLARE_CCTK_READS(y);

  const void *val;
  int type;

  val = CCTK_ParameterGet("dx","CoordBase",&type);
  double dx = *(double *)val;
  val = CCTK_ParameterGet("xmin","CoordBase",&type);
  double xmin = *(double *)val;
  val = CCTK_ParameterGet("xmax","CoordBase",&type);
  double xmax = *(double *)val;
  double delx = cctk_nghostzones[0]*dx;
  xmin -= delx;
  xmax += delx;

  val = CCTK_ParameterGet("dy","CoordBase",&type);
  double dy = *(double *)val;
  val = CCTK_ParameterGet("ymin","CoordBase",&type);
  double ymin = *(double *)val;
  val = CCTK_ParameterGet("ymax","CoordBase",&type);
  double ymax = *(double *)val;
  double dely = cctk_nghostzones[1]*dy;
  ymin -= dely;
  ymax += dely;

  val = CCTK_ParameterGet("depth_file_offset_x","FunWave",&type);
  int depth_file_offset_x = *(int *)val;
  val = CCTK_ParameterGet("depth_file_offset_y","FunWave",&type);
  int depth_file_offset_y = *(int *)val;

  int xsize = (xmax - xmin + dx*1.0e-6)/dx+1;
  int ysize = (ymax - ymin + dy*1.0e-6)/dy+1;

  std::vector<CCTK_REAL> vint;
  vint.resize(xsize*ysize);
  const CCTK_REAL nan17 = nan("17");
  for(int i=0;i<vint.size();i++)
    vint[i] = nan17;

  std::cout << "Reading file '" << file << "'" << std::endl;
  std::string s = file;
  int nfile = s.size();
  bool is_txt = false;
  bool is_hdf5 = false;
  if(nfile > 5
      && file[nfile-4] == '.'
      && file[nfile-3] == 't'
      && file[nfile-2] == 'x'
      && file[nfile-1] == 't')
    is_txt = true;
  if(nfile > 4
      && file[nfile-3] == '.' 
      && file[nfile-2] == 'h' 
      && file[nfile-1] == '5')
    is_hdf5 = true;
  if(is_txt) {
    std::string h = s.substr(0,nfile-4)+".h5";
    std::ifstream htst(h);
    if(htst.good()) {
      s = h;
      is_txt = false;
      is_hdf5 = true;
      htst.close();
      std::cout << "Reading from HDF5 file instead: " << h << std::endl;
    }
  }
  int col = 0, row = 0;
  int mpi_rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&mpi_rank);
  if(mpi_rank == 0) {
    if(is_hdf5) {
      hid_t hfile  = H5Fopen(s.c_str(),H5F_ACC_RDONLY,H5P_DEFAULT);
      hid_t dset   = H5Dopen(hfile,"grid",H5P_DEFAULT);
      hid_t dspace = H5Dget_space(dset);
      const hsize_t dims = H5Sget_simple_extent_ndims(dspace);
      assert(dims == 2);
      hsize_t dims_out[dims];
      H5Sget_simple_extent_dims(dspace,dims_out,NULL);
      const int NX = dims_out[1];
      const int NY = dims_out[0];
      vint.resize(NX*NY);
      col = NX;
      row = NY;
      H5Dread(dset,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL, H5P_DEFAULT, &vint[0]);
    } else {
      std::ifstream in(s);
      if(!in.good()) {
        std::ostringstream ostr;
        ostr << "Could not open file '" << file << "'";
        std::string str = ostr.str();
        CCTK_Warn(0,__LINE__,__FILE__,CCTK_THORNSTRING,str.c_str());
      }
      std::string line;

      int index = 0;
      std::string str;
      while (true)
      {
        if (!getline(in, line))
          break;
        col = 0;

        std::istringstream ins(line);
        while (!ins.eof())
        {
          str = "";
          ins >> str;
          if(str.size() > 0) {
            if(index >= vint.size())
              vint.resize(index+100);
            vint.at(index++) = std::stod(str);
            col++;
          }
        }
        row++;
      }
      in.close();
    }
  }
  int grid_par[3];
  if(mpi_rank == 0) {
    grid_par[0] = vint.size();
    grid_par[1] = col;
    grid_par[2] = row;
  }
  std::cout << "Before broadcast" << std::endl;
  MPI_Bcast(&grid_par,3,MPI_INT,0,MPI_COMM_WORLD);
  if(mpi_rank > 0) {
    vint.resize(grid_par[0]);
    col = grid_par[1];
    row = grid_par[2];
  }
  MPI_Bcast(&vint[0],vint.size(),MPI_DOUBLE,0,MPI_COMM_WORLD);
  std::cout << "After broadcast" << std::endl;

  // stretch the grid to match
  // the input data.
  xsize = col;
  ysize = row;
  std::cout << "xsize=" << xsize << ", ysize=" << ysize << std::endl;
  dx = (xmax-xmin)/(xsize-1);
  dy = (ymax-ymin)/(ysize-1);

  for(int j=0;j<cctk_lsh[1];j++) {
    for(int i=0;i<cctk_lsh[0];i++) {
      int cc = FUNWAVE_INDEX(i,j);

      int xi = (x[cc]-xmin+1.0e-6*dx)/dx;
      int yi = (y[cc]-ymin+1.0e-6*dy)/dy;

      if(yi < 0) yi=0;
      if(xi < 0) xi=0;
      if(xi+1 >= xsize) xi = xsize-2;
      if(yi+1 >= ysize) yi = ysize-2;

      double xv = xmin+dx*xi;
      double yv = ymin+dy*yi;

      int xc = xi - depth_file_offset_x;
      int yc = yi - depth_file_offset_y;
      int xc2 = xc + 1;
      int yc2 = yc + 1;

      if(xc < 0) xc = 0;
      if(xc2 < 0) xc2 = 0;
      if(yc < 0) yc = 0;
      if(yc2 < 0) yc2 = 0;

      if(xc >= xsize) xc = xsize-1;
      if(xc2 >= xsize) xc2 = xsize-1;
      if(yc >= ysize) yc = ysize-1;
      if(yc2 >= ysize) yc2 = ysize-1;

      CCTK_REAL v22 = vint.at(xc +yc *xsize);
      CCTK_REAL v12 = vint.at(xc2+yc *xsize);
      CCTK_REAL v21 = vint.at(xc +yc2*xsize);
      CCTK_REAL v11 = vint.at(xc2+yc2*xsize);
      assert(!std::isnan(v11));
      assert(!std::isnan(v12));
      assert(!std::isnan(v21));
      assert(!std::isnan(v22));

      CCTK_REAL wx1 = (x[cc]-xv)/dx;
      CCTK_REAL wx2 = 1-wx1;
      CCTK_REAL wy1 = (y[cc]-yv)/dy;
      CCTK_REAL wy2 = 1-wy1;

      inp[cc] = v11*wx1*wy1+
                v12*wx1*wy2+
                v21*wx2*wy1+
                v22*wx2*wy2;
    }
  }
}

