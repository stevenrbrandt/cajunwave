#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "here.hh"
#include "funwave_util.hh"
#include "cctk_declare.h"


extern "C"
int funwave_banner() {
	// Generated with the linux command "Figlet"
	// figlet CajunWave|perl -p -e 's/\\/\\\\/g'|perl -p -e 's/.*/"$&\\n"/'
	std::cout <<
			"  ____       _          __        __              \n"
			" / ___|__ _ (_)_   _ _ _\\ \\      / /_ ___   _____ \n"
			"| |   / _` || | | | | '_ \\ \\ /\\ / / _` \\ \\ / / _ \\\n"
			"| |__| (_| || | |_| | | | \\ V  V / (_| |\\ V /  __/\n"
			" \\____\\__,_|/ |\\__,_|_| |_|\\_/\\_/ \\__,_| \\_/ \\___|\n"
			"          |__/                                    \n";
	return 0;
}
/*
 * funwave_init to initialize funwave.
 * */
void funwave_init_dx_dy(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

}

//void checkForModifications(const cGH *GH);

void copy_fundata(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_func fdecl("copy_fun_data");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS1(u);
  DECLARE_CCTK_READS1(v);
  DECLARE_CCTK_READS1(hu);
  DECLARE_CCTK_READS1(hv);
  DECLARE_CCTK_READS1(mask);
  DECLARE_CCTK_READWRITES(u);
  DECLARE_CCTK_READWRITES(v);
  DECLARE_CCTK_READWRITES(hu);
  DECLARE_CCTK_READWRITES(hv);
  DECLARE_CCTK_READWRITES(mask);
  if(u_p != 0) {
    //#pragma omp parallel for
    for(int j=0;j<cctk_lsh[1];j++) {
      for(int i=0;i<cctk_lsh[0];i++) {
        int cc = FUNWAVE_INDEX(i,j);
        u[cc] = u_p[cc];
        v[cc] = v_p[cc];
        hu[cc] = hu_p[cc];
        hv[cc] = hv_p[cc];
        mask[cc] = mask_p[cc];
      }
    }
  }
}

void funwave_init_zero(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_func fdecl("funwave_init_zero");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITES_ALL(fx);
  DECLARE_CCTK_WRITES_ALL(huxl);
  DECLARE_CCTK_WRITES_ALL(u4xr);
  DECLARE_CCTK_WRITES_ALL(etaryr);
  DECLARE_CCTK_WRITES_ALL(uyl);
  DECLARE_CCTK_WRITES_ALL(delyetar);
  DECLARE_CCTK_WRITES_ALL(uxx);
  DECLARE_CCTK_WRITES_ALL(vxy);
  DECLARE_CCTK_WRITES_ALL(slopex);
  DECLARE_CCTK_WRITES_ALL(sxl);
  DECLARE_CCTK_WRITES_ALL(etasum);
  DECLARE_CCTK_WRITES_ALL(qr);
  DECLARE_CCTK_WRITES_ALL(sxr);
  DECLARE_CCTK_WRITES_ALL(delyhu);
  DECLARE_CCTK_WRITES_ALL(ubar_rhs);
  DECLARE_CCTK_WRITES_ALL(vbar_rhs);
  DECLARE_CCTK_WRITES_ALL(gx);
  DECLARE_CCTK_WRITES_ALL(etax);
  DECLARE_CCTK_WRITES_ALL(delyu);
  DECLARE_CCTK_WRITES_ALL(gyl);
  DECLARE_CCTK_WRITES_ALL(sourcey);
  DECLARE_CCTK_WRITES_ALL(mask);
  DECLARE_CCTK_WRITES_ALL(huyr);
  DECLARE_CCTK_WRITES_ALL(duy);
  DECLARE_CCTK_WRITES_ALL(windu2d);
  DECLARE_CCTK_WRITES_ALL(vyr);
  DECLARE_CCTK_READWRITES(hu);
  DECLARE_CCTK_WRITES_ALL(windv2d);
  DECLARE_CCTK_WRITES_ALL(vxl);
  DECLARE_CCTK_WRITES_ALL(vx);
  DECLARE_CCTK_WRITES_ALL(u4xl);
  DECLARE_CCTK_WRITES_ALL(int2flo);
  DECLARE_CCTK_WRITES_ALL(delyhv);
  DECLARE_CCTK_WRITES_ALL(duxx);
  DECLARE_CCTK_WRITES_ALL(v2);
  DECLARE_CCTK_WRITES_ALL(uy);
  DECLARE_CCTK_WRITES_ALL(depthy);
  DECLARE_CCTK_WRITES_ALL(coriolis);
  DECLARE_CCTK_WRITES_ALL(dux);
  DECLARE_CCTK_WRITES_ALL(syl);
  DECLARE_CCTK_WRITES_ALL(u);
  DECLARE_CCTK_WRITES1(u);
  DECLARE_CCTK_WRITES_ALL(hyr);
  DECLARE_CCTK_WRITES_ALL(hxl);
  DECLARE_CCTK_WRITES_ALL(u1pp);
  DECLARE_CCTK_WRITES_ALL(v3);
  DECLARE_CCTK_WRITES_ALL(gyr);
  DECLARE_CCTK_SCALAR(b1);
  DECLARE_CCTK_WRITES_ALL(ql);
  DECLARE_CCTK_WRITES_ALL(delyv);
  DECLARE_CCTK_WRITES_ALL(tmp4preview);
  DECLARE_CCTK_WRITES_ALL(fxr);
  DECLARE_CCTK_WRITES_ALL(hv);
  DECLARE_CCTK_WRITES_ALL(hxr);
  DECLARE_CCTK_WRITES_ALL(u3);
  DECLARE_CCTK_WRITES_ALL(u4);
  DECLARE_CCTK_WRITES_ALL(mask_struc);
  DECLARE_CCTK_WRITES_ALL(huxr);
  DECLARE_CCTK_WRITES_ALL(etat);
  DECLARE_CCTK_WRITES_ALL(height_max);
  DECLARE_CCTK_WRITES_ALL(fy);
  DECLARE_CCTK_WRITES_ALL(eta);
  DECLARE_CCTK_WRITES_ALL(nu_smg);
  DECLARE_CCTK_WRITES_ALL(v4yr);
  DECLARE_CCTK_WRITES_ALL(dvyy);
  DECLARE_CCTK_WRITES_ALL(ux);
  DECLARE_CCTK_WRITES_ALL(etaryl);
  DECLARE_CCTK_WRITES_ALL(vmean);
  DECLARE_CCTK_WRITES_ALL(mask9);
  DECLARE_CCTK_WRITES_ALL(etay);
  DECLARE_CCTK_WRITES_ALL(gxr);
  DECLARE_CCTK_WRITES_ALL(sourcex);
  DECLARE_CCTK_WRITES_ALL(umean);
  DECLARE_CCTK_WRITES_ALL(u2);
  DECLARE_CCTK_WRITES_ALL(hvxl);
  DECLARE_CCTK_WRITES_ALL(uxr);
  DECLARE_CCTK_WRITES_ALL(hyl);
  DECLARE_CCTK_WRITES_ALL(dvy);
  DECLARE_CCTK_WRITES_ALL(dvxy);
  DECLARE_CCTK_SCALAR(fun_dt);
  DECLARE_CCTK_WRITES_ALL(etamean);
  DECLARE_CCTK_WRITES_ALL(etarxr);
  DECLARE_CCTK_WRITES_ALL(v0);
  DECLARE_CCTK_WRITES_ALL(q);
  DECLARE_CCTK_WRITES_ALL(v4yl);
  DECLARE_CCTK_WRITES_ALL(vbar);
  DECLARE_CCTK_WRITES_ALL(pr);
  DECLARE_CCTK_WRITES_ALL(hvyr);
  DECLARE_CCTK_WRITES_ALL(delxu);
  DECLARE_CCTK_WRITES_ALL(fyr);
  DECLARE_CCTK_WRITES_ALL(slopey);
  DECLARE_CCTK_WRITES_ALL(uyr);
  DECLARE_CCTK_WRITES_ALL(vyl);
  DECLARE_CCTK_WRITES_ALL(delxhu);
  DECLARE_CCTK_WRITES_ALL(ubar);
  DECLARE_CCTK_WRITES_ALL(fxl);
  DECLARE_CCTK_WRITES_ALL(gy);
  DECLARE_CCTK_WRITES_ALL(pl);
  DECLARE_CCTK_WRITES_ALL(etarxl);
  DECLARE_CCTK_WRITES_ALL(uxy);
  DECLARE_CCTK_WRITES_ALL(vy);
//  DECLARE_CCTK_WRITES_ALL(vsum);
  DECLARE_CCTK_WRITES_ALL(u1p);
  DECLARE_CCTK_WRITES_ALL(v4);
  DECLARE_CCTK_WRITES_ALL(u0);
  DECLARE_CCTK_WRITES_ALL(vxr);
  DECLARE_CCTK_WRITES_ALL(depthnode);
  DECLARE_CCTK_WRITES_ALL(usum);
  DECLARE_CCTK_WRITES_ALL(hvxr);
  DECLARE_CCTK_WRITES_ALL(syr);
  DECLARE_CCTK_WRITES_ALL(delxhv);
  DECLARE_CCTK_WRITES_ALL(mask_wind);
  DECLAREi_CCTK_SCALAR(wind_counter);
  DECLARE_CCTK_WRITES_ALL(huyl);
  DECLARE_CCTK_WRITES_ALL(v);
  DECLARE_CCTK_WRITES_ALL(hvyl);
  DECLARE_CCTK_WRITES_ALL(duxy);
  DECLARE_CCTK_WRITEREADS(depth);
  DECLARE_CCTK_WRITEREADS(vegetation_height);
  DECLARE_CCTK_WRITES_ALL(v1p);
  DECLARE_CCTK_WRITEST1(ubar);
  DECLARE_CCTK_SCALAR(b2);
  DECLARE_CCTK_WRITES_ALL(din);
  DECLARE_CCTK_WRITEST1(vbar);
  DECLARE_CCTK_WRITES_ALL(fyl);
  DECLARE_CCTK_WRITES_ALL(v1pp);
  DECLARE_CCTK_WRITES_ALL(h);
  DECLARE_CCTK_WRITES_ALL(gxl);
  DECLARE_CCTK_WRITEST1(eta);
  DECLARE_CCTK_WRITES_ALL(uxl);
  DECLARE_CCTK_WRITES_ALL(dvx);
  DECLARE_CCTK_WRITES_ALL(delxv);
  DECLARE_CCTK_WRITES_ALL(sponge);
  DECLARE_CCTK_WRITES_ALL(p);
  DECLARE_CCTK_WRITES_ALL(vyy);
  DECLARE_CCTK_WRITES_ALL(delxetar);
  DECLARE_CCTK_WRITES_ALL(eta_rhs);
  DECLARE_CCTK_WRITES_ALL(depthx);
  cctk_auto_thorn = "FUNWAVECOORD";
  DECLARE_CCTK_READS(dx);
  DECLARE_CCTK_READS(dy);
  DECLARE_CCTK_READS(lat_theta);

//  ASSERT_(false);
  /*
   if(cctk_nghostzones[0] != cctk_nghostzones[1])
   CCTK_WARN(0,"ghostzones must be the same in x and y");
   if(stretch_grid)
   CCTK_WARN(0,"stretch grid not implemented");
   else {
   *dphi_r = dphi*M_PI/180.0;
   *dtheta_r=dtheta*M_PI/180.0;
   } */

  {
	  for (int i = 0; i < cctk_lsh[0]; i++)
	  {
      //#pragma omp parallel for
		  for (int j = 0; j < cctk_lsh[1]; j++)
		  {
			  for(int k=0;k<cctk_lsh[2];k++) {
				  int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
				  u4xl[cc] = 0.0;
				  u4xr[cc] = 0.0;
				  v4yr[cc] = 0.0;
				  v4yl[cc] = 0.0;
				  u4[cc] = 0.0;
				  v4[cc] = 0.0;
				  u1pp[cc] = 0.0;
				  v1pp[cc] = 0.0;
				  u2[cc] = 0.0;
				  u3[cc] = 0.0;
				  v2[cc] = 0.0;
				  v3[cc] = 0.0;
			  }
		  }
	  }
  }

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
	  for (int j = 0; j < cctk_lsh[1]; j++)
	  {
		  //for(int k=0;k<cctk_lsh[2];k++)
      if(true)
		  {
        int k = (cctk_lsh[2]>>1);
			  int cc = CCTK_GFINDEX3D(cctkGH, i, j,k);

			  // Which window of theta we're looking at
			  //lat_theta[cc] = lat_south * M_PI / 180.0
			  //              - cctk_nghostzones[0] * (*dtheta_r) + j * (*dtheta_r);
			  // X = R_earth*sin(\theta), dX = R_earth*cos(\theta)*d\theta
			  // Y = R_earth*phi, dY = R_earth*d\phi
			  //DxSpherical[cc] = R_earth * (*Dphi_r) * cos(lat_theta[cc]);
			  //DySpherical[cc] = R_earth * (*Dtheta_r);
			  //coriolis[cc] = M_PI * sin(lat_theta[cc]) / 21600.0;

			  /* varaibles with a dimension of (mloc + 1, nloc + 1) */
			  depthnode[cc] = 0.0;

			  /* varaibles with a dimension of (mloc, nloc) */
			  delxu[cc] = 0.0;
			  delxhu[cc] = 0.0;
			  delxv[cc] = 0.0;
			  delxetar[cc] = 0.0;
			  delyu[cc] = 0.0;
			  delyhv[cc] = 0.0;
			  delyv[cc] = 0.0;
			  delyetar[cc] = 0.0;
			  delxhv[cc] = 0.0;
			  delyhu[cc] = 0.0;
			  uxy[cc] = 0.0;
			  vxy[cc] = 0.0;
			  duxy[cc] = 0.0;
			  dvxy[cc] = 0.0;
			  uxx[cc] = 0.0;
			  vyy[cc] = 0.0;
			  duxx[cc] = 0.0;
			  dvyy[cc] = 0.0;

			  ux[cc] = 0.0;
			  uy[cc] = 0.0;
			  vx[cc] = 0.0;
			  vy[cc] = 0.0;
			  dux[cc] = 0.0;
			  duy[cc] = 0.0;
			  dvx[cc] = 0.0;
			  dvy[cc] = 0.0;
			  etax[cc] = 0.0;
			  etay[cc] = 0.0;
			  etat[cc] = 0.0;
			  if(dt_size == 0) {
				  *fun_dt = CCTK_DELTA_TIME;
			  } else {
				  *fun_dt = dt_size;
			  }

			  u0[cc] = 0;
			  v0[cc] = 0;

			  sponge[cc] = 0.0;
			  u[cc] = 0.0;
        if(u_p != 0)
          u_p[cc] = 0.0;
			  //u_p[cc] = 0.0;
			  v[cc] = 0.0;
			  //v_p[cc] = 0.0;
			  hu[cc] = 0.0;
			  //hu_p[cc] = 0.0;
			  hv[cc] = 0.0;
			  //hv_p[cc] = 0.0;
			  sourcex[cc] = 0.0;
			  sourcey[cc] = 0.0;
			  //sourcex_p[cc] = 0.0;
			  //sourcey_p[cc] = 0.0;
			  umean[cc] = 0.0;
			  vmean[cc] = 0.0;
			  etamean[cc] = 0.0;
			  usum[cc] = 0.0;
			  etasum[cc] = 0.0;
			  nu_smg[cc] = 0.0;

			  depth[cc] = 0.0;
			  vegetation_height[cc] = 0.0;
			  h[cc] = 0.0;
			  mask[cc] = 1.0;
			  mask_struc[cc] = 1.0;
			  mask9[cc] = 1.0;
			  tmp4preview[cc] = 0.0;
			  //tmp4preview_p[cc] = 0.0;
			  int2flo[cc] = 0.0;
			  height_max[cc] = 0.0;

			  eta[cc] = 0.0;
			  ubar[cc] = 0.0;
			  vbar[cc] = 0.0;
			  ubar_p[cc] = 0.0;
			  vbar_p[cc] = 0.0;
			  eta_p[cc] = 0.0;
			  eta_rhs[cc] = 0.0;
			  ubar_rhs[cc] = 0.0;
			  vbar_rhs[cc] = 0.0;

			  u1p[cc] = 0.0;
			  v1p[cc] = 0.0;

			  din[cc] = 0.0;

			  /* varaibles with a dimension of (mloc+1, nloc) */
			  uxl[cc] = 0.0;
			  uxr[cc] = 0.0;
			  vxl[cc] = 0.0;
			  vxr[cc] = 0.0;
			  huxl[cc] = 0.0;
			  huxr[cc] = 0.0;
			  hvxl[cc] = 0.0;
			  hvxr[cc] = 0.0;
			  hxl[cc] = 0.0;
			  hxr[cc] = 0.0;
			  pl[cc] = 0.0;
			  pr[cc] = 0.0;
			  fxl[cc] = 0.0;
			  fxr[cc] = 0.0;
			  gxl[cc] = 0.0;
			  gxr[cc] = 0.0;
			  sxl[cc] = 0.0;
			  sxr[cc] = 0.0;
			  etarxl[cc] = 0.0;
			  etarxr[cc] = 0.0;
			  fx[cc] = 0.0;
			  gx[cc] = 0.0;
			  p[cc] = 0.0;
			  depthx[cc] = 0.0;

			  /* varaibles with a dimension of (mloc, nloc+1) */
			  uyl[cc] = 0.0;
			  uyr[cc] = 0.0;
			  vyl[cc] = 0.0;
			  vyr[cc] = 0.0;
			  hvyl[cc] = 0.0;
			  hvyr[cc] = 0.0;
			  huyl[cc] = 0.0;
			  huyr[cc] = 0.0;
			  hyl[cc] = 0.0;
			  hyr[cc] = 0.0;
			  ql[cc] = 0.0;
			  qr[cc] = 0.0;
			  fyl[cc] = 0.0;
			  fyr[cc] = 0.0;
			  gyl[cc] = 0.0;
			  gyr[cc] = 0.0;
			  syl[cc] = 0.0;
			  syr[cc] = 0.0;
			  etaryl[cc] = 0.0;
			  etaryr[cc] = 0.0;
			  fy[cc] = 0.0;
			  gy[cc] = 0.0;
			  //gy_p[cc] = 0.0;
			  q[cc] = 0.0;
			  //q_p[cc] = 0.0;
			  depthy[cc] = 0.0;

			  windu2d[cc] = 0.0;
			  windv2d[cc] = 0.0;
			  mask_wind[cc] = 1.0;

			  //      fx_p[cc] = fy_p[cc] = u_p[cc] = 0.0;
			  //      v_p[cc] = hu_p[cc] = hv_p[cc] = 0.0;
			  //      gx_p[cc] = gy_p[cc] = p_p[cc] = q_p[cc] = 0.0;
			  //      sourcex_p[cc] = sourcey_p[cc] = int2flo_p[cc] = 0.0;
			  //      tmp4preview_p[cc] = 0.0;
		  }
	  }
  }

  if(spherical_coordinates) {
	  *b1=1.0/3.0;
	  *b2=-1.0/2.0;
  } else {
	  *b1 = beta_ref*beta_ref;
	  *b2 = beta_ref;
  }
  *wind_counter = 0;
  // initialize for the Cartesian case
  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      slopex[cc] = 0.0;
      slopey[cc] = 0.0;
      coriolis[cc] = 0.0;
    }
  }
  if (spherical_coordinates)
  {
    for (int i = 0; i < cctk_lsh[0]-1; i++)
    {
      for (int j = 0; j < cctk_lsh[1]-1; j++)
      {
        int cc = FUNWAVE_INDEX( i, j);
        int ip1 = FUNWAVE_INDEX( i+1, j);
        int jp1 = FUNWAVE_INDEX( i, j+1);
        slopex[cc] = (depth[ip1]-depth[cc])/dx[cc];
        slopey[cc] = (depth[jp1]-depth[cc])/dy[cc];
        coriolis[cc] = M_PI*sin(lat_theta[cc])/21600.0;
      }
    }
  }
}
