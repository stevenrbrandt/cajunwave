#include <cstdlib>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"
#include "here.hh"
#include <math.h>
#include "cctk_declare.h"

int debug = false;

void wave_speed(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("wave_speed");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITES(sxl);
  DECLARE_CCTK_WRITES(sxr);
  DECLARE_CCTK_READS(hxl);
  DECLARE_CCTK_READS(hxr);
  DECLARE_CCTK_READS(uxl);
  DECLARE_CCTK_READS(uxr);
  DECLARE_CCTK_READS(hyl);
  DECLARE_CCTK_READS(hyr);
  DECLARE_CCTK_READS(vyl);
  DECLARE_CCTK_READS(vyr);
  DECLARE_CCTK_WRITES(syl);
  DECLARE_CCTK_WRITES(syr);
  DECLARE_CCTK_PARAMETERS;
  for (int j = 1; j < cctk_lsh[1]-1; j++)
  {
    //#pragma omp simd
    for (int i = 3; i < cctk_lsh[0]-2; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      CCTK_REAL sqr_phi_l = sqrt(grav * fabs(hxl[cc]));
      CCTK_REAL sqr_phi_r = sqrt(grav * fabs(hxr[cc]));
      CCTK_REAL sqr_phi_s = 0.5 * (sqr_phi_l + sqr_phi_r) + 0.25 * (uxl[cc]
          - uxr[cc]);
      CCTK_REAL u_s = 0.5 * (uxl[cc] + uxr[cc]) + sqr_phi_l - sqr_phi_r;
      sxl[cc] = min(uxl[cc] - sqr_phi_l, u_s - sqr_phi_s);
      sxr[cc] = max(uxr[cc] + sqr_phi_r, u_s + sqr_phi_s);
    }
  }

  // y interface;
  for (int j = 3; j < cctk_lsh[1]-2; j++)
  {
    for (int i = 1; i < cctk_lsh[0]-1; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      CCTK_REAL sqr_phi_l = sqrt(grav * fabs(hyl[cc]));
      CCTK_REAL sqr_phi_r = sqrt(grav * fabs(hyr[cc]));
      CCTK_REAL sqr_phi_s = 0.5 * (sqr_phi_l + sqr_phi_r) + 0.25 * (vyl[cc]
          - vyr[cc]);
      CCTK_REAL u_s = 0.5 * (vyl[cc] + vyr[cc]) + sqr_phi_l - sqr_phi_r;

      syl[cc] = min(vyl[cc] - sqr_phi_l, u_s - sqr_phi_s);
      syr[cc] = max(vyr[cc] + sqr_phi_r, u_s + sqr_phi_s);
    }
  }
}

// The id parameter is just here to help with debugging
// It allows you to see which HLLC you're in
void HLLC(CCTK_ARGUMENTS, CCTK_READ_ARG sl, CCTK_READ_ARG sr, CCTK_READ_ARG fl,
    CCTK_READ_ARG fr, CCTK_READ_ARG ul, CCTK_READ_ARG ur, CCTK_WRITE_ARG fout,int id)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  for (int j = 3; j < cctk_lsh[1]-2; j++)
  {
	  for (int i = 3; i < cctk_lsh[0]-2; i++)
	  {
		  int cc = FUNWAVE_INDEX( i, j);

		  if (sl[cc] >= 0.0)
		  {
			  fout[cc] = fl[cc];
		  }
		  else if (sr[cc] <= 0.0)
		  {
			  fout[cc] = fr[cc];
		  }
		  else
		  {
			  CCTK_REAL fv;
			  fv = sr[cc] * fl[cc] - sl[cc] * fr[cc] + sl[cc] * sr[cc]
			                                                                  * (ur[cc] - ul[cc]);
			  if ((fabs((CCTK_REAL)(sr[cc] - sl[cc]))) < small)
			  {
				  fout[cc] = fv / small;
				  //         !write(*,*) 'warning: (sr-sl) at left point is too small!';
				  //         !write(*,*)'sr-sl=',(sr[cc]-sl[cc]),'[cc]=', i,j;
			  }
			  else
			  {
				  fout[cc] = fv / (sr[cc] - sl[cc]);
			  }
		  }
	  }
  }
}

void construct_ho_y(CCTK_ARGUMENTS, CCTK_READ_ARG vin, CCTK_WRITE_ARG outl,
    CCTK_WRITE_ARG outr,int id)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(mask);
  DECLARE_CCTK_WRITEREADS(din);

  for (int j = 2; j < cctk_lsh[1]-1; j++) 
  {
    //#pragma omp parallel for
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j - 1);
      int jp1 = FUNWAVE_INDEX( i, j + 1);
      int jm2 = FUNWAVE_INDEX( i, j - 2);

      din[cc] = 0;
      CCTK_REAL typ1 = vin[jm1] - vin[jm2];
      CCTK_REAL typ2 = vin[cc] - vin[jm1];
      CCTK_REAL typ3 = vin[jp1] - vin[cc];

      CCTK_REAL dvp1, dvp2, dvp3;
      if (typ1 >= 0.0)
      {
        dvp1 = max(0.0, min(typ1, 2.0 * typ2, 2.0 * typ3));
      }
      else
      {
        dvp1 = min(0.0, max(typ1, 2.0 * typ2, 2.0 * typ3));
      }
      if (typ2 >= 0.0)
      {
        dvp2 = max(0.0, min(typ2, 2.0 * typ3, 2.0 * typ1));
      }
      else
      {
        dvp2 = min(0.0, max(typ2, 2.0 * typ3, 2.0 * typ1));
      }
      if (typ3 >= 0.0)
      {
        dvp3 = max(0.0, min(typ3, 2.0 * typ1, 2.0 * typ2));
      }
      else
      {
        dvp3 = min(0.0, max(typ3, 2.0 * typ1, 2.0 * typ2));
      }

      // dry d-2 j-1 j d+1, lower-order;
      if (mask[jm2] < 0.5 || mask[jp1] < 0.5)
      {
        typ2 = vin[cc] - vin[jm1];
        typ1 = typ2;
        typ3 = typ2;
        if (typ1 >= 0.0)
        {
          dvp1 = max(0.0, min(typ1, 2.0 * typ2, 2.0 * typ3));
        }
        else
        {
          dvp1 = min(0.0, max(typ1, 2.0 * typ2, 2.0 * typ3));
        }
        if (typ2 >= 0.0)
        {
          dvp2 = max(0.0, min(typ2, 2.0 * typ3, 2.0 * typ1));
        }
        else
        {
          dvp2 = min(0.0, max(typ2, 2.0 * typ3, 2.0 * typ1));
        }
        if (typ3 >= 0.0)
        {
          dvp3 = max(0.0, min(typ3, 2.0 * typ1, 2.0 * typ2));
        }
        else
        {
          dvp3 = min(0.0, max(typ3, 2.0 * typ1, 2.0 * typ2));
        }
        // here actually dvp1=dvp2=dvp3=typ2;
      }
      // dry j-2 d-1 d j+1, zero gradient;
      if (mask[jm1] < 0.5 || mask[cc] < 0.5)
      {
        dvp1 = 0;
        dvp2 = 0;
        dvp3 = 0;
      }

      din[cc] = typ2 - 1.0 / 6.0 * (dvp3 - 2.0 * dvp2 + dvp1);
    }
  }

  for (int j = 3; j < cctk_lsh[1]-2; j++)
  {
    //#pragma omp parallel for
    for (int i = 0; i < cctk_lsh[0]; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int jm1 = FUNWAVE_INDEX( i, j - 1);
      int jp1 = FUNWAVE_INDEX( i, j + 1);

      CCTK_REAL typ1, typ2, typ3, typ4;
      // jeff modified the following statements 02/14/2011;
      if (din[jm1] >= 0.0)
      {
        typ1 = max(0.0, min((CCTK_REAL)din[jm1], 4.0 * din[cc]));
      }
      else
      {
        typ1 = min(0.0, max((CCTK_REAL)din[jm1], 4.0 * din[cc]));
      }
      if (din[cc] >= 0.0)
      {
        typ2 = max(0.0, min((CCTK_REAL)din[cc], 4.0 * din[jm1]));
      }
      else
      {
        typ2 = min(0.0, max((CCTK_REAL)din[cc], 4.0 * din[jm1]));
      }
      // there was a huge bug here, 12 should versus 43, fixed. fyshi;
      if (din[cc] >= 0.0)
      {
        typ4 = max(0.0, min((CCTK_REAL)din[cc], 4.0 * din[jp1]));
      }
      else
      {
        typ4 = min(0.0, max((CCTK_REAL)din[cc], 4.0 * din[jp1]));
      }
      if (din[jp1] >= 0.0)
      {
        typ3 = max(0.0, min((CCTK_REAL)din[jp1], 4.0 * din[cc]));
      }
      else
      {
        typ3 = min(0.0, max((CCTK_REAL)din[jp1], 4.0 * din[cc]));
      }

      outl[cc] = vin[jm1] + 1.0 / 6.0 * (typ1 + 2.0 * typ2);
      outr[cc] = vin[cc] - 1.0 / 6.0 * (typ3 + 2.0 * typ4);
    }
  }
}

void construct_ho_x(CCTK_ARGUMENTS, CCTK_READ_ARG vin, CCTK_WRITE_ARG outl,
    CCTK_WRITE_ARG outr)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_WRITEREADS(din);
  DECLARE_CCTK_READS(mask);

  //#pragma omp parallel for
  for (int j = 0; j < cctk_lsh[1]; j++)
  {
    for (int i = 2; i < cctk_lsh[0]-1; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int im1 = FUNWAVE_INDEX( i - 1, j);
      int im2 = FUNWAVE_INDEX( i - 2, j);
      int ip1 = FUNWAVE_INDEX( i + 1, j);

      din[cc] = 0;
      CCTK_REAL txp1 = vin[im1] - vin[im2];
      CCTK_REAL txp2 = vin[cc] - vin[im1];
      CCTK_REAL txp3 = vin[ip1] - vin[cc];

      CCTK_REAL dvp1, dvp2, dvp3;
      if (txp1 >= 0.0)
      {
        dvp1 = max(0.0, min(txp1, 2.0 * txp2, 2.0 * txp3));
      }
      else
      {
        dvp1 = min(0.0, max(txp1, 2.0 * txp2, 2.0 * txp3));
      }
      if (txp2 >= 0.0)
      {
        dvp2 = max(0.0, min(txp2, 2.0 * txp3, 2.0 * txp1));
      }
      else
      {
        dvp2 = min(0.0, max(txp2, 2.0 * txp3, 2.0 * txp1));
      }
      if (txp3 >= 0.0)
      {
        dvp3 = max(0.0, min(txp3, 2.0 * txp1, 2.0 * txp2));
      }
      else
      {
        dvp3 = min(0.0, max(txp3, 2.0 * txp1, 2.0 * txp2));
      }

      // dry d-2 i-1 i d+1, lower-order;
      if (mask[im2] < 0.5 || mask[ip1] < 0.5)
      {
        txp2 = vin[cc] - vin[im1];
        txp1 = txp2;
        txp3 = txp2;
        if (txp1 >= 0.0)
        {
          dvp1 = max(0.0, min(txp1, 2.0 * txp2, 2.0 * txp3));
        }
        else
        {
          dvp1 = min(0.0, max(txp1, 2.0 * txp2, 2.0 * txp3));
        }
        if (txp2 >= 0.0)
        {
          dvp2 = max(0.0, min(txp2, 2.0 * txp3, 2.0 * txp1));
        }
        else
        {
          dvp2 = min(0.0, max(txp2, 2.0 * txp3, 2.0 * txp1));
        }
        if (txp3 >= 0.0)
        {
          dvp3 = max(0.0, min(txp3, 2.0 * txp1, 2.0 * txp2));
        }
        else
        {
          dvp3 = min(0.0, max(txp3, 2.0 * txp1, 2.0 * txp2));
        }
        // here actually dvp1=dvp2=dvp3=txp2;
      }
      // dry i-2 d-1 d i+1, zero gradient;
      if (mask[im1] < 0.5 || mask[cc] < 0.5)
      {
        dvp1 = 0;
        dvp2 = 0;
        dvp3 = 0;
      }

      din[cc] = txp2 - 1.0 / 6.0 * (dvp3 - 2.0 * dvp2 + dvp1);
    }
  }

  //#pragma omp parallel for
  for (int j = 0; j < cctk_lsh[1]; j++) 
  {
    for (int i = 3; i < cctk_lsh[0]-2; i++)
    {
      int cc = FUNWAVE_INDEX( i, j);
      int im1 = FUNWAVE_INDEX( i - 1, j);
      int ip1 = FUNWAVE_INDEX( i + 1, j);

      CCTK_REAL txp1, txp2, txp3, txp4;
      // jeff modified the following statements 02/14/2011;
      if (din[im1] >= 0.0)
      {
        txp1 = max(0.0, min((CCTK_REAL)din[im1], 4.0 * din[cc]));
      }
      else
      {
        txp1 = min(0.0, max((CCTK_REAL)din[im1], 4.0 * din[cc]));
      }
      if (din[cc] >= 0.0)
      {
        txp2 = max(0.0, min((CCTK_REAL)din[cc], 4.0 * din[im1]));
      }
      else
      {
        txp2 = min(0.0, max((CCTK_REAL)din[cc], 4.0 * din[im1]));
      }
      // there was a huge bug here, 12 should versus 43, fixed. fyshi;
      if (din[cc] >= 0.0)
      {
        txp4 = max(0.0, min((CCTK_REAL)din[cc], 4.0 * din[ip1]));
      }
      else
      {
        txp4 = min(0.0, max((CCTK_REAL)din[cc], 4.0 * din[ip1]));
      }
      if (din[ip1] >= 0.0)
      {
        txp3 = max(0.0, min((CCTK_REAL)din[ip1], 4.0 * din[cc]));
      }
      else
      {
        txp3 = min(0.0, max((CCTK_REAL)din[ip1], 4.0 * din[cc]));
      }

      outl[cc] = vin[im1] + 1.0 / 6.0 * (txp1 + 2.0 * txp2);
      outr[cc] = vin[cc] - 1.0 / 6.0 * (txp3 + 2.0 * txp4);
    }
  }

}

/* place holder */
void construction_mix(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  assert(0);
}

void construction_ho(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  // construct in x-direction;
  cctk_func fdecl("construction_ho");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(u);
  DECLARE_CCTK_READS(v);
  DECLARE_CCTK_READS(hu);
  DECLARE_CCTK_READS(hv);
  DECLARE_CCTK_READS(eta);
  DECLARE_CCTK_WRITEREADS(uxl);
  DECLARE_CCTK_WRITEREADS(uxr);
  DECLARE_CCTK_WRITEREADS(huxl);
  DECLARE_CCTK_WRITEREADS(huxr);
  DECLARE_CCTK_WRITEREADS(vxl);
  DECLARE_CCTK_WRITEREADS(vxr);
  DECLARE_CCTK_WRITES(hvxl);
  DECLARE_CCTK_WRITES(hvxr);
  DECLARE_CCTK_WRITEREADS(etarxr);
  DECLARE_CCTK_WRITEREADS(etarxl);
  DECLARE_CCTK_WRITEREADS(hxl);
  DECLARE_CCTK_WRITEREADS(hxr);
  DECLARE_CCTK_READS(depthx);
  DECLARE_CCTK_READS(u4);
  DECLARE_CCTK_WRITEREADS(u4xl);
  DECLARE_CCTK_WRITEREADS(u4xr);
  DECLARE_CCTK_WRITEREADS(pl);
  DECLARE_CCTK_WRITEREADS(pr);
  DECLARE_CCTK_READS(mask9);
  DECLARE_CCTK_WRITES(fxl);
  DECLARE_CCTK_WRITES(fxr);
  DECLARE_CCTK_WRITES(gxl);
  DECLARE_CCTK_WRITES(gxr);
  DECLARE_CCTK_WRITEREADS(uyl);
  DECLARE_CCTK_WRITEREADS(uyr);
  DECLARE_CCTK_WRITEREADS(vyl);
  DECLARE_CCTK_WRITEREADS(vyr);
  DECLARE_CCTK_WRITEREADS(hvyl);
  DECLARE_CCTK_WRITEREADS(hvyr);
  DECLARE_CCTK_WRITES(huyl);
  DECLARE_CCTK_WRITES(huyr);
  DECLARE_CCTK_WRITEREADS(etaryl);
  DECLARE_CCTK_WRITEREADS(etaryr);
  DECLARE_CCTK_WRITEREADS(hyl);
  DECLARE_CCTK_WRITEREADS(hyr);
  DECLARE_CCTK_READS(depthy);
  DECLARE_CCTK_READS(v4);
  DECLARE_CCTK_WRITEREADS(v4yl);
  DECLARE_CCTK_WRITEREADS(v4yr);
  DECLARE_CCTK_WRITEREADS(ql);
  DECLARE_CCTK_WRITEREADS(qr);
  DECLARE_CCTK_WRITES(gyl);
  DECLARE_CCTK_WRITES(gyr);
  DECLARE_CCTK_WRITES(fyl);
  DECLARE_CCTK_WRITES(fyr);

  debug = false;
  construct_ho_x(CCTK_PASS_CTOC, u, uxl, uxr);
  construct_ho_x(CCTK_PASS_CTOC, v, vxl, vxr);
  construct_ho_x(CCTK_PASS_CTOC, hu, huxl, huxr);
  construct_ho_x(CCTK_PASS_CTOC, hv, hvxl, hvxr);
  debug = true;
  construct_ho_x(CCTK_PASS_CTOC, eta, etarxl, etarxr);
  debug = false;

  if(!dispersion_on) {
	  for (int i = 0; i < cctk_lsh[0]; i++)
	  {
		  for (int j = 0; j < cctk_lsh[1]; j++)
		  {
			int cc = FUNWAVE_INDEX( i, j);
            u4xl[cc] = 0;
            u4xr[cc] = 0;
            v4yl[cc] = 0;
            v4yr[cc] = 0;
          }
      }
  }

  // dispersion;
  for (int i = 3; i < cctk_lsh[0]-2; i++)
  {
    for (int j = 0; j < cctk_lsh[1]; j++) 
    {
      int cc = FUNWAVE_INDEX( i, j);

      hxl[cc] = etarxl[cc] + depthx[cc];
      hxr[cc] = etarxr[cc] + depthx[cc];
    }
  }

  if(!spherical_coordinates)
  {
	  if (dispersion_on)
	  {
		  construct_ho_x(CCTK_PASS_CTOC,u4,u4xl,u4xr);
	  }

    //#pragma omp parallel for
	  for (int j = 2; j < cctk_lsh[1]-2; j++)
	  {
        //#pragma omp simd
	      for (int i = 3; i < cctk_lsh[0]-2; i++)
	      {
			  int cc = FUNWAVE_INDEX( i, j);

			  pl[cc] = huxl[cc] + gamma1 * mask9[cc] * hxl[cc] * u4xl[cc];
			  pr[cc] = huxr[cc] + gamma1 * mask9[cc] * hxr[cc] * u4xr[cc];
			  fxl[cc] = gamma3 * pl[cc] * (uxl[cc] + gamma1 * mask9[cc] * u4xl[cc])
        		  + 0.5 * grav * ((etarxl[cc]) * (etarxl[cc]) * gamma3 + 2.0
        				  * (etarxl[cc]) * (depthx[cc]));
			  fxr[cc] = gamma3 * pr[cc] * (uxr[cc] + gamma1 * mask9[cc] * u4xr[cc])
        		  + 0.5 * grav * ((etarxr[cc]) * (etarxr[cc]) * gamma3 + 2.0
        				  * (etarxr[cc]) * (depthx[cc]));
			  gxl[cc] = gamma3 * hxl[cc] * uxl[cc] * vxl[cc];
			  gxr[cc] = gamma3 * hxr[cc] * uxr[cc] * vxr[cc];
		  }
	  }
  } else {
    //#pragma omp parallel for
	  for (int i = 3; i < cctk_lsh[0]-2; i++)
	  {
      //#pragma omp simd
		  for (int j = 0; j < cctk_lsh[1]; j++)
		  {
			  int cc = FUNWAVE_INDEX( i, j);
			  pl[cc]=huxl[cc];
			  pr[cc]=huxr[cc];
			  fxl[cc]=gamma3*pl[cc]*uxl[cc]
			                            +0.5*grav*((etarxl[cc])*(etarxl[cc])*gamma3+2.0*(etarxl[cc])*(depthx[cc]));
			  fxr[cc]=gamma3*pr[cc]*uxr[cc]
			                            +0.5*grav*((etarxr[cc])*(etarxr[cc])*gamma3+2.0*(etarxr[cc])*(depthx[cc]));
			  gxl[cc] = gamma3 * hxl[cc] * uxl[cc] * vxl[cc];
			  gxr[cc] = gamma3 * hxr[cc] * uxr[cc] * vxr[cc];
		  }
	  }
  }

  // construct in y-direction;
  construct_ho_y(CCTK_PASS_CTOC, u, (CCTK_WRITE_ARG)uyl, (CCTK_WRITE_ARG)uyr, 0);
  construct_ho_y(CCTK_PASS_CTOC, v, (CCTK_WRITE_ARG)vyl, (CCTK_WRITE_ARG)vyr, 1);
  construct_ho_y(CCTK_PASS_CTOC, hv, (CCTK_WRITE_ARG)hvyl, (CCTK_WRITE_ARG)hvyr, 2);
  construct_ho_y(CCTK_PASS_CTOC, hu, (CCTK_WRITE_ARG)huyl, (CCTK_WRITE_ARG)huyr, 3);
  construct_ho_y(CCTK_PASS_CTOC, eta, (CCTK_WRITE_ARG)etaryl, (CCTK_WRITE_ARG)etaryr, 4);

  for (int i = 0; i < cctk_lsh[0]; i++)
  {
    for (int j = 3; j < cctk_lsh[1]-2; j++)
    {
      int cc = FUNWAVE_INDEX( i, j);

      // dispersion;
      hyl[cc] = etaryl[cc] + depthy[cc];
      hyr[cc] = etaryr[cc] + depthy[cc];
    }
  }

  if(!spherical_coordinates)
  {
	  if (dispersion_on)
	  {
		  construct_ho_y(CCTK_PASS_CTOC,(CCTK_READ_ARG)v4,(CCTK_WRITE_ARG)v4yl,(CCTK_WRITE_ARG)v4yr,99);
	  }

    //#pragma omp parallel for
	  for (int j = 3; j < cctk_lsh[1]-2; j++)
	  {
		  for (int i = 2; i < cctk_lsh[0]-2; i++)
		  {
			  int cc = FUNWAVE_INDEX( i, j);

			  ql[cc] = hvyl[cc] + gamma1 * mask9[cc] * hyl[cc] * v4yl[cc];
			  qr[cc] = hvyr[cc] + gamma1 * mask9[cc] * hyr[cc] * v4yr[cc];
			  gyl[cc] = gamma3 * ql[cc] * (vyl[cc] + gamma1 * mask9[cc] * v4yl[cc])
        		  + 0.5 * grav * ((etaryl[cc]) * (etaryl[cc]) * gamma3 + 2.0
        				  * (etaryl[cc]) * (depthy[cc]));
			  gyr[cc] = gamma3 * qr[cc] * (vyr[cc] + gamma1 * mask9[cc] * v4yr[cc])
        		  + 0.5 * grav * ((etaryr[cc]) * (etaryr[cc]) * gamma3 + 2.0
        				  * (etaryr[cc]) * (depthy[cc]));
			  fyl[cc] = gamma3 * hyl[cc] * uyl[cc] * vyl[cc];
			  fyr[cc] = gamma3 * hyr[cc] * uyr[cc] * vyr[cc];
		  }
	  }
  } else {
    //#pragma omp parallel for
	  for (int i = 0; i < cctk_lsh[0]; i++)
	  {
		  for (int j = 3; j < cctk_lsh[1]-2; j++)
		  {
			  int cc = FUNWAVE_INDEX( i, j);
			  ql[cc]=hvyl[cc];
			  qr[cc]=hvyr[cc];
			  gyl[cc]=gamma3*ql[cc]*vyl[cc]
			                            +0.5*grav*((etaryl[cc])*(etaryl[cc])*gamma3+2.0*(etaryl[cc])*(depthy[cc]));
			  gyr[cc]=gamma3*qr[cc]*vyr[cc]
			                            +0.5*grav*((etaryr[cc])*(etaryr[cc])*gamma3+2.0*(etaryr[cc])*(depthy[cc]));

			  fyl[cc] = gamma3 * hyl[cc] * uyl[cc] * vyl[cc];
			  fyr[cc] = gamma3 * hyr[cc] * uyr[cc] * vyr[cc];
		  }
	  }

  }

}

void flux_at_interface_hllc(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("flux_at_interface_hllc");
  cctk_auto_thorn = "FUNWAVE";
  DECLARE_CCTK_READS(sxl);
  DECLARE_CCTK_READS(sxr);
  DECLARE_CCTK_READS(pl);
  DECLARE_CCTK_READS(pr);
  DECLARE_CCTK_READS(etarxl);
  DECLARE_CCTK_READS(etarxr);
  DECLARE_CCTK_READS(syl);
  DECLARE_CCTK_READS(syr);
  DECLARE_CCTK_READS(ql);
  DECLARE_CCTK_READS(qr);
  DECLARE_CCTK_READS(etaryl);
  DECLARE_CCTK_READS(etaryr);
  DECLARE_CCTK_READS(fxl);
  DECLARE_CCTK_READS(fxr);
  DECLARE_CCTK_READS(huxl);
  DECLARE_CCTK_READS(huxr);
  DECLARE_CCTK_READS(fyl);
  DECLARE_CCTK_READS(fyr);
  DECLARE_CCTK_READS(huyl);
  DECLARE_CCTK_READS(huyr);
  DECLARE_CCTK_READS(gxl);
  DECLARE_CCTK_READS(gxr);
  DECLARE_CCTK_READS(hvxl);
  DECLARE_CCTK_READS(hvxr);
  DECLARE_CCTK_READS(gyl);
  DECLARE_CCTK_READS(gyr);
  DECLARE_CCTK_READS(hvyl);
  DECLARE_CCTK_READS(hvyr);
  DECLARE_CCTK_WRITES(p);
  DECLARE_CCTK_WRITES(q);
  DECLARE_CCTK_WRITES(fx);
  DECLARE_CCTK_WRITES(fy);
  DECLARE_CCTK_WRITES(gx);
  DECLARE_CCTK_WRITES(gy);
  HLLC(CCTK_PASS_CTOC, sxl, sxr, pl, pr, etarxl, etarxr, p,5);
  HLLC(CCTK_PASS_CTOC, syl, syr, ql, qr, etaryl, etaryr, q,6);
  HLLC(CCTK_PASS_CTOC, sxl, sxr, fxl, fxr, huxl, huxr, fx,7);
  HLLC(CCTK_PASS_CTOC, syl, syr, fyl, fyr, huyl, huyr, fy,8);
  HLLC(CCTK_PASS_CTOC, sxl, sxr, gxl, gxr, hvxl, hvxr, gx,9);
  HLLC(CCTK_PASS_CTOC, syl, syr, gyl, gyr, hvyl, hvyr, gy,10);
}

/* a dummy function */
void funwave_fluxes(CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  cctk_func fdecl("funwave_fluxes");
  DECLARE_CCTK_PARAMETERS;
  if (CCTK_Equals(reconstruction_scheme, "fourth"))
  {
    construction_ho( CCTK_PASS_CTOC);
  }
  else if (CCTK_Equals(reconstruction_scheme, "mixed"))
  {
    construction_mix( CCTK_PASS_CTOC);
  }
  else
  {
    CCTK_WARN(0, "specified reconstruction scheme is not supported !");
  }
  wave_speed( CCTK_PASS_CTOC);

  if (CCTK_Equals(riemann_solver, "HLLC"))
  {
    flux_at_interface_hllc(CCTK_PASS_CTOC);
  }
  else
  {
    CCTK_WARN(0, "specified riemann solver is not supported !");
  }
}
