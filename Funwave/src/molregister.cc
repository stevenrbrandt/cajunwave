#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "funwave_util.hh"

void funwave_registervars (CCTK_ARGUMENTS)
{
  _DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT ierr = 0, group, rhs;

  group = CCTK_GroupIndex ("funwave::evolvar_group");
  rhs = CCTK_GroupIndex ("funwave::evolrhs_group");

  if (CCTK_IsFunctionAliased ("MoLRegisterEvolvedGroup"))
  {
    ierr += MoLRegisterEvolvedGroup (group, rhs);
  }
  else
  {
    CCTK_WARN (0, "MoL function not aliased !");
    ierr++;
  }

  if (ierr)
    CCTK_WARN (0, "Problems registering with MoL !");

  return;
}
