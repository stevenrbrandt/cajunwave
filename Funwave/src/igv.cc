#include <math.h>
#include <stdlib.h>
#include <assert.h>

inline double flog(double x) {
  return log(fabs(x)+1.0e-100);
}

inline double igv2(double a,double b,double c,double z0,double z2) {
  double del = 0.5*(z2-z0);
  double z1 = 0.5*(z0+z2);
  double i1 = sqrt(a+b*z1+c*z1*z1);
  double i0 = sqrt(a+b*z0+c*z0*z0);
  double i2 = sqrt(a+b*z2+c*z2*z2);
  double pl = 2*del*i1;
  double s2 = sqrt(2.);
  double i02 = i0*i0;
  double i04 = i02*i02;
  double i06 = i04*i02;
  double i22 = i2*i2;
  double i24 = i22*i22;
  double i26 = i24*i22;

  return (del*pow(s2,-1.)*pow(i02 + i22 - 2*pow(i1,2.),-1.5)*
      (flog(-((3*i02 + i22 - 4*pow(i1,2.) - 2*i0*s2*pow(i02 + i22 - 2*pow(i1,2.),0.5))*
             pow(i02 + 3*i22 - 4*pow(i1,2.) + 2*i2*s2*pow(i02 + i22 - 2*pow(i1,2.),0.5),-1.)))*
         (i04 - 2*i02*(i22 + 4*pow(i1,2.)) + pow(i22 - 4*pow(i1,2.),2.)) + 
        2*i0*s2*(3*i02 + i22 - 4*pow(i1,2.))*pow(i02 + i22 - 2*pow(i1,2.),0.5) + 
        2*i2*s2*(i02 + 3*i22 - 4*pow(i1,2.))*pow(i02 + i22 - 2*pow(i1,2.),0.5)))/8.;
}

inline double igv3(double a,double b,double c,double z0,double z2) {
  return -pow((a + b*z0)*pow(b,-6.)*pow(2*a*b*(5*b - 2*c*z0) + 8*c*pow(a,2.) + z0*(10*b + 3*c*z0)*pow(b,2.),2.),
       0.5)/15. + pow((a + b*z2)*pow(b,-6.)*pow(2*a*b*(5*b - 2*c*z2) + 8*c*pow(a,2.) + z2*(10*b + 3*c*z2)*pow(b,2.),
        2.),0.5)/15.;
}

inline double igv0(double a,double b,double c,double z0,double z2) {
  return (-(z0*pow(pow(a,-3.)*pow(4*a*z0*(3*b + 2*c*z0) + 48*pow(a,2.) - 3*b*c*pow(z0,3.),2.),0.5)) + 
      z2*pow(pow(a,-3.)*pow(4*a*z2*(3*b + 2*c*z2) + 48*pow(a,2.) - 3*b*c*pow(z2,3.),2.),0.5))/48.;
}

double igv(double a,double b,double c,double z0,double z2) {
  // sqrt(a+b*z+c*z*z)
  if(z0 == z2) return 0;
  assert(z0 < z2);
  double a2 = a+b*z0+c*z0*z0;
  double b2 = -(b*z0) - 2*c*z0*z0 + b*z2 + 2*c*z0*z2;
  double c2 = c*(z0*z0-2*z0*z2+z2*z2);
  assert(a2 > 0);
  assert(b2 > 0);
  assert(c2 > 0);
  double lx = flog(c2)-flog(a2);
  double ly = flog(b2)-flog(a2);
  // Empirically determined
  if(lx < -14 and ly < -10)
    return igv0(a2,b2,c2,0,1);
  else if(ly > lx+4)
    return igv3(a2,b2,c2,0,1);
  else
    return igv2(a2,b2,c2,0,1);
}
