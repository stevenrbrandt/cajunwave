#!/usr/bin/env perl
use strict;
open(FD, $ARGV[0]) or die;

my $data = {};
my %t = ();
my %x = ();
my %y = ();

while(<FD>) {
    # skip blank or comment lines 
    next if(/^\s(#.*)$/);

    # Remove trailing spaces
    s/\s+$//;

    my @cols = split(/\s+/);
    if($#cols < 2) {
        print;
        next;
    }
    my ($t,$x,$y) = ($cols[0], $cols[1], $cols[2]);
    $t{$t} = 1; $x{$x} = 1; $y{$y} = 1;
    $data->{$t}->{$x}->{$y} = \@cols;
}
for my $t (sort keys %t) {
    for my $x (sort keys %x) {
        for my $y (sort keys %y) {
            print join(" ",@{$data->{$t}->{$x}->{$y}}),"\n"
                if(defined($data->{$t}->{$x}->{$y}));
        }
    }
}
