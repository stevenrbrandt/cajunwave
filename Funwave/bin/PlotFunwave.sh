#!/bin/sh

#================================================
# Plotting parameters (Only change this sec)
#================================================

# If the file name of the last time step is 
DATAFILE="eta_xy_[0].asc"
PREFIX=gauhump
TITLE="Gaussian Hump"
NUM_STEPS=1000
NUM_CORES=8
# Animation format. It can be gif, mpg, etc.
ANIM_FORMAT=gif

#================================================
# No need to change the rest under this line
# Depending on if you want to plot the coast line
# the plot comand will be different though 
#================================================
printlogo()
{
echo "
============================================
o--o           o--o  o      o  
|              |   | |      |  
O-o  o  o o-o  O--o  | o-o -o- 
|    |  | |  | |     | | |  |  
o    o--o o  o o     o o-o  o 
============================================

A plotting script for FunWAVE 

Jian Tao <jtao@cct.lsu.edu>
Sept, 2011
"
}

printlogo


#================================================
# Parsing trajectory analysis data
#================================================

for step in `seq -w $NUM_STEPS`
do
  echo "  Processing ${PREFIX}_${step}"
  row=`expr $step / $NUM_CORES`
  col=`expr $step % $NUM_CORES`

#================================================
# Generating gnuplot scripts for all cores
#================================================

  echo "
!echo \"  Plotting time step ${step}\"
set xlabel \"X\" 
set ylabel \"Y\" 
set zlabel \"H\" 
unset key
#unset grid
unset surface
set palette rgbformulae 22,13,-31
set cbrange [-0.5:1]
set pm3d

#set term postscript eps enhanced color colortext
set term png giant
set title \"${TITLE} ${step}\"
set output \"${PREFIX}_${step}.png\"
set xrange [0:30]
set yrange [0:30]
#set view 10,70,1,1
#unset xtics
#unset ytics
splot [][][-1:1] \"${DATAFILE}\" index `expr ${step} + 0` t \"Time = ${step}\"

  " >> myLE$col.gpl
done

echo "Done parsing !"


#================================================
# Creating figures for all time steps
#================================================

echo "Creating PNG files on $NUM_CORES cores for each time step ..."
for (( k = 0; k<$NUM_CORES; k++ ))
do
  gnuplot myLE$k.gpl &
done

# wait until all parallel jobs finish.
wait

echo "Done plotting !"
echo ""


#================================================
# Creating movies and cleaning up the mess
#================================================

echo "Creating movies from PNG files ..."
convert -verbose -quality 100 ${PREFIX}*.png ${PREFIX}.${ANIM_FORMAT}
echo "Done converting !"
rm myLE*.gpl
printlogo
