#!/usr/bin/perl
# Determine the size of numeric data files that
# are consumed or produced by funwave
use strict;
use FileHandle;
use Data::Dumper;

my %sizes = ();
my $fd = new FileHandle;
open($fd,$ARGV[0]) or die "cannot open $ARGV[0]";
while(<$fd>) {
    s/^\s+//;
    s/\s+$//;
    my @a = split(/\s+/);
    if($#a >= 0) {
        $sizes{$#a+1}++;
    }
}
my @sizes = keys %sizes;
if($#sizes == 0) {
    print "Grid is $sizes[0] x $sizes{$sizes[0]}\n";
} else {
    print "Bad Grid: ",Dumper(\%sizes);
}
