# Parameter definitions for thorn FUNWAVE

#####################################################
#            Shared Parameters                  #
#####################################################
shares: funwavecoord

USES CCTK_REAL earth_a

USES CCTK_REAL earth_b

USES CCTK_REAL xmin

USES CCTK_REAL xmax 

USES CCTK_REAL ymin 

USES CCTK_REAL ymax 

USES KEYWORD coordinate_units

shares: MethodOfLines

USES CCTK_INT MoL_Num_Evolved_Vars

shares: FunwaveCoord

USES BOOLEAN spherical_coordinates

#shares: Time

#USES CCTK_REAL dtfac

restricted:

CCTK_INT funwave_maxnum_evolvedvars "The maximum number of evolved variables used by Funwave" ACCUMULATOR-BASE=MethodofLines::MoL_Num_Evolved_Vars
{
  3:3:: "Number of evolved variables used by FreeSurf"
}3

#----------------------------------------------------
#  Parameter Sections (metric unit systems)
#  1. Station Data
#  2. Parameters Controling Theoretical Models
#  3. Parameters Controling Numerical Schemes
#  4. Parameters Controling Intial Condition
#  5. Some Semi-Constants and Controlling Parameters
#----------------------------------------------------

#####################################################
#               Station Data                        #
#####################################################

#----------------------------------------------------
# Number of stations
#----------------------------------------------------

CCTK_INT number_stations "number of stations"
{

  0: :: "if not 0, station_file must be set"

}0

#----------------------------------------------------
# Observational station file
#----------------------------------------------------

STRING station_file "list of position of stations"
{

  .* :: "list of station postions in x, y pairs"

}"station_file.txt"


BOOLEAN periodic "Do we want periodic conditions?"
{
}"no"

#----------------------------------------------------
# Whether to turn on mixing
#----------------------------------------------------

BOOLEAN mixing "Do we want to use mixing?"
{

}"no"

#----------------------------------------------------
# Enable or disable the linear dispersive terms
#----------------------------------------------------

CCTK_REAL gamma1 "parameter for linear dispersive terms"
{

  0.0:1.0:: "1 inclusion of linear dispersive terms, 0 no linear dispersive terms"

}1.0

#----------------------------------------------------
# Enable or disable the nonlinear dispersive terms
#----------------------------------------------------

CCTK_REAL gamma2 "parameter for nonlinear dispersive terms"
{

  0.0:1.0:: "1 inclusion of nonlinear dispersive terms, 0 no nonlinear dispersive terms"

}1.0

#----------------------------------------------------
# Simplify to NSWE or stay with the Boussinesq equ.
#----------------------------------------------------

CCTK_REAL gamma3 "parameter for linear shallow water equations"
{

  0.0:1.0:: "When gamma3 = 0.0, gamma1 and gamma2 automatically become zero"

}1.0

#----------------------------------------------------
# A parameter to define the reference level
#----------------------------------------------------

CCTK_REAL beta_ref "parameter beta defined for the reference level"
{

  -1.0:-0.01 :: "beta = -0.531 for NG's and FUNWAVE equations"

}-0.531

#----------------------------------------------------
# Bottom friction coefficient
#----------------------------------------------------

CCTK_REAL cd "bottom friction coefficient"
{

  0.0:10.0 :: "need to set a reasonable upper limit"

}5.0e-3

#----------------------------------------------------
# Determine when we can safely switch from Boussinesq to NSWE
#----------------------------------------------------

CCTK_REAL swe_eta_dep "ratio of height/depth for switching from Boussinesq to NSWE"
{

  0.1:1.0 :: ""

}0.7

#----------------------------------------------------
# Determine if we want to turn on the dispersion terms
#----------------------------------------------------

BOOLEAN dispersion_on "Do we want to enable dispersion terms ?"
{

}"no"

#----------------------------------------------------
# Shall we move the time derivative dispersive terms to LHS?
#----------------------------------------------------

BOOLEAN disp_time_left "Do we want to move the time derivate dispersive terms to LHS ?"
{

}"no"

BOOLEAN enable_masks "Do we want to include mask logic in the simulation?"
{
} "yes"

#----------------------------------------------------
# Boundary condition
#----------------------------------------------------

KEYWORD boundary "boundary condition for the evolved variables"
{
  "funwave" :: "what's in Dr. Shi's code"
}"funwave"

#----------------------------------------------------
# Wavemakers
#----------------------------------------------------

KEYWORD wavemaker_type "types of wave makers"
{
  "ini_rec" :: "initial rectangular hump, need xc,yc and wid"
  "lef_sol" :: "initial solitary wave, WKN B solution, need amp, dep"
  "ini_oth" :: "other initial distribution specified by users"
  "wk_reg" :: "Wei and Kirby 1999 internal wave maker, need xc_wk, tperiod, amp_wk, dep_wk, theta_wk, and time_ramp (factor of period)"
  "wk_irr" :: "Wei and Kirby 1999 TMA spectrum wavemaker, need xc_wk, dep_wk, time_ramp, delta_wk, freqpeak, freqmin, freqmax, hmo, gammatma, theta-peak"
  "wk_time_series" :: "fft a time series to get each wave component and then use Wei and Kirby's ( 1999) wavemaker. Need input wavecompfile (including 3 columns:per,amp,pha) and numwavecomp, peakperiod, dep_wk, xc_wk, ywidth_wk"
  "ini_gau" :: "initial Gaussian hump, need amp, xc, yc, and wid"
  "ini_sol" :: "initial solitary wave, xwavemaker"
}"wk_reg"

CCTK_BOOLEAN ini_uvz "Whether to read initial values for u, v, eta from data files"
{
} false

#----------------------------------------------------
# Wavemakers (mainly ini_sol, lef_sol related)
#----------------------------------------------------

CCTK_REAL amp "amplitude (m) of initial eta, if wavemaker = ini_rec, wavemaker = ini_sol, wavemaker = lef_sol, wavemaker = gausian"
{
  0.0:1000.0 :: "need proper range"
}1.0

CCTK_REAL wdep "water depth at wavemaker location, if wavemaker = ini_sol, wavemaker = lef_sol"
{
  0.0:10000.0 :: "need proper range"
}10.0

CCTK_REAL lagtime "time lag (s) for the solitary wave generated on the left boundary, e.g.,wavemaker = lef_sol"
{
  0.0:100.00 :: "need proper range"
}10.0

CCTK_REAL xwavemaker "x (m) coordinate for wavemaker = ini_sol"
{
  0.0:10000000.0 :: "need proper range"
} 0.0

#----------------------------------------------------
# Wavemakers (mainly ini_rec, ini_gau related) gaussian requires amp as well
#----------------------------------------------------

CCTK_REAL xc "x (m) coordinate of the center of a rectangular hump if wavemaker = ini_rec"
{
  -1.0e7:1.0e7 :: "need proper range"
}3.0

CCTK_REAL yc "y (m) coordinate of the center of a rectangular hump if wavemaker = ini_rec"
{
  -1.0e7:1.0e7 :: "need proper range"
}3.0

CCTK_REAL wid "width (m) of a rectangular hump if wavemaker = ini_rec or ini_gau"
{
  1.0e-7:*  :: "need a positive number"
}1.0

#----------------------------------------------------
# Wavemakers (Wei and Kirby regular (1999))
#----------------------------------------------------

CCTK_REAL time_ramp "time ramp (s) for Wei and Kirby (1999) wavemaker"
{
  0.01:100.0 :: "need proper range"
}1.0

CCTK_REAL delta_wk "width parameter delta for Wei and Kirby (1999) wavemaker"
{
  0.1:1.0 :: "need proper range"
}0.4

CCTK_REAL dep_wk "water depth (m) for Wei and Kirby (1999) wavemaker"
{
  0.0:10000.0 :: "need proper range"
}0.5

CCTK_REAL xc_wk "x coordinate (m) for Wei and Kirby (1999) wavemaker"
{
  0.0:100000.0 :: "need proper range"
}0.0

CCTK_REAL ywidth_wk "width (m) in y direction for Wei and Kirby (1999) wavemaker"
{
  1.0:1000000.0 :: "need proper range"
}1000.0

CCTK_REAL tperiod "period (s) of regular wave for Wei and Kirby (1999) wavemaker"
{
  1.0:1000.0 :: "need proper range"
}2.0

CCTK_REAL amp_wk "amplitude (m) of regular wave for Wei and Kirby (1999) wavemaker"
{
  0.0:100000.0 :: "need proper range"
}1.0

CCTK_REAL theta_wk "direction (degrees) of regular wave for Wei and Kirby (1999) wavemaker. Note: it may be adjusted for a periodic boundary case by the program. A warning will be given if adjustment is made"
{
  -180.0:180.0 :: "need proper range"
}0.0

#----------------------------------------------------
# Wavemakers (Wei and Kirby (1999) irregular)
#----------------------------------------------------

CCTK_REAL peak_period "Peak period for wavemaker"
{
  0:1000.0 :: "Figure out what a good range is for this"
}5.0

CCTK_INT num_frequencies "number of frequences for irregular wavemaker"
{
  1:10000 :: "no particular limit"
}100

CCTK_INT num_wave_components "number of wave components for time series wavemaker"
{
  1:10000 :: "no particular limit"
}10

CCTK_STRING wave_component_file "the list of wave components"
{
  .* :: ""
} "wave_components.txt"

CCTK_REAL freqpeak "peak frequency (1/s) for Wei and Kirby (1999) irregular wavemaker"
{
  0.0 : :: "need proper range"
}1.0

CCTK_REAL freqmin "low frequency cutoff (1/s) for Wei and Kirby (1999) irregular wavemaker"
{
  0.0 : :: "need proper range"
}1.0e-6

CCTK_REAL freqmax "high frequency cutoff (1/s) for Wei and Kirby (1999) irregular wavemaker"
{
  0.0 : :: "need proper range"
}1.0e6

CCTK_REAL hmo "hmo (m) for Wei and Kirby (1999) irregular wavemaker"
{
  0.0 : :: "need proper range"
}1.0

CCTK_REAL gammatma "TMA parameter gamma for Wei and Kirby (1999) irregular wavemaker"
{
  0.0:20.0 :: "need proper range"
}1.0

CCTK_REAL thetapeak "peak direction (degrees) for Wei and Kirby (1999) irregular wavemaker"
{
  -180.0:180.0 :: "need proper range"
}1.0

CCTK_REAL sigma_theta "parameter of directional spectrum for Wei and Kirby (1999) irregular wavemaker"
{
  0.0:* :: "need proper range"
}1.0

#####################################################
#    Parameters Controling Numerical Schemes        #
#####################################################

#----------------------------------------------------
# Enable time estimator to make adaptive time integration
#----------------------------------------------------

BOOLEAN estimate_dt_on "do you want to turn on the dt estimator ?"
{
} "true"

CCTK_REAL dt_size "Use zero for adaptive"
{
	0:10000 :: "the time step size"
}0

CCTK_REAL dtfac "dt factor. should be the same as time::dtfac for consistency"
{

  0:* :: "need a reasonable range"

}0.5

#----------------------------------------------------
# The Riemann Solver uses in getting the fluxes
#----------------------------------------------------

KEYWORD riemann_solver "the riemann_solver"
{

  "HLLC" :: "Harten-Lax-van Leer Contact Riemann solver"

}"HLLC"

#----------------------------------------------------
# Reconstruction scheme
#----------------------------------------------------

KEYWORD reconstruction_scheme "the reconstruction scheme"
{

  "mixed" :: "mixed-order interface construction"
  "fourth" :: "fourth-order interface construction"

}"fourth"

#----------------------------------------------------
# Wave breaking
#----------------------------------------------------

CCTK_REAL cbrk1 "parameter C1 in Kennedy et al. (2000)"
{

  0.0:10.0 :: "need a reasonable range"

}0.65

CCTK_REAL cbrk2 "parameter C2 in Kennedy et al. (2000)"
{

  0.0:10.0 :: "need a reasonable range"

}0.35

#----------------------------------------------------
# Sponge treatment
#----------------------------------------------------

BOOLEAN sponge_on "Do we want to enable sponge layer ?"
{

}"yes"

CCTK_REAL sponge_decay_rate "decay rate in in sponge layer, r_sponge in Funwave"
{

  0.0:10.0 :: "its values are between 0.85 and 0.95."

}0.9

CCTK_REAL sponge_damping_magnitude "maximum damping magnitude, a_sponge in Funwave"
{

  0.0:100.0 :: "The value is ~ 5.0."

}5.0

CCTK_REAL sponge_east_width "width of sponge layer at east boundary"
{

  0.0:* :: "any number greater than zero"

}0.0

CCTK_REAL sponge_south_width "width of sponge layer at south boundary"
{

  0.0:* :: "any number greater than zero"

}0.0

CCTK_REAL sponge_west_width "width of sponge layer at west boundary"
{

  0.0:* :: "any number greater than zero"

}0.0

CCTK_REAL sponge_north_width "width of sponge layer at north boundary"
{

  0.0:* :: "any number greater than zero"

}0.0

#####################################################
#     Parameters Controling Intial Condition        #
#####################################################

#----------------------------------------------------
# Default value of all the variables
#----------------------------------------------------

#----------------------------------------------------
# Initial condition
#----------------------------------------------------

KEYWORD initial_data "initial condition"
{

  "gausian" :: "Gausian Wave"

}"gausian"

#----------------------------------------------------
# Depth type
#----------------------------------------------------

KEYWORD depth_type "how to intialized depth ?"
{

  "data" :: "read depth information from a file"
  "flat" :: "flat bottom, need DEPTH FLAT"
  "slope" :: "plane beach along x direction"

}"flat"

#----------------------------------------------------
# (depth_type == "data")
#----------------------------------------------------

STRING depth_file "bathymetry file if depth_type==data"
{

  .* :: ""

}"depth_file.txt"

STRING u_file "u-velocity data"
{

  .* :: ""

}"u_file.txt"

STRING v_file "v-velocity data"
{

  .* :: ""

}"v_file.txt"

STRING z_file "eta data"
{

  .* :: ""

}"z_file.txt"

STRING vegetation_height_file "bathymetry file if depth_type==data"
{

  .* :: ""

}"NONE"

CCTK_INT depth_file_offset_x "X position of start of depth file"
{
    -100:100 :: "offset"
}3

CCTK_INT depth_file_offset_y "Y position of start of depth file"
{
    -100:100 :: "offset"
}3

KEYWORD depth_format "what is the format of the depth file ?"
{

  "ele" :: "the depth in depth_file is cell centered"
  "nod" :: "the depth in depth_file is on the grid points"

}"ele"

#----------------------------------------------------
# (depth_type == "flat") || (depth_type == "slope")
#----------------------------------------------------

CCTK_REAL depth_flat "water depth of flat bottom"
{

  0.0:1.1e4 :: "a reasonable water depth of flat bottom"

}4.5e3

#----------------------------------------------------
# (depth_type == "slope")
#----------------------------------------------------

CCTK_REAL depth_xslp "starting x position of a slope"
{

  0.0:1.0e3 :: "a reasonable starting x position"

}3.0e2

CCTK_REAL depth_slope "slope if depth_type==slope"
{

  0.0:10.0 :: "a reasonable depth slope"

}3.5e-2

#####################################################
#  Some Semi-Constants and Controlling Parameters   #
#####################################################

#----------------------------------------------------
# Earth radius
#----------------------------------------------------

CCTK_REAL r_earth "Earth radius"
{

  6.351e6:6.385e6 :: "A reasonable radius of the Earth"

}6.371e6

#----------------------------------------------------
# Acceleration of gravity near Earth surface
#----------------------------------------------------

CCTK_REAL grav "Earth gravity acceleration"
{

  9.780:9.832 :: "A reasonable Earth gravity acceleration"

}9.810

#----------------------------------------------------
# Minimium where tricks may appear (in metric units)
#----------------------------------------------------

CCTK_REAL small "the number that we consider small in the code"
{

  0.0:1.0e-5 :: "a reasonalblely small number"

}1.0e-6

#----------------------------------------------------
# Maximium where tricks may appear (in metric units)
#----------------------------------------------------

CCTK_REAL large "the number that we consider large in the code"
{

  0.0:1.0e10 :: "a reasonalblely large number"

}1.0e5

#----------------------------------------------------
# The maxinum Froude number
# The Froude number is defined as:
# F=V/c, where V is a characteristic velocity ,
# and c is a characteristic water wave propagation
# velocity. The Froude number is thus analogous to
# the Mach number. The greater the Froude number,
# the greater the resistance.
#----------------------------------------------------

CCTK_REAL froudecap "the maxium Froude number "
{

  1.0:100.0 :: "cap for Froude number in velocity calculation for efficiency"

}10.0

#----------------------------------------------------
# Minimal water depth
#----------------------------------------------------

CCTK_REAL mindepth "the minimum water depth for wettting and drying scheme"
{

  1.0e-6:1.0 :: "0.001 for lab scale and 0.01 for field scale"

}1.0e-3

#----------------------------------------------------
# Minimal water depth to limit bottom frition value
#----------------------------------------------------

CCTK_REAL mindepthfrc "the minimum water depth to limit bottom frition value"
{

  1.0e-6:1.0 :: "0.01 for lab scale and 0.1 for field scale"

}1.0e-2

CCTK_BOOLEAN stretch_grid "Whether stretch grid is enabled"
{

}"no"

CCTK_REAL wind_crest_percent "percentage of wind crest for wind mask"
{
	0.0:100.0 :: "Percentage"
}10

CCTK_INT num_time_wind_data "Time sequence data for wind"
{
	0:100 :: "Numerical values for wind vectors"
}0

CCTK_REAL timewind[100] "times for wind speeds"
{
	0:1e6 :: "times for wind speeds"
} 0

CCTK_REAL wu[100] "wind at timestep n for the u direction"
{
	-1e6:1e6 :: "Wind speed"
} 0

CCTK_REAL wv[100] "wind at timestep n for the u direction"
{
	-1e6:1e6 :: "Wind speed"
} 0

BOOLEAN wind_force "Whether to use wind force"
{
} "false"

CCTK_REAL cdw "CDW is what?"
{
	0:1 :: "CDW"
} .0026

BOOLEAN use_wind_mask "Whether to apply the wind mask"
{
} "false"

BOOLEAN show_breaking "Whether to show breaking waves"
{
} "false"

CCTK_REAL drain_center_x "Center of the drain in x"
{
	: :: "Drain center x"
} 15.0

CCTK_REAL drain_center_y "Center of the drain in y"
{
  : :: "Drain center y"
} 15.0

CCTK_REAL drain_radius "Radius of the draining region"
{
	: :: "Drain radius"
} 5.0

CCTK_REAL drain_rate "Negative removes water, positive adds"
{
	: :: "Drain rate"
} 0.0

BOOLEAN use_correct_dispersion "Use the correct dispersion term"
{
} "true"

BOOLEAN generate_test_depth_data "Scales automatically to the grid size"
{
} false

CCTK_REAL test_depth_island_x "The x position of the island in the test depth data"
{
	*:* :: "Island x-pos"
} 10

CCTK_REAL test_depth_island_y "The y position of the island in the test depth data"
{
	*:* :: "Island y-pos"
} 10

CCTK_REAL test_depth_shore_x "The position of the shore in the test depth data"
{
	*:* :: "Shore pos"
} 25

CCTK_INT levee_pts "The number of points used to describe the levee"
{
  0:10000 :: "Zero means off"
} 0

CCTK_REAL xlevee[10000] "The x-coords of the levee points"
{
  *:* :: "any value"
} 0.0

CCTK_REAL ylevee[10000] "The y-coords of the levee points"
{
  *:* :: "any value"
} 0.0

CCTK_REAL veg_cd "vegetation drag coefficient"
{
  0:3 :: "zero means off"
} .15

# TODO: This is a diameter
CCTK_REAL veg_bv "vegetation stem cross-section"
{
  0:10.0 :: "zero means off"
} .01

CCTK_REAL veg_N "vegetation stem density"
{
  0:1000000 :: "veg density"
} 500

CCTK_BOOLEAN first_order_veg "whether to use the sophisticated vegetation model"
{
} true

CCTK_BOOLEAN show_dt "whether to display the value of dt calculated by estimate_dt"
{
} false

CCTK_REAL static_mask "turns on if static_mask > 0, and static_mask > depth"
{
  0:100 :: "The mask threshold"
} 0
