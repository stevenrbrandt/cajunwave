#!/usr/bin/perl
use strict;
use POSIX;
use FileHandle;
my $fdi = new FileHandle;
my $fdw = new FileHandle;
my $fdc = new FileHandle;

#############
# edit below
#############

# grid
my $scale = 1;
my $dx = 0.5/$scale;
my $dy = 0.5/$scale;
my $nx = 61*$scale; # number of grid points (not cells)
my $ny = 61*$scale; # number of grid points (not cells)

# wind
my $wind_force = 0;
my $wind_mask = 0;
my @winds = (
    [0,25,50],
    [1000,100,100]
    );

# hump
my $xc = 3.1;
my $yc = 3.5;
my $amp = 2;
my $width = 1;

# other
my $depth_flat = 0.8;
my $itlast = 3*$scale;
my $fitlast = 3*$scale+1;
my $nghost = 3; # Cactus only
my $dt = 00/$scale;
my $dispersion = 1;
my $enable_masks = 1;

#############
# edit above
#############

my $cnz = 2*$nghost+1;
my ($swf,$cwf) = boolf($wind_force);
my ($swm,$cwm) = boolf($wind_mask);
my ($disp_shi,$disp_cactus) = boolf($dispersion);
my ($masks_shi,$masks_cactus) = boolf($enable_masks);
my $cmaxx = ($nx-1)*$dx;
my $cminx = 0;
my $cmaxy = ($ny-1)*$dy;
my $cminy = 0;
my $nw = $#winds+1;
my $cmaxz =  $dx;
my $cminz = -$dx;
my $dtf = ff($dt);
my $widthf = ff($width);
my $ampf = ff($amp);
my $xcf = ff($xc);
my $ycf = ff($yc);

sub boolf {
    my $b = shift;
    if($b) {
        return ("T","true");
    } else {
        return ("F","false");
    }
}

sub ff {
    my $val = "".shift;
    $val .= ".0" if($val !~ /\./);
    return $val;
}

openf(\$fdi,">input.txt");
openf(\$fdw,">wind.txt");
openf(\$fdc,">hump_corner_low.par");

print $fdc qq{
#Reorder the parameters for easy comparison to the input.txt in example 3
ActiveThorns = "CoordBase FunWave FunwaveCoord CartGrid3D Carpet CarpetIOASCII CartGrid3D IOUtil CarpetIOBasic CarpetSlab Boundary SymBase MoL CarpetReduce LocalReduce InitBase CarpetLib LoopControl Tridiagonal"

#----------------------------------------------------
# Flesh and CCTK parameters
#----------------------------------------------------

# flesh
Cactus::cctk_run_title = "Test Run"
Cactus::cctk_show_schedule = "yes"
Cactus::cctk_itlast = $itlast
Cactus::allow_mixeddim_gfs = "yes"

# CartGrid3D
CartGrid3D::type = "coordbase"
CartGrid3D::avoid_origin = "no"
CoordBase::domainsize = "minmax"
CoordBase::spacing    = "gridspacing"
CoordBase::xmin =  $cminx
CoordBase::xmax =  $cmaxx
CoordBase::ymin =  $cminy
CoordBase::ymax =  $cmaxy
CoordBase::zmin =  0.0
CoordBase::zmax =  0.0
CoordBase::dx   =  0.5
CoordBase::dy   =  0.5

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_lower     = 0
CoordBase::boundary_size_z_upper     = 0
CoordBase::boundary_shiftout_x_lower = 1
CoordBase::boundary_shiftout_x_upper = 1
CoordBase::boundary_shiftout_y_lower = 1
CoordBase::boundary_shiftout_y_upper = 1
CoordBase::boundary_shiftout_z_lower = 1
CoordBase::boundary_shiftout_z_upper = 1

# Carpet
Carpet::domain_from_coordbase = "yes"
Carpet::ghost_size_x = $nghost
Carpet::ghost_size_y = $nghost
Carpet::ghost_size_z = 1
carpet::adaptive_stepsize = yes

# MoL
MoL::ODE_Method = "RK3"
MoL::disable_prolongation        = "yes"

# the output dir will be named after the parameter file name
IO::out_dir = \$parfile
IO::out_fileinfo="none"
IOBasic::outInfo_every = 1
IOBasic::outInfo_vars = "FunWave::eta"

IOASCII::out2D_every = 1
IOASCII::out2D_xyplane_z = 0
IOASCII::out2D_vars = "FunWave::eta FunWave::u FunWave::v"
IOASCII::out2D_xz = "no"
IOASCII::out2D_yz = "no"
IOASCII::output_ghost_points = "no"


#----------------------------------------------------
# Funwave parameters
#----------------------------------------------------

# Funwave depth 
FunWave::depth_type = "flat"
FunWave::depth_format = "ele"
FunWave::depth_file = "depth.txt"
FunWave::depth_flat = $depth_flat
FunWave::depth_xslp = 10.0
FunWave::depth_slope = 0.05
FunWave::dt_size = $dt

# Funwave wind forcing
Funwave::wind_force = $cwf
Funwave::use_wind_mask = $cwm
Funwave::num_time_wind_data = $nw},"\n";
for(my $i=0;$i<=$#winds;$i++) {
    my @w = @{$winds[$i]};
    print $fdc "Funwave::timewind[$i] = $w[0]\n";
    print $fdc "Funwave::wu[$i] = $w[1]\n";
    print $fdc "Funwave::wv[$i] = $w[2]\n";
}

print $fdc qq{Funwave::boundary = funwave

# Funwave wave maker
FunWave::wavemaker_type = "ini_gau"
FunWave::xc = $xc
FunWave::yc = $yc
FunWave::amp =  $amp
FunWave::wid =  $width

# Funwave sponge 
FunWave::sponge_on = "no"
FunWave::sponge_west_width = 2.0
FunWave::sponge_east_width = 2.0
FunWave::sponge_north_width = 0.0
FunWave::sponge_south_width = 0.0
FunWave::sponge_decay_rate = 0.9
FunWave::sponge_damping_magnitude = 5.0

# Funwave dispersion (example 3 enables dispersion)
FunWave::dispersion_on = "$disp_cactus"
FunWave::gamma1 = 1.0
FunWave::gamma2 = 1.0
FunWave::gamma3 = 1.0
FunWave::beta_ref = -0.531
FunWave::swe_eta_dep = 0.80
FunWave::cd = 0.0

# Funwave numerics (MoL parameter controls time integration scheme)
FunWave::reconstruction_scheme = "fourth"
FunWave::riemann_solver = "HLLC"
FunWave::dtfac = 0.5
FunWave::froudecap = 10.0
FunWave::mindepth = 0.001
FunWave::mindepthfrc = 0.001
FunWave::enable_masks = "$masks_cactus"
Funwave::estimate_dt_on = "true"
},"\n";

print $fdw qq{wind data
$nw  - number of data},"\n";
for(my $i=0;$i<=$#winds;$i++) {
    my @w = @{$winds[$i]};
    print $fdw "$w[0] $w[1] $w[2]   time(s), wu, wv (m/s)\n";
}
print $fdw "end of file\n";
close($fdw);

print $fdi qq{
!INPUT FILE FOR BOUSS_TVD
  ! NOTE: all input parameter are capital sensitive
  ! --------------------TITLE-------------------------------------
  ! title only for log file
TITLE = TEST RUN
  ! -------------------HOT START---------------------------------
HOT_START = F
FileNumber_HOTSTART = 1
  ! -------------------PARALLEL INFO-----------------------------
  ! 
  !    PX,PY - processor numbers in X and Y
  !    NOTE: make sure consistency with mpirun -np n (px*py)
  !    
PX = 1
PY = 1
  ! --------------------DEPTH-------------------------------------
  ! Depth types, DEPTH_TYPE=DATA: from depth file
  !              DEPTH_TYPE=FLAT: idealized flat, need depth_flat
  !              DEPTH_TYPE=SLOPE: idealized slope, 
  !                                 need slope,SLP starting point, Xslp
  !                                 and depth_flat
DEPTH_TYPE = FLAT
  ! Depth file
  ! depth format NOD: depth at node (M1xN1), ELE: depth at ele (MxN) 
  ! where (M1,N1)=(M+1,N+1)  
DEPTH_FILE = ../input/depth.txt
DepthFormat = ELE
  ! if depth is flat and slope, specify flat_depth
DEPTH_FLAT = $depth_flat
  if depth is slope, specify slope and starting point
SLP = 0.05
Xslp = 10.0

  ! -------------------PRINT---------------------------------
  ! PRINT*,
  ! result folder
RESULT_FOLDER = ./

  ! ------------------DIMENSION-----------------------------
  ! global grid dimension
Mglob = $nx
Nglob = $ny

  ! ----------------- TIME----------------------------------
  ! time: total computational time/ plot time / screen interval 
  ! all in seconds
itlast = $fitlast
TOTAL_TIME = 1200000.0
PLOT_INTV = 0.000001
PLOT_INTV_STATION = 0.02
SCREEN_INTV = 0.1
HOTSTART_INTV = 360000000000.0

  ! -----------------GRID----------------------------------
  ! if use spherical grid, in decimal degrees
StretchGrid = F
Lon_West = 120.0
Lat_South = 0.0
Dphi = 0.0042
Dtheta = 0.0042 
!DX_FILE = ../input/dx_str.txt
!DY_FILE = ../input/dy_str.txt
CORIOLIS_FILE = ../input/cori_str.txt
  ! cartesian grid sizes
DX = $dx
DY = $dy
  ! --------------- INITIAL UVZ ---------------------------
  ! INI_UVZ - initial UVZ e.g., initial deformation
  !         must provide three (3) files 
INI_UVZ = F
  ! if true, input eta u and v file names
ETA_FILE = z.txt
U_FILE = u.txt
V_FILE = v.txt
  ! ----------------WAVEMAKER------------------------------
  !  wave makeer
  ! LEF_SOL- left boundary solitary, need AMP,DEP, LAGTIME
  ! INI_SOL- initial solitary wave, WKN B solution, 
  ! need AMP, DEP, XWAVEMAKER 
  ! INI_REC - rectangular hump, need to specify Xc,Yc and WID
  ! WK_REG - Wei and Kirby 1999 internal wave maker, Xc_WK,Tperiod
  !          AMP_WK,DEP_WK,Theta_WK, Time_ramp (factor of period)
  ! WK_IRR - Wei and Kirby 1999 TMA spectrum wavemaker, Xc_WK,
  !          DEP_WK,Time_ramp, Delta_WK, FreqPeak, FreqMin,FreqMax,
  !          Hmo,GammaTMA,ThetaPeak
  ! WK_TIME_SERIES - fft time series to get each wave component
  !                 and then use Wei and Kirby 1999 
  !          need input WaveCompFile (including 3 columns: per,amp,pha)
  !          NumWaveComp,PeakPeriod,DEP_WK,Xc_WK,Ywidth_WK
WAVEMAKER = INI_GAU 
  ! wave components based on fft time series
NumWaveComp = 1505
PeakPeriod = 1.0
WaveCompFile = ../fft/wavemk_per_amp_pha.txt
  ! solitary wave
AMP = $ampf
DEP = 0.78
LAGTIME = 5.0
XWAVEMAKER = 400.0
  ! Xc, Yc and WID (degrees) are for rectangular hump with AMP
Xc = $xcf
Yc = $ycf
WID = $widthf
  ! Wei and Kirby 1999
Time_ramp = 1.0 
Delta_WK = 0.5    ! width parameter 0.3-0.6
DEP_WK = 0.45
Xc_WK = 3.0
Ywidth_WK = 10000.0
  ! Wei and Kirby regular wave
Tperiod = 1.0
AMP_WK = 0.0232
Theta_WK = 0.0
  ! Wei and Kirby irregular wave
FreqPeak = 0.2
FreqMin = 0.1
FreqMax = 0.4
Hmo = 1.0
GammaTMA = 5.0
ThetaPeak = 10.0
Sigma_Theta = 15.0
  ! ---------------- PERIODIC BOUNDARY CONDITION ---------
  ! South-North periodic boundary condition
  !
PERIODIC = F
  ! ---------------- SPONGE LAYER ------------------------
  ! DHI type sponge layer
  ! need to specify widths of four boundaries and parameters
  ! set width=0.0 if no sponge
  ! R_sponge: decay rate
  ! A_sponge: maximum decay rate
  ! e.g., sharp: R=0.85
  !       mild:  R=0.90, A=5.0
  !       very mild, R=0.95, A=5.0
SPONGE_ON = F
Sponge_west_width =  2.0
Sponge_east_width =  2.0
Sponge_south_width = 0.0
Sponge_north_width = 0.0
R_sponge = 0.90
A_sponge = 5.0
  ! ----------------OBSTACLES-----------------------------
  ! obstacle structures using mask_struc file
  ! mask_struc =0 means structure element
  ! give a file contains a mask array with Mloc X Nloc
!OBSTACLE_FILE= struc2m4m.txt

  ! ----------------PHYSICS------------------------------
  ! parameters to control type of equations
  ! dispersion: all dispersive terms
  ! gamma1=1.0,gamma2=0.0: NG's equations
  ! gamma1=1.0,gamma2=1.0: Fully nonlinear equations
DISPERSION = $disp_shi
Gamma1 = 1.0
Gamma2 = 1.0
Gamma3 = 1.0
Beta_ref=-0.531
SWE_ETA_DEP = 0.80
Cd = 0.0

  ! ----------------NUMERICS----------------------------
  ! time scheme: runge_kutta for all types of equations
  !              predictor-corrector for NSWE
  ! space scheme: second-order
  !               fourth-order
  ! construction: HLLC
  ! cfl condition: CFL
  ! froude number cap: FroudeCap

Time_Scheme = Runge_Kutta
!Time_Scheme = Predictor_Corrector
  ! spacial differencing
HIGH_ORDER = FOURTH
!HIGH_ORDER = THIRD
CONSTRUCTION = HLLC
  ! CFL
CFL = 0.5
  ! Froude Number Cap (to avoid jumping drop, set 10)
FroudeCap = 10.0

  ! --------------WET-DRY-------------------------------
  ! MinDepth for wetting-drying
MinDepth=0.001
  ! -----------------
  ! MinDepthfrc to limit bottom friction
MinDepthFrc = 0.001

  ! -------------- SHOW BREAKING -----------------------
  ! breaking is calculated using shock wave capturing scheme
  ! the criteria is only for demonstration or bubble calculation
  ! Cbrk1=0.65,Cbrk2=0.35, for irregular waves, there are much small!
SHOW_BREAKING = F
Cbrk1 = 0.1
Cbrk2 = 0.075
  ! ----------------- MIXING ---------------------------
  ! if use smagorinsky mixing, have to set -DMIXING in Makefile
  ! and set averaging time interval, T_INTV_mean, default: 20s
T_INTV_mean = 25.0
C_smg = 0.25
  ! ----------------- COUPLING -------------------------
  ! if do coupling, have to set -DCOUPLING in Makefile
COUPLING_FILE = coupling.txt
  ! -----------------OUTPUT-----------------------------
  ! stations 
  ! if NumberStations>0, need input i,j in STATION_FILE
NumberStations = 0
STATIONS_FILE = nothing
  ! output variables, T=.TRUE, F = .FALSE.
DEPTH_OUT = F
U = T
Ubar = T
V = T
Vbar = T
ETA = T
Hmax = F
Umean = F
Vmean = F
ETAmean = F
MASK = F
MASK9 = F
SXL = F
SXR = F
SYL = F
SYR = F
SourceX = T
SourceY = T
P = F
Q = F
Fx = F
Fy = F
Gx = F
Gy = F
AGE = F
TMP = F
WindForce = $swf
UseWindMask = $swm
FixDT=$dtf
Cdw = 0.0026
WindCrestPercent = .10
EnableMasks = $masks_shi
WIND_FILE = wind.txt},"\n";
close($fdi);

sub openf {
    my $fd = shift;
    my $file = shift;
    print "opening $file\n";
    open($$fd,$file) or die "error opening $file";
}
