use strict;
use FileHandle;

my $fd_ca = new FileHandle;
my $fd_fw = new FileHandle;
my $data = [];
my $t;
my $diff;
my $max_diff;
my $max_diff_str;

if($#ARGV < 2) {
    die "usage: code_diff.pl var cactus_dir funwave_dir";
}
my $var = $ARGV[0];
my $cac_dir = $ARGV[1];
my $fw_dir = $ARGV[2];
print "cac_dir=$cac_dir\n";
print "fw_dir=$fw_dir\n";
print "var=$var\n";

my $fd_diff = new FileHandle;

my $fd = new FileHandle;
open($fd,">/tmp/code_diff.gnu");

my $f = "$cac_dir/$var.xy.asc";
print "READING CAC: $f\n";
open($fd_ca,$f)
    or die $f;
while(<$fd_ca>) {
    s/#.*//;
    s/[\r\n]+$//;
    my @cols = split(/\s+/);
    next if($#cols < 0);
    my ($x,$y,$v);
    ($t,$x,$y,$v)=($cols[0],$cols[5],$cols[6],$cols[12]);
    if($cols[12] =~ /nan/i) {
    	$v = "-9999.0";
    }
    $data->[$t]->[$x]->[$y] = $v;
}
print "TIME=$t\n";
for(my $i=0;$i<$t;$i++) {
    $max_diff = 0.0;
    $max_diff_str = "PERFECT t=$i ";
    my $file = sprintf("$fw_dir/${var}_%04d",$i+1);
    open($fd_diff,">$cac_dir/diff-t=$i.123.xg") or die;
    print "READING FW:$file\n";
    open($fd_fw,$file) or die "cannot open '$file'";
    my $y = 0;
    while(<$fd_fw>) {
        s/^\s+//;
        s/\s+$//;
        s/\s+/ /g;
        my @cols = split(/ /);
        for(my $x=0;$x <= $#cols;$x++) {
            my $d1 = $cols[$x];
            my $d2 = $data->[$i]->[$x]->[$y];
            my $diff = abs($d1-$d2);
            print $fd_diff "$x $y $diff\n";
            if($diff > $max_diff) {
                $max_diff = $diff;
                $max_diff_str = "diff=$diff x=$x y=$y t=$i fw=$d1 cac=$d2\n";
            }
            print $fd "$i $x $y $d1 $d2\n";
        }
        $y++;
    }
    print $max_diff_str;
    close($fd_diff);
}
close($fd);
