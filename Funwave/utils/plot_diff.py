#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import rc

# stuff
fontsize  = 10
linewidth = 1
rc('text', usetex=True)
rc('font', family='serif')
rc('font', serif='palatino')
rc('font', weight='bolder')
rc('mathtext', default='sf')
rc("lines", markeredgewidth=1)
rc("lines", linewidth=linewidth)
rc('axes', labelsize=fontsize)
rc("axes", linewidth=(linewidth+1)//2)
rc('xtick', labelsize=fontsize)
rc('ytick', labelsize=fontsize)
rc('legend', fontsize=fontsize)
rc('xtick.major', pad=8)
rc('ytick.major', pad=8)

print 'reading'
its, funx, funy, fundata, cacx, cacy, cacdata, cacproc = np.loadtxt("eta_combined_1.dat", comments="#", usecols=(0,1,2,3,4,5,6,7), unpack=True)
#its, funx, funy, fundata, cacx, cacy, cacdata, cacproc = np.loadtxt("test.dat", comments="#", usecols=(0,1,2,3,4,5,6,7), unpack=True)

print 'reshaping'
# only works for regular data
nx  = 0
ny  = 0
nit = 0
for i in np.arange(len(its)):
  nx  = max(nx,  funx[i])
  ny  = max(ny,  funy[i])
  nit = max(nit, its[i]+1)

x    = np.reshape(funx,    (nit, ny, nx))
y    = np.reshape(funy,    (nit, ny, nx))
fun  = np.reshape(fundata, (nit, ny, nx))
cac  = np.reshape(cacdata, (nit, ny, nx))
proc = np.reshape(cacproc, (nit, ny, nx))
procmax = proc.max()

print 'plotting'
#fig.subplots_adjust(top=0.85, bottom=0.16, left=0.11,right=0.97)

fig = plt.figure(figsize=(8, 12))

colors = np.empty((nx,ny,4))

for it in np.arange(nit):
  print it

  for j in range(int(ny)):
    for i in range(int(nx)):
      colors[i,j] = plt.cm.jet(proc[it,i,j]/(procmax*1.0))
#      colors[i,j] = 'b' if proc[it,i,j]==0 else 'r'

  fig.clf()
  fig.suptitle('it = %04d' % it)

  rect = fig.add_subplot(3,1,1).get_position()
  ax = Axes3D(fig, rect)
  ax.w_zaxis.set_major_formatter(mticker.FormatStrFormatter("%g"))
  ax.plot_surface  (x[it], y[it], fun[it], rstride=1, cstride=1, linewidth=0, antialiased=False)
  ax.plot_wireframe(x[it], y[it], fun[it], rstride=1, cstride=1, linewidth=0.2, color='k')
  ax.set_zlim3d((0,1.5))
  
  rect = fig.add_subplot(3,1,2).get_position()
  ax = Axes3D(fig, rect)
  g1,g2 = np.atleast_2d(cac[it]).shape
  ax.w_zaxis.set_major_formatter(mticker.FormatStrFormatter("%g"))
  ax.plot_surface  (x[it], y[it], cac[it], rstride=1, cstride=1, linewidth=0, antialiased=False, edgecolor='k', facecolors=colors)
  ax.plot_wireframe(x[it], y[it], cac[it], rstride=1, cstride=1, linewidth=0.2, color='k')
  ax.set_zlim3d((0,1.5))
  
  rect = fig.add_subplot(3,1,3).get_position()
  ax = Axes3D(fig, rect)
  ax.w_zaxis.set_major_formatter(mticker.FormatStrFormatter("%g"))
  ax.plot_surface  (x[it], y[it], fun[it]-cac[it], rstride=1, cstride=1, linewidth=0, antialiased=False, facecolors=colors)
  ax.plot_wireframe(x[it], y[it], fun[it]-cac[it], rstride=1, cstride=1, linewidth=0.2, color='k')
  ax.set_zlim3d((-1.e-14,1.e-14))
  
#  plt.show()
  plt.savefig('frame_eta_%04d.pdf' % (int(it)))
#  plt.savefig('frame_eta_%04d.png' % (int(it)))

#  fig.close()

