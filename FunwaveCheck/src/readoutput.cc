#include <fstream>
#include <sstream>
#include <iostream>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#ifndef LINELENGTH
#define LINELENGTH 100
#endif
/*
 * prototypes of local functions
 * */
void fw_readmatrix(const char *file, int *origin, int *lsh, int *offset,
    CCTK_REAL * vals, const cGH * cctkGH);
bool fw_getline(std::istream & in, std::string & line);

void funwavecheck_read(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int offset[2];
  char filename[LINELENGTH];

  std::cout << "cctk iteration " << cctk_iteration << std::endl;

  int grpidx = CCTK_GroupIndex("funwavecheck::funwave_vars");
  std::cout << "Group index " << grpidx << std::endl;
  int firstvaridx = CCTK_FirstVarIndexI(grpidx);
  std::cout << "First variable index in this group " << firstvaridx
      << std::endl;

  int numvars = CCTK_NumVarsInGroupI(grpidx);
  std::cout << "Number of variables in this group " << numvars << std::endl;

  offset[0] = offset_x;
  offset[1] = offset_y;

  for (int varidx = firstvaridx; varidx < firstvaridx + numvars; varidx++)
  {
    std::cout << "  Variable index " << varidx << std::endl;
    sprintf(filename, "%s/%s_%04d", funwave_data_dir, CCTK_VarName(varidx),
        cctk_iteration);
    std::cout << "  File name " << filename << std::endl;
    fw_readmatrix(filename, cctk_lbnd, cctk_lsh, offset,
        (CCTK_REAL*) CCTK_VarDataPtrI(cctkGH, 0, varidx), cctkGH);
  }
}

// read a single line of input
bool fw_getline(std::istream & in, std::string & line)
{
  bool ret = false;

  line.resize(0);
  while (true)
  {
    char ch = in.get();

    if (ch == EOF)
      break;
    ret = true;
    if (ch == '\n')
      break;
    line += ch;
  }
  return ret;
}

/*
 * This function reads the submatrix from the matrix contained in
 * the text file. The origin of the matrix is given by int*origin,
 * and the shape by int*lsh. Format is 0 when the input depth is
 * cell centered and 1 when the input depth is on grid point.
 */
void fw_readmatrix(const char *file, int *origin, int *lsh, int *offset,
    CCTK_REAL * vals, const cGH * cctkGH)
{
  std::cout << "Reading file '" << file << "'" << std::endl;
  std::cout << "Columns between " << origin[0] << " and " << (origin[0]
      + lsh[0]) << std::endl;
  std::cout << "Rows between " << origin[1] << " and " << (origin[1] + lsh[1])
      << std::endl;
  std::ifstream in(file);
  std::string line;
  int row = 0;
  CCTK_REAL min = 0.0, max = 1.0;
  bool set_min_max = true;

  while (true)
  {
    if (!fw_getline(in, line))
      break;
    int col = 0;

    std::istringstream ins(line);
    while (!ins.eof())
    {
      CCTK_REAL v;

      ins >> v;
      if (col >= origin[0] && col < origin[0] + lsh[0] && row >= origin[1]
          && row < origin[1] + lsh[1])
      {
        int i = col - origin[0] + offset[0];
        int j = row - origin[1] + offset[1];
        int cc = CCTK_GFINDEX2D(cctkGH, i, j);

        vals[cc] = v;
        if (set_min_max)
        {
          set_min_max = false;
          min = max = v;
        }
        else if (v < min)
        {
          min = v;
        }
        else if (v > max)
        {
          max = v;
        }
      }
      col++;
    }
    /*    if (((col != origin[0] + lsh[0] - 1) && (format == 0)) || ((col
     != origin[0] + lsh[0]) && (format == 1)))
     {
     CCTK_WARN(0, "Shape mismatch in column");
     }
     */
    row++;
  }
  /*
   if (((row != origin[1] + lsh[1] - 1) && (format == 0)) || ((row != origin[1]
   + lsh[1]) && (format == 1)))
   {
   CCTK_WARN(0, "Shape mismatch in row");
   }
   */
  in.close();
  std::cout << "min=" << min << ", max=" << max << std::endl;
}
