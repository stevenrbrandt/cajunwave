#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void CoastalBase_Breeze_Over_River (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        uc1[i3D] = current_velx;
        uc2[i3D] = current_vely;
        uw1[i3D] = wind_velx;
        uw2[i3D] = wind_vely;
      }
}

void CoastalBase_Init (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        uc1[i3D] = 0.0;
        uc2[i3D] = 0.0;
        uw1[i3D] = 0.0;
        uw2[i3D] = 0.0;
        depth[i3D] = 0.0;
        eta[i3D] = 0.0;
      }
}
