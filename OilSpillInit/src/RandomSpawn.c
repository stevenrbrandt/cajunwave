#include <stdlib.h>
#include <time.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

/* we separate the particles distribution from velocity distribution */

/* particle distribution */
void OilSpillInit_RandomSpawn (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i;
  CCTK_INT lsh;                 /* local size */

  /* initialize random generator */
  srand (time (NULL));

  CCTK_GrouplshGN (cctkGH, 1, &lsh, "OilSpillBase::parcel_position");

  for (i = 0; i < lsh; i++)
  {
    xp1[i] = random_position_range * (rand () / (RAND_MAX + 1.0) - 0.5);
    xp2[i] = random_position_range * (rand () / (RAND_MAX + 1.0) - 0.5);
    xp3[i] = 0.0;

    xp1_p[i] = random_position_range * (rand () / (RAND_MAX + 1.0) - 0.5);
    xp2_p[i] = random_position_range * (rand () / (RAND_MAX + 1.0) - 0.5);
    xp3_p[i] = 0.0;
  }
}

/* velocity distribution */

void OilSpillInit_DrunkenFish (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;

  /* initialize random generator */
  srand (time (NULL));
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        ua1[i3D] = random_velocity_range * (rand () / (RAND_MAX + 1.0) - 0.5);
        ua2[i3D] = random_velocity_range * (rand () / (RAND_MAX + 1.0) - 0.5);
      }

}

void OilSpillInit_GoneWithWind (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;

  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        ua1[i3D] = parcel_velx;
        ua2[i3D] = parcel_vely;
      }
}
