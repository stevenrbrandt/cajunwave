#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#define LINELENGTH 100
inline void InitDistrbFromFile (const int start_idx, const int size, const char *filename, CCTK_REAL * x1,
                               CCTK_REAL * x2);

/* we separate the particles distribution from velocity distribution */

/* particle distribution */
void OilSpillInit_InitDistrbFromFile (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int i;
  int num_fromfile;
  CCTK_INT lbnd, lsh;                 /* local size */

  /* get the lower bound for the array */
  CCTK_GrouplbndGN(cctkGH, 1, &lbnd, "OilSpillBase::parcel_position");

  CCTK_GrouplshGN (cctkGH, 1, &lsh, "OilSpillBase::parcel_position");

  printf("my lower bound is %d, my lsh is %d \n", lbnd, lsh);
/* check to make sure that the number of lines of the file
 * matches with the number of parcels set in the parameter file */

  InitDistrbFromFile (lbnd, lsh, parcel_distrib_file, xp1, xp2);

  for (i = 0; i < lsh; i++)
  {
    xp1_p[i] = xp1[i];
    xp2_p[i] = xp2[i];

/* set xp3 (dummy) */
    xp3[i] = 0.0;
    xp3_p[i] = 0.0;
  }
}

inline void InitDistrbFromFile (const int start_idx, const int size, const char *filename, CCTK_REAL * x1,
                               CCTK_REAL * x2)
{
  FILE *fp;
  char line[LINELENGTH];
  int i;

  if ((fp = fopen (filename, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                "can't open: %s", filename);
  }

  /* skip first start_idx lines */
  for (i = 0; i < start_idx; i++)
  {
    fgets (line, 80, fp);
  }

  for (i = 0; i < size; i++)
  {
    fgets (line, 80, fp);
    sscanf (line, "%lf %lf", &x1[i], &x2[i]);
  }

  fclose (fp);
}
