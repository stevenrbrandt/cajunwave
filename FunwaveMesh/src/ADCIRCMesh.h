#ifndef _ADCIRCMESH_H_
#define _ADCIRCMESH_H_

#include "cctk.h"

#ifdef __cplusplus

extern "C"
{
#endif


/* structure for 2d vertices */
  typedef struct _vert2d
  {
    /* x = x[0], y = x[1] */
    CCTK_REAL x[2];
  } vert2d;

/* structure for 2d cells (only consider triangular elements here) */
  typedef struct _cell2d
  {
    CCTK_INT vertid[3];
  } cell2d;

/* structure for ADCIRC data */

  typedef struct _field2d
  {
    CCTK_INT vertid;
    CCTK_REAL d;
  } field2d;


/* todo need a generic ADCIRC mesh reader */
  void ADCIRCMeshReaderASCII (const char *mesh_file);
/*  void ADCIRCMeshReader(const char *mesh_file, const char *format); */
/*  void ADCIRCMeshReaderF5(const char *mesh_file);*/
/*  void ADCIRCMeshReaderHDF5(const char *mesh_file);*/

/* sort the cells based on the position of the centroid */
/*  void ADCIRCMeshSorter (cell2d *unsorted_cell, const int num_cell);*/

/* prototype for routines reading current and wind data
 * where field = 74 will read the wind data and 64 for the current data
 * */

/* used by qsort and bsearch for searching vertices */
  extern int CompareVertid (const void *c1, const void *c2);

/* max buffer size for reading files */
#define LINELENGTH 100
/*
#define NUM_VERT   2409635
#define NUM_CELL   4721491
*/
  extern int vert_count;
  extern int cell_count;
  extern vert2d *adcirc_vert;
  extern cell2d *adcirc_cell;
  extern field2d *adcirc_depth;

#ifdef __cplusplus
}
#endif

#endif                          //_ADCIRCMESH_H_
