#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ADCIRCMesh.h"

/*
 * Interface to the readers of different type of mesh formats.
 * Potential formats will be HDF5 and F5
 * */

int vert_count;
int cell_count;
vert2d *adcirc_vert;
cell2d *adcirc_cell;
field2d *adcirc_depth;

int CompareCentroidY(const void *c1, const void *c2);

/* local inline functions */
int PointInTriangle(const CCTK_REAL * p, const CCTK_REAL * x1,
    const CCTK_REAL * x2, const CCTK_REAL * x3);

CCTK_REAL VecDotProduct(const CCTK_INT n, const CCTK_REAL * v1,
    const CCTK_REAL * v2);

CCTK_REAL Distance(const CCTK_INT n, const CCTK_REAL * x1,
    const CCTK_REAL * x2);

void RestrictMesh(CCTK_ARGUMENTS);
void FunwaveMesh_ADCIRCScatteredInterp_CalcWeight( CCTK_ARGUMENTS);

void FunwaveMesh_ADCIRCMeshReader( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  // average of y (long), in radiants
  CCTK_REAL yavg = M_PI/360*(ymax+ymin);
  // approximate radius at center of grid
	// average of y (long), in radiants
  CCTK_REAL mean_radius = sqrt(pow(earth_a*cos(yavg),2) +
			pow(earth_b*sin(yavg),2));

  /* read cells into adcirc_cell and read vertices into adcirc_vert */
  ADCIRCMeshReaderASCII(depth_file);

  /* Restrict mesh to portion which we will use, for faster processing later */
  RestrictMesh(CCTK_PASS_CTOC);

  if (cell_count < 1)
      CCTK_WARN(0, "No cells to interpolate from, something is wrong with the data setup");

  /*sort cells by y coordinate of the centoid */
  /* we sort along y coordinate. notice that we sort the all cells here */

  CCTK_INFO("start sorting cells ...");
  qsort(adcirc_cell, cell_count, sizeof(cell2d), CompareCentroidY);

  if (veryverbose)
  {
    printf("Vert1   Index =  %13d \n", adcirc_cell[280190].vertid[0]);
    printf("        Vert1 =  (%13.5lf, %13.5lf) \n",
        adcirc_vert[adcirc_cell[280190].vertid[0]].x[0],
        adcirc_vert[adcirc_cell[280190].vertid[0]].x[1]);
    printf("Vert2   Index =  %13d \n", adcirc_cell[280190].vertid[1]);
    printf("        Vert2 =  (%13.5lf, %13.5lf) \n",
        adcirc_vert[adcirc_cell[280190].vertid[1]].x[0],
        adcirc_vert[adcirc_cell[280190].vertid[1]].x[1]);
    printf("Vert3   Index =  %13d \n", adcirc_cell[280190].vertid[2]);
    printf("        Vert3 =  (%13.5lf, %13.5lf) \n",
        adcirc_vert[adcirc_cell[280190].vertid[2]].x[0],
        adcirc_vert[adcirc_cell[280190].vertid[2]].x[1]);
  }

  CCTK_INFO("finish sorting cells.");
  CCTK_INFO("Transforming coordinates / units");
  if (CCTK_EQUALS(coordinate_units, "metric"))
  {}
  else if (CCTK_EQUALS(coordinate_units, "WGS84"))
  {
    CCTK_VInfo(CCTK_THORNSTRING, "Mesh size: %.3gm x %.3gm", \
        mean_radius * (xmax-xmin)*cos(yavg), mean_radius * (ymax-ymin));
    #pragma omp parallel for
    for (int vert=0; vert<vert_count; vert++)
    {
      //adcirc_vert[vert].x[0] = mean_radius * (adcirc_vert[vert].x[0]-xmin)*cos(yavg);
      adcirc_vert[vert].x[0] = mean_radius * (adcirc_vert[vert].x[0]-xmin);
      adcirc_vert[vert].x[1] = mean_radius * (adcirc_vert[vert].x[1]-ymin);
    }
  }
  else
  {
    CCTK_WARN(0, "value of coordinate_units not supported");
  }

  CCTK_INFO(
      "calculate the weights for inverse distance weighted interpolation.");
  FunwaveMesh_ADCIRCScatteredInterp_CalcWeight(CCTK_PASS_CTOC);

  if (CCTK_Equals(interp_algorithm, "average"))
  {
    #pragma omp parallel for
    for (int j = 0; j < cctk_lsh[1]; j++)
      for (int i = 0; i < cctk_lsh[0]; i++)
      {
        int cc = CCTK_GFINDEX2D(cctkGH, i, j);
        /* averaging */
        depthx[cc] = depthy[cc] = depth[cc] = 0.33333333333333333
            * (  adcirc_depth[vert1[cc]].d 
               + adcirc_depth[vert2[cc]].d
               + adcirc_depth[vert3[cc]].d );
      }
  }
  else if (CCTK_Equals(interp_algorithm, "invdist"))
  {
    #pragma omp parallel for
    for (int j = 0; j < cctk_lsh[1]; j++)
      for (int i = 0; i < cctk_lsh[0]; i++)
      {
        int cc = CCTK_GFINDEX2D(cctkGH, i, j);
        depthx[cc] = depthy[cc] = depth[cc] =
              w1[cc] * adcirc_depth[vert1[cc]].d
            + w2[cc] * adcirc_depth[vert2[cc]].d
            + w3[cc] * adcirc_depth[vert3[cc]].d;
      }
  }
  else
    CCTK_WARN(0, "the specified interp_algorithm is not supported !");
  free(adcirc_vert);
  free(adcirc_cell);
  free(adcirc_depth);
  return;
}

/* Weight functions solely depend on the mesh and targetting points
 * when both the mesh and targetting points are fixed, we can store
 * the weight functions on each targetting point and use them during
 * the time evolution without any more calculations. */

void FunwaveMesh_ADCIRCScatteredInterp_CalcWeight( CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* counting lost points */CCTK_INT num_lost_pt = 0;

  CCTK_INFO(
      "start interpolating scattered data with inverse distance weighted method.");

  /* for each structured grid point find the cell that contains it */

  /* Note that the debug statements are NOT thread-safe! */
  #pragma omp parallel for
  for (CCTK_INT j = 0; j < cctk_lsh[1]; j++)
    for (CCTK_INT i = 0; i < cctk_lsh[0]; i++)
    {
      CCTK_INT i2D = CCTK_GFINDEX2D(cctkGH, i, j);

      /* reset all the status variables */
      CCTK_INT cellidxmin = 0;
      CCTK_INT cellidxmax = cell_count - 1;
      CCTK_INT cell_found = 0;

      CCTK_REAL centroidymin, centroidymax;

      /*
       * this is working because we sorted the cells with coordinate y.
       * This will greatly limit the number of cells and vertices we are
       * going to search.
       * */
#define BISECT
#ifdef BISECT
      CCTK_INT left = 0, right = cell_count-1;
      while (left!=right && left!=right-1)
      {
        CCTK_INT middle = (right+left)/2;
        centroidymin =
             (adcirc_vert[adcirc_cell[middle].vertid[0]].x[1]
            + adcirc_vert[adcirc_cell[middle].vertid[1]].x[1]
            + adcirc_vert[adcirc_cell[middle].vertid[2]].x[1]) / 3.0;
        if (y[i2D] >= (centroidymin + search_range))
          left = middle;
        else
          right = middle;
      }
      cellidxmin = left;
      left = 0, right = cell_count-1;
      while (left!=right && left!=right-1)
      {
        CCTK_INT middle = (right+left)/2;
        centroidymax =
             (adcirc_vert[adcirc_cell[middle].vertid[0]].x[1]
            + adcirc_vert[adcirc_cell[middle].vertid[1]].x[1]
            + adcirc_vert[adcirc_cell[middle].vertid[2]].x[1]) / 3.0;
        if (y[i2D] <= (centroidymax - search_range))
          right = middle;
        else
          left = middle;
      }
      cellidxmax = right;
#else
      for (CCTK_INT l = 0; l < cell_count; l++)
      {
        centroidymin =
             (adcirc_vert[adcirc_cell[l].vertid[0]].x[1]
            + adcirc_vert[adcirc_cell[l].vertid[1]].x[1]
            + adcirc_vert[adcirc_cell[l].vertid[2]].x[1]) / 3.0;

        if (y[i2D] >= (centroidymin + search_range))
        {
          cellidxmin = l;
        }
        centroidymax =
             (adcirc_vert[adcirc_cell[cell_count - l].vertid[0]].x[1]
            + adcirc_vert[adcirc_cell[cell_count - l].vertid[1]].x[1]
            + adcirc_vert[adcirc_cell[cell_count - l].vertid[2]].x[1]) / 3.0;

        if (y[i2D] <= (centroidymax - search_range))
        {
          cellidxmax = cell_count - l;
        }
      }
#endif

      if (veryverbose)
      {
        printf("--------------------------------------------------\n");
        printf("(i, j)        =  (%13d, %13d) \n", i, j);
        printf("(X, Y)        =  (%13.5lf, %13.5lf) \n", x[i2D], y[i2D]);
        printf("Search Y     in  (%13.5lf, %13.5lf) \n", y[i2D] - search_range,
            y[i2D] + search_range);
        printf("Search Cell  in  (%13d, %13d)\n", cellidxmin, cellidxmax);
        printf(
            "Centroid Y   in  (%13.5lf, %13.5lf)\n",
            (adcirc_vert[adcirc_cell[cellidxmin].vertid[0]].x[1]
                + adcirc_vert[adcirc_cell[cellidxmin].vertid[1]].x[1]
                + adcirc_vert[adcirc_cell[cellidxmin].vertid[2]].x[1]) / 3.0,
            (adcirc_vert[adcirc_cell[cellidxmax].vertid[0]].x[1]
                + adcirc_vert[adcirc_cell[cellidxmax].vertid[1]].x[1]
                + adcirc_vert[adcirc_cell[cellidxmax].vertid[2]].x[1]) / 3.0);
      }

      /* internal vertex for checking if the target point is in a triangle formed by the 3 closest vertices */
      vert2d target_pt;
      target_pt.x[0] = x[i2D];
      target_pt.x[1] = y[i2D];

      for (CCTK_INT m = cellidxmin; m < cellidxmax; m++)
      {
        /* things to do if we found it */
        if (PointInTriangle(target_pt.x,
            adcirc_vert[adcirc_cell[m].vertid[0]].x,
            adcirc_vert[adcirc_cell[m].vertid[1]].x,
            adcirc_vert[adcirc_cell[m].vertid[2]].x) != 0)
        {
          /* record the vertices of the cell */
          vert1[i2D] = adcirc_cell[m].vertid[0];
          vert2[i2D] = adcirc_cell[m].vertid[1];
          vert3[i2D] = adcirc_cell[m].vertid[2];

          /* calculate the weight functions */
          w1[i2D] = pow(Distance(2, target_pt.x, adcirc_vert[vert1[i2D]].x),
              -invdist_power);
          w2[i2D] = pow(Distance(2, target_pt.x, adcirc_vert[vert2[i2D]].x),
              -invdist_power);
          w3[i2D] = pow(Distance(2, target_pt.x, adcirc_vert[vert3[i2D]].x),
              -invdist_power);

          CCTK_REAL w_denom = w1[i2D] + w2[i2D] + w3[i2D];

          w1[i2D] = w1[i2D] / w_denom;
          w2[i2D] = w2[i2D] / w_denom;
          w3[i2D] = w3[i2D] / w_denom;

          if (veryverbose)
          {
            printf("Found in Cell =  %13d \n", m);
            printf("Vert1   Index =  %13d \n", adcirc_cell[m].vertid[0]);
            printf("        Vert1 =  (%13.5lf, %13.5lf) \n",
                adcirc_vert[adcirc_cell[m].vertid[0]].x[0],
                adcirc_vert[adcirc_cell[m].vertid[0]].x[1]);
            printf("Vert2   Index =  %13d \n", adcirc_cell[m].vertid[1]);
            printf("        Vert2 =  (%13.5lf, %13.5lf) \n",
                adcirc_vert[adcirc_cell[m].vertid[1]].x[0],
                adcirc_vert[adcirc_cell[m].vertid[1]].x[1]);
            printf("Vert3   Index =  %13d \n", adcirc_cell[m].vertid[2]);
            printf("        Vert3 =  (%13.5lf, %13.5lf) \n",
                adcirc_vert[adcirc_cell[m].vertid[2]].x[0],
                adcirc_vert[adcirc_cell[m].vertid[2]].x[1]);
            printf("--------------------------------------------------\n");
          }

          cell_found = 1;
          break;
        }
      } /* end iteration through cells */

      /* if we haven't found any cells for the grid point after
       * all the work, we will set the weight function to zero. */
      if (cell_found == 0)
      {
        w1[i2D] = 0.0;
        w2[i2D] = 0.0;
        w3[i2D] = 0.0;

        num_lost_pt++;
        if (verbose || veryverbose)
        {
          printf("Failed to find a cell to contain the grid point !!!\n");
          printf("Total Failed  =  %13d\n", num_lost_pt);
          printf("--------------------------------------------------\n");
        }
      }
    } /* iteration over all Cactus grid point. */

  CCTK_VInfo(
      CCTK_THORNSTRING,
      "Done interpolating scattered data. Totally %d points are found outside of the mesh.\n",
      num_lost_pt);

  return;
}

/*
 * For a triangle formed by three vertice x1,x2,x3. The cooridnate of any
 * point P can be expressed in term of x1,x2,x3 and two coefficients u, v.
 * P = x1 + u * (x2 - x1) + v * (x3 - x1) test if a point is in a given
 * trangle form by three vertice x1, x2, x3 1 in, 0 not in.
 */

inline int PointInTriangle(const CCTK_REAL * p, const CCTK_REAL * x1,
    const CCTK_REAL * x2, const CCTK_REAL * x3)
{
  CCTK_REAL sub0[2], sub1[2], sub2[2];
  CCTK_REAL u, v, d00, d01, d02, d11, d12, invdenom;
  int i;

#define min(x,y,z) ((x<z)?(x<y)?x:y:(y<z)?y:z)
#define max(x,y,z) ((x>z)?(x>y)?x:y:(y>z)?y:z)
  if (p[0] < min(x1[0], x2[0], x3[0]) ||
      p[1] < min(x1[1], x2[1], x3[1]) ||
      p[0] > max(x1[0], x2[0], x3[0]) ||
      p[1] > max(x1[1], x2[1], x3[1]))
    return 0;
#undef min
#undef max

  for (i = 0; i < 2; i++)
  {
    sub0[i] = x3[i] - x1[i];
    sub1[i] = x2[i] - x1[i];
    sub2[i] = p[i] - x1[i];
  }

  /* Compute dot products */
  d00 = VecDotProduct(2, sub0, sub0);
  d01 = VecDotProduct(2, sub0, sub1);
  d02 = VecDotProduct(2, sub0, sub2);
  d11 = VecDotProduct(2, sub1, sub1);
  d12 = VecDotProduct(2, sub1, sub2);

  /* Compute barycentric coordinates */
  invdenom = 1.0 / (d00 * d11 - d01 * d01);
  u = (d11 * d02 - d01 * d12) * invdenom;
  v = (d00 * d12 - d01 * d02) * invdenom;

  /* 0 not in, 1 in */
  return (u >= 0) && (v >= 0) && (u + v <= 1.0);
}

/* return dot product of vector x and y with size n */

inline CCTK_REAL VecDotProduct(const CCTK_INT n, const CCTK_REAL * v1,
    const CCTK_REAL * v2)
{
  CCTK_INT i;
  CCTK_REAL sum = 0.0;
  for (i = 0; i < n; i++)
  {
    sum += v1[i] * v2[i];
  }
  return sum;
}

/* return the distance between points x1 and x2 of n dimension */

inline CCTK_REAL Distance(const CCTK_INT n, const CCTK_REAL * x1,
    const CCTK_REAL * x2)
{
  CCTK_INT i;
  CCTK_REAL distance = 0.0;
  for (i = 0; i < n; i++)
  {
    distance += (x1[i] - x2[i]) * (x1[i] - x2[i]);
  }
  return sqrt(distance);
}

/*
 * Default ASCII reader.
 * */

void ADCIRCMeshReaderASCII(const char *mesh_file)
{
  FILE *fp;
  char line[LINELENGTH];
  int i;
  CCTK_INT discardint;

  if ((fp = fopen(mesh_file, "r")) == NULL)
  {
    CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, "can't open: %s\n",
        mesh_file);
  }

  /* skip the first line */
  fgets(line, LINELENGTH, fp);

  CCTK_VInfo(CCTK_THORNSTRING, "start reading ADCIRC mesh file %s with %s",
      mesh_file, line);

  /* read in the total number of vertices and cells */
  fscanf(fp, "%d %d", &cell_count, &vert_count);

  CCTK_VInfo(CCTK_THORNSTRING, "found %d vertices and %d cells", vert_count,
      cell_count);

  /* Allocate space for vertex and cell arrays. */

  /* notice that if we want to use the array index to refer to the vertices we
   * need to allocate an extra room for the 0th */
  if (((adcirc_vert = (vert2d *) malloc((vert_count + 1) * sizeof(vert2d)))
      == NULL)
      || ((adcirc_depth = (field2d *) malloc((vert_count + 1) * sizeof(vert2d)))
          == NULL)
      || ((adcirc_cell = (cell2d *) malloc(cell_count * sizeof(cell2d))) == NULL))
  {
    CCTK_WARN(0, "failed to allocate memory for the mesh.");
  }

  for (i = 1; i <= vert_count; i++)
  {
    fscanf(fp, "%d %lf %lf %lf", &(adcirc_depth[i].vertid),
        &(adcirc_vert[i].x[0]), &(adcirc_vert[i].x[1]), &(adcirc_depth[i].d));
  }
  /* Read in which verts are associated with each cell. */
  for (i = 0; i < cell_count; i++)
  {
    fscanf(fp, "%d %d %d %d %d", &discardint, &discardint,
        &(adcirc_cell[i].vertid[0]), &(adcirc_cell[i].vertid[1]),
        &(adcirc_cell[i].vertid[2]));
  }

  fclose(fp);

  CCTK_VInfo(CCTK_THORNSTRING, "finish reading ADCIRC mesh file %s\n",
      mesh_file);

  return;
}

void RestrictMesh(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS

  int r_vert_count = 0;
  int r_cell_count = 0;
  vert2d *r_adcirc_vert;
  cell2d *r_adcirc_cell;
  field2d *r_adcirc_depth;

  CCTK_REAL xbuffer = (xmax-xmin)/100;
  CCTK_REAL ybuffer = (ymax-ymin)/100;

  // Count how many cells do actually have vertices within the interesting
  // region
  int *r_vertex_mask;
  if ((r_vertex_mask = (int*) calloc((vert_count + 1), sizeof(int))) == NULL)
    CCTK_WARN(0, "Failed to allocate memory for r_vertex_mask");
  for (int i = 0; i < cell_count; i++)
  {
    int include_cell = 0;
    for (int vert = 0; vert<3; vert++)
      if (adcirc_vert[adcirc_cell[i].vertid[vert]].x[0] >= xmin-xbuffer &&
          adcirc_vert[adcirc_cell[i].vertid[vert]].x[0] <= xmax+xbuffer &&
          adcirc_vert[adcirc_cell[i].vertid[vert]].x[1] >= ymin-ybuffer &&
          adcirc_vert[adcirc_cell[i].vertid[vert]].x[1] <= ymax+ybuffer)
        include_cell++;
    if (include_cell)
    {
      r_cell_count++;
      for (int vert = 0; vert<3; vert++)
        r_vertex_mask[adcirc_cell[i].vertid[vert]]++;
    }
  }
  // Count how many vertices we have, and remember which old vertex index
  // will become which new vertex index
  for (int vert=0, new_index=0; vert<vert_count; vert++)
    if (r_vertex_mask[vert])
    {
      r_vert_count++;
      r_vertex_mask[vert] = new_index++;
    }
    else
      r_vertex_mask[vert] = -1;
  // Now use the obtained sizes to allocate memory for the reduced set
  if (((r_adcirc_vert  = (vert2d *)  malloc((r_vert_count + 1) * sizeof(vert2d))) == NULL) ||
      ((r_adcirc_depth = (field2d *) malloc((r_vert_count + 1) * sizeof(field2d))) == NULL) ||
      ((r_adcirc_cell  = (cell2d *)  malloc((r_cell_count + 1) * sizeof(cell2d))) == NULL))
  {
    CCTK_WARN(0, "Failed to allocate memory for reduced mesh.");
    // Never reached - but silences compiler warning
    exit(1);
  }
  CCTK_VInfo(CCTK_THORNSTRING, "Reducing to %d vertices in %d cells", r_vert_count, r_cell_count);
  // Copy all vertices and data
  for (int vert=0; vert<vert_count; vert++)
    if (r_vertex_mask[vert] >= 0)
    {
      r_adcirc_vert[r_vertex_mask[vert]] = adcirc_vert[vert];
      r_adcirc_depth[r_vertex_mask[vert]] = adcirc_depth[vert];
    }
  // Now connect cells with new vertex indices
  int ii = 0;
  for (int i = 0; i < cell_count; i++)
  {
    int include_cell = 0;
    for (int vert = 0; vert<3; vert++)
      if (adcirc_vert[adcirc_cell[i].vertid[vert]].x[0] >= xmin-xbuffer &&
          adcirc_vert[adcirc_cell[i].vertid[vert]].x[0] <= xmax+xbuffer &&
          adcirc_vert[adcirc_cell[i].vertid[vert]].x[1] >= ymin-ybuffer &&
          adcirc_vert[adcirc_cell[i].vertid[vert]].x[1] <= ymax+ybuffer)
        include_cell++;
    if (include_cell)
    {
      for (int vert = 0; vert<3; vert++)
        r_adcirc_cell[ii].vertid[vert] = r_vertex_mask[adcirc_cell[i].vertid[vert]];
      ii++;
    }
  }

  // Now free original global set and replace with reduced set
  free(adcirc_vert);
  free(adcirc_cell);
  free(adcirc_depth);
  adcirc_vert = r_adcirc_vert;
  adcirc_cell = r_adcirc_cell;
  adcirc_depth= r_adcirc_depth;
  vert_count  = r_vert_count;
  cell_count  = r_cell_count;
}


/* compare the Y coordinate of the centroid of the cells */
inline int CompareCentroidY(const void *c1, const void *c2)
{
  CCTK_REAL centroidy1, centroidy2;
  const cell2d *ic1 = (const cell2d *) c1;
  const cell2d *ic2 = (const cell2d *) c2;
  centroidy1 = (
        adcirc_vert[ic1->vertid[0]].x[1]
      + adcirc_vert[ic1->vertid[1]].x[1]
      + adcirc_vert[ic1->vertid[2]].x[1]) / 3.0;
  centroidy2 = (
        adcirc_vert[ic2->vertid[0]].x[1]
      + adcirc_vert[ic2->vertid[1]].x[1]
      + adcirc_vert[ic2->vertid[2]].x[1]) / 3.0;

  if (centroidy1 > centroidy2)
  {
    return 1;
  }
  else if (centroidy1 < centroidy2)
  {
    return -1;
  }
  else
  {
    return 0;
  }
}

