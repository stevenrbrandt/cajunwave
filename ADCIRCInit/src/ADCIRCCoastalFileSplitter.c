#include <stdio.h>
#include <string.h>
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ADCIRCReader.h"

void ADCIRCInit_ADCIRCCoastalFileSplitter (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int myproc;

  myproc = CCTK_MyProc (cctkGH);

/* if we have more than 2 processes, proc 0 will split wind_file
 * and proc 1 will split current file*/
  if (CCTK_nProcs (cctkGH) > 1)
  {
    switch (myproc)
    {
    case 0:
      ADCIRCCoastalFileSplitter (adcirc_wind_file, "wind");
      break;
    case 1:
      ADCIRCCoastalFileSplitter (adcirc_current_file, "current");
      break;
    defaul:
      break;
    }
    CCTK_Barrier (cctkGH);
  }

  else
  {
    ADCIRCCoastalFileSplitter (adcirc_wind_file, "wind");
    ADCIRCCoastalFileSplitter (adcirc_current_file, "current");
  }
  return;
}

void ADCIRCCoastalFileSplitter (const char *datafile, const char *prefix)
{
  FILE *fpin = NULL;
  FILE *fpout = NULL;
  char line[LINELENGTH];
  int i;
  int count = 0;
  double time1 = 0.0;
  double time2 = 0.0;
  char onestepfile[LINELENGTH];

  if (strlen (datafile) > LINELENGTH + 10)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                "input file name %s is too long !", datafile);
  }

  if ((fpin = fopen (datafile, "r")) == NULL)
  {
    CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                "can't open: %s", datafile);
  }

  CCTK_VInfo (CCTK_THORNSTRING, "start splitting ADCIRC data file %s",
              datafile);

  /* skip the first 2 lines */
  fgets (line, LINELENGTH, fpin);
  printf ("%s", line);

  fgets (line, LINELENGTH, fpin);
  printf ("%s", line);

  while (fgets (line, LINELENGTH, fpin) != NULL)
  {
    sscanf (line, "%lf %lf", &time1, &time2);

    if (time1 == time2)
    {
      if (fpout != NULL)
        fclose (fpout);

      sprintf (onestepfile, "%s.%d", prefix, count);
      CCTK_VInfo (CCTK_THORNSTRING,
                  "processing timestep %lf : writing to %s ...", time1,
                  onestepfile);

      if ((fpout = fopen (onestepfile, "w")) == NULL)
      {
        CCTK_VWarn (0, __LINE__, __FILE__, CCTK_THORNSTRING,
                    "can't open: %s", datafile);
      }
      count++;
      continue;
    }
    else
    {
      fputs (line, fpout);
    }
  }
  fclose (fpout);
  fclose (fpin);
  CCTK_VInfo (CCTK_THORNSTRING, "finish splitting ADCIRC data file %s",
              datafile);
  return;
}
