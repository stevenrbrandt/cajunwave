#include "trid_head.hh"

void proc_map(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  std::cout << "Proc map start" << std::endl;
  std::cout << "local size " << cctk_lsh[0] << " x " << cctk_lsh[1] << std::endl;

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  std::cout << "MPI rank/size=" << rank << "/" << size << std::endl;
  std::cout << "cctk_bbox =";
  for(int i=0;i<6;i++)
    std::cout << " " << cctk_bbox[i];
  std::cout << std::endl;

  double *sendbuf = new double[3];
  double *recvbuf = new double[3*size];
  for(int i=0;i<3*size;i++) {
    recvbuf[i] = 0;
  }

  int cc = CCTK_GFINDEX3D(cctkGH,0,0,0);
  sendbuf[0] = x[cc];
  sendbuf[1] = y[cc];
  sendbuf[2] = z[cc];

  int ret_code;
  ret_code = MPI_Allgather(
    sendbuf,3,MPI_DOUBLE,
    recvbuf,3,MPI_DOUBLE,
    MPI_COMM_WORLD);
  if(ret_code != MPI_SUCCESS) {
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      "MPI_Allgather failed.");
  }

  std::map<double,std::map<double,int>> rank_map;
  std::set<double> xset, yset, zset;
  for(int i=0;i<size;i++) {
    double xv = recvbuf[3*i];
    double yv = recvbuf[3*i+1];
    double zv = recvbuf[3*i+2];
    xset.insert(xv);
    yset.insert(yv);
    zset.insert(zv);
    rank_map[xv][yv] = i+1;
  }
  std::vector<double> xvec;
  for(auto i : xset) xvec.push_back(i);
  std::sort(xvec.begin(), xvec.end());
  std::vector<double> yvec;
  for(auto i : yset) yvec.push_back(i);
  std::sort(yvec.begin(), yvec.end());

  bool mesh = true;
  std::ostringstream mtext;
  mtext << "MPI Processor Map:" << std::endl;
  int xindex = -1, yindex = -1,ix=0,iy=0;
  for(auto xc : xvec) {
    if(fabs(xc - x[cc]) < 1e-10) xindex = ix;
    ix++;
    iy=0;
    for(auto yc : yvec) {
      if(fabs(yc - y[cc]) < 1e-10) yindex = iy;
      iy++;
      int mrank = rank_map[xc][yc];
      if(mrank == 0)
        mesh = false;
      mtext << std::setw(6) << mrank-1;
    }
    mtext << std::endl;
  }
  mtext << "MPI Proc Mesh: " << xvec.size() << " x " << yvec.size() << std::endl;
  if(!mesh) {
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      "MPI Processes do not form a mesh.");
  } else if(zset.size() != 1) {
    std::ostringstream msg;
    msg << "Number of procs along z should be one, not " << zset.size();
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      msg.str().c_str());
  } else {
    std::cout << mtext.str();
  }

  ret_code = MPI_Comm_split(MPI_COMM_WORLD,yindex,xindex,&x_com);
  if(ret_code != MPI_SUCCESS) {
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      "MPI_Comm_split failed.");
  }
  std::cout << std::endl;

  MPI_Comm_rank(x_com,&xrank);
  MPI_Comm_size(x_com,&xsize);
  sendbuf[0] = x[cc];
  sendbuf[1] = y[cc];
  MPI_Allgather(
    sendbuf,2,MPI_DOUBLE,
    recvbuf,2,MPI_DOUBLE,
    x_com);
  std::cout << "X-COM: xsize=" << xsize << ", xrank=" << xrank << std::endl;
  for(int i=0;i<xsize;i++) {
    double xc = recvbuf[2*i];
    double yc = recvbuf[2*i+1];
    char ch = ' ';
    if(xc == x[cc] && yc == y[cc]) ch = '*';
    std::cout << std::setw(10) << xc << "," << std::setw(10) << yc << ch << std::endl;
  }
  std::cout << std::endl;

  ret_code = MPI_Comm_split(MPI_COMM_WORLD,xindex,yindex,&y_com);
  if(ret_code != MPI_SUCCESS) {
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      "MPI_Comm_split failed.");
  }

  MPI_Comm_rank(y_com,&yrank);
  MPI_Comm_size(y_com,&ysize);
  sendbuf[0] = x[cc];
  sendbuf[1] = y[cc];
  MPI_Allgather(
    sendbuf,2,MPI_DOUBLE,
    recvbuf,2,MPI_DOUBLE,
    y_com);
  std::cout << "Y-COM: ysize=" << ysize << ", yrank=" << yrank << std::endl;
  for(int i=0;i<ysize;i++) {
    double xc = recvbuf[2*i];
    double yc = recvbuf[2*i+1];
    char ch = ' ';
    if(xc == x[cc] && yc == y[cc]) ch = '*';
    std::cout << std::setw(10) << xc << "," << std::setw(10) << yc << ch << std::endl;
  }
}

void trid_alloc(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  if(coefs_size > 0)
    return;

  const int ilo = *trid_ilo;
  const int ihi = *trid_ihi;
  const int jlo = *trid_jlo;
  const int jhi = *trid_jhi;
  const int deli = ihi-ilo+1;
  const int delj = jhi-jlo+1;

  int mx = 6*delj;
  int my = 6*deli;
  int mm = mx > my ? mx : my;
  coefs = new double[mm];
  coefs_size = mm;
  assert(coefs_size >= 6*delj);
  if(coefs == 0) {
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      "Could not allocate coefs for tridiagonal solver");
  }
  mx = 6*delj*xsize;
  my = 6*deli*ysize;
  mm = mx > my ? mx : my;
  all_coefs = new double[mm];
  if(all_coefs == 0) {
    CCTK_Error(__LINE__,__FILE__,CCTK_THORNSTRING,
      "Could not allocate all_coefs for tridiagonal solver");
  }
}

extern "C"
void do_nothing(CCTK_ARGUMENTS)
{
}

extern "C"
void copyout2(CCTK_POINTER_TO_CONST const cctkGH_,CCTK_REAL *_rxx) {
  cGH *cctkGH = (cGH *)cctkGH_;
  DECLARE_CCTK_ARGUMENTS;

  int ilo = *trid_ilo;
  int ihi = *trid_ihi;
  int jlo = *trid_jlo;
  int jhi = *trid_jhi;
  int klo = *trid_klo;
  int khi = *trid_khi;
  int k = (klo+khi)>>1;

  for(int j=jlo;j<=jhi;j++) {
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      _rxx[cc] = rxx[cc];
    }
  }
}

extern "C"
void copyin2(CCTK_POINTER_TO_CONST cctkGH_,CCTK_POINTER rxx__) {
  cGH *cctkGH = (cGH *)cctkGH_;
  DECLARE_CCTK_ARGUMENTS;
  const CCTK_REAL *rxx_ = (const CCTK_REAL *)rxx__;

  int ilo = *trid_ilo;
  int ihi = *trid_ihi;
  int jlo = *trid_jlo;
  int jhi = *trid_jhi;
  int klo = *trid_klo;
  int khi = *trid_khi;
  int k = (klo+khi)>>1;

  for(int j=jlo;j<=jhi;j++) {
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      rxx[cc] = rxx_[cc];
    }
  }
}

struct copy_array {
  int n;
  CCTK_REAL *data;
  copy_array(int sz,CCTK_REAL *v) : n(sz), data(new CCTK_REAL[n]) {
    for(int i=0;i<n;i++) {
      data[i] = v[i];
    }
  }
  ~copy_array() { delete[] data; }
  CCTK_REAL& operator[](int i) {
    return data[i];
  }
};

void solve_trid(CCTK_REAL *a,CCTK_REAL *b,CCTK_REAL *c,CCTK_REAL *d,CCTK_REAL *u,int n) {
  a[0] = 0;
  c[n-1] = 0;
  copy_array ca(n,a),cb(n,b),cd(n,d);
  for(int i=0;i<n;i++) {
    u[i] = nan("17");
    assert(!std::isnan(a[i]));
    assert(!std::isnan(b[i]));
    assert(!std::isnan(c[i]));
    assert(!std::isnan(d[i]));
  }
  for(int i=1;i<n;i++) {
    if(b[i-1] != 0) {
      CCTK_REAL f = a[i]/b[i-1];
      //a[i] -= f*b[i-1];
      a[i] = 0;
      b[i] -= f*c[i-1];
      d[i] -= f*d[i-1];
    }
  }
  for(int i=0;i<n;i++) {
    if(a[i] == 0) {
      if(b[i] == 0 && c[i] != 0) {
        u[i+1] = d[i]/c[i];
        assert(i+1 < n);
      } else if(c[i] == 0 && b[i] != 0) {
        u[i] = d[i]/b[i];
      }
    } else if(b[i] == 0 && c[i] == 0 && a[i] != 0) {
      u[i-1] = d[i]/a[i];
      assert(i > 0);
    }
  }
  for(int i=n-1;i>=0;i--) {
    CCTK_REAL aval = nan("17");
    if(a[i] == 0) {
      aval = 0;
    } else if(!std::isnan(u[i-1])) {
      aval = a[i]*u[i-1];
      assert(i > 0);
    }
    CCTK_REAL bval = nan("17");
    if(b[i] == 0) {
      bval = 0;
    } else if(!std::isnan(u[i])) {
      bval = b[i]*u[i];
    }
    CCTK_REAL cval = nan("17");
    if(c[i] == 0) {
      cval = 0;
    } else if(!std::isnan(u[i+1])) {
      cval = c[i]*u[i+1];
      assert(i+1 < n);
    }
    if(a[i] != 0 && std::isnan(aval) && !std::isnan(bval) && !std::isnan(cval)) {
      u[i-1] = (d[i] - bval - cval)/a[i];
    } else if(b[i] != 0 && !std::isnan(aval) && std::isnan(bval) && !std::isnan(cval)) {
      u[i] = (d[i] - aval - cval)/b[i];
    } else if(c[i] != 0 && !std::isnan(aval) && !std::isnan(bval) && std::isnan(cval)) {
      u[i] = (d[i] - aval - bval)/c[i];
    }
  }
  for(int i=0;i<n;i++) {
    assert(!std::isnan(u[i]));
    CCTK_REAL diff = cb[i]*u[i]-cd[i];
    if(i > 0) diff += ca[i]*u[i-1];
    if(i+1<n) diff += c[i]*u[i+1];
    if(fabs(diff)>1.0e-14) {
      std::cout << "diff=" << diff << " i=" << i << " n=" << n << " u=" << u[i] << std::endl;
    }
  }
}
