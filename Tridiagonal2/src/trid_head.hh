#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <mpi.h>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>

extern MPI_Comm x_com, y_com;
extern int xsize, xrank, ysize, yrank;
extern double *coefs;
extern double *all_coefs;
extern int coefs_size;
extern void trid_alloc(CCTK_ARGUMENTS);
