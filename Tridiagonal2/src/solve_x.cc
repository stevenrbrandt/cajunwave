#include "trid_head.hh"

void solve_trid(CCTK_REAL *a,CCTK_REAL *b,CCTK_REAL *c,CCTK_REAL *d,CCTK_REAL *u,int n);

void solve_x(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  trid_alloc(CCTK_PASS_CTOC);

  const int ilo = *trid_ilo;
  const int ihi = *trid_ihi;
  const int jlo = *trid_jlo;
  const int jhi = *trid_jhi;
  const int klo = *trid_klo;
  const int khi = *trid_khi;
  const int delj = (jhi-jlo+1);

  // Memory usage:
  // If the size of the problem is n*p in each
  // direction, where n is the number of grid
  // points in that direction and p is the number
  // of MPI processes, then the overhead of the
  // method is (2*p-2)**2*n, in terms of GF's
  // this is (2*p-2)**2/n. That shouldn't be bad.

  const int k = (klo+khi)>>1;

  if(xsize == 1) {
    //std::cout << "Using the non-parallel tridiagonal solver for the x-direction." << std::endl;
    // This is a trivial solve, we have only
    // one process in the x-direction.
    // a[i]*r[i-1]+b[i]*r[i]+c[i]*r[i+1] = d[i]
    //#pragma omp parallel for schedule(dynamic,1)
    for(int j=jlo;j<=jhi;j++) {
      int clo = CCTK_GFINDEX3D(cctkGH,ilo,j,k);
      nbxx[clo] = bxx[clo];
      ndxx[clo] = dxx[clo];
      for(int i=ilo+1;i<=ihi;i++) {
        int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
        int im1 = CCTK_GFINDEX3D(cctkGH,i-1,j, k);
        CCTK_REAL f = axx[cc]/nbxx[im1];
        nbxx[cc] = bxx[cc]-f*cxx[im1];
        ndxx[cc] = dxx[cc]-f*ndxx[im1];
      }
      int chi = CCTK_GFINDEX3D(cctkGH,ihi,j,k);
      rxx[chi] = ndxx[chi]/nbxx[chi];
      for(int i=ihi-1;i>=ilo;i--) {
        int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
        int ip1 = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
        rxx[cc] = (ndxx[cc]-rxx[ip1]*cxx[cc])/nbxx[cc];
      }
    }
    return;
  }

  //#pragma omp parallel for schedule(dynamic,1)
  for(int j=jlo;j<=jhi;j++) {
    
    // Step 1: Zero out du and dl
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
      duxx[cc] = 0;
      dlxx[cc] = 0;
    }

    // Step 2: Perform the local tridiagonal solve 3 times
    int clo = CCTK_GFINDEX3D(cctkGH,ilo,j,k);
    int chi = CCTK_GFINDEX3D(cctkGH,ihi,j,k);
    nbxx[clo] = bxx[clo];
    ndxx[clo] = dxx[clo];
    duxx[clo] = -axx[clo];
    dlxx[chi] = -cxx[chi];

    // Step 2a: forward elimination
    for(int i=ilo+1;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
      int im1 = CCTK_GFINDEX3D(cctkGH,i-1,j, k);
      CCTK_REAL f = axx[cc]/nbxx[im1];
      nbxx[cc] = bxx[cc]-f*cxx[im1];
      ndxx[cc] = dxx[cc]-f*ndxx[im1];
      duxx[cc] = duxx[cc]-f*duxx[im1];
      dlxx[cc] = dlxx[cc]-f*dlxx[im1];
    }

    // Step 2b: back substitution
    rxx[chi] = ndxx[chi]/nbxx[chi];
    ruxx[chi] = duxx[chi]/nbxx[chi];
    rlxx[chi] = dlxx[chi]/nbxx[chi];
    for(int i=ihi-1;i>=ilo;i--) {
      int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
      int ip1 = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
      rxx[cc] = (ndxx[cc]-rxx[ip1]*cxx[cc])/nbxx[cc];
      ruxx[cc] = (duxx[cc]-ruxx[ip1]*cxx[cc])/nbxx[cc];
      rlxx[cc] = (dlxx[cc]-rlxx[ip1]*cxx[cc])/nbxx[cc];
    }

    // Step 3: Fill in the coefficients to exchange
    int jj = j - jlo;
    assert(6*jj < 6*delj);
    assert(!std::isnan(duxx[clo]));
    coefs[0+6*jj] = ruxx[clo];
    coefs[1+6*jj] = rlxx[clo];
    coefs[2+6*jj] = rxx[clo];
    coefs[3+6*jj] = ruxx[chi];
    coefs[4+6*jj] = rlxx[chi];
    coefs[5+6*jj] = rxx[chi];
  }
  // Step 4: Exchange coefficients
  MPI_Allgather(
    coefs    ,6*delj,MPI_DOUBLE,
    all_coefs,6*delj,MPI_DOUBLE,
    x_com);

  for(int j=jlo;j<=jhi;j++) {
    // Step 5: Construct the new tridiagonal system
    int ncof = 2*xsize-2;
    double ma[ncof],mb[ncof],mc[ncof],md[ncof],mr[ncof];
    for(int i=0;i<ncof;i++)
      ma[i]=mb[i]=mc[i]=md[i]=mr[i]=0.0;
    int jj = j - jlo;

    // Iterate through all processors
    for(int r=0;r<xsize;r++) {
      int off = 6*jj+6*delj*r;
      int nth = 2*r-1;
      if(nth > 0) {
        mc[nth-1] = -1.0;
      }
      if(nth >= 0) {
        ma[nth] = -1.0;
        mb[nth] = all_coefs[0+off]; // xu[0]
        mc[nth] = all_coefs[1+off]; // xl[0]
      }
      if(nth+1 < ncof) {
        ma[nth+1] = all_coefs[3+off]; // xu[m]
        mb[nth+1] = all_coefs[4+off]; // xl[m]
      }
      if(nth > 0) {
        md[nth] = -all_coefs[2+off]; // xr[0]
      }
      if(nth+1 < ncof) {
        md[nth+1] = -all_coefs[5+off]; // xr[m]
      }
    }

    // Step 6: Perform the 2nd tridiagonal solve

    #if 0
    // Step 6a: forward elimination
    for(int i=1;i<ncof;i++) {
      CCTK_REAL f = ma[i]/mb[i-1];
      mb[i] = mb[i] - f*mc[i-1];
      md[i] = md[i] - f*md[i-1];
    }

    // Step 6b: back substitution
    mr[ncof-1] = md[ncof-1]/mb[ncof-1];
    for(int i=ncof-1;i>=0;i--) {
      mr[i] = (md[i]-mr[i+1]*mc[i])/mb[i];
    }
    #endif
    solve_trid(ma,mb,mc,md,mr,ncof);

    // Step 7: Assemble the answer
    double ku,kl;
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
      if(xrank == 0)
        ku = 0;
      else
        ku = mr[2*xrank-1];
      if(xrank==xsize-1)
        kl = 0;
      else
        kl = mr[2*xrank];
      rxx[cc] = rxx[cc]+kl*rlxx[cc]+ku*ruxx[cc];
    }
  }
}

void check_all_x(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  return;

  const int ilo = *trid_ilo;
  const int ihi = *trid_ihi;
  const int jlo = *trid_jlo;
  const int jhi = *trid_jhi;
  const int klo = *trid_klo;
  const int khi = *trid_khi;
  int k = (klo+khi)>>1;
  CCTK_REAL fm = 0.0;
  for(int j=jlo;j<=jhi;j++) {
    // check
    for(int j=jlo;j<=jhi;j++) {
      for(int i=ilo;i<=ihi;i++) {
        int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
        int cp = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
        int cm = CCTK_GFINDEX3D(cctkGH,i-1,j,k);
        CCTK_REAL diff = fabs(axx[cc]*rxx[cm]+bxx[cc]*rxx[cc]+cxx[cc]*rxx[cp]-dxx[cc]);
        if(diff > fm) fm = diff;
      }
    }
  }
  if(fm > 1.0e-14) {
    for(int j=jlo;j<=jhi;j++) {
      // check
      std::cout << "Check tridiagonal solve X" << std::endl;
      for(int j=jlo;j<=jhi;j++) {
        for(int i=ilo;i<=ihi;i++) {
          int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
          int cp = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
          int cm = CCTK_GFINDEX3D(cctkGH,i-1,j,k);
          CCTK_REAL diff = fabs(axx[cc]*rxx[cm]+bxx[cc]*rxx[cc]+cxx[cc]*rxx[cp]-dxx[cc]);
          if(diff > 1.0e-14)
            std::cout << " *";
          else
            std::cout << " .";
        }
        std::cout << std::endl;
      }

      std::cout << " y-error" << std::endl;
    }
  }
}
