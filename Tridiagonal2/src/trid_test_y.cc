#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <math.h>
#include <iostream>

CCTK_REAL func_y(CCTK_REAL y) {
  return sin(y)+cos(y)*cos(y);
}

void prepare_y_solve(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *trid_ilo = cctk_nghostzones[0];
  *trid_ihi = cctk_lsh[0]-cctk_nghostzones[0]-1;
  *trid_jlo = cctk_nghostzones[1];
  *trid_jhi = cctk_lsh[1]-cctk_nghostzones[1]-1;
  *trid_klo = cctk_nghostzones[2];
  *trid_khi = cctk_lsh[2]-cctk_nghostzones[2]-1;

  int ilo = *trid_ilo;
  int ihi = *trid_ihi;
  int jlo = *trid_jlo;
  int jhi = *trid_jhi;
  int klo = *trid_klo;
  int khi = *trid_khi;

  int k = (klo+khi)>>1;

  // a[j]*r[j-1]+b[j]*r[j]+c[j]*r[j+1] = d[j]
  CCTK_REAL dy;
  for(int j=0;j<cctk_lsh[1];j++) {
    for(int i=0;i<cctk_lsh[0];i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      rxx[cc] = 0;
      axx[cc] = 0;
      bxx[cc] = 0;
      cxx[cc] = 0;
      dxx[cc] = 0;
      duxx[cc] = 0;
      dlxx[cc] = 0;
    }
  }

  for(int i=ilo;i<=ihi;i++) {

    // inner
    for(int j=jlo;j<=jhi;j++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      int cp = CCTK_GFINDEX3D(cctkGH,i,j+1,k);
      int cm = CCTK_GFINDEX3D(cctkGH,i,j-1,k);
      CCTK_REAL vp = func_y(y[cp]);
      CCTK_REAL vm = func_y(y[cm]);
      CCTK_REAL vc = func_y(y[cc]);
      dy = 0.5*(y[cp]-y[cm]);
      CCTK_REAL idy = 1.0/dy;
      CCTK_REAL idy2 = idy*idy;
      if((j == jlo && cctk_bbox[2]) || (j == jhi && cctk_bbox[3])) {
        axx[cc] = 0.0;
        cxx[cc] = 0.0;
        bxx[cc] = 1.0;
        dxx[cc] = vc;
      } else {
        cxx[cc] = idy2 + 0.5*idy;
        bxx[cc] = -2.0*idy2+1.0;
        axx[cc] = idy2 - 0.5*idy;
        dxx[cc] =  (vp+vm-2*vc)*idy2+0.5*(vp-vm)*idy+vc;
      }
      rxx[cc] = func_y(y[cc]);
    }
  }
}

void check_y_solve(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *trid_ilo = cctk_nghostzones[0];
  *trid_ihi = cctk_lsh[0]-cctk_nghostzones[0]-1;
  *trid_jlo = cctk_nghostzones[1];
  *trid_jhi = cctk_lsh[1]-cctk_nghostzones[1]-1;
  *trid_klo = cctk_nghostzones[2];
  *trid_khi = cctk_lsh[2]-cctk_nghostzones[2]-1;

  int ilo = *trid_ilo;
  int ihi = *trid_ihi;
  int jlo = *trid_jlo;
  int jhi = *trid_jhi;
  int klo = *trid_klo;
  int khi = *trid_khi;

  int k = (klo+khi)>>1;

  std::cout << "Finding errors in the y-solution parallel tridiagonal solver" << std::endl;
  std::cout << "Grid: " << cctk_lsh[0] << " x " << cctk_lsh[1] << std::endl;
  std::cout << "===============" << std::endl << std::endl;

  CCTK_REAL max_diff = 0;
  for(int j=jlo;j<=jhi;j++) {
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      int cp = CCTK_GFINDEX3D(cctkGH,i,j+1,k);
      int cm = CCTK_GFINDEX3D(cctkGH,i,j-1,k);
      CCTK_REAL diff = 0;
      assert(!std::isnan(axx[cc]));
      assert(!std::isnan(cxx[cc]));
      assert(!std::isnan(bxx[cc]));
      assert(!std::isnan(dxx[cc]));
      assert(!std::isnan(rxx[cm]));
      assert(!std::isnan(rxx[cc]));
      assert(!std::isnan(rxx[cp]));
      diff=axx[cc]*rxx[cm]+bxx[cc]*rxx[cc]+cxx[cc]*rxx[cp] - dxx[cc];
      assert(!std::isnan(diff));
      diff = fabs(diff);
      if(max_diff < diff)
        max_diff = diff;
      if(i == ilo+1)
        std::cout << "i,j=" << i << "," << j
          << " v=" << rxx[cc]
          << " diff=" << diff
          << std::endl;
    }
  }
  std::cout << "max_diff=" << max_diff << std::endl;
}
