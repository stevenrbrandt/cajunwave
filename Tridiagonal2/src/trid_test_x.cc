#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include <math.h>
#include <iostream>

CCTK_REAL func_x(CCTK_REAL x) {
  return sin(x)+cos(x)*cos(x);
}

void prepare_x_solve(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *trid_ilo = cctk_nghostzones[0];
  *trid_ihi = cctk_lsh[0]-cctk_nghostzones[0]-1;
  *trid_jlo = cctk_nghostzones[1];
  *trid_jhi = cctk_lsh[1]-cctk_nghostzones[1]-1;
  *trid_klo = cctk_nghostzones[2];
  *trid_khi = cctk_lsh[2]-cctk_nghostzones[2]-1;

  int ilo = *trid_ilo;
  int ihi = *trid_ihi;
  int jlo = *trid_jlo;
  int jhi = *trid_jhi;
  int klo = *trid_klo;
  int khi = *trid_khi;

  int k = (klo+khi)>>1;

  // a[i]*r[i-1]+b[i]*r[i]+c[i]*r[i+1] = d[i]
  CCTK_REAL dx;
  for(int j=0;j<cctk_lsh[1];j++) {
    for(int i=0;i<cctk_lsh[0];i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      rxx[cc] = 0;
      axx[cc] = 0;
      bxx[cc] = 0;
      cxx[cc] = 0;
      dxx[cc] = 0;
      duxx[cc] = 0;
      dlxx[cc] = 0;
    }
  }

  for(int j=jlo;j<=jhi;j++) {

    // inner
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      int cp = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
      int cm = CCTK_GFINDEX3D(cctkGH,i-1,j,k);
      CCTK_REAL vp = func_x(x[cp]);
      CCTK_REAL vm = func_x(x[cm]);
      CCTK_REAL vc = func_x(x[cc]);
      dx = 0.5*(x[cp]-x[cm]);
      CCTK_REAL idx = 1.0/dx;
      CCTK_REAL idx2 = idx*idx;
      if((i == ilo && cctk_bbox[0]) || (i == ihi && cctk_bbox[1])) {
        axx[cc] = 0.0;
        cxx[cc] = 0.0;
        bxx[cc] = 1.0;
        dxx[cc] = vc;
      } else {
        cxx[cc] = idx2 + 0.5*idx;
        bxx[cc] = -2.0*idx2+1.0;
        axx[cc] = idx2 - 0.5*idx;
        dxx[cc] =  (vp+vm-2*vc)*idx2+0.5*(vp-vm)*idx+vc;
      }
      rxx[cc] = func_x(x[cc]);
    }
  }
}

void check_x_solve(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *trid_ilo = cctk_nghostzones[0];
  *trid_ihi = cctk_lsh[0]-cctk_nghostzones[0]-1;
  *trid_jlo = cctk_nghostzones[1];
  *trid_jhi = cctk_lsh[1]-cctk_nghostzones[1]-1;
  *trid_klo = cctk_nghostzones[2];
  *trid_khi = cctk_lsh[2]-cctk_nghostzones[2]-1;

  int ilo = *trid_ilo;
  int ihi = *trid_ihi;
  int jlo = *trid_jlo;
  int jhi = *trid_jhi;
  int klo = *trid_klo;
  int khi = *trid_khi;

  int k = (klo+khi)>>1;

  std::cout << "Finding errors in the x-solution parallel tridiagonal solver" << std::endl;
  std::cout << "Grid: " << cctk_lsh[0] << " x " << cctk_lsh[1] << std::endl;
  std::cout << "===============" << std::endl << std::endl;

  CCTK_REAL max_diff = 0;
  for(int j=jlo;j<=jhi;j++) {
    for(int i=ilo;i<=ihi;i++) {
      int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
      int cp = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
      int cm = CCTK_GFINDEX3D(cctkGH,i-1,j,k);
      CCTK_REAL diff = 0;
      assert(!std::isnan(axx[cc]));
      assert(!std::isnan(cxx[cc]));
      assert(!std::isnan(bxx[cc]));
      assert(!std::isnan(dxx[cc]));
      assert(!std::isnan(rxx[cm]));
      assert(!std::isnan(rxx[cc]));
      assert(!std::isnan(rxx[cp]));
      diff=axx[cc]*rxx[cm]+bxx[cc]*rxx[cc]+cxx[cc]*rxx[cp] - dxx[cc];
      assert(!std::isnan(diff));
      diff = fabs(diff);
      if(max_diff < diff)
        max_diff = diff;
      if(j == jlo)
        std::cout << "i,j=" << i << "," << j
          << " v=" << rxx[cc]
          << " diff=" << diff
          << std::endl;
    }
  }
  std::cout << "max_diff=" << max_diff << std::endl;
}
