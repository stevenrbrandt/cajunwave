#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void OilSpillEvolve_RegisterVars(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT ierr = 0, group, rhs;

  group = CCTK_GroupIndex ("oilspillbase::parcel_position");
  rhs = CCTK_GroupIndex ("oilspillbase::parcel_velocity");

  if (CCTK_IsFunctionAliased ("MoLRegisterEvolvedGroup"))
  {
    ierr += MoLRegisterEvolvedGroup (group, rhs);
  }
  else
  {
    CCTK_WARN (0, "MoL function not aliased !");
    ierr++;
  }

  if (ierr) CCTK_WARN(0,"Problems registering with MoL !");

  return ;
}
