#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

/* prepare for the calculation of the RHS */

void OilSpillEvolve_CalcAdvecVelocity (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_REAL kc = current_factor;
  CCTK_REAL kw = wind_drift_factor;

  int i, j, k, i3D;
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        if (uc1[i3D] == 0.0 && uc2[i3D] == 0.0)
        {
          ua1[i3D] = 0.0;
          ua2[i3D] = 0.0;
        }
        else
        {
          ua1[i3D] = kc * uc1[i3D] + kw * uw1[i3D];
          ua2[i3D] = kc * uc2[i3D] + kw * uw2[i3D];
        }
        ua3[i3D] = 0.0;
      }
}


/* fill slices that k != 0 here to save time */
void OilSpillEvolve_Fill3D (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  int i, lsh;
  CCTK_GrouplshGN (cctkGH, 1, &lsh, "OilSpillBase::parcel_position");
  for (i = 0; i < lsh; i++)
  {
    velx3[i] = 0.0;
  }
}
