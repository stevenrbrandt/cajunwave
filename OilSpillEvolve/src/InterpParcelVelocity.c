#include "cctk.h"
#include "util_Table.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

/* Interpolate the parcel velocity based on the advection velocity */

void OilSpillEvolve_InterpParcelVelocity (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int interp_handle, coord_system_handle;

  /* interpolation points */
  const void *interp_coords[3];

  char interp_order_param[10];

  /* input and output arrays */
  CCTK_INT in_array[2];

  /* types of output arrays */
  static const CCTK_INT out_array_type[2] = { CCTK_VARIABLE_REAL, CCTK_VARIABLE_REAL };

  /* pointers to output arrays */
  void *out_array[2];

  /* local size */
  CCTK_INT lsh;

  CCTK_GrouplshGN (cctkGH, 1, &lsh, "OilSpillBase::parcel_position");

  if ((interp_handle = CCTK_InterpHandle ("uniform cartesian")) < 0)
  {
    CCTK_WARN (0, "can’t get operator handle!");
  }

  if ((coord_system_handle = CCTK_CoordSystemHandle("cart3d")) < 0)
  {
    CCTK_WARN (0, "can’t get coordinate-system handle!");
  }

  interp_coords[0] = (const void *) xp1;
  interp_coords[1] = (const void *) xp2;
  interp_coords[2] = (const void *) xp3;

  in_array[0] = CCTK_VarIndex ("OilSpillBase::ua1");
  in_array[1] = CCTK_VarIndex ("OilSpillBase::ua2");

  out_array[0] = (void *) velx1;
  out_array[1] = (void *) velx2;

  sprintf(interp_order_param, "order = %d", advecvel_interp_order);

  if (CCTK_InterpGridArrays (cctkGH, 3,
                             interp_handle,
                             Util_TableCreateFromString (interp_order_param),
                             coord_system_handle,
                             lsh, CCTK_VARIABLE_REAL,
                             interp_coords,
                             2, in_array,
                             2, out_array_type, out_array) < 0)
  {
    CCTK_WARN (0, "error return from interpolator!");
  }
}
