/*
 * here.hh
 *
 *  Created on: Sep 6, 2011
 *      Author: sbrandt
 */

#ifndef HERE_HH_
#define HERE_HH_
#include <iostream>
#include <iomanip>

// anywhere
#define HERE { std::cout << "HERE " << __FILE__ << ":" << __LINE__ << std::setprecision(17) << std::scientific

// inside loops
#define HEREL if(i==idebug && j==jdebug) { std::cout << "HERE " << __FILE__ << ":" << __LINE__ << std::setprecision(17) << std::scientific

// outside loops but loop variables
#define HEREC { int cc = FUNWAVE_INDEX(idebug,jdebug); std::cout << "HERE " << __FILE__ << ":" << __LINE__ << std::setprecision(17) << std::scientific

#define VAR(X) << " " << #X << "=" << X

#define END << std::endl; }

const int idebug = 40, jdebug = 43;

#endif /* HERE_HH_ */
