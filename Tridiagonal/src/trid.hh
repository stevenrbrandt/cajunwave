#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include <iostream>
#include "here.hh"

// cctk_lsh[0] == 8
// cctk_nghostzones[0] == 2
// int ilo == 2
// int ihi == 8 - 2 - 1 = 5
// [1] [2] | [3] [4] [5] | [6] [7]

// The if cases in the z direction below make sure both 3D and 2D grid functions work. They
// should eventually be switched to support only 2D grid functions. This doesn't mean real
// 2D grid functions, but 3D grid functions with only 1 point in z-direction.

#define DECLARE_TRID_BOUNDS \
	int ilo = cctk_nghostzones[0]; \
	int jlo = cctk_nghostzones[1]; \
	int klo = cctk_lsh[2]>1?cctk_nghostzones[2]:0; \
	int ihi = cctk_lsh[0]-cctk_nghostzones[0]-1; \
	int jhi = cctk_lsh[1]-cctk_nghostzones[1]-1; \
	int khi = cctk_lsh[2]>1?cctk_lsh[2]-cctk_nghostzones[2]-1:0;

inline double sq(double x) { return x*x; }

const CCTK_REAL kk = 0.1; // arbitrary choice
