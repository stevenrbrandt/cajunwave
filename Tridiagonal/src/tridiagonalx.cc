/*
 * tridiagonal.cc
 *
 *  Created on: Feb 3, 2012
 *      Author: sbrandt
 */

#include "trid.hh"
#include <fstream>
#include <sstream>
#include <time.h>
#include "mpi.h"

#include "cctk_ScheduleFunctions.h"

extern "C" int GetRefinementLevel(const cGH *);

// D_xx f = kk**2 f
// (f_(i+1) + f_(i-1) - 2*f_i)/h**2 - kk**2 f_i = 0
// c*f_(i+1) + a*f_(i-1) + b*f_i = 0
// axx = 1/h**2
// bxx = -2/h**2-kk**2
// cxx = 1/h**2
// dxx = 0
void tridiagonal_set_x_arrays(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_TRID_BOUNDS;
    // Need to have this initialized for the test, but we
    // only need to do it for the x direction (since that
    // runs first).
    *trid_ilo = ilo;
    *trid_ihi = ihi;
    *trid_jlo = jlo;
    *trid_jhi = jhi;
    *trid_klo = klo;
    *trid_khi = khi;
    for(int k=klo;k<=khi;k++) {
        //#pragma omp parallel for
        for(int j=jlo;j<=jhi;j++) {
            for(int i=ilo;i<=ihi;i++) {
                int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                if(cctk_bbox[0] > 0 && i == ilo) {
                    bxx[cc] = 1.0;
                    axx[cc] = cxx[cc] = 0.0;
                    dxx[cc] = exp(kk*x[cc]);
                } else if(cctk_bbox[1] > 0 && i == ihi) {
                    bxx[cc] = 1.0;
                    axx[cc] = cxx[cc] = 0.0;
                    dxx[cc] = exp(kk*x[cc]);
                } else {
                    CCTK_REAL f = 1/sq(CCTK_DELTA_SPACE(0));
                    axx[cc] = f;
                    cxx[cc] = f;
                    bxx[cc] = -2*f - sq(kk);
                    dxx[cc] = 0.0;
                }
            }
        }
    }
}
void tridiagonal_test_part1(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_TRID_BOUNDS;

    if(cctk_bbox[0]>0 && cctk_bbox[1]>0) {
        tridiagonal_set_x_arrays(CCTK_PASS_CTOC);
        tridiagonal_x_impl(CCTK_PASS_CTOC,trid_test_answer,ilo,ihi,jlo,jhi,klo,khi);
    } else {
        for(int k=0;k<cctk_lsh[2];k++) {
            for(int j=0;j<cctk_lsh[1];j++) {
                for(int i=0;i<cctk_lsh[0];i++) {
                    int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                    trid_test_answer[cc] = exp(kk*x[cc]);
                }
            }
        }
    }
    tridiagonal_set_x_arrays(CCTK_PASS_CTOC);
}

void tridiagonal_setup_x(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_TRID_BOUNDS;
    ilo = *trid_ilo;
    ihi = *trid_ihi;
    jlo = *trid_jlo;
    jhi = *trid_jhi;
    for(int k=0;k<cctk_lsh[2];k++) {
        for(int j=0;j<cctk_lsh[1];j++) {
            for(int i=0;i<cctk_lsh[0];i++) {
                int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                progress_mask[cc] = 0;
            }
        }
    }
    *trid_not_done = 1;
    if(GetRefinementLevel(cctkGH)>0) {
        if(klo > khi) klo = khi = (klo+khi)/2;

        int k = (khi+klo)>>1;
        {
            for(int j=jlo;j<=jhi;j++) {
                int cc = CCTK_GFINDEX3D(cctkGH,ilo,j,k);
                axx[cc] = cxx[cc] = 0;
                bxx[cc] = 1.0;
                dxx[cc] = rxx[cc];
                cc = CCTK_GFINDEX3D(cctkGH,ihi,j,k);
                axx[cc] = cxx[cc] = 0;
                bxx[cc] = 1.0;
                dxx[cc] = rxx[cc];
            }
        }
    }
}

void tridiagonal_test_part2(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_TRID_BOUNDS;

    CCTK_REAL n,err,err2,sig,avg,avg2;
    n = 0;
    err = err2 = 0;
    for(int k=klo;k<=khi;k++) {
        for(int j=jlo;j<=jhi;j++) {
            for(int i=ilo;i<=ihi;i++) {
                int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                CCTK_REAL diff = rxx[cc] - exp(kk*x[cc]);//trid_test_answer[cc];
                n ++;
                err += diff;
                err2 += sq(diff);
            }
        }
    }

    CCTK_INT operation_handle=0;
    double sum_err=0,sum_err2=0,sum_n=0;

    operation_handle = CCTK_ReductionArrayHandle ("sum");

    /* -1 here will let Cactus distribute the data to all processors */
    CCTK_ReduceLocScalar (cctkGH, -1, operation_handle, &err, &sum_err,
            CCTK_VARIABLE_REAL);
    CCTK_ReduceLocScalar (cctkGH, -1, operation_handle, &err2, &sum_err2,
            CCTK_VARIABLE_REAL);
    CCTK_ReduceLocScalar (cctkGH, -1, operation_handle, &n, &sum_n,
            CCTK_VARIABLE_REAL);

    avg = sum_err / sum_n;
    avg2 = sum_err2 / sum_n;
    sig = sqrt(avg2-sq(avg));
    std::cout << "RESULT X: " << avg << " +/- " << sig << std::endl;
}

extern "C"
void tridiagonal_x_parallel_impl(CCTK_ARGUMENTS) {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_TRID_BOUNDS;
    int cc;
    ilo = *trid_ilo;
    ihi = *trid_ihi;
    jlo = *trid_jlo;
    jhi = *trid_jhi;
    klo = *trid_klo;
    khi = *trid_khi;

    // forward elimination
    int progress = 0;
    int rl = GetRefinementLevel(cctkGH);
    for(int k=klo;k<=khi;k++) {
        //#pragma omp parallel for
        for(int j=jlo;j<=jhi;j++) {
            for(int i=ilo;i<=ihi;i++) {
                cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
                int im1 = CCTK_GFINDEX3D(cctkGH,i-1,j, k);
                if(progress_mask[cc] == 0) {
                    if((rl+cctk_bbox[0])>0 && i == ilo) {
                        nbxx[cc] = bxx[cc];
                        ndxx[cc] = dxx[cc];
                        progress_mask[cc] = 1;
                        progress = 1;
                    } else if(progress_mask[im1] == 1) {
                        if(nbxx[im1] != 0) {
                            CCTK_REAL f = axx[cc]/nbxx[im1];
                            nbxx[cc] = bxx[cc]-f*cxx[im1];
                            ndxx[cc] = dxx[cc]-f*ndxx[im1];
                        }
                        progress_mask[cc] = 1;
                        progress = 1;
                    }
                }
            }
        }
    }

    // determine if we are done
    *trid_not_done = 0;
    for(int k=klo;!*trid_not_done && k<=khi;k++) {
        for(int j=jlo;!*trid_not_done && j<=jhi;j++) {
            for(int i=ilo;!*trid_not_done && i<=ihi;i++) {
                cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                if(progress_mask[cc] != 2) {
                    *trid_not_done = 1;
                }
            }
        }
    }

    // back substitution
    for(int k=klo;k<=khi;k++) {
        //#pragma omp parallel for
        for(int j=jlo;j<=jhi;j++) {
            for(int i=ihi;i>=ilo;i--) {
                cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
                int ip1 = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
                if(progress_mask[cc] == 1) {
                    if((rl+cctk_bbox[1])> 0 && i == ihi) {
                        rxx[cc] = ndxx[cc]/nbxx[cc];
                        progress_mask[cc] = 2;
                        progress = 1;
                    } else if(progress_mask[ip1] == 2) {
                        rxx[cc] = (ndxx[cc]-rxx[ip1]*cxx[cc])/nbxx[cc];
                        progress_mask[cc] = 2;
                        progress = 1;
                    }
                }
            }
        }
    }
    CCTK_INT operation_handle=0;
    int all_not_done = *trid_not_done;

    operation_handle = CCTK_ReductionArrayHandle ("maximum");
    std::cout << "operation_handle=" << operation_handle << std::endl;
    assert(operation_handle >= 0);
    int r1 = CCTK_ReduceLocalScalar (cctkGH, -1, operation_handle, trid_not_done, &all_not_done,
            CCTK_VARIABLE_INT);
    assert(r1 >= 0);
    *trid_not_done = all_not_done;
    int some_progress = 0;
    int r2 = CCTK_ReduceLocalScalar (cctkGH, -1, operation_handle, &progress,     &some_progress,
            CCTK_VARIABLE_INT);
    assert(r2 >= 0);
    if(*trid_not_done && !some_progress)
        assert("failed to make progress in tridiagonal x"==0);
}

// Generic tridiagonal solver
// b c . . .   x   d
// a b c . .   x   d
// . a b c . * x = d
// . . a b c   x   d
// . . . a b   x   d
extern "C"
void tridiagonal_x_impl(CCTK_POINTER_TO_CONST const cctkGH_,CCTK_REAL *_rxx,int ilo,int ihi,int jlo,int jhi,int klo,int khi) {
    const cGH *cctkGH = (const cGH *)cctkGH_;
    DECLARE_CCTK_ARGUMENTS;
    int cc;
    for(int k=klo;k<=khi;k++) {
        //#pragma omp parallel for
        for(int j=jlo;j<=jhi;j++) {

            cc = CCTK_GFINDEX3D(cctkGH,ilo,j,k);
            nbxx[cc] = bxx[cc];
            ndxx[cc] = dxx[cc];

            // forward elimination
            for(int i=ilo+1;i<=ihi;i++) {
                cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
                int im1 = CCTK_GFINDEX3D(cctkGH,i-1,j, k);
                CCTK_REAL f = axx[cc]/nbxx[im1];
                nbxx[cc] = bxx[cc]-f*cxx[im1];
                ndxx[cc] = dxx[cc]-f*ndxx[im1];
            }

            // back substitution
            cc = CCTK_GFINDEX3D(cctkGH,ihi,j,k);
            _rxx[cc] = ndxx[cc]/nbxx[cc];
            for(int i=ihi-1;i>=ilo;i--) {
                cc = CCTK_GFINDEX3D(cctkGH, i, j, k);
                int ip1 = CCTK_GFINDEX3D(cctkGH,i+1,j,k);
                _rxx[cc] = (ndxx[cc]-_rxx[ip1]*cxx[cc])/nbxx[cc];
            }
        }
    }
}

extern "C"
void copyout(CCTK_POINTER_TO_CONST const cctkGH_,CCTK_REAL *_rxx) {
    cGH *cctkGH = (cGH *)cctkGH_;
    DECLARE_CCTK_ARGUMENTS;
    int ilo = 0, jlo = 0, klo = 0;
    int ihi = cctk_lsh[0]-1;
    int jhi = cctk_lsh[1]-1;
    int khi = cctk_lsh[2]-1;
    for(int k=klo;k<=khi;k++) {
        for(int j=jlo;j<=jhi;j++) {
            for(int i=ilo;i<=ihi;i++) {
                int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                if(progress_mask[cc] == 2) {
                    _rxx[cc] = rxx[cc];
                } else {
                    _rxx[cc] = 0;
                }
            }
        }
    }
}

extern "C"
void copyin(CCTK_POINTER_TO_CONST cctkGH_,CCTK_POINTER rxx__) {
    cGH *cctkGH = (cGH *)cctkGH_;
    DECLARE_CCTK_ARGUMENTS;
    const CCTK_REAL *rxx_ = (const CCTK_REAL *)rxx__;
    int ilo = 0, jlo = 0, klo = 0;
    int ihi = cctk_lsh[0]-1;
    int jhi = cctk_lsh[1]-1;
    int khi = cctk_lsh[2]-1;
    for(int k=klo;k<=khi;k++) {
        for(int j=jlo;j<=jhi;j++) {
            for(int i=ilo;i<=ihi;i++) {
                int cc = CCTK_GFINDEX3D(cctkGH,i,j,k);
                rxx[cc] = rxx_[cc];
            }
        }
    }
}
