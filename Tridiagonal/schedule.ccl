# Schedule definitions for thorn Tridiagonal
storage: trid_scratch, trid_scratchi, trid_args, trid_scalars, trid_control_scalars

# Do tridiagonal solves in the X-direction
schedule group TridSolveX
{
} "all x solver bins"

schedule group TridSolveXSetup in TridSolveX
{
} "setup in x"

schedule group TridSolveXIter in TridSolveX after TridSolveXSetup
{
} "multi-phase calculation"

schedule tridiagonal_setup_x in TridSolveXSetup
{
	LANG: C
} "set things up"

schedule tridiagonal_x_parallel_impl in TridSolveX after tridiagonal_setup_x while Tridiagonal::trid_not_done
{
	LANG: C
	SYNC: trid_scratch, trid_scratchi, trid_args
} "forward elimination and backsubstitution"

# Do tridiagonal solves in the Y-direction

schedule group TridSolveY
{
} "all y solver bins"

schedule group TridSolveYSetup in TridSolveY
{
} "setup in y"

schedule group TridSolveYIter in TridSolveY after TridSolveYSetup
{
} "multi-phase calculation"

schedule tridiagonal_setup_y in TridSolveYSetup
{
	LANG: C
} "set things up"

schedule tridiagonal_y_parallel_impl after tridiagonal_setup_y in TridSolveY while Tridiagonal::trid_not_done
{
	LANG: C
	SYNC: trid_scratch, trid_scratchi, trid_args
} "forward elimination and backsubstitution"

if(test_solve) {
	storage: trid_test
	
	schedule tridiagonal_test_part1 at CCTK_INITIAL {
		LANG: C
	} "part1"
	
	schedule group TridSolveX at CCTK_INITIAL after tridiagonal_test_part1 {
	} "where the solve happens"
	
	schedule tridiagonal_test_part2 at CCTK_INITIAL after TridSolveX {
		LANG: C
	} "part2"
	
	schedule tridiagonal_test_part3 at CCTK_INITIAL after tridiagonal_test_part2 {
		LANG: C
	} "part3"
	
	schedule group TridSolveY at CCTK_INITIAL after tridiagonal_test_part3 {
	} "where the solve happens"
	
	schedule tridiagonal_test_part4 at CCTK_INITIAL after TridSolveY {
		LANG: C
	} "part4"
}
