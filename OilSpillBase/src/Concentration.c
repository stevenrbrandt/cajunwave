#include <stdio.h>
#include <stdlib.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

/*
 * Calculate the concentration of oil parcels
 * We count the total number of oil parcels in each cell
 * centered at each grid point and scale that number by the
 * total number of oil parcels. Other physical quantities can
 * be used to scale this number later.
 * */

void OilSpillBase_CalcConcentration (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  /* local array shape */
  CCTK_INT larrsh, i3D;
  CCTK_REAL *conc_arr;
  CCTK_REAL dx, dy, dz, xcoord, ycoord, zcoord;
  int i, j, k, l;

  /* construct a cell around a grid point
   * It is in the order of xmin, xmax, ymin, ymax, zmin, zmax.
   * */
  int cell_bnd[6];
  int arr_size = 1;

  dx = CCTK_DELTA_SPACE (0);
  dy = CCTK_DELTA_SPACE (1);
  dz = CCTK_DELTA_SPACE (2);

  CCTK_GrouplshGN (cctkGH, 1, &larrsh, "OilSpillBase::parcel_position");

  for (i = 0; i < cctkGH->cctk_dim; i++)
  {
    arr_size *= cctkGH->cctk_gsh[i];
  }

  printf ("total number of grid points is : %d\n", arr_size);

  if ((conc_arr =
       (CCTK_REAL *) malloc (arr_size * sizeof (CCTK_REAL))) == NULL)
  {
    CCTK_WARN (0, "failed to allocate memory for the concentration array !");
  }

  /* set concentration to zero */
  for (i = 0; i < arr_size; i++)
  {
    conc_arr[i] = 0.0;
  }

  for (k = 0; k < cctk_gsh[2]; k++)
  {
    for (j = 0; j < cctk_gsh[1]; j++)
    {
      for (i = 0; i < cctk_gsh[0]; i++)
      {
        /* calculate the global xyz coordinates of the (i,j,k) grid point */
        /* (in a multipatch/multiblock context, this gives the per-patch coordinates) */
        xcoord = CCTK_ORIGIN_SPACE (0) + i * dx;
        ycoord = CCTK_ORIGIN_SPACE (1) + j * dy;
        zcoord = CCTK_ORIGIN_SPACE (2) + k * dz;
        for (l = 0; l < larrsh; l++)
        {
          if (xp1[l] > (xcoord - dx * 0.5)
              && xp1[l] < (xcoord + dx * 0.5)
              && xp2[l] > (ycoord - dy * 0.5)
              && xp2[l] < (ycoord + dy * 0.5)
              && xp3[l] > (zcoord - dz * 0.5) && xp3[l] < (zcoord + dz * 0.5))
          {
            conc_arr[((i) +
                      cctkGH->cctk_gsh[0] * ((j) +
                                             cctkGH->cctk_gsh[1] * (k)))] +=
              1.0;
          }
        }
      }
    }
  }

  /* let's set the grid function to the proper value */
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        conc[i3D] = conc_arr[(cctk_lbnd[0] + i) +
                             cctkGH->cctk_gsh[0] * ((cctk_lbnd[1] + j) +
                                                    cctkGH->cctk_gsh[1] *
                                                    (cctk_lbnd[1] + k))];
      }

  for (k = 0; k < cctk_gsh[2]; k++)
  {
    for (j = 0; j < cctk_gsh[1]; j++)
    {
      for (i = 0; i < cctk_gsh[0]; i++)
      {
        printf
          ("i=%d, j=%d, k=%d, x=%lf, y=%lf, z=%lf, conc_arr[%d]=%lf\n",
           i, j, k, CCTK_ORIGIN_SPACE (0) + i * dx,
           CCTK_ORIGIN_SPACE (1) + j * dy, CCTK_ORIGIN_SPACE (2) + k * dz,
           ((i) +
            cctkGH->cctk_gsh[0] * ((j) +
                                   cctkGH->cctk_gsh[1] * (k))),
           conc_arr[((i) +
                     cctkGH->cctk_gsh[0] * ((j) +
                                            cctkGH->cctk_gsh[1] * (k)))]);
      }
    }
  }
  return;
}
