#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void OilSpillBase_InitPosition_None (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT lsh;                 /* local size */
  int i;

  CCTK_GrouplshGN (cctkGH, 1, &lsh, "OilSpillBase::parcel_position");

  for (i = 0; i < lsh; i++)
  {
    xp1[i] = parcel_init_x;
    xp2[i] = parcel_init_y;

    xp1_p[i] = parcel_init_x;
    xp2_p[i] = parcel_init_y;
  }
}

void OilSpillBase_InitVelocity_Zero (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        ua1[i3D] = 0.0;
        ua2[i3D] = 0.0;
      }
}
