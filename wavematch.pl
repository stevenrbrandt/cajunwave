#!/usr/bin/perl
use strict;
use POSIX;
use FileHandle;
my $fdi = new FileHandle;
my $fdw = new FileHandle;
my $fdc = new FileHandle;

#############
# edit below
#############

# grid
my $fake2d = 0;
my $scale = 1;
my $dx = 0.5/$scale;
my $dy = 0.5/$scale;
my $nx = 61*$scale; # number of grid points (not cells)
my $ny = 61*$scale; # number of grid points (not cells)
my ($ini_uvz,$u_file,$v_file,$z_file);
my $min_depth;
my $min_depth_frc;

my $spherical = 0;
my $lat_south = 0.0666;
my $lon_west = 0.0666;
my $wave_component_file = "/home/sbrandt/workspace/shi_funwave/example_2/fft/wavemk_per_amp_pha.txt";
my $num_wave_components = 1;
my $peak_period = 1;

# wind
my $wind_force = 0;
my $wind_mask = 0;
my @winds = (
    [0,25,50],
    [1000,100,100]
    );

# hump
my $wave_type = "ini_gau";
my $xc = 0;
my $xc_wk = 14;
my $dep_wk = .45;
my $amp_wk = 2;
my $theta_wk = 0;
my $delta_wk = 0.5;
my $ywidth_wk = 10000;
my $hmo = 1.0;
my $time_ramp = 1.0;
my $tperiod = 1;
my $yc = 17.5;
my $amp = 2;
my $dep = .78;
my $width = 6.0;
my $xwavemaker = 25.0;
my $gen_test_depth_data = "false";
my $sponge_decay = .9;
my $sponge_damping = 5.0;

# other
my $depth_type = "flat";
#my $depth_type = "data";
#my $depth_file = "/home/sbrandt/Desktop/depth_wkshop.txt"
my $depth_file = "/tmp/__depth__.txt";
my $veg_ht_file = undef;
my $veg_ht_matrix = "T";
my $shore_x = 80.0;
my $island_x = 40.0;
my $island_y = 40.0;
my $depth_flat = 0.8;
my $itlast = 3*$scale;
my $fitlast = $itlast+1;
my $nghost = 3; # Cactus only
my $dt = 0.00/$scale;
my $dispersion = 1;
my $enable_masks = 1;
my $cd = .00;
my $froude_cap=10.0;

my $sponge_on = 0;
my $sponge_west_width = 2;
my $sponge_east_width = 2;
my $sponge_north_width = 2;
my $sponge_south_width = 2;
my $veg_cd = 0;
my $veg_n = 500;
my $veg_dia = 0.01;

#############
# edit above
#############

my $ifile = undef;
my $from_cactus = 0;
if($#ARGV==0) {
  $ifile = $ARGV[0];
  $from_cactus = 1 if($ifile =~ /\.par$/);
}
my %thorn = (
  "DX"=>"CoordBase",
  "DY"=>"CoordBase",
  "xmin"=>"CoordBase",
  "ymin"=>"CoordBase",
  "xmax"=>"CoordBase",
  "ymax"=>"CoordBase",
  "itlast"=>"Cactus",
);
my %thorn_kw = (
  "WAVEMAKER" => "wavemaker_type",
  "itlast" => "cctk_itlast",
  "DISPERSION" => "dispersion_on",
  "R_sponge" => "sponge_decay_rate",
  "A_sponge" => "sponge_damping_magnitude",
  "CdVeg" => "veg_cd",
  "NVeg" => "veg_N",
  "VegDia" => "veg_bv",
  "Cd" => "cd",
  "hVeg_file" => "vegetation_height_file",
  "FixDT" => "dt_size",
);

sub t_or_f {
  my $arg = shift;
  return 1 if($arg =~ /^(t(rue|)|y(es|)|o(n|)|1)$/i);
  return 0 if($arg =~ /^(f(alse|)|n(o|)|o(ff|)|0)$/i);
  die "bad t_or_f arg '$arg'";
}

sub getpar {
  my $c = shift;
  my $kw = shift;
  my $thorn = "";
  if($from_cactus) {
    $thorn = "FunWave::";
    $thorn = $thorn{$kw}."::" if(defined($thorn{$kw}));
    $kw = $thorn_kw{$kw} if(defined($thorn_kw{$kw}));
  }
  if($c =~ /^$thorn$kw\s*=\s*(\S+)/mi) {
    my $res = $1;#"\L$1";
    $res =~ s/\.0+$//;
    print "    $kw = $res\n";
    $res =~ s/^"//;
    $res =~ s/"$//;
    return $res;
  } else {
    die "$kw = ?";
  }
}

sub getpard {
  my $c = shift;
  my $kw = shift;
  my $def = shift;
  eval {
    return getpar($c,$kw);
  };
  print "    $kw = $def\n";
  return $def;
}

sub getpari {
  my $res = getpar(@_);
  return "\L$res";
}

if(defined($ifile) and open($fdi,$ifile)) {
  print "OVERRIDING SETTINGS USING $ifile\n";
  local $/ = undef;
  my $contents = <$fdi>;
  # Strip comments
  if($from_cactus) {
    $contents =~ s/#.*//g;
  } else {
    $contents =~ s/!.*//g;
  }
  $spherical = t_or_f(getpar($contents,"SPHERICAL","F"));

  if($from_cactus) {
    $dx = getpar($contents,"DX");
    $dx = getpar($contents,"DY");
    my $xmin = getpar($contents,"xmin");
    my $xmax = getpar($contents,"xmax");
    my $ymin = getpar($contents,"ymin");
    my $ymax = getpar($contents,"ymax");
    $nx = ($xmax-$xmin)/$dx+1;
    $ny = ($ymax-$ymin)/$dy+1;
  } else {
    if($spherical) {
      $dx = getpar($contents,"Dphi");
    } else {
      $dx = getpar($contents,"DX");
    }

    if($spherical) {
      $dy = getpar($contents,"Dtheta");
    } else  {
      $dy = getpar($contents,"DY");
    }

    $ny = getpar($contents,"Nglob");
    $nx = getpar($contents,"Mglob");
  }

  my $dispt = t_or_f(getpar($contents,"DISPERSION","F"));

  $dt = getpard($contents,"FixDT",0);

  $wave_type = getpari($contents,"WAVEMAKER");

  $sponge_west_width = getpar($contents,"Sponge_west_width"); 
  $sponge_east_width = getpar($contents,"Sponge_east_width"); 
  $sponge_north_width = getpar($contents,"Sponge_north_width"); 
  $sponge_south_width = getpar($contents,"Sponge_south_width"); 

  if($wave_type eq "wk_reg") {
    $xc_wk     = getpar($contents,"Xc_WK");
    $amp_wk    = getpar($contents,"AMP_WK");
    $dep_wk    = getpar($contents,"DEP_WK");
    $theta_wk  = getpar($contents,"Theta_WK");
    $delta_wk  = getpar($contents,"Delta_WK");
    $ywidth_wk  = getpar($contents,"Ywidth_WK");
    $time_ramp = getpar($contents,"Time_ramp");
    $tperiod   = getpar($contents,"Tperiod");
  } elsif($wave_type eq "wk_irr") {
    $xc_wk     = getpar($contents,"Xc_WK");
    $amp_wk    = getpar($contents,"AMP_WK");
    $dep_wk    = getpar($contents,"DEP_WK");
    $hmo       = getpar($contents,"Hmo");
    $theta_wk  = getpar($contents,"Theta_WK");
    $delta_wk  = getpar($contents,"Delta_WK");
    $ywidth_wk  = getpar($contents,"Ywidth_WK");
    $time_ramp = getpar($contents,"Time_ramp");
    $tperiod   = getpar($contents,"Tperiod");
  } else {
    $xc  = getpar($contents,"Xc");
    $yc  = getpar($contents,"Yc");
    $amp = getpar($contents,"AMP");
    $width = getpar($contents,"WID");
  }
  $u_file = getpar($contents,"U_FILE","/dev/null");
  $v_file = getpar($contents,"V_FILE","/dev/null");
  $z_file = getpar($contents,"ETA_FILE","/dev/null");
  $sponge_on = t_or_f(getpar($contents,"SPONGE_ON"));

  $cd = getpar($contents,"Cd");
  $veg_cd = getpard($contents,"CdVeg",0);
  $veg_n = getpard($contents,"NVeg",500);
  $veg_dia = getpard($contents,"VegDia",.01);
  $veg_ht_file = getpard($contents,"hVeg_file","NONE");
  $veg_ht_matrix = "F" if($veg_ht_file eq "NONE");

  $depth_type = getpari($contents,"DEPTH_TYPE");
  $depth_file = getpar($contents,"DEPTH_FILE");
  $depth_flat = getpar($contents,"DEPTH_FLAT");
  $sponge_damping = getpar($contents,"A_sponge");
  $sponge_decay = getpar($contents,"R_sponge");
  $ini_uvz = t_or_f(getpar($contents,"INI_UVZ","F"));
  $itlast = getpar($contents,"itlast");
  $fitlast=$itlast+1;

  $lon_west = ff(getpar($contents,"Lon_West",0.0666));
  $lat_south = ff(getpar($contents,"Lat_South",0.0666));
  $froude_cap = ff(getpar($contents,"FroudeCap",666));
  $min_depth = ff(getpar($contents,"MinDepth",.01));
  $min_depth_frc = ff(getpar($contents,"MinDepthFrc",0.001));

  print "END CONFIG\n";
} 

####

my ($fw_wave_type,$cac_wave_type);
if($wave_type eq "ini_gau") {
    $fw_wave_type = "INI_GAU";
    $cac_wave_type = "ini_gau";
} elsif($wave_type eq "ini_sol") {
    $fw_wave_type = "INI_SOL";
    $cac_wave_type = "ini_sol";
} elsif($wave_type eq "ini_irr") {
    $fw_wave_type = "INI_IRR";
    $cac_wave_type = "wk_irr";
} elsif($wave_type eq "wk_time") {
    $fw_wave_type = "WK_TIME";
    $cac_wave_type = "wk_time_series";
} elsif($wave_type eq "wk_irr") {
    $fw_wave_type = "WK_IRR";
    $cac_wave_type = "wk_irr";
} elsif($wave_type eq "wk_reg") {
    $fw_wave_type = "WK_REG";
    $cac_wave_type = "wk_reg";
} elsif($ini_uvz) {
    $cac_wave_type = "INI_UVZ";
} else {
    die "Invalid wave type '$wave_type'";
}
my ($fw_depth_type,$ca_depth_type);
if($depth_type eq "data") {
    $fw_depth_type = "DATA";
    $ca_depth_type = "data";
} elsif($depth_type eq "flat") {
    $fw_depth_type = "FLAT";
    $ca_depth_type = "flat";
}
my $cnz = 2*$nghost+1;
my ($fw_ini_uvz,$ca_ini_uvz) = boolf($ini_uvz);
my ($sponge_shi,$sponge_cac) = boolf($sponge_on);
my ($swf,$cwf) = boolf($wind_force);
my ($swm,$cwm) = boolf($wind_mask);
my ($fw_spherical,$cac_spherical) = boolf($spherical);
my ($disp_shi,$disp_cactus) = boolf($dispersion);
my ($masks_shi,$masks_cactus) = boolf($enable_masks);
my $cmaxx = ($nx-1)*$dx;
my $cminx = 0;
my $cmaxy = ($ny-1)*$dy;
my $cminy = 0;
my $nw = $#winds+1;
my $cmaxz =  $dx;
my $cminz = -$dx;
my $dtf = ff($dt);
my $depth_flatf = ff($depth_flat);
my $widthf = ff($width);
my $ampf = ff($amp);
my $amp_wkf = ff($amp_wk);
my $theta_wkf = ff($theta_wk);
my $delta_wkf = ff($delta_wk);
my $ywidth_wkf = ff($ywidth_wk);
my $time_rampf = ff($time_ramp);
my $dep_wkf = ff($dep_wk);
my $hmof = ff($hmo);
my $tperiodf = ff($tperiod);
my $depf = ff($dep);
my $xwavemakerf = ff($xwavemaker);
my $xcf = ff($xc);
my $ycf = ff($yc);
my $xc_wkf = ff($xc_wk);
my $cdf = ff($cd);
my $veg_cdf = ff($veg_cd);
my $veg_nf = ff($veg_n);
my $veg_diaf = ff($veg_dia);
my $froude_capf = ff($froude_cap);
my $peak_period_f = ff($peak_period);

my $sponge_west_widthf = ff($sponge_west_width);
my $sponge_east_widthf = ff($sponge_east_width);
my $sponge_north_widthf = ff($sponge_north_width);
my $sponge_south_widthf = ff($sponge_south_width);
my $sponge_dampingf = ff($sponge_damping);
my $sponge_decayf = ff($sponge_decay);

my $boundary_z = 0;
my $ghost_z = 1;
my $zmax = 0;
my $zmin = 0;
if($fake2d) {
  $boundary_z = 3;
  $ghost_z = 3;
  $zmin = -$dx;
  $zmax = $dx;
}

sub boolf {
    my $b = shift;
    if($b) {
        return ("T","true");
    } else {
        return ("F","false");
    }
}

sub ff {
    my $val = "".shift;
    $val .= ".0" if($val !~ /\./);
    return $val;
}

openf(\$fdi,">input.txt");
openf(\$fdw,">wind.txt");
openf(\$fdc,">funw-carpet.par");

print $fdc qq{
#Reorder the parameters for easy comparison to the input.txt in example 3
ActiveThorns = "CoordBase FunWave FunwaveCoord CartGrid3D Carpet CarpetIOASCII CartGrid3D IOUtil CarpetIOBasic CarpetSlab Boundary SymBase MoL CarpetReduce LocalReduce InitBase CarpetLib LoopControl Tridiagonal"

#----------------------------------------------------
# Flesh and CCTK parameters
#----------------------------------------------------

# flesh
Cactus::cctk_run_title = "Test Run"
Cactus::cctk_show_schedule = "yes"
Cactus::cctk_itlast = $itlast
Cactus::allow_mixeddim_gfs = "yes"

# CartGrid3D
CartGrid3D::type = "coordbase"
CartGrid3D::avoid_origin = "no"
CoordBase::domainsize = "minmax"
CoordBase::spacing    = "gridspacing"
CoordBase::xmin =  $cminx
CoordBase::xmax =  $cmaxx
CoordBase::ymin =  $cminy
CoordBase::ymax =  $cmaxy
CoordBase::zmin =  $zmin
CoordBase::zmax =  $zmax
CoordBase::dx   =  $dx
CoordBase::dy   =  $dy

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_lower     = $boundary_z
CoordBase::boundary_size_z_upper     = $boundary_z
CoordBase::boundary_shiftout_x_lower = 1
CoordBase::boundary_shiftout_x_upper = 1
CoordBase::boundary_shiftout_y_lower = 1
CoordBase::boundary_shiftout_y_upper = 1
CoordBase::boundary_shiftout_z_lower = 1
CoordBase::boundary_shiftout_z_upper = 1

# Carpet
Carpet::domain_from_coordbase = "yes"
Carpet::ghost_size_x = $nghost
Carpet::ghost_size_y = $nghost
Carpet::ghost_size_z = $ghost_z
carpet::adaptive_stepsize = yes

# MoL
MoL::ODE_Method = "RK3"
MoL::disable_prolongation        = "yes"

# the output dir will be named after the parameter file name
IO::out_dir = \$parfile
IO::out_fileinfo="none"
IOBasic::outInfo_every = 1
IOBasic::outInfo_vars = "FunWave::eta FunWave::u FunWave::v"

#IOASCII::out1D_every = 1
#IOASCII::out1d_vars = "FunWave::eta Funwave::depth"
IOASCII::out2D_every = 1
IOASCII::out2D_xyplane_z = 0
IOASCII::out2D_vars = "FunWave::eta FunWave::u FunWave::v"
IOASCII::out2D_xz = "no"
IOASCII::out2D_yz = "no"
IOASCII::output_ghost_points = "no"

#Cactus::debug_watch = "Funwave::eta"

#----------------------------------------------------
# Funwave parameters
#----------------------------------------------------

# Funwave depth 
FunWave::depth_file_offset_x = 3
FunWave::depth_file_offset_y = 3
FunWave::depth_type = "$ca_depth_type"
FunWave::depth_format = "ele"
FunWave::depth_file = "$depth_file"
FunWave::depth_flat = $depth_flat
#Funwave::test_depth_shore_x = $shore_x
#Funwave::test_depth_island_x = $island_x
#Funwave::test_depth_island_y = $island_y
FunWave::depth_xslp = 10.0
FunWave::depth_slope = 0.05
FunWave::dt_size = $dt
Funwave::generate_test_depth_data = $gen_test_depth_data
Funwave::num_wave_components = $num_wave_components
Funwave::wave_component_file = "$wave_component_file"
Funwave::peak_period = $peak_period
Funwave::veg_cd = $veg_cd
Funwave::veg_bv = $veg_dia
Funwave::veg_N = $veg_n
Funwave::vegetation_height_file = "$veg_ht_file"
Funwave::first_order_veg = true

Funwave::u_file = "$u_file"
Funwave::v_file = "$v_file"
Funwave::z_file = "$z_file"

# import
Funwave::time_ramp = $time_ramp
Funwave::delta_wk = $delta_wk
Funwave::dep_wk = $dep_wk
Funwave::xc_wk = $xc_wk
Funwave::ywidth_wk = $ywidth_wk
Funwave::tperiod = $tperiod
Funwave::amp_wk = $amp_wk
Funwave::theta_wk = $theta_wk
Funwave::freqpeak = 0.2
Funwave::freqmin = 0.1
Funwave::freqmax = 0.4
Funwave::hmo = $hmo
Funwave::gammatma = 5.0
Funwave::thetapeak = 10.0
Funwave::sigma_theta = 15.0

# Funwave wind forcing
Funwave::wind_force = $cwf
Funwave::use_wind_mask = $cwm
Funwave::num_time_wind_data = $nw},"\n";
for(my $i=0;$i<=$#winds;$i++) {
    my @w = @{$winds[$i]};
    print $fdc "Funwave::timewind[$i] = $w[0]\n";
    print $fdc "Funwave::wu[$i] = $w[1]\n";
    print $fdc "Funwave::wv[$i] = $w[2]\n";
}

print $fdc qq{Funwave::boundary = funwave

# Funwave wave maker
FunWave::wavemaker_type = "$cac_wave_type"
FunWave::xc = $xc
FunWave::yc = $yc
FunWave::amp =  $ampf
FunWave::wid =  $width
Funwave::wdep = $depf
Funwave::xwavemaker = $xwavemakerf

# Funwave sponge 
FunWave::sponge_on = $sponge_cac
FunWave::sponge_west_width = $sponge_west_width
FunWave::sponge_east_width = $sponge_east_width
FunWave::sponge_north_width = $sponge_north_width
FunWave::sponge_south_width = $sponge_south_width
FunWave::sponge_decay_rate = $sponge_decay
FunWave::sponge_damping_magnitude = $sponge_damping

# Funwave dispersion (example 3 enables dispersion)
FunWave::dispersion_on = "$disp_cactus"
FunWave::gamma1 = 1.0
FunWave::gamma2 = 1.0
FunWave::gamma3 = 1.0
FunWave::beta_ref = -0.531
FunWave::swe_eta_dep = 0.80
FunWave::cd = $cdf

# Funwave numerics (MoL parameter controls time integration scheme)
FunWave::reconstruction_scheme = "fourth"
FunWave::riemann_solver = "HLLC"
FunWave::dtfac = 0.5
FunWave::froudecap = $froude_capf
FunWave::mindepth = $min_depth
FunWave::mindepthfrc = $min_depth_frc
FunWave::enable_masks = "$masks_cactus"
Funwave::estimate_dt_on = "true"

FunwaveCoord::earth_a = 6371000.0
FunwaveCoord::earth_b = 6371000.0

FunwaveCoord::lon_west  = $lon_west
FunwaveCoord::lat_south = $lat_south
FunwaveCoord::spherical_coordinates = $cac_spherical

IOASCII::out1D_every          = 10
IOASCII::out1D_x              = "yes"
IOASCII::out1D_xline_y        = 0
IOASCII::out1D_xline_z        = 0
IOASCII::out1D_y              = "no"
IOASCII::out1D_z              = "no"
IOASCII::out1D_d              = "no"
IOASCII::out1D_vars           = "FunWave::eta"

ActiveThorns = "CarpetIOHDF5"
IOHDF5::out2D_xyplane_z = 0 
IOHDF5::out2D_every = 10
IOHDF5::out2D_vars = " 
  FunWave::eta
  FunWave::u
  FunWave::v
"
IOHDF5::out2D_xz = no
IOHDF5::out2D_yz = no
},"\n";

print $fdw qq{wind data
$nw  - number of data},"\n";
for(my $i=0;$i<=$#winds;$i++) {
    my @w = @{$winds[$i]};
    print $fdw "$w[0] $w[1] $w[2]   time(s), wu, wv (m/s)\n";
}
print $fdw "end of file\n";
close($fdw);

print $fdi qq{
!INPUT FILE FOR BOUSS_TVD
  ! NOTE: all input parameter are capital sensitive
  ! --------------------TITLE-------------------------------------
  ! title only for log file
TITLE = TEST RUN
  ! -------------------HOT START---------------------------------
HOT_START = F
FileNumber_HOTSTART = 1
SPHERICAL_ON = $fw_spherical
  ! -------------------PARALLEL INFO-----------------------------
  ! 
  !    PX,PY - processor numbers in X and Y
  !    NOTE: make sure consistency with mpirun -np n (px*py)
  !    
PX = 1
PY = 1
  ! --------------------DEPTH-------------------------------------
  ! Depth types, DEPTH_TYPE=DATA: from depth file
  !              DEPTH_TYPE=FLAT: idealized flat, need depth_flat
  !              DEPTH_TYPE=SLOPE: idealized slope, 
  !                                 need slope,SLP starting point, Xslp
  !                                 and depth_flat
DEPTH_TYPE = $fw_depth_type
  ! Depth file
  ! depth format NOD: depth at node (M1xN1), ELE: depth at ele (MxN) 
  ! where (M1,N1)=(M+1,N+1)  
DEPTH_FILE = $depth_file
DepthFormat = ELE
  ! if depth is flat and slope, specify flat_depth
DEPTH_FLAT = $depth_flatf
  if depth is slope, specify slope and starting point
SLP = 0.05
Xslp = 10.0

  ! -------------------PRINT---------------------------------
  ! PRINT*,
  ! result folder
RESULT_FOLDER = ../results/

  ! ------------------DIMENSION-----------------------------
  ! global grid dimension
Mglob = $nx
Nglob = $ny

  ! ----------------- TIME----------------------------------
  ! time: total computational time/ plot time / screen interval 
  ! all in seconds
itlast = $fitlast
TOTAL_TIME = 120000000.0
PLOT_INTV = 0.000001
PLOT_INTV_STATION = 0.02
SCREEN_INTV = 0.1
HOTSTART_INTV = 360000000000.0

  ! -----------------GRID----------------------------------
  ! if use spherical grid, in decimal degrees
StretchGrid = F
Lon_West = $lon_west
Lat_South = $lat_south
Dphi = $dx
Dtheta = $dy 
!DX_FILE = ../input/dx_str.txt
!DY_FILE = ../input/dy_str.txt
CORIOLIS_FILE = ../input/cori_str.txt
  ! cartesian grid sizes
DX = $dx
DY = $dy
  ! --------------- INITIAL UVZ ---------------------------
  ! INI_UVZ - initial UVZ e.g., initial deformation
  !         must provide three (3) files 
INI_UVZ = $fw_ini_uvz
  ! if true, input eta u and v file names
ETA_FILE = $z_file
U_FILE = $u_file
V_FILE = $v_file
  ! ----------------WAVEMAKER------------------------------
  !  wave makeer
  ! LEF_SOL- left boundary solitary, need AMP,DEP, LAGTIME
  ! INI_SOL- initial solitary wave, WKN B solution, 
  ! need AMP, DEP, XWAVEMAKER 
  ! INI_REC - rectangular hump, need to specify Xc,Yc and WID
  ! WK_REG - Wei and Kirby 1999 internal wave maker, Xc_WK,Tperiod
  !          AMP_WK,DEP_WK,Theta_WK, Time_ramp (factor of period)
  ! WK_IRR - Wei and Kirby 1999 TMA spectrum wavemaker, Xc_WK,
  !          DEP_WK,Time_ramp, Delta_WK, FreqPeak, FreqMin,FreqMax,
  !          Hmo,GammaTMA,ThetaPeak
  ! WK_TIME_SERIES - fft time series to get each wave component
  !                 and then use Wei and Kirby 1999 
  !          need input WaveCompFile (including 3 columns: per,amp,pha)
  !          NumWaveComp,PeakPeriod,DEP_WK,Xc_WK,Ywidth_WK
WAVEMAKER = $fw_wave_type
  ! wave components based on fft time series
NumWaveComp = $num_wave_components
PeakPeriod = $peak_period_f
WaveCompFile = $wave_component_file
  ! solitary wave
AMP = $ampf
DEP = $depf
LAGTIME = 5.0
XWAVEMAKER = $xwavemakerf
  ! Xc, Yc and WID (degrees) are for rectangular hump with AMP
Xc = $xcf
Yc = $ycf
WID = $widthf
  ! Wei and Kirby 1999
Time_ramp = $time_rampf
Delta_WK = $delta_wkf    ! width parameter 0.3-0.6
DEP_WK = $dep_wkf
Xc_WK = $xc_wkf
Ywidth_WK = $ywidth_wkf
  ! Wei and Kirby regular wave
Tperiod = $tperiodf
AMP_WK = $amp_wkf
Theta_WK = $theta_wkf
  ! Wei and Kirby irregular wave
FreqPeak = 0.2
FreqMin = 0.1
FreqMax = 0.4
Hmo = $hmof
GammaTMA = 5.0
ThetaPeak = 10.0
Sigma_Theta = 15.0
  ! ---------------- PERIODIC BOUNDARY CONDITION ---------
  ! South-North periodic boundary condition
  !
PERIODIC = F
  ! ---------------- SPONGE LAYER ------------------------
  ! DHI type sponge layer
  ! need to specify widths of four boundaries and parameters
  ! set width=0.0 if no sponge
  ! R_sponge: decay rate
  ! A_sponge: maximum decay rate
  ! e.g., sharp: R=0.85
  !       mild:  R=0.90, A=5.0
  !       very mild, R=0.95, A=5.0
SPONGE_ON = $sponge_shi
Sponge_west_width =  $sponge_west_widthf
Sponge_east_width =  $sponge_east_widthf
Sponge_south_width = $sponge_north_widthf
Sponge_north_width = $sponge_south_widthf
R_sponge = $sponge_decayf
A_sponge = $sponge_dampingf
  ! ----------------OBSTACLES-----------------------------
  ! obstacle structures using mask_struc file
  ! mask_struc =0 means structure element
  ! give a file contains a mask array with Mloc X Nloc
!OBSTACLE_FILE= struc2m4m.txt

  ! ----------------PHYSICS------------------------------
  ! parameters to control type of equations
  ! dispersion: all dispersive terms
  ! gamma1=1.0,gamma2=0.0: NG's equations
  ! gamma1=1.0,gamma2=1.0: Fully nonlinear equations
DISPERSION = $disp_shi
Gamma1 = 1.0
Gamma2 = 1.0
Gamma3 = 1.0
Beta_ref=-0.531
SWE_ETA_DEP = 0.80
  !----------------Friction-----------------------------
Friction_Matrix= F
Cd_file= btrad.txt
Cd = $cdf
  !----------------Veg Drag-----------------------------
Veg_Drag_Matrix= F
CdVeg_file= ../input/CdVeg.txt
CdVeg = $veg_cdf
  !----------------Veg Height-----------------------------
Veg_Height_Matrix= $veg_ht_matrix
hVeg_file= $veg_ht_file
hVeg = 0.6
  !----------------Veg Density-----------------------------
Veg_Density_Matrix= F
NVeg_file= ../input/vegDensity.txt
NVeg = $veg_nf
  !----------------Veg Diameter-----------------------------
Veg_Diameter_Matrix= F
VegDia_file= ../input/VegDia.txt
VegDia = $veg_diaf

  ! ----------------NUMERICS----------------------------
  ! time scheme: runge_kutta for all types of equations
  !              predictor-corrector for NSWE
  ! space scheme: second-order
  !               fourth-order
  ! construction: HLLC
  ! cfl condition: CFL
  ! froude number cap: FroudeCap

Time_Scheme = Runge_Kutta
!Time_Scheme = Predictor_Corrector
  ! spacial differencing
HIGH_ORDER = FOURTH
!HIGH_ORDER = THIRD
CONSTRUCTION = HLLC
  ! CFL
CFL = 0.5
  ! Froude Number Cap (to avoid jumping drop, set 10)
FroudeCap = $froude_capf

  ! --------------WET-DRY-------------------------------
  ! MinDepth for wetting-drying
MinDepth=$min_depth
  ! -----------------
  ! MinDepthfrc to limit bottom friction
MinDepthFrc = $min_depth_frc

  ! -------------- SHOW BREAKING -----------------------
  ! breaking is calculated using shock wave capturing scheme
  ! the criteria is only for demonstration or bubble calculation
  ! Cbrk1=0.65,Cbrk2=0.35, for irregular waves, there are much small!
SHOW_BREAKING = F
Cbrk1 = 0.1
Cbrk2 = 0.075
  ! ----------------- MIXING ---------------------------
  ! if use smagorinsky mixing, have to set -DMIXING in Makefile
  ! and set averaging time interval, T_INTV_mean, default: 20s
T_INTV_mean = 25.0
C_smg = 0.25
  ! ----------------- COUPLING -------------------------
  ! if do coupling, have to set -DCOUPLING in Makefile
COUPLING_FILE = coupling.txt
  ! -----------------OUTPUT-----------------------------
  ! stations 
  ! if NumberStations>0, need input i,j in STATION_FILE
NumberStations = 0
STATIONS_FILE = nothing
  ! output variables, T=.TRUE, F = .FALSE.
DEPTH_OUT = F
U = T
Ubar = T
V = T
Vbar = T
ETA = T
Hmax = F
Umean = F
Vmean = F
ETAmean = F
MASK = F
MASK9 = F
SXL = F
SXR = F
SYL = F
SYR = F
SourceX = T
SourceY = T
P = F
Q = F
Fx = F
Fy = F
Gx = F
Gy = F
AGE = F
TMP = F
WindForce = $swf
UseWindMask = $swm
FixDT=$dtf
Cdw = 0.0026
WindCrestPercent = .10
EnableMasks = $masks_shi
WIND_FILE = wind.txt},"\n";
close($fdi);

sub openf {
    my $fd = shift;
    my $file = shift;
    my $real_file = $file;
    if($file =~ /^>/) {
      $real_file = $';
      if(-r $real_file) {
        warn "The file '$real_file' already exists. Please move it out of the way.";
        open($$fd,"/dev/null");
        return;
      }
    }
    print "opening '$real_file'\n";
    open($$fd,$file) or die "error opening $file";
}
