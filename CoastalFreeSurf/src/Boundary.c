#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void FreeSurf_Boundary (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  if (CCTK_EQUALS (boundary, "flat"))
  {
    CCTK_INFO ("select flat boundary condition");
    Boundary_SelectGroupForBC (cctkGH, CCTK_ALL_FACES, stencil_size,
                               -1, "freesurf::hydro", "Flat");
  }
  else if (CCTK_EQUALS (boundary, "static"))
  {
    CCTK_INFO ("select static boundary condition");
    Boundary_SelectGroupForBC (cctkGH, CCTK_ALL_FACES, stencil_size,
                               -1, "freesurf::hydro", "Static");
  }
  else if (CCTK_EQUALS (boundary, "None"))
  {
    CCTK_INFO ("select none boundary condition");
    Boundary_SelectGroupForBC (cctkGH, CCTK_ALL_FACES, stencil_size,
                               -1, "freesurf::hydro", "None");
  }
  else
  {
    CCTK_WARN (0, "specified boundary condition is not supported !");
  }
  return;
}
