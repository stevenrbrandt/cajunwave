#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#define abs(a) (((a) > 0 )? (a):(-a))

//#define GAVINDEBUG
/* prepare for the calculation of the RHS */

void FreeSurf_FluxX (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int istart, iend, jstart, jend, kstart, kend;
  int i, j, k, l, m, n, i3D, i3Dr;
  CCTK_REAL fluxes[5][2], dflux[5];
  CCTK_REAL prims[5][2], avgs[5];
  CCTK_REAL lamdas[5];
  CCTK_REAL kapa, speedc;
  CCTK_REAL eigenvecs[5][5], inveigenvecs[5][5], dmatrix[5][5];

  istart = stencil_size;
  jstart = stencil_size;
  kstart = stencil_size;

  /* stencil size should be smaller than ghost zone size */
  iend = cctk_lsh[0] - stencil_size;
  jend = cctk_lsh[1] - stencil_size;
  kend = cctk_lsh[2] - stencil_size;

/* main loop */
#pragma omp parallel for
  for (k = istart; k < iend; k++)
    for (j = istart; j < iend; j++)
      for (i = istart; i < iend; i++)
      {

        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);

/* rho -> 0; u1 -> 1; u2 -> 2; u3 -> 3; press -> 4*/

        prims[0][0] = rho_l[i3D];
        prims[0][1] = rho_r[i3D];

        prims[1][0] = u1_l[i3D];
        prims[1][1] = u1_r[i3D];

        prims[2][0] = u2_l[i3D];
        prims[2][1] = u2_r[i3D];

        prims[3][0] = u3_l[i3D];
        prims[3][1] = u3_r[i3D];

        prims[4][0] = press_l[i3D];
        prims[4][1] = press_r[i3D];

        /* make sure density and pressure are always positive */
        for (l = 0; l < 2; l++)
        {
          if (prims[1][l] <= 0.0)
          {
            prims[i][l] = min_density;
          }
          if (prims[4][l] <= 0.0)
          {
            prims[4][l] = min_pressure;
          }
        }

        for (l = 0; l < 5; l++)
        {
          if (i == 15 && j == 15 && k == 15)
            printf ("prims[%d][0] = %lf, prims[%d][1] = %lf\n",
                    l, prims[l][0], l, prims[l][1]);
        }


        /* calculate both left and right fluxes */
        for (l = 0; l < 2; l++)
        {
          fluxes[0][l] = prims[0][l] * prims[1][l];

          fluxes[1][l] = prims[0][l] * prims[1][l] * prims[1][l]
            + prims[4][l] * compress_param;

          fluxes[2][l] = prims[0][l] * prims[1][l] * prims[2][l];

          fluxes[3][l] = prims[0][l] * prims[1][l] * prims[3][l];

          fluxes[4][l] = prims[1][l];
        }

        /* calculate Roe average */

        kapa = sqrt (prims[0][1] / prims[0][0]);

        avgs[0] = kapa * prims[0][0];
        avgs[1] = (prims[1][0] + kapa * prims[1][1]) / (1 + kapa);
        avgs[2] = (prims[2][0] + kapa * prims[2][1]) / (1 + kapa);
        avgs[3] = (prims[3][0] + kapa * prims[3][1]) / (1 + kapa);
        avgs[4] = (prims[4][0] + prims[4][1]) * 0.5;

        /* calculate the D matrix */
        /* 1. set the D matrix, eigenvec, inverse eigenvec, and D flux to zero */
        for (l = 0; l < 5; l++)
        {
          dflux[l] = 0.0;
          for (m = 0; m < 5; m++)
          {
            dmatrix[l][m] = 0.0;
            eigenvecs[l][m] = 0.0;
            inveigenvecs[l][m] = 0.0;
          }
        }

        /* 2. set the eigenvalues (will take abs and one half a bit later) */
        speedc = sqrt (avgs[1] * avgs[1] + 4.0 * compress_param / avgs[0]);

        lamdas[0] = avgs[1];
        lamdas[1] = lamdas[0];
        lamdas[2] = lamdas[0];
        lamdas[3] = avgs[1] - speedc;
        lamdas[4] = avgs[1] + speedc;

        /* 3. set the eignevectors T */

        eigenvecs[0][2] = 1.0 / avgs[1];
        eigenvecs[0][3] = (avgs[0] * lamdas[3]) / lamdas[4];
        eigenvecs[0][4] = (avgs[0] * lamdas[4]) / lamdas[3];

        eigenvecs[1][2] = 1.0;
        eigenvecs[1][3] = (avgs[0] * avgs[1] * lamdas[3]
                           - 2 * compress_param) / lamdas[4];
        eigenvecs[1][4] = (avgs[0] * avgs[1] * lamdas[4]
                           + 2 * compress_param) / lamdas[3];
        eigenvecs[2][1] = 1.0;
        eigenvecs[2][3] = (avgs[0] * avgs[2] * lamdas[3]) / lamdas[4];
        eigenvecs[2][4] = (avgs[0] * avgs[2] * lamdas[4]) / lamdas[3];

        eigenvecs[3][0] = 1.0;
        eigenvecs[3][3] = (avgs[0] * avgs[3] * lamdas[3]) / lamdas[4];
        eigenvecs[3][4] = (avgs[0] * avgs[3] * lamdas[4]) / lamdas[3];

        eigenvecs[4][3] = 1.0;
        eigenvecs[4][4] = 1.0;

        /* 4. set the inverse eignevectors T^(-1) */

        inveigenvecs[0][0] =
          (avgs[0] * pow (avgs[1], 2.0) * avgs[3]) / compress_param;
        inveigenvecs[0][1] = -(avgs[0] * avgs[1] * avgs[3]) / compress_param;
        inveigenvecs[0][3] = 1.0;
        inveigenvecs[0][4] = -avgs[0] * avgs[3];

        inveigenvecs[1][0] =
          (avgs[0] * pow (avgs[1], 2.0) * avgs[2]) / compress_param;
        inveigenvecs[1][1] = -(avgs[0] * avgs[1] * avgs[2]) / compress_param;
        inveigenvecs[1][2] = 1.0;
        inveigenvecs[1][4] = -avgs[0] * avgs[2];

        inveigenvecs[2][0] =
          avgs[1] + (avgs[0] * pow (avgs[1], 3.0)) / compress_param;
        inveigenvecs[2][1] = -(avgs[0] * pow (avgs[1], 2.0)) / compress_param;
        inveigenvecs[2][4] = -avgs[0] * avgs[1];

        inveigenvecs[3][0] = avgs[1] / (speedc * avgs[0]);
        inveigenvecs[3][1] = -1.0 / (speedc * avgs[0]);
        inveigenvecs[3][4] = lamdas[4] / (2.0 * speedc);

        inveigenvecs[4][0] = -avgs[1] / (speedc * avgs[0]);
        inveigenvecs[4][1] = 1.0 / (speedc * avgs[0]);
        inveigenvecs[4][4] = lamdas[3] / (2.0 * speedc);

        /* 5. take absolute value and divide by half to get the final lamdas */

        lamdas[0] = abs (lamdas[0]);
        lamdas[1] = lamdas[0];
        lamdas[2] = lamdas[0];
        lamdas[3] = 0.5 * abs (lamdas[3]);
        lamdas[4] = 0.5 * abs (lamdas[4]);

        /* 6. time the diagnal matrix into the inverse eigenvectors */
        for (l = 0; l < 5; l++)
        {
          for (m = 0; m < 5; m++)
          {
            inveigenvecs[l][m] *= lamdas[l];
          }
        }

        /* 7. calculate dmatrix */
        for (l = 0; l < 5; l++)
        {
          for (m = 0; m < 5; m++)
          {
            for (n = 0; n < 5; n++)
            {
              dmatrix[l][m] += eigenvecs[l][n] * inveigenvecs[n][m];
            }
          }
        }

        /* 8. calculate dflux */
        for (l = 0; l < 5; l++)
        {
          for (m = 0; m < 5; m++)
          {
            dflux[l] += dmatrix[l][m] * (prims[m][1] - prims[m][0]);
          }
        }

#ifdef  GAVINDEBUG
        printf ("PRoeFluxX \n");
        printf ("(i, j, k)    =  (%13d, %13d, %13d) \n", i, j, k);
#endif /* end debugging output */

/* update the right hand side with flux in X direction */
        dens_rhs[i3D] += (fluxes[0][0] + fluxes[0][1] - dflux[0]) * 0.5;
        sx_rhs[i3D] += (fluxes[1][0] + fluxes[1][1] - dflux[1]) * 0.5;
        sy_rhs[i3D] += (fluxes[2][0] + fluxes[2][1] - dflux[2]) * 0.5;
        sz_rhs[i3D] += (fluxes[3][0] + fluxes[3][1] - dflux[3]) * 0.5;
        press_rhs[i3D] += (fluxes[4][0] + fluxes[4][1] - dflux[4]) * 0.5;

/*
        for (i = 0; i < 5; i++)
        {
          printf ("dflux [%d]= %lf\n", i, dflux[i]);
        }
*/

      }                         /* main loop */

}                               /* void FreeSurf_FluxX */
