#include "cctk.h"

int FreeSurf_InitBanner (void)
{
  const char *const banner =
    "\n=================================================\n"
    "     o--o              o-o            o-o\n"
    "     |                |               |  \n"
    "     O-o  o-o o-o o-o  o-o  o  o o-o -O- \n"
    "     |    |   |-' |-'     | |  | |    |  \n"
    "     o    o   o-o o-o o--o  o--o o    o  \n\n"
    "  Make Cactus Flourish In Northern Gulf Coast ! \n\n"
    "         (c) Copyright The Authors \n"
    "         GNU Licensed. No Warranty \n"
    "=================================================\n\n";
  CCTK_RegisterBanner (banner);
  return 0;
}
