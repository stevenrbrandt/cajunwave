#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"


void FreeSurf_DamBreak (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;
  CCTK_REAL value;

  CCTK_INFO ("Simulating a breaking dam !");

  /* intialize the evolved variables */
#pragma omp parallel for
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);

        sx[i3D] = 0.0;
        sy[i3D] = 0.0;
        sz[i3D] = 0.0;

        u1[i3D] = 0.0;
        u2[i3D] = 0.0;
        u3[i3D] = 0.0;
      }

#pragma omp parallel for
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        dens[i3D] = damn_left_density;
        rho[i3D] = damn_left_density;
        press[i3D] = damn_left_pressure;
        p[i3D] = damn_left_pressure;

        value = damn_vect_x * x[i3D] + damn_vect_y * y[i3D] + damn_vect_z * z[i3D];
        if (value >= 0.0)
        {
          dens[i3D] = damn_right_density;
          rho[i3D] = damn_right_density;
          press[i3D] = damn_right_pressure;
          p[i3D] = damn_right_pressure;
        }
        dens_p[i3D] = dens[i3D];
        sx_p[i3D] = sx[i3D];
        sy_p[i3D] = sy[i3D];
        sz_p[i3D] = sz[i3D];
        press_p[i3D] = press[i3D];

        rho_p[i3D] = dens[i3D];
        u1_p[i3D] = sx[i3D];
        u2_p[i3D] = sy[i3D];
        u3_p[i3D] = sz[i3D];
        p_p[i3D] = press[i3D];
      }
}
