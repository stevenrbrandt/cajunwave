#ifndef _FREESURF_H_
#define _FREESURF_H_

#include "cctk.h"

#ifdef __cplusplus

extern "C"
{
#endif

void FreeSurf_FluxX (CCTK_ARGUMENTS);
void FreeSurf_FluxY (CCTK_ARGUMENTS);
void FreeSurf_FluxZ (CCTK_ARGUMENTS);

#ifdef __cplusplus
}
#endif

#endif                          //_FREESURF_H_
