#include "cctk.h"
#include "cctk_Arguments.h"

void FreeSurf_InitZero (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  int i, np;

  np = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];

#pragma omp parallel for
  for (i = 0; i < np; i++)
  {
/* evolved variables in FreeSurf */
    dens[i] = 0.0;
    sx[i] = 0.0;
    sy[i] = 0.0;
    sz[i] = 0.0;
    press[i] = 0.0;

/* primitive variables in CFDBase */
    rho[i] = 0.0;
    u1[i] = 0.0;
    u2[i] = 0.0;
    u3[i] = 0.0;
    p[i] = 0.0;
  }
}
