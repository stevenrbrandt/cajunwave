#include "FreeSurf.h"

inline CCTK_REAL SlopeLimiter (CCTK_REAL r, int type)
{
  CCTK_REAL phi;

  switch (type)
  {
    /* minmod */
  case 1:
    if (r >= 1.0)
      phi = 1.0;
    else if (r > 0.0)
      phi = r;
    else
      phi = 0.0;
    break;

    /* Superbee */
  case 2:
    if (r >= 2.0)
      phi = 2.0;
    else if (r >= 1.0)
      phi = r;
    else if (r >= 0.5)
      phi = 0.5;
    else if (r >= 0.0)
      phi = 2.0 * r;
    else
      phi = 0.0;
    break;

    /* MUSCL */
  case 3:
    if (r >= 0.3333333333333)
      phi = (1.0 + r) * 0.5;
    else if (r >= 0.0)
      phi = 2.0 * r;
    else
      phi = 0.0;
    break;

  default:
    CCTK_WARN (0, "The slope limiter is not supported yet !");
  }
  return phi;
}
