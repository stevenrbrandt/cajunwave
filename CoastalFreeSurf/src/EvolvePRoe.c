#include <stdio.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "FreeSurf.h"
/* prepare for the calculation of the RHS */

void FreeSurf_CalcRHS (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  int istart, iend, jstart, jend, kstart, kend;
  int i, j, k, i3D, i3Dr;

  istart = stencil_size;
  jstart = stencil_size;
  kstart = stencil_size;

/* stencil size should be smaller than ghost zone size */
  iend = cctk_lsh[0] - stencil_size;
  jend = cctk_lsh[1] - stencil_size;
  kend = cctk_lsh[2] - stencil_size;
/*

      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, 15, 15);
        printf ("dens_rhs [%d,15,15]= %lf\n", i, dens[i3D]);
      }
*/

/* initialize all the flux terms to zero */
#pragma omp parallel for
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        dens_rhs[i3D] = 0.0;
        sx_rhs[i3D] = 0.0;
        sy_rhs[i3D] = 0.0;
        sz_rhs[i3D] = 0.0;
        press_rhs[i3D] = 0.0;
      }
/* TODO: current only first order piecewise reconstruction is available */
/* update X flux */

  for (k = kstart; k < kend; k++)
    for (j = jstart; j < jend; j++)
      for (i = istart; i < iend; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        i3Dr = CCTK_GFINDEX3D (cctkGH, i + 1, j, k);

        rho_l[i3D] = rho[i3D];
        rho_r[i3D] = rho[i3Dr];
        u1_l[i3D] = u1[i3D];
        u1_r[i3D] = u1[i3Dr];
        u2_l[i3D] = u2[i3D];
        u2_r[i3D] = u2[i3Dr];
        u3_l[i3D] = u3[i3D];
        u3_r[i3D] = u3[i3Dr];
        press_l[i3D] = p[i3D];
        press_r[i3D] = p[i3Dr];

/*
          if (i == 14 && j ==15 && k == 15)
        	  printf("\nrho_l = %lf, rho_r = %lf\n"
        			 "u1_l = %lf, u1_r = %lf\n"
        			 "u2_l = %lf, u2_r = %lf\n"
        			 "u3_l = %lf, u3_r = %lf\n"
        			 "press_l = %lf, press_r = %lf\n",
        	  rho_l[i3D], rho_r[i3D],
        	  u1_l[i3D], u1_r[i3D],
        	  u2_l[i3D], u2_r[i3D],
        	  u3_l[i3D], u3_r[i3D],
        	  press_l[i3D], press_r[i3D]);
*/
      }
  FreeSurf_FluxX (CCTK_PASS_CTOC);

/* update Y flux */

  for (k = kstart; k < kend; k++)
    for (j = jstart; j < jend; j++)
      for (i = istart; i < iend; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        i3Dr = CCTK_GFINDEX3D (cctkGH, i, j + 1, k);

        rho_l[i3D] = rho[i3D];
        rho_r[i3D] = rho[i3Dr];
        u1_l[i3D] = u1[i3D];
        u1_r[i3D] = u1[i3Dr];
        u2_l[i3D] = u2[i3D];
        u2_r[i3D] = u2[i3Dr];
        u3_l[i3D] = u3[i3D];
        u3_r[i3D] = u3[i3Dr];
        press_l[i3D] = p[i3D];
        press_r[i3D] = p[i3Dr];
      }
  FreeSurf_FluxY (CCTK_PASS_CTOC);

/* update Z flux */

  for (k = kstart; k < kend; k++)
    for (j = jstart; j < jend; j++)
      for (i = istart; i < iend; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        i3Dr = CCTK_GFINDEX3D (cctkGH, i, j, k + 1);

        rho_l[i3D] = rho[i3D];
        rho_r[i3D] = rho[i3Dr];
        u1_l[i3D] = u1[i3D];
        u1_r[i3D] = u1[i3Dr];
        u2_l[i3D] = u2[i3D];
        u2_r[i3D] = u2[i3Dr];
        u3_l[i3D] = u3[i3D];
        u3_r[i3D] = u3[i3Dr];
        press_l[i3D] = p[i3D];
        press_r[i3D] = p[i3Dr];
      }
  FreeSurf_FluxZ (CCTK_PASS_CTOC);

  /* this part can be moved to a separate file when he body force
   * becomes complicated */

  dens_rhs[i3D] += 0;
  sx_rhs[i3D] += 0;
  sy_rhs[i3D] += 0;
  sz_rhs[i3D] += rho[i3D] * grav_acc;
  press_rhs[i3D] += 0;

}
