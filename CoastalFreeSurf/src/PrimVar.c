#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"


void FreeSurf_PrimVar (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int i, j, k, i3D;

#pragma omp parallel for
  for (k = 0; k < cctk_lsh[2]; k++)
    for (j = 0; j < cctk_lsh[1]; j++)
      for (i = 0; i < cctk_lsh[0]; i++)
      {
        i3D = CCTK_GFINDEX3D (cctkGH, i, j, k);
        rho[i3D] = dens[i3D];
        u1[i3D] = sx[i3D] / rho[i3D];
        u2[i3D] = sy[i3D] / rho[i3D];
        u3[i3D] = sz[i3D] / rho[i3D];
        p[i3D] = press[i3D];
      }
}
